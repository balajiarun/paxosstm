/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.abcast;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

import soa.paxosstm.common.ByteArrayStorage;
import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.RecoveryListener;
import soa.paxosstm.common.Storage;
import soa.paxosstm.common.StorageException;
import soa.paxosstm.consensus.CommitableConsensus;
import soa.paxosstm.consensus.ConsensusDelegateProposer;
import soa.paxosstm.consensus.ConsensusListener;
import soa.paxosstm.utils.SerializationHelper;

/**
 * Implementation of the Atomic Broadcast module. It is built on top of the
 * module responsible for solving distributed consensus problem so it implements
 * the {@link ConsensusListener} interface. This implementation support the
 * crash-recovery failure model and therefore this class implements the
 * {@link CheckpointListener}, {@link RecoveryListener} interfaces.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class AtomicBroadcastImpl implements ConsensusListener, CheckpointListener,
		RecoveryListener, AtomicBroadcast
{
	private static final int numberOfProposers = 20;

	private CommitableConsensus consensus = null;
	private ConsensusDelegateProposer[] proposers = new ConsensusDelegateProposer[numberOfProposers];
	private int lastProposerUsed = 0;
	private CopyOnWriteArrayList<AtomicDeliverListener> adeliverListeners = new CopyOnWriteArrayList<AtomicDeliverListener>();
	private CopyOnWriteArrayList<CheckpointListener> checkpointListeners = new CopyOnWriteArrayList<CheckpointListener>();
	private CopyOnWriteArrayList<RecoveryListener> recoveryListeners = new CopyOnWriteArrayList<RecoveryListener>();

	private TreeSet<Message> unordered = new TreeSet<Message>();
	private int numberOfProcesses = 0;
	private int processId;
	private int lastMessageSent = -1;
	private int lastMessageReceived[];
	private boolean abcastReady = true;

	private static final String INNER = "inner";
	private static final String UNORDERED = "unordered";
	// private static final String NUMBER_OF_PROCESSES = "numberOfProcesses";
	// private static final String PROCESS_ID = "processId";
	private static final String LAST_MESSAGE_SENT = "lastMessageSent";
	private static final String LAST_MESSAGE_RECEIVED = "lastMessageReceived";

	private Object lock = new Object();

	// public AtomicBroadcastImpl(int processId, int numberOfProcesses,
	// CommitableConsensus consensus) throws StorageException
	// {
	// this(processId, numberOfProcesses, consensus, consensus, null);
	// }
	//
	// public AtomicBroadcastImpl(int processId, int numberOfProcesses,
	// CommitableConsensus consensus, AtomicDeliverListener listener)
	// throws StorageException
	// {
	// this(processId, numberOfProcesses, consensus, consensus, listener);
	// }

	/**
	 * Constructor. The programmer is forbidden to create multiple instances of
	 * AtomicBroadcast with the same processId.
	 * 
	 * @param processId
	 *            the replica number
	 * @param numberOfProcesses
	 *            number of replicas
	 * @param consensus
	 *            reference to the Consensus module main object
	 * @param storage
	 *            reference to the Storage object
	 * @param listener
	 *            reference to the object which handles the adelivered messages
	 * @throws StorageException
	 *             when storage is unoperational
	 */
	public AtomicBroadcastImpl(int processId, int numberOfProcesses, CommitableConsensus consensus,
			AtomicDeliverListener listener) throws StorageException
	{
		this.processId = processId;
		this.numberOfProcesses = numberOfProcesses;
		this.consensus = consensus;

		for (int i = 0; i < numberOfProposers; i++)
		{
			try
			{
				proposers[i] = consensus.getNewDelegateProposer();
			}
			catch (IOException e)
			{
				throw new StorageException(e);
			}
		}

		this.lastMessageReceived = new int[this.numberOfProcesses];
		for (int i = 0; i < this.numberOfProcesses; i++)
			this.lastMessageReceived[i] = -1;

		if (listener != null)
			addAtomicDeliverListener(listener);
		consensus.addConsensusListener(this);
		consensus.addCheckpointListener(this);
		consensus.addRecoveryListener(this);
	}

	@Override
	public synchronized void start() throws StorageException
	{
		try
		{
			consensus.start();
		}
		catch (IOException e)
		{
			throw new StorageException(e);
		}
	}

	@Override
	public void addAtomicDeliverListener(AtomicDeliverListener listener)
	{
		adeliverListeners.add(listener);
	}

	@Override
	public void removeAtomicDeliverListener(AtomicDeliverListener listener)
	{
		adeliverListeners.remove(listener);
	}

	@Override
	public void abcast(byte[] obj)
	{
		synchronized (lock)
		{
			while (!abcastReady)
			{
				try
				{
					lock.wait();
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
		synchronized (this)
		{
			Message message = new Message(processId, ++lastMessageSent, obj);
			unordered.add(message);
			issueProposal();
		}
	}

	@Override
	public void decide(byte[] obj)
	{
		@SuppressWarnings("unchecked")
		Set<Message> decision = (Set<Message>) SerializationHelper.byteArrayToObject(obj);

		// result = decision minus adelivered
		Set<Message> result = new TreeSet<Message>();
		synchronized (this)
		{
			boolean ownMessageDelivered = false;

			for (Message message : decision)
			{
				if (message.getProcessId() == processId && message.getMessageId() > lastMessageSent)
				{
					lastMessageSent = message.getMessageId();
					ownMessageDelivered = true;
				}
				if (message.getMessageId() > lastMessageReceived[message.getProcessId()])
					result.add(message);
			}

			// adelivered = adelivered plus result
			for (Message message : result)
				lastMessageReceived[message.getProcessId()] = message.getMessageId();

			// unordered = unordered minus adelivered
			// === unordered = unordered minus result
			unordered.removeAll(result);

			// JPaxos optimization (JPaxos as abcast)
			if (ownMessageDelivered)
				issueProposal();
		}

		for (Message message : result)
			fireAdeliverEvent(message.getContent());
	}

	private void issueProposal()
	{
		if (!unordered.isEmpty())
		{
			Set<Message> proposition = new TreeSet<Message>(unordered);
			ConsensusDelegateProposer proposer = proposers[lastProposerUsed++];
			lastProposerUsed %= numberOfProposers;
			proposer.propose(SerializationHelper.byteArrayFromObject(proposition));
		}
	}

	private void fireAdeliverEvent(byte[] obj)
	{
		for (AtomicDeliverListener listener : adeliverListeners)
		{
			listener.adeliver(obj);
		}
	}

	@Override
	public synchronized void onCheckpoint(int seqNumber, Storage storage)
	{
		System.out.println("ABCAST - onCommit");

		ByteArrayStorage innerStorage = new ByteArrayStorage();
		for (CheckpointListener listener : checkpointListeners)
			listener.onCheckpoint(seqNumber, innerStorage);

		// int logVersion = seqNumber % 2;
		int logVersion = 0;

		try
		{
			storage.log(INNER + "_" + logVersion, innerStorage);
			storage.log(UNORDERED + "_" + logVersion, unordered);
			// storage.log(NUMBER_OF_PROCESSES + "_" + logVersion,
			// numberOfProcesses);
			// storage.log(PROCESS_ID + "_" + logVersion, processId);
			storage.log(LAST_MESSAGE_SENT + "_" + logVersion, lastMessageSent);
			storage.log(LAST_MESSAGE_RECEIVED + "_" + logVersion, lastMessageReceived);
		}
		catch (StorageException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public void onCheckpointFinished(int seqNumber)
	{
		for (CheckpointListener listener : checkpointListeners)
			listener.onCheckpointFinished(seqNumber);
	}

	@Override
	public synchronized void recoverFromCheckpoint(Storage storage)
	{
		Storage innerStorage = null;
		try
		{
			//int logVersion = commitNumber % 2;
			int logVersion = 0;

			@SuppressWarnings("unchecked")
			TreeSet<Message> dummy = (TreeSet<Message>) storage.retrieve(UNORDERED + "_" + logVersion);
			unordered = dummy;
			// numberOfProcesses = (Integer)
			// storage.retrieve(NUMBER_OF_PROCESSES + "_" + logVersion);
			// processId = (Integer) storage.retrieve(PROCESS_ID + "_" +
			// logVersion);
			lastMessageSent = (Integer) storage.retrieve(LAST_MESSAGE_SENT + "_" + logVersion);
			lastMessageReceived = (int[]) storage
					.retrieve(LAST_MESSAGE_RECEIVED + "_" + logVersion);
			innerStorage = (Storage) storage.retrieve(INNER + "_" + logVersion);
		}
		catch (StorageException ex)
		{
			System.out.println("Cannot recover the abcast layer, exiting...");
			ex.printStackTrace();
			System.exit(1);
		}

		System.out.println("ABCAST - recoverFromCommit");

		for (RecoveryListener listener : recoveryListeners)
			listener.recoverFromCheckpoint(innerStorage);
	}

	@Override
	public void recoveryFinished()
	{
		synchronized (lock)
		{
			abcastReady = true;
			lock.notifyAll();
		}

		for (RecoveryListener listener : recoveryListeners)
			listener.recoveryFinished();

		System.out.println("ABCAST - recoveryFinished");
	}

	@Override
	public boolean addCheckpointListener(CheckpointListener listener)
	{
		checkpointListeners.add(listener);
		return true;
	}

	@Override
	public boolean addRecoveryListener(RecoveryListener listener)
	{
		recoveryListeners.add(listener);
		return true;
	}

	@Override
	public void scheduleCheckpoint()
	{
		consensus.scheduleCheckpoint();
	}

	@Override
	public boolean removeCheckpointListener(CheckpointListener listener)
	{
		checkpointListeners.remove(listener);
		return true;
	}

	@Override
	public boolean removeRecoveryListener(RecoveryListener listener)
	{
		recoveryListeners.remove(listener);
		return true;
	}

	@Override
	public void blockingAbcast(byte[] obj)
	{
		abcast(obj);
	}
}
