/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.abcast;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Scanner;

import lsr.common.Configuration;
import lsr.paxos.test.utils.BarrierClient;
import soa.paxosstm.common.Storage;
import soa.paxosstm.common.StorageException;
import soa.paxosstm.common.StorageImpl;
import soa.paxosstm.consensus.CommitableConsensus;
import soa.paxosstm.consensus.ConsensusDelegateProposer;
import soa.paxosstm.consensus.LocalConsensus;
import soa.paxosstm.consensus.PaxosConsensus;

/**
 * Simple implementation of the {@link AtomicDeliverListener} class created for
 * the {@link AtomicBroadcastExec} class.
 * 
 * @author tkob
 * 
 */
class AtomicDeliverListenerImpl implements AtomicDeliverListener
{
	public void adeliver(byte[] obj)
	{
		System.out.println("adelivered: " + new String(obj));
	}
}

/**
 * This class can be used to test the Atomic Broadcast module. Since this class
 * is not used normally by Paxos STM it is not properly secured (some exceptions
 * are thrown by the {@link AtomicBroadcastExec#main(String[])} method instead
 * of being handled.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class AtomicBroadcastExec
{
	/**
	 * The main method, used for invoking test methods. The brief description of
	 * the tests is presented upon starting the application.
	 * 
	 * @param args
	 *            determines which test to be run
	 * @throws IOException
	 * @throws StorageException
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws IOException, StorageException,
			InterruptedException
	{
		CommitableConsensus consensus;

		if (args.length < 1)
		{
			System.out.println("usage: replica_id [test_type [parameters]]");
			System.out.println("replica_id = -1   -> use LocalConsensus");
			System.out.println("test:");
			System.out.println("* 0 -> no test run, connect only to the consensus layer");
			System.out.println("* 1 -> manual testing (default)");
			System.out.println("* 2 -> stress test, arguments are: threads repeats size rounds");
			System.exit(1);
		}

		int localId = 0;
		try
		{
			localId = Integer.parseInt(args[0]);
		}
		catch (NumberFormatException e)
		{
			System.err.println("Need replica number");
			System.exit(1);
		}

		Configuration conf = new Configuration();
		if (localId == -1)
		{
			consensus = new LocalConsensus();
			localId = 0;
		}
		else
		{
			// consensus = new SerializablePaxosConsensus(conf, localId);
			consensus = new PaxosConsensus(conf, localId);
		}

		int testType = 1;
		if (args.length >= 2)
		{
			try
			{
				testType = Integer.parseInt(args[1]);
			}
			catch (NumberFormatException e)
			{
				System.err.println("test type needs to be in {0,1,2}.");
				System.exit(1);
			}
		}

		switch (testType)
		{
		case 0:
			{
				consensus.start();
				((PaxosConsensus) consensus).setSingleAdeliverListener(new AtomicDeliverListener()
				{
					@Override
					public void adeliver(byte[] obj)
					{
					}
				});
				break;
			}
		case 1:
			{
				Storage st = new StorageImpl("PaxosSTM_" + localId);
				AtomicDeliverListener dl = new AtomicDeliverListenerImpl();
				AtomicBroadcast abcast = new AtomicBroadcastJPaxosImpl(localId, conf.getN(),
						consensus, st, dl);
				abcast.start();

				manualTest(abcast);
				System.exit(0);
			}
		case 2:
			{
				Storage st = new StorageImpl("PaxosSTM_" + localId);
				AtomicBroadcast abcast = new AtomicBroadcastJPaxosImpl(localId, conf.getN(),
						consensus, st, null);

				int threads = 20;
				int size = 330;
				long mills = 10000;
				int rounds = 30;
				String host = "localhost";
				int port = 1234;
				try
				{
					threads = Integer.parseInt(args[2]);
					size = Integer.parseInt(args[3]);
					mills = Long.parseLong(args[4]);
					rounds = Integer.parseInt(args[5]);
					host = args[6];
					port = Integer.parseInt(args[7]);
				}
				catch (Exception e)
				{
				}

				stressTest(abcast, consensus, localId, threads, size, mills, rounds, host, port);
				System.exit(0);
			}
		default:
			{
				System.err.println("test type needs to be in {0,1,2}.");
				System.exit(1);
			}
		}
	}

	private static void manualTest(AtomicBroadcast abcast)
	{
		while (true)
		{
			System.out.println("Hello!\n" + "  a <message> - abcast\n" + "  c           - commit\n"
					+ "  q           - quit\n" + "    :");
			try
			{
				Scanner sc = new Scanner(System.in);
				String command = sc.nextLine();
				if (command.length() != 0)
				{
					switch (command.charAt(0))
					{
					case 'a':
						abcast.abcast(command.substring(2).getBytes());
						break;
					case 'c':
						abcast.scheduleCheckpoint();
						break;
					case 'q':
						System.exit(0);
					default:
						throw new Exception();
					}
				}
			}
			catch (Exception e)
			{
				System.out.println("Invalid command");
			}
		}
	}

	private static void stressTest(final AtomicBroadcast abcast,
			final CommitableConsensus consensus, final int localId, final int numberOfThreads,
			final int packetSize, final long mills, final int numberOfRounds, final String host,
			final int port) throws InterruptedException, StorageException, IOException
	{
		long totalTime = 0;
		int totalRequests = 0;

		class TestRunner implements Runnable
		{
			int v;
			public long start;
			public long end;
			public long mills;
			public int requests;

			private ConsensusDelegateProposer proposer;

			public TestRunner(int v, long mills, ConsensusDelegateProposer proposer)
			{
				this.v = v;
				this.mills = mills;
				this.proposer = proposer;
			}

			private void makeProposition()
			{
				byte[] proposition = new byte[packetSize];
				ByteBuffer bb = ByteBuffer.wrap(proposition);
				bb.asIntBuffer().put(0, v);
				proposer.propose(proposition);
			}

			public void run()
			{
				requests = 0;
				start = System.currentTimeMillis();
				while ((end = System.currentTimeMillis()) - start <= mills)
				{
					synchronized (this)
					{
						// int[] message = new int[(packetSize + 3) / 4];
						// message[0] = v;
						// abcast.abcast(message);
						makeProposition();
						try
						{
							wait();
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					requests++;
				}
			}
		}

		abcast.start();
		final TestRunner[] runners = new TestRunner[numberOfThreads];
		for (int i = 0; i < numberOfThreads; i++)
		{
			runners[i] = new TestRunner(localId * 1000 + i, mills, consensus
					.getNewDelegateProposer());
			abcast.addAtomicDeliverListener(new AtomicDeliverListener()
			{
				@Override
				public void adeliver(byte[] obj)
				{
					ByteBuffer bb = ByteBuffer.wrap(obj);
					int v = bb.asIntBuffer().get(0);
					if (v / 1000 != localId)
						return;
					int r = v % 1000;
					synchronized (runners[r])
					{
						runners[r].notifyAll();
					}
				}
			});
		}

		System.out
				.println("round     threads       total abcasts     avg time     abcasts per second");
		for (int round = 1; round <= numberOfRounds; round++)
		{
			totalTime = 0;
			totalRequests = 0;
			BarrierClient barrierClient = new BarrierClient();

			Thread[] threads = new Thread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new Thread(runners[i]);
			}
			barrierClient.enterBarrier(host, port, 0, 0, numberOfThreads);
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				totalTime += runners[i].end - runners[i].start;
				totalRequests += runners[i].requests;
				threads[i] = null;
			}
			barrierClient.enterBarrier(host, port, totalRequests, totalTime, numberOfThreads);
			abcast.scheduleCheckpoint();
			threads = null;

			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			double abcastsPerSec = totalRequests / avgTime;
			String format = "%3d       %3d                   %5d              %8.3f            %7.2f\n";
			System.out
					.format(format, round, numberOfThreads, totalRequests, avgTime, abcastsPerSec);

			abcast.scheduleCheckpoint();
		}
	}
}
