/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.abcast;

import java.io.Serializable;

/**
 * All messages exchanged between Atomic Broadcast modules working on different
 * replicas are of this type.
 * 
 * @author tkob
 * 
 */
public class Message implements Comparable<Message>, Serializable
{
	private static final long serialVersionUID = 1L;

	private int processId;
	private int messageId;
	private byte[] content;

	/**
	 * Constructor.
	 * 
	 * @param processId
	 *            number of the replica issuing the message
	 * @param messageId
	 *            sequential number of the message
	 * @param content
	 *            payload of the message
	 */
	public Message(int processId, int messageId, byte[] content)
	{
		this.processId = processId;
		this.messageId = messageId;
		this.content = content;
	}

	/**
	 * Returns number of the replica issuing the message.
	 * 
	 * @return number of the replica issuing the message
	 */
	public int getProcessId()
	{
		return processId;
	}

	/**
	 * Returns sequential number of message.
	 * 
	 * @return sequential number of message
	 */
	public int getMessageId()
	{
		return messageId;
	}

	/**
	 * Returns the content of the message.
	 * 
	 * @return the content of the message
	 */
	public byte[] getContent()
	{
		return content;
	}

	@Override
	public int hashCode()
	{
		return processId ^ messageId;
	}

	@Override
	public int compareTo(Message other)
	{
		if (other.getProcessId() != getProcessId())
			return getProcessId() - other.getProcessId();
		if (other.getMessageId() != getMessageId())
			return getMessageId() - other.getMessageId();
		return 0;
	}
}
