/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.abcast;

import soa.paxosstm.common.Checkpointable;
import soa.paxosstm.common.StorageException;

/**
 * An interface that should be implemented by a module providing Atomic
 * Broadcast.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public interface AtomicBroadcast extends Checkpointable
{

	/**
	 * Starts the module implementing the interface. This method is necessary
	 * since the process of starting a stack of protocols has to be properly
	 * coordinated.
	 * 
	 * @throws StorageException
	 *             when the storage is unoperational
	 */
	public void start() throws StorageException;

	/**
	 * Adds the listener to the Atomic Broadcast module listeners lists. Once a
	 * new abcasted message is ready to be delivered, all the listeners are
	 * notified by invocation of the
	 * {@link AtomicDeliverListener#adeliver(Object)} method.
	 * 
	 * @param listener
	 *            the new listener to be added to the listeners list
	 */
	public void addAtomicDeliverListener(AtomicDeliverListener listener);

	/**
	 * Removes the listener from the Atomic Broadcast module listeners list.
	 * 
	 * @param listener
	 *            the listener to be removed from the listeners list
	 * 
	 */
	public void removeAtomicDeliverListener(AtomicDeliverListener listener);

	/**
	 * Used for abcasting a message.
	 * 
	 * @param obj
	 *            message to be abcasted
	 */
	public void abcast(byte[] obj);

	/**
	 * This method does not guarantee it will block until the message is
	 * delivered. When the user does not require for the abcast to be
	 * asynchronous this method is preferential to {@link abcast(byte[])}.
	 * 
	 * @param obj
	 *            message to be abcasted
	 */
	void blockingAbcast(byte[] obj);
}
