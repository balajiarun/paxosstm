/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.abcast;

import java.io.IOException;
import java.util.concurrent.CopyOnWriteArrayList;

import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.RecoveryListener;
import soa.paxosstm.common.Storage;
import soa.paxosstm.common.StorageException;
import soa.paxosstm.consensus.CommitableConsensus;
import soa.paxosstm.consensus.ConsensusDelegateProposer;
import soa.paxosstm.consensus.ConsensusListener;
import soa.paxosstm.consensus.PaxosConsensus;

/**
 * Implementation of the Atomic Broadcast module. It is built on top of the
 * module responsible for solving distributed consensus problem so it implements
 * the {@link ConsensusListener} interface. This implementation support the
 * crash-recovery failure model and therefore this class implements the
 * {@link CheckpointListener}, {@link RecoveryListener} interfaces.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class AtomicBroadcastJPaxosImpl implements ConsensusListener, CheckpointListener,
		RecoveryListener, AtomicBroadcast
{
	private static final int numberOfProposers = 20;

	private CommitableConsensus consensus = null;
	private ConsensusDelegateProposer[] proposers = new ConsensusDelegateProposer[numberOfProposers];
	private int lastProposerUsed = 0;
	private CopyOnWriteArrayList<AtomicDeliverListener> adeliverListeners = new CopyOnWriteArrayList<AtomicDeliverListener>();
	private CopyOnWriteArrayList<CheckpointListener> checkpointListeners = new CopyOnWriteArrayList<CheckpointListener>();
	private CopyOnWriteArrayList<RecoveryListener> recoveryListeners = new CopyOnWriteArrayList<RecoveryListener>();

	// private boolean abcastReady = false;

	// private Object lock = new Object();

	// public AtomicBroadcastJPaxosImpl(int processId, int numberOfProcesses,
	// CommitableConsensus consensus) throws StorageException
	// {
	// this(processId, numberOfProcesses, consensus, consensus, null);
	// }
	//
	// public AtomicBroadcastJPaxosImpl(int processId, int numberOfProcesses,
	// CommitableConsensus consensus, AtomicDeliverListener listener) throws
	// StorageException
	// {
	// this(processId, numberOfProcesses, consensus, consensus, listener);
	// }

	/**
	 * Constructor. The programmer is forbidden to create multiple instances of
	 * AtomicBroadcast with the same processId.
	 * 
	 * @param processId
	 *            the replica number
	 * @param numberOfProcesses
	 *            number of replicas
	 * @param consensus
	 *            reference to the Consensus module main object
	 * @param storage
	 *            reference to the Storage object
	 * @param listener
	 *            reference to the object which handles the adelivered messages
	 * @throws StorageException
	 *             when storage is unoperational
	 */
	public AtomicBroadcastJPaxosImpl(int processId, int numberOfProcesses,
			CommitableConsensus consensus, Storage storage, AtomicDeliverListener listener)
			throws StorageException
	{
		this.consensus = consensus;

		if (listener != null)
			addAtomicDeliverListener(listener);
		consensus.addConsensusListener(this);
		consensus.addCheckpointListener(this);
		consensus.addRecoveryListener(this);
	}

	@Override
	public synchronized void start() throws StorageException
	{
		try
		{
			consensus.start();

			for (int i = 0; i < numberOfProposers; i++)
			{
				try
				{
					proposers[i] = consensus.getNewDelegateProposer();
				}
				catch (IOException e)
				{
					throw new StorageException(e);
				}
			}

		}
		catch (IOException e)
		{
			throw new StorageException(e);
		}
	}

	@Override
	public void addAtomicDeliverListener(AtomicDeliverListener listener)
	{
		if (consensus instanceof PaxosConsensus)
			((PaxosConsensus)consensus).setSingleAdeliverListener(listener);
		adeliverListeners.add(listener);
	}

	@Override
	public void removeAtomicDeliverListener(AtomicDeliverListener listener)
	{
		adeliverListeners.remove(listener);
	}

	@Override
	public void abcast(byte[] obj)
	{
		// synchronized (lock)
		// {
		// while (!abcastReady)
		// {
		// try
		// {
		// lock.wait();
		// }
		// catch (InterruptedException e)
		// {
		// e.printStackTrace();
		// }
		// }
		// }
		ConsensusDelegateProposer proposer;
		synchronized (this)
		{
			proposer = proposers[lastProposerUsed++];
			lastProposerUsed %= numberOfProposers;
		}
		// consensus.propose(obj);
		proposer.propose(obj);
	}

	@Override
	public void decide(byte[] obj)
	{
		fireAdeliverEvent(obj);
	}

	private void fireAdeliverEvent(byte[] obj)
	{
		for (AtomicDeliverListener listener : adeliverListeners)
		{
			listener.adeliver(obj);
		}
	}

	@Override
	public void onCheckpoint(int seqNumber, Storage storage)
	{
		// System.out.println("ABCAST - onCommit: " + commitData);

		for (CheckpointListener listener : checkpointListeners)
			listener.onCheckpoint(seqNumber, storage);
	}

	@Override
	public void onCheckpointFinished(int seqNumber)
	{
		for (CheckpointListener listener : checkpointListeners)
			listener.onCheckpointFinished(seqNumber);
	}

	@Override
	public synchronized void recoverFromCheckpoint(Storage storage)
	{
		System.out.println("ABCAST - recoverFromCommit");

		for (RecoveryListener listener : recoveryListeners)
			listener.recoverFromCheckpoint(storage);
	}

	@Override
	public void recoveryFinished()
	{
		// synchronized (lock)
		// {
		// abcastReady = true;
		// lock.notifyAll();
		// }

		for (RecoveryListener listener : recoveryListeners)
			listener.recoveryFinished();

		System.out.println("ABCAST - recoveryFinished");
	}

	@Override
	public boolean addCheckpointListener(CheckpointListener listener)
	{
		checkpointListeners.add(listener);
		return true;
	}

	@Override
	public boolean addRecoveryListener(RecoveryListener listener)
	{
		recoveryListeners.add(listener);
		return true;
	}

	@Override
	public void scheduleCheckpoint()
	{
		consensus.scheduleCheckpoint();
	}

	@Override
	public boolean removeCheckpointListener(CheckpointListener listener)
	{
		checkpointListeners.remove(listener);
		return true;
	}

	@Override
	public boolean removeRecoveryListener(RecoveryListener listener)
	{
		recoveryListeners.remove(listener);
		return true;
	}

	@Override
	public void blockingAbcast(byte[] obj)
	{
		// TODO Auto-generated method stub
		abcast(obj);
	}
}
