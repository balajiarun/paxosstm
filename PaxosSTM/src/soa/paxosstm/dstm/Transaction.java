/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm;

import soa.paxosstm.dstm.internal.exceptions.TransactionAbortedException;
import soa.paxosstm.dstm.internal.exceptions.TransactionCommittedException;
import soa.paxosstm.dstm.internal.exceptions.TransactionCorruptedException;
import soa.paxosstm.dstm.internal.exceptions.TransactionFinishedException;
import soa.paxosstm.dstm.internal.exceptions.TransactionRolledbackException;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager.TransactionOutcome;

/**
 * In order to perform a set of operations within a transaction, the programmer
 * has to create an object of a class derived from the Transaction class. The
 * new class has to reimplement the abstract {@link #atomic()} method. When the
 * execution reaches the end of the {@link #atomic()} method an attempt to
 * commit the transaction is automatically undertaken. The programmer can
 * however modify the flow of execution of the transaction by calling
 * {@link #commit()}, {@link #retry()} or {@link #rollback()} methods.
 * 
 * The easiest way to create a transaction is to create an object of an
 * anonymous class derived from the Transaction class:
 * 
 * <pre>
 * {@code
 * new Transaction() {
 *   public void atomic() {
 *     ...
 *     operations performed within transaction
 *     ...
 *   }
 * }; 
 * }
 * </pre>
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public abstract class Transaction
{
	/**
	 * Default constructor. The processing of the operations declared in the
	 * {@link #atomic()} method is started as soon as an object of the class is
	 * created.
	 */
	public Transaction()
	{
		this(false);
	}

	/**
	 * Constructor. Normally, the defined in the {@link #atomic()} method group
	 * of operations are performed as soon as an object of the class is created.
	 * However sometimes it might be convenient to first create all the
	 * necessary objects of Transaction class and execute the transactions
	 * later. Therefore, by passing <i>true</i> by the argument of this
	 * constructor the programmer indicates that he is going to call the
	 * {@link #execute()} method manually to start processing the transaction.
	 * 
	 * @param delayed
	 *            true if the programmer wants to manually start processing the
	 *            transaction, false otherwise
	 */
	public Transaction(boolean readOnly)
	{
		this(readOnly, false);
	}

	public Transaction(boolean readOnly, boolean delayed)
	{
		if (!delayed)
			execute(readOnly);
	}

	/**
	 * Undertakes the attempt to commit the transaction before the execution
	 * reaches the end of the {@link #atomic()} method. This operation is
	 * blocking, i.e. it ends when the transaction is committed.
	 * 
	 * Can be called only from within the {@link #atomic()} method.
	 */
	protected final void commit()
	{
		if (LocalTransactionManager.commit())
			throw new TransactionCommittedException();
		else
			throw new TransactionAbortedException();
	}

	/**
	 * Aborts the transaction. It guarantees that no changes performed by the
	 * transaction would be applied to all of the shared objects.
	 * 
	 * Can be called only from within the {@link #atomic()} method.
	 */
	protected final void rollback()
	{
		LocalTransactionManager.rollback();
		throw new TransactionRolledbackException();
	}

	/**
	 * Aborts the transaction and starts it again.
	 * 
	 * Can be called only from within the {@link #atomic()} method.
	 */
	protected final void retry()
	{
		LocalTransactionManager.retry();
		throw new TransactionAbortedException();
	}

	/**
	 * This method consists of operations intended to be performed atomically.
	 */
	abstract protected void atomic();

	protected void statistics(TransactionStatistics statistics)
	{
	}
	
	/**
	 * Starts executing the defined in the {@link #atomic()} method set of
	 * operations atomically. When using {@link #Transaction()} or
	 * {@link #Transaction(boolean)} with <i>false</i> as the argument this
	 * method is called automatically after the Transaction object is created.
	 * If the object was created by calling {@link #Transaction(boolean)} with
	 * <i>true</i> as the argument, in order to start processing the transaction
	 * the programmer has to call this method manually.
	 */
	public final void execute()
	{
		execute(false);
	}

	public final void execute(boolean readOnly)
	{
		boolean topLevel = LocalTransactionManager.newTransaction(readOnly);

		if (topLevel)
		{
			while (true)
			{
				try
				{
					atomic();
					if (LocalTransactionManager.commit())
					{
						LocalTransactionManager.notifyEnd();
						statistics(TransactionStatistics.getStatistics());
						return;
					}
					LocalTransactionManager.notifyEnd();
					statistics(TransactionStatistics.getStatistics());
				}
				catch (TransactionAbortedException e)
				{
					LocalTransactionManager.notifyEnd();
					statistics(TransactionStatistics.getStatistics());
				}
				catch (TransactionFinishedException e)
				{
					LocalTransactionManager.notifyEnd();
					statistics(TransactionStatistics.getStatistics());
					return;
				}
				catch (TransactionCorruptedException e)
				{
					e.getCause().printStackTrace();
					System.err.println("===================");
					
					TransactionOutcome outcome = LocalTransactionManager.unexpected();
					switch (outcome)
					{
					case Committed:
					case Rolledback:
						statistics(TransactionStatistics.getStatistics());
						return;
					case Aborted:
						statistics(TransactionStatistics.getStatistics());
						continue;
					case Unexpected:
						throw e;
					}
				}
				catch (Throwable e)
				{
					e.printStackTrace();
					System.err.println("===================");
					
					TransactionOutcome outcome = LocalTransactionManager.unexpected();
					switch (outcome)
					{
					case Committed:
					case Rolledback:
						statistics(TransactionStatistics.getStatistics());
						return;
					case Aborted:
						statistics(TransactionStatistics.getStatistics());
						continue;
					case Unexpected:
						statistics(TransactionStatistics.getStatistics());
						throw new TransactionCorruptedException(e);
					}
				}
			}
		}
		else
		{
			try
			{
				atomic();
				if (LocalTransactionManager.commit())
				{
					LocalTransactionManager.notifyEnd();
					return;
				}
				else
				{
					LocalTransactionManager.notifyEnd();
					throw new TransactionAbortedException();
				}
			}
			catch (TransactionAbortedException e)
			{
				LocalTransactionManager.notifyEnd();
				throw e;
			}
			catch (TransactionFinishedException e)
			{
				LocalTransactionManager.notifyEnd();
				return;
			}
			catch (TransactionCorruptedException e)
			{
				e.getCause().printStackTrace();
				System.err.println("-+===============+-");

				TransactionOutcome outcome = LocalTransactionManager.unexpected();
				switch (outcome)
				{
				case Committed:
				case Rolledback:
					return;
				case Aborted:
					throw new TransactionAbortedException();
				case Unexpected:
					throw e;
				}
			}
			catch (Throwable e)
			{
				e.printStackTrace();
				System.err.println("-+===============+-");

				TransactionOutcome outcome = LocalTransactionManager.unexpected();
				switch (outcome)
				{
				case Committed:
				case Rolledback:
					return;
				case Aborted:
					throw new TransactionAbortedException();
				case Unexpected:
					throw new TransactionCorruptedException(e);
				}
			}
		}
	}
}
