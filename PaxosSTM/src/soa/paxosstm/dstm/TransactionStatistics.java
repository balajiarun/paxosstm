package soa.paxosstm.dstm;

public class TransactionStatistics
{
	private static ThreadLocal<TransactionStatistics> statistics = new ThreadLocal<TransactionStatistics>()
	{
		@Override
		protected TransactionStatistics initialValue()
		{
			return new TransactionStatistics();
		}
	};

	public enum State
	{
		LocalAbort, GlobalAbort, Committed, RolledBack
	}

	State state = State.LocalAbort;
	int readSetSize; // for Committed and GlobalAbort
	int writeSetSize; // for Committed and GlobalAbort
	int newSetSize; // for Committed and GlobalAbort
	int typeSetSize; // for Committed and GlobalAbort
	int packageSize; // for Committed and GlobalAbort
	long executionTime; // for all
	long committingPhaseLatency; // for Committed and GlobalAbort
	long updateTime; // for Committed 
	long validationTime; // for Committed and GlobalAbort
	long abcastDuration; // for Committed and GlobalAbort

	public static TransactionStatistics getStatistics()
	{
		return statistics.get();
	}

	public State getState()
	{
		return state;
	}

	public void setState(State state)
	{
		this.state = state;
	}

	public int getReadSetSize()
	{
		return readSetSize;
	}

	public void setReadSetSize(int readSetSize)
	{
		this.readSetSize = readSetSize;
	}

	public int getWriteSetSize()
	{
		return writeSetSize;
	}

	public void setWriteSetSize(int writeSetSize)
	{
		this.writeSetSize = writeSetSize;
	}

	public int getNewSetSize()
	{
		return newSetSize;
	}

	public void setNewSetSize(int newSetSize)
	{
		this.newSetSize = newSetSize;
	}

	public int getTypeSetSize()
	{
		return typeSetSize;
	}

	public void setTypeSetSize(int typeSetSize)
	{
		this.typeSetSize = typeSetSize;
	}

	public int getPackageSize()
	{
		return packageSize;
	}

	public void setPackageSize(int packageSize)
	{
		this.packageSize = packageSize;
	}

	public void setExecutionTime(long executionTime)
	{
		this.executionTime = executionTime;
	}

	public long getExecutionTime()
	{
		return executionTime;
	}

	public void setCommittingPhaseLatency(long committingPhaseLatency)
	{
		this.committingPhaseLatency = committingPhaseLatency;
	}

	public long getCommittingPhaseLatency()
	{
		return committingPhaseLatency;
	}
	
	public void reset()
	{
		state = State.LocalAbort;
		readSetSize = 0;
		writeSetSize = 0;
		newSetSize = 0;
		typeSetSize = 0;
		packageSize = 0;
		executionTime = 0;
		committingPhaseLatency = 0;
		updateTime = 0;
		validationTime = 0;
		abcastDuration = 0;
	}

	public long getUpdateTime()
	{
		return updateTime;
	}
	
	public void setUpdateTime(long updateTime)
	{
		this.updateTime = updateTime;
	}

	public long getValidationTime()
	{
		return validationTime;
	}
	
	public void setValidationTime(long validationTime)
	{
		this.validationTime = validationTime;
	}

	public long getAbcastDuration()
	{
		return abcastDuration;
	}
	
	public void setAbcastDuration(long abcastDuration)
	{
		this.abcastDuration = abcastDuration;
	}
}
