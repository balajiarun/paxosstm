/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lsr.common.Configuration;
import lsr.common.PID;
import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.Checkpointable;
import soa.paxosstm.common.RecoveryListener;
import soa.paxosstm.common.StorageException;
import soa.paxosstm.consensus.LocalConsensus;
import soa.paxosstm.consensus.PaxosConsensus;
import soa.paxosstm.dstm.internal.SharedObjectRegistry;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.dstm.internal.local.PerThreadBufferedOutputStreamWrapper;
import soa.paxosstm.dstm.internal.local.PerThreadLimitingInputStream;
import soa.paxosstm.utils.Holder;

/**
 * The PaxosSTM class is the facade of the Paxos STM framework - it lets the
 * programmer to start all the necessary modules of Paxos STM framework, use the
 * storage for logging purposes and couple the Paxos STM with the application in
 * order to provide synchronized check-pointing scheme. It also allows to
 * operate on <i>Shared Object Registry</i> (see
 * {@link GlobalTransactionManager}).
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class PaxosSTM implements Checkpointable
{
	public static final boolean DEBUG = true;

	private static boolean isInitialized = false;
	private static final PaxosSTM instance = new PaxosSTM();

	public static final PerThreadBufferedOutputStreamWrapper ptbos = new PerThreadBufferedOutputStreamWrapper(
			System.out);
	public static final PerThreadLimitingInputStream ptlis = new PerThreadLimitingInputStream(
			System.in);

	public static int localId = 0;
	public static int numberOfNodes = 0;

	/**
	 * Returns the instance of PaxosSTM.
	 * 
	 * @return the instance of PaxosSTM
	 */
	public static PaxosSTM getInstance()
	{
		return instance;
	}

	/**
	 * Invocation of this method is automatically added to the definition of the
	 * main method of the application utilizing Paxos STM, thus it should not be
	 * called directly by the programmer.
	 * 
	 * @param args
	 *            arguments passed when starting soa.paxosstm.dstm.Main
	 * @return arguments passed later to the application's main method
	 */
	public static String[] init(String[] args)
	{
		int n = args.length;
		String[] args2 = new String[n - 2];
		for (int i = 0; i < n - 2; ++i)
			args2[i] = args[i + 2];

		if (isInitialized)
			return args2;

		try
		{
			localId = 0;
			try
			{
				localId = Integer.parseInt(args[1]);
			}
			catch (NumberFormatException e)
			{
				System.err
						.println("invalid arguments, try <application name> <replica_number> ...");
				System.exit(1);
			}

			Configuration conf;
			if (localId == -1)
			{
				System.err.println("Warning: using LocalConsensus.");

				List<PID> processes = new ArrayList<PID>();
				processes.add(new PID(0, "localhost", 0, 0));
				conf = new Configuration(processes);
				localId = 0;
				numberOfNodes = 1;

				// Storage st = new StorageImpl("PaxosSTMLogs_" + localId);
				// AtomicBroadcast abcast = new
				// AtomicBroadcastJPaxosImpl(localId, conf.getN(),
				// new LocalConsensus(), st, null);

				LocalConsensus lc = new LocalConsensus();
				GlobalTransactionManager mgr = new GlobalTransactionManager(
						localId, conf.getN(), lc);
				GlobalTransactionManager.setInstance(mgr);
			}
			else
			{
				conf = new Configuration();
				numberOfNodes = conf.getN();

				PaxosConsensus paxos = new PaxosConsensus(conf, localId);
				// Storage st = new StorageImpl("PaxosSTMLogs_" + localId);

				GlobalTransactionManager mgr = new GlobalTransactionManager(
						localId, conf.getN(), paxos);
				paxos.setSingleAdeliverListener(mgr);
				GlobalTransactionManager.setInstance(mgr);
			}

			SharedObjectRegistry.init();

			System.setOut(new PrintStream(ptbos));
			System.setIn(ptlis);

			isInitialized = true;
			return args2;
		}
		catch (Throwable e)
		{
			throw new RuntimeException("PaxosSTM initialization failed.", e);
		}
	}

	/**
	 * Used to start the Paxos STM framework. This is the only method required
	 * to be invoked by the programmer in his application in order to be able to
	 * use Paxos STM.
	 * 
	 * @throws StorageException
	 */
	public void start() throws StorageException
	{
		if (!isInitialized)
			throw new RuntimeException(
					"Cannot start; initialize PaxosSTM first.");
		GlobalTransactionManager.getInstance().start();

		// try
		// {
		// Controller c = new Controller();
		// c.startCPUProfiling(ProfilingModes.CPU_SAMPLING |
		// ProfilingModes.SNAPSHOT_WITHOUT_HEAP,
		// " ", Controller.DEFAULT_WALLTIME_SPEC);
		// }
		// catch (Exception e)
		// {
		// e.printStackTrace();
		// }
	}

	public int getId()
	{
		return localId;
	}

	/**
	 * Adds an object <i>o</i> to the Shared Object Registry (see
	 * {@link GlobalTransactionManager}) under label <i>name</i>.
	 * 
	 * @param name
	 *            label of the object in the Shared Object Registry
	 * @param o
	 *            object to be put in the Shared Object Registry
	 * @return object that was previously labeled with the given name or null if
	 *         there was no such object
	 */
	public Object addToSharedObjectRegistry(final String name, final Object o)
	{
		final Holder<Object> returnValue = new Holder<Object>();

		new Transaction()
		{
			public void atomic()
			{
				returnValue.set(SharedObjectRegistry.registry.put(name,
						(BaseTransactionObject) o));
			}
		};

		return returnValue.get();
	}

	/**
	 * Adds an object <i>o</i> to the Shared Object Registry (see
	 * {@link GlobalTransactionManager}) under label <i>name</i> if there is no
	 * other object labeled with the given name at the moment.
	 * 
	 * @param name
	 *            label of the object in the Shared Object Registry
	 * @param o
	 *            object to be put in the Shared Object Registry
	 * @return object that was previously labeled with the given name or null if
	 *         there was no such object
	 */
	public Object addIfAbsentToSharedObjectRegistry(final String name,
			final Object o)
	{
		final Holder<Object> returnValue = new Holder<Object>();

		new Transaction()
		{
			public void atomic()
			{
				returnValue.set(SharedObjectRegistry.registry.get(name));
				if (returnValue.get() == null)
				{
					SharedObjectRegistry.registry.put(name,
							(BaseTransactionObject) o);
				}
			}
		};
		return returnValue.get();
	}

	// /**
	// * Adds an object <i>newObject</i> to the Shared Object Registry (see
	// * {@link GlobalTransactionManager}) under label <i>name</i> if the object
	// * <i>oldObject</i> is labeled with the given name at the moment.
	// *
	// * @param name
	// * label of the object in the Shared Object Registry
	// * @param oldObject
	// * object that determines whether the new one is going to be put
	// * in the registry
	// * @param newObject
	// * object to be put in the Shared Object Registry
	// * @return object that was previously labeled with the given name or null
	// if
	// * there was no such object
	// */
	// public Object replaceInTheSharedObjectRegistry(String name, Object
	// oldObject, Object newObject)
	// {
	// return
	// GlobalTransactionManager.getInstance().replaceInTheSharedObjectRegistry(name,
	// (Observable) oldObject, (Observable) newObject);
	// }

	/**
	 * Returns the object labeled with <i>name</i> from the Shared Object
	 * Registry.
	 * 
	 * @param name
	 *            label of the object to be retrieved from the Shared Object
	 *            Registry
	 * @return the object labeled with <i>name</i> from Shared Object Registry
	 */
	public Object getFromSharedObjectRegistry(final String name)
	{
		final Holder<Object> returnValue = new Holder<Object>();

		new Transaction(true)
		{
			public void atomic()
			{
				returnValue.set(SharedObjectRegistry.registry.get(name));
			}
		};

		return returnValue.get();
	}

	/**
	 * Removes the object labeled with <i>name</i> from the Shared Object
	 * Registry
	 * 
	 * @param name
	 *            label of the object to be removed from the Shared Object
	 *            Registry
	 * @return the object that was labeled with the given name of null if there
	 *         was no such object
	 */
	public Object removeFromSharedObjectRegistry(final String name)
	{
		final Holder<Object> returnValue = new Holder<Object>();

		new Transaction()
		{
			public void atomic()
			{
				returnValue.set(SharedObjectRegistry.registry.remove(name));
			}
		};

		return returnValue.get();
	}

	public void enterBarrier(String name, int n)
	{
		GlobalTransactionManager.getInstance().enterBarrier(name, n);
	}

	@Override
	public boolean addCheckpointListener(CheckpointListener listener)
	{
		return GlobalTransactionManager.getInstance().addCheckpointListener(
				listener);
	}

	@Override
	public boolean addRecoveryListener(RecoveryListener listener)
	{
		return GlobalTransactionManager.getInstance().addRecoveryListener(
				listener);
	}

	@Override
	public void scheduleCheckpoint()
	{
		GlobalTransactionManager.getInstance().scheduleCheckpoint();
	}

	@Override
	public boolean removeCheckpointListener(CheckpointListener listener)
	{
		return GlobalTransactionManager.getInstance().removeCheckpointListener(
				listener);
	}

	@Override
	public boolean removeRecoveryListener(RecoveryListener listener)
	{
		return GlobalTransactionManager.getInstance().removeRecoveryListener(
				listener);
	}

	public int getNumberOfNodes()
	{
		return numberOfNodes;
	}

	public int getNumberOfSharedObjects()
	{
		return GlobalTransactionManager.getInstance().getSharedObjectLibrary()
				.getSize();
	}

	public Map<Class<?>, Integer> getSharedObjectsTypeCount()
	{
		return GlobalTransactionManager.getInstance().getSharedObjectLibrary()
				.getSharedObjectsTypeCount();
	}
}
