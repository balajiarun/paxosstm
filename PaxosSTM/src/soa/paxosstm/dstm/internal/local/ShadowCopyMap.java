package soa.paxosstm.dstm.internal.local;

import java.util.Arrays;

import soa.paxosstm.dstm.TransactionStatistics;
import soa.paxosstm.dstm.TransactionStatistics.State;
import soa.paxosstm.dstm.internal.exceptions.InvalidNestedTransactionException;
import soa.paxosstm.dstm.internal.exceptions.InvalidOperationException;
import soa.paxosstm.dstm.internal.exceptions.TransactionAbortedException;
import soa.paxosstm.dstm.internal.exceptions.WriteOnReadOnlyException;
import soa.paxosstm.dstm.internal.global.TransactionPackage;
import soa.paxosstm.dstm.internal.global.UpdateCursor;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;

public final class ShadowCopyMap extends MultiLevelHashMap
{
	private int currentLevel;
	private int firstReadOnlyLevel;
	private int newObjects = 0;
	private final ReadWriteTransactionHandler handler;
	private final UpdateCursor updateCursor;

	public ShadowCopyMap(ReadWriteTransactionHandler handler, UpdateCursor updateCursor)
	{
		currentLevel = 1;
		firstReadOnlyLevel = -1;
		this.handler = handler;
		this.updateCursor = updateCursor;
	}

	public int getCurrentLevel()
	{
		return currentLevel;
	}

	public long getHighestVersion()
	{
		return updateCursor.getVersion();
	}

	public void newLevel(boolean readOnly)
	{
		if (isReadOnly())
		{
			if (!readOnly)
				throw new InvalidNestedTransactionException(
						"Read-write transactions cannot be nested in a read-only transaction.");
		}
		else
		{
			if (readOnly)
				firstReadOnlyLevel = currentLevel + 1;

		}
		currentLevel++;
	}

	public void commitLevel()
	{
		Entry toBeMovedList = null;
		Entry firstMoved = null;

		Entry e, n;
		for (e = header; e != null && e.level == currentLevel; e = n)
		{
			n = e.after;
			Value v = e.nextValue;
			if (v != null && (v.level == currentLevel - 1))
			{
				BaseTransactionObject newCopy = e.value;
				BaseTransactionObject oldCopy = v.value;
				if (newCopy.__tc_isWritten())
				{
					if (!oldCopy.__tc_isVersion())
					{
						newCopy.__tc_copy(oldCopy);
						oldCopy.__tc_setWritten(true);
						e.value = oldCopy;
					}
				}
				e.level = v.level;
				e.nextValue = v.nextValue;
				e.after = v.after;
				totalSize--;
			}
			else
			{
				if (firstMoved == null)
					firstMoved = e;
				e.after = toBeMovedList;
				toBeMovedList = e;
				e.level--;
			}
		}

		if (firstMoved != null)
		{
			header = toBeMovedList;
			firstMoved.after = e;
		}
		else
			header = e;
		if (--currentLevel < firstReadOnlyLevel)
			firstReadOnlyLevel = -1;
	}

	public void rollbackLevel()
	{
		Entry e, n;
		for (e = header; e != null && e.level == currentLevel; e = n)
		{
			n = e.after;
			Value v = e.nextValue;
			if (v != null)
			{
				e.value = v.value;
				e.level = v.level;
				e.nextValue = v.nextValue;
				e.after = v.after;
				totalSize--;
			}
			else
			{
				if (e.handle.__tc_retrieveNewest() == null)
					newObjects--;
				removeEntry(e);
			}
		}
		header = e;
		if (--currentLevel < firstReadOnlyLevel)
			firstReadOnlyLevel = -1;
	}

	public void registerNewObject(BaseTransactionObject o)
	{
		if (isReadOnly())
			throw new InvalidOperationException(
					"Cannot create new shared objects in a read-only transaction.");

		newObjects++;
		int id = -newObjects;
		o.__tc_initHandle(id);

		BaseTransactionObject copy = o.__tc_newInstance();
		copy.__tc_initShadowCopy(id);
		copy.__tc_setVersion(o.__tc_getVersion());
		copy.__tc_setNext(o);
		copy.__tc_setWritten(true);
		add(o, copy, currentLevel);
	}

	public BaseTransactionObject getTargetObjectForRead(BaseTransactionObject handle)
	{
		Entry e = getEntry(handle.__tc_getId());
		if (e != null)
		{
			return e.getValue();
		}

		BaseTransactionObject version = handle.__tc_retrieveNewest();

		long versionNumber = version.__tc_getVersion();
		if (!runConflictCheck(versionNumber))
			abort();

		add(handle, version, currentLevel);
		return version;
	}

	public BaseTransactionObject getTargetObjectForWrite(BaseTransactionObject handle)
	{
		if (isReadOnly())
			throw new WriteOnReadOnlyException();

		Entry e = getEntry(handle.__tc_getId());
		if (e != null)
		{
			BaseTransactionObject copy = e.getValue();
			if (e.getLevel() == currentLevel)
			{
				if (copy.__tc_isShadowCopy())
					return copy;
				copy = copy.__tc_makeCopy();
				e.setValue(copy);
				return copy;
			}
			BaseTransactionObject newCopy = copy.__tc_makeCopy();
			newCopy.__tc_setNext(handle);
			addNewValue(e, newCopy, currentLevel);
			return newCopy;
		}

		BaseTransactionObject version = handle.__tc_retrieveNewest();

		long versionNumber = version.__tc_getVersion();
		if (!runConflictCheck(versionNumber))
			abort();

		BaseTransactionObject copy = version.__tc_makeCopy();
		copy.__tc_setNext(handle);

		add(handle, copy, currentLevel);
		return copy;
	}

	public BaseTransactionObject getTargetObjectForCall(BaseTransactionObject handle)
	{
		Entry e = getEntry(handle.__tc_getId());
		if (e != null)
		{
			BaseTransactionObject copy = e.getValue();
			if (isReadOnly())
			{
				if (e.getLevel() == currentLevel || copy.__tc_isVersion())
				{
					return copy;
				}
			}
			else
			{
				if (e.getLevel() == currentLevel)
				{
					if (copy.__tc_isShadowCopy())
						return copy;
					BaseTransactionObject newCopy = copy.__tc_makeCopy();
					newCopy.__tc_setNext(handle);
					e.setValue(newCopy);
					return newCopy;
				}
			}
			BaseTransactionObject newCopy = copy.__tc_makeCopy();
			newCopy.__tc_setNext(handle);
			addNewValue(e, newCopy, currentLevel);
			return newCopy;
		}

		BaseTransactionObject version = handle.__tc_retrieveNewest();

		long versionNumber = version.__tc_getVersion();
		if (!runConflictCheck(versionNumber))
			abort();

		BaseTransactionObject copy;
		if (isReadOnly())
			copy = version;
		else
		{
			copy = version.__tc_makeCopy();
			copy.__tc_setNext(handle);
		}

		add(handle, copy, currentLevel);
		return copy;
	}

	private boolean isReadOnly()
	{
		return firstReadOnlyLevel >= 0;
	}

	private boolean runConflictCheck(long versionNumber)
	{
		if (versionNumber > updateCursor.getVersion())
		{
			do
			{
				int[] writeSet = updateCursor.next();
				if (!validate(writeSet))
					return false;
			} while (versionNumber > updateCursor.getVersion());
			handler.updateCursor.setVersion(versionNumber);
		}
		return true;
	}

	private void abort()
	{
		TransactionStatistics.getStatistics().setState(State.LocalAbort);
		handler.abort();
		throw new TransactionAbortedException();
	}

	private boolean validate(int[] writeSet)
	{
		int log = 32 - Integer.numberOfLeadingZeros(writeSet.length);
		if (writeSet.length <= TransactionPackage.WRITE_SET_SORT_THRESHOLD
				|| writeSet.length <= table.length * log)
		{
			for (int id : writeSet)
			{
				if (getEntry(id) != null)
					return false;
			}
		}
		else
		{
			for (Entry e : table)
			{
				for (; e != null; e = e.nextEntry)
				{
					if (Arrays.binarySearch(writeSet, e.getId()) >= 0)
						return false;
				}
			}
		}
		return true;
	}
}
