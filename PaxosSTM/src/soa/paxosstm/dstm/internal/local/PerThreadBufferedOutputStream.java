/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.local;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * This class provides buffered output stream for a transaction.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class PerThreadBufferedOutputStream extends OutputStream
{
	private ByteArrayOutputStream baos;
	private OutputStream originalStream;

	/**
	 * Constructor.
	 * 
	 * @param originalStream
	 *            reference to the original {@link OutputStream}
	 */
	public PerThreadBufferedOutputStream(OutputStream originalStream)
	{
		this.originalStream = originalStream;
	}

	@Override
	public void write(int b) throws IOException
	{
		if (baos == null)
			baos = new ByteArrayOutputStream();
		baos.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException
	{
		if (baos == null)
			baos = new ByteArrayOutputStream();
		baos.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException
	{
		if (baos == null)
			baos = new ByteArrayOutputStream();
		baos.write(b, off, len);
	}

	@Override
	public void flush() throws IOException
	{
		if (baos == null)
			baos = new ByteArrayOutputStream();
		baos.flush();
	}

	@Override
	public void close() throws IOException
	{
		if (baos == null)
			baos = new ByteArrayOutputStream();
		baos.close();
	}

	/**
	 * Disables buffered output.
	 */
	public OutputStream discard()
	{
		return originalStream;
	}

	/**
	 * Flushes the buffered output to the original output stream and removes
	 * open streams.
	 * 
	 * @throws IOException
	 */
	public OutputStream finish() throws IOException
	{
		if (baos != null)
		{
			byte[] data = baos.toByteArray();
			originalStream.write(data);
			baos.close();
		}

		return originalStream;
	}
}
