package soa.paxosstm.dstm.internal.local;

import java.io.IOException;
import java.io.OutputStream;

public class PerThreadBufferedOutputStreamWrapper extends OutputStream
{
	private ThreadLocal<PerThreadBufferedOutputStream> currentOutputStream = new ThreadLocal<PerThreadBufferedOutputStream>();
	private OutputStream originalStream;

	public PerThreadBufferedOutputStreamWrapper(OutputStream originalStream)
	{
		this.originalStream = originalStream;
	}
	
	@Override
	public void write(int b) throws IOException
	{
		OutputStream os = currentOutputStream.get();
		if (os == null)
			os = originalStream;
		os.write(b);
	}

	@Override
	public void write(byte[] b) throws IOException
	{
		OutputStream os = currentOutputStream.get();
		if (os == null)
			os = originalStream;
		os.write(b);
	}

	@Override
	public void write(byte[] b, int off, int len) throws IOException
	{
		OutputStream os = currentOutputStream.get();
		if (os == null)
			os = originalStream;
		os.write(b, off, len);
	}

	@Override
	public void flush() throws IOException
	{
		OutputStream os = currentOutputStream.get();
		if (os == null)
			os = originalStream;
		os.flush();
	}

	@Override
	public void close() throws IOException
	{
		OutputStream os = currentOutputStream.get();
		if (os == null)
			os = originalStream;
		os.close();
	}

	public void startLevel()
	{
		OutputStream os = currentOutputStream.get();
		if (os == null)
			os = originalStream;
		PerThreadBufferedOutputStream ptbos = new PerThreadBufferedOutputStream(os);
		currentOutputStream.set(ptbos);
	}

	public void commitLevel() throws IOException
	{
		PerThreadBufferedOutputStream os = currentOutputStream.get();
		if (os == null)
			return;

		OutputStream oos = os.finish();

		if (oos instanceof PerThreadBufferedOutputStream)
			currentOutputStream.set((PerThreadBufferedOutputStream) oos);
		else
			currentOutputStream.remove();
	}

	public void rollbackLevel()
	{
		PerThreadBufferedOutputStream os = currentOutputStream.get();
		if (os == null)
			return;

		OutputStream oos = os.discard();

		if (oos instanceof PerThreadBufferedOutputStream)
			currentOutputStream.set((PerThreadBufferedOutputStream) oos);
		else
			currentOutputStream.remove();
	}

	public void reset()
	{
		while (currentOutputStream.get() != null)
			rollbackLevel();
		startLevel();
	}
}
