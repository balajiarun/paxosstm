package soa.paxosstm.dstm.internal.local;

import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.TransactionStatistics;
import soa.paxosstm.dstm.TransactionStatistics.State;
import soa.paxosstm.dstm.internal.exceptions.InvalidNestedTransactionException;
import soa.paxosstm.dstm.internal.exceptions.InvalidOperationException;
import soa.paxosstm.dstm.internal.exceptions.NoTransactionInProgressException;
import soa.paxosstm.dstm.internal.exceptions.WriteOnReadOnlyException;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager.TransactionOutcome;

public final class ReadOnlyTransactionHandler extends TransactionHandler
{
	int depth;
	
	long timerStart;

	public ReadOnlyTransactionHandler()
	{
		updateCursor = GlobalTransactionManager.getInstance().getUpdateCursor();
		depth = 1;
		
		if (PaxosSTM.DEBUG)
		{
			timerStart = System.currentTimeMillis();
		}
	}

	@Override
	public void newTransaction(boolean readOnly)
	{
		checkLastOp();

		if (!readOnly)
			throw new InvalidNestedTransactionException(
					"Read-write transactions cannot be nested in a read-only transaction.");
		depth++;
	}

	@Override
	public boolean commit()
	{
		checkLastOp();

		if (depth <= 0) // should not happen
			throw new NoTransactionInProgressException();
		
		if (PaxosSTM.DEBUG)
		{
			if (depth == 1)
				TransactionStatistics.getStatistics().setExecutionTime(
						System.currentTimeMillis() - timerStart);
		}

		if (--depth == 0)
			clearFlag = true;
		
		TransactionStatistics.getStatistics().setState(State.Committed);

		lastOperation = TransactionOutcome.Committed;
		notifyCount = 1;
		return true;
	}

	@Override
	public void rollback()
	{
		checkLastOp();

		if (depth <= 0) // should not happen
			throw new NoTransactionInProgressException();
		
		if (PaxosSTM.DEBUG)
		{
			if (depth == 1)
				TransactionStatistics.getStatistics().setExecutionTime(
						System.currentTimeMillis() - timerStart);
		}

		if (--depth == 0)
		{
			clearFlag = true;
		}
		
		TransactionStatistics.getStatistics().setState(State.RolledBack);

		lastOperation = TransactionOutcome.Rolledback;
		notifyCount = 1;
	}

	@Override
	public void retry()
	{
		checkLastOp();
		
		if (PaxosSTM.DEBUG)
		{
			if (depth == 1)
			{
				long currentTime = System.currentTimeMillis();
				TransactionStatistics.getStatistics().setExecutionTime(
						currentTime - timerStart);
				timerStart = currentTime;
			}
		}

		updateCursor = GlobalTransactionManager.getInstance().getUpdateCursor();
		int oldDepth = depth;
		depth = 1;

		lastOperation = TransactionOutcome.Aborted;
		notifyCount = oldDepth;
	}

	@Override
	public void registerNewObject(BaseTransactionObject o)
	{
		checkLastOp();

		throw new InvalidOperationException(
				"Cannot create new shared objects in a read-only transaction.");
	}

	@Override
	public BaseTransactionObject getTargetObjectForRead(BaseTransactionObject o)
	{
		checkLastOp();

		return o.__tc_retrieve(updateCursor.getVersion());
	}

	@Override
	public BaseTransactionObject getTargetObjectForWrite(BaseTransactionObject o)
	{
		checkLastOp();

		throw new WriteOnReadOnlyException();
	}

	@Override
	public BaseTransactionObject getTargetObjectForCall(BaseTransactionObject o)
	{
		checkLastOp();

		return o.__tc_retrieve(updateCursor.getVersion());
	}
}
