/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.local;

import java.io.IOException;
import java.io.InputStream;

/**
 * This class provides mechanism which forbids writing to input stream when
 * executing code inside a transaction.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class PerThreadLimitingInputStream extends InputStream
{
	static class InputForbiddenException extends RuntimeException
	{
		private static final long serialVersionUID = -8699855391156588542L;
	}

	private ThreadLocal<Boolean> inputAllowed = new ThreadLocal<Boolean>();
	private InputStream originalStream;

	/**
	 * Constructor.
	 * 
	 * @param originalStream
	 *            reference to the original {@link InputStream}
	 */
	public PerThreadLimitingInputStream(InputStream originalStream)
	{
		this.originalStream = originalStream;
	}

	@Override
	public int available() throws IOException
	{
		if ((inputAllowed.get() != null) && !inputAllowed.get())
			throw new InputForbiddenException();
		return originalStream.available();
	}

	@Override
	public void close() throws IOException
	{
		if ((inputAllowed.get() != null) && !inputAllowed.get())
			throw new InputForbiddenException();
		originalStream.close();
	}

	@Override
	public void mark(int readlimit)
	{
		if ((inputAllowed.get() != null) && !inputAllowed.get())
			throw new InputForbiddenException();
		originalStream.mark(readlimit);
	}

	@Override
	public boolean markSupported()
	{
		if ((inputAllowed.get() != null) && !inputAllowed.get())
			throw new InputForbiddenException();
		return originalStream.markSupported();
	}

	@Override
	public int read() throws IOException
	{
		if ((inputAllowed.get() != null) && !inputAllowed.get())
			throw new InputForbiddenException();
		return originalStream.read();
	}

	@Override
	public int read(byte[] b) throws IOException
	{
		if ((inputAllowed.get() != null) && !inputAllowed.get())
			throw new InputForbiddenException();
		return originalStream.read(b);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException
	{
		if ((inputAllowed.get() != null) && !inputAllowed.get())
			throw new InputForbiddenException();
		return originalStream.read(b, off, len);
	}

	@Override
	public void reset() throws IOException
	{
		if ((inputAllowed.get() != null) && !inputAllowed.get())
			throw new InputForbiddenException();
		originalStream.reset();
	}

	@Override
	public long skip(long n) throws IOException
	{
		if ((inputAllowed.get() != null) && !inputAllowed.get())
			throw new InputForbiddenException();
		return originalStream.skip(n);
	}

	/**
	 * Enables the mechanism forcing limited input during performing a
	 * transaction.
	 */
	public void enable()
	{
		inputAllowed.set(false);
	}

	/**
	 * Disables the mechanism forcing limited input during performing a
	 * transaction.
	 */
	public void disable()
	{
		inputAllowed.remove();
	}
}
