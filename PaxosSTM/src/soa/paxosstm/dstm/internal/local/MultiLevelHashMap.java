package soa.paxosstm.dstm.internal.local;

import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;

import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;

public class MultiLevelHashMap implements Iterable<MultiLevelHashMap.Entry>
{
	/**
	 * The default initial capacity - MUST be a power of two.
	 */
	private static final int INITIAL_CAPACITY = 16;

	/**
	 * The load factor used.
	 */
	private static final float LOAD_FACTOR = 0.75f;

	/**
	 * The table, resized as necessary. Length MUST Always be a power of two.
	 */
	protected Entry[] table;

	/**
	 * The number of entries contained in this map.
	 */
	protected int size;

	/**
	 * The number of key-value mappings contained in this map.
	 */
	protected int totalSize;

	protected Entry header;

	private int threshold;

	public MultiLevelHashMap()
	{
		table = new Entry[INITIAL_CAPACITY];
		header = null;
		threshold = (int) (INITIAL_CAPACITY * LOAD_FACTOR);
	}

	/**
	 * Returns the entry associated with the specified key in the HashMap.
	 * Returns null if the HashMap contains no mapping for the key.
	 */
	public final Entry getEntry(int key)
	{
		int hash = key;
		for (Entry e = table[indexFor(hash, table.length)]; e != null; e = e.nextEntry)
		{
			if (e.getId() == key)
				return e;
		}
		return null;
	}

	public final void removeEntry(Entry entry)
	{
		int hash = entry.getId();
		int idx = indexFor(hash, table.length);
		Entry e = table[idx];

		if (e == entry)
		{
			table[idx] = e.nextEntry;
		}
		else
		{
			for (; e != null; e = e.nextEntry)
			{
				if (e.nextEntry == entry)
				{
					e.nextEntry = entry.nextEntry;
					break;
				}
			}
		}
		size--;

		Value v = entry.nextValue;
		do
		{
			totalSize--;
		} while (v != null);
	}

	public final Entry add(BaseTransactionObject handle, BaseTransactionObject value, int level)
	{
		int hash = handle.__tc_getId();
		int bucketIndex = indexFor(hash, table.length);
		Entry e = new Entry(handle, value, level, header, table[bucketIndex]);
		table[bucketIndex] = e;
		header = e;
		if (size++ > threshold)
			resize(2 * table.length);
		totalSize++;
		return e;
	}

	public final void addNewValue(Entry e, BaseTransactionObject v, int level)
	{
		Value value = new Value(e.value, e.level, e.nextValue, e.after);
		e.value = v;
		e.level = level;
		e.nextValue = value;
		e.after = header;
		header = e;
		totalSize++;
	}

	/**
	 * Rehashes the contents of this map into a new array with a larger
	 * capacity. This method is called automatically when the number of keys in
	 * this map reaches its threshold.
	 * 
	 * If current capacity is MAXIMUM_CAPACITY, this method does not resize the
	 * map, but sets threshold to Integer.MAX_VALUE. This has the effect of
	 * preventing future calls.
	 * 
	 * @param newCapacity
	 *            the new capacity, MUST be a power of two; must be greater than
	 *            current capacity unless current capacity is MAXIMUM_CAPACITY
	 *            (in which case value is irrelevant).
	 */
	private void resize(int newCapacity)
	{
		Entry[] newTable = new Entry[newCapacity];
		transfer(newTable);
		table = newTable;
		threshold = (int) (newCapacity * LOAD_FACTOR);
	}

	/**
	 * Transfers all entries from current table to newTable.
	 */
	private void transfer(Entry[] newTable)
	{
		Entry[] src = table;
		int newCapacity = newTable.length;
		for (int j = 0; j < src.length; j++)
		{
			Entry e = src[j];
			if (e != null)
			{
				src[j] = null;
				do
				{
					Entry next = e.nextEntry;
					int hash = e.getId();
					int i = indexFor(hash, newCapacity);
					e.nextEntry = newTable[i];
					newTable[i] = e;
					e = next;
				} while (e != null);
			}
		}
	}

	public final Iterator<Entry> iterator()
	{
		return new EntryIterator();
	}

	/**
	 * Returns index for hash code h.
	 */
	private static int indexFor(int h, int length)
	{
		return h & (length - 1);
	}

	// /**
	// * Applies a supplemental hash function to a given hashCode, which defends
	// * against poor quality hash functions. This is critical because HashMap
	// * uses power-of-two length hash tables, that otherwise encounter
	// collisions
	// * for hashCodes that do not differ in lower bits. Note: Null keys always
	// * map to hash 0, thus index 0.
	// */
	// private static int hash(int h)
	// {
	// // This function ensures that hashCodes that differ only by
	// // constant multiples at each bit position have a bounded
	// // number of collisions (approximately 8 at default load factor).
	// h ^= (h >>> 20) ^ (h >>> 12);
	// return h ^ (h >>> 7) ^ (h >>> 4);
	// }

	protected static class Value
	{
		protected BaseTransactionObject value;
		protected int level;
		protected Value nextValue;
		protected Entry after;

		protected Value(BaseTransactionObject v, int l, Value n, Entry a)
		{
			value = v;
			level = l;
			nextValue = n;
			after = a;
		}
	}

	public final static class Entry extends Value implements Map.Entry<Integer, BaseTransactionObject>, Comparable<Entry>
	{
		protected final BaseTransactionObject handle;
		protected Entry nextEntry;

		/**
		 * Creates new entry.
		 */
		protected Entry(BaseTransactionObject o, BaseTransactionObject v, int level,
				Entry a, Entry n)
		{
			super(v, level, null, a);
			handle = o;
			nextEntry = n;
		}

		public BaseTransactionObject getHandle()
		{
			return handle;
		}

		public int getLevel()
		{
			return level;
		}

		public Integer getKey()
		{
			return handle.__tc_getId();
		}
		
		public int getId()
		{
			return handle.__tc_getId();
		}

		public BaseTransactionObject getValue()
		{
			return value;
		}

		public int hashCode()
		{
			return getId() ^ (value == null ? 0 : value.hashCode());
		}

		public String toString()
		{
			return getId() + "=" + getValue();
		}

		@Override
		public BaseTransactionObject setValue(BaseTransactionObject value)
		{
			BaseTransactionObject oldValue = this.value;
			this.value = value;
			return oldValue;
		}

		public void purge()
		{
			nextEntry = null;
			nextValue = null;
		}

		public int getTypeId()
		{
			return level;
		}
		
		public void setTypeId(int typeId)
		{
			level = typeId;
		}

		public Entry getNext()
		{
			return after;
		}

		@Override
		public int compareTo(Entry other)
		{
			return getId() - other.getId();
		}
	}

	protected final class EntryIterator implements Iterator<Entry>
	{
		Entry nextEntry = header;
		Entry lastReturned = null;

		@Override
		public boolean hasNext()
		{
			return nextEntry != null;
		}

		@Override
		public Entry next()
		{
			if (nextEntry == null)
				throw new NoSuchElementException();

			Entry e = lastReturned = nextEntry;
			nextEntry = e.after;
			return e;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}
	}
}
