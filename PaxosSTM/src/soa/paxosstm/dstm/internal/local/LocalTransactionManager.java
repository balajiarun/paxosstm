package soa.paxosstm.dstm.internal.local;

import soa.paxosstm.dstm.internal.exceptions.NoTransactionInProgressException;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;

public final class LocalTransactionManager
{
	public enum TransactionOutcome
	{
		Aborted, Committed, Rolledback, Unexpected
	}

	private static ThreadLocal<TransactionHandler> handler = new ThreadLocal<TransactionHandler>();
	
	protected static void removeHandler()
	{
		handler.remove();
	}

	/**
	 * Starts new (possibly nested) transaction.
	 * 
	 * @return the value indicating whether the new transaction is a top level
	 *         transaction.
	 */
	public static boolean newTransaction(boolean readOnly)
	{
		TransactionHandler h = handler.get();
		if (h != null)
		{
			h.newTransaction(readOnly);
			return false;
		}

		if (readOnly)
			h = new ReadOnlyTransactionHandler();
		else
			h = new ReadWriteTransactionHandler();
		handler.set(h);
		
		return true;
	}
	
	public static void localCommit()
	{
		((ReadWriteTransactionHandler)getHandler()).localCommit();
	}

	/**
	 * Commits current (possibly nested) transaction.
	 * 
	 * @return the value indicating whether the commit was successful.
	 */
	public static boolean commit()
	{
		return getHandler().commit();
	}

	/**
	 * Rollbacks current (possibly nested) transaction.
	 */
	public static void rollback()
	{
		getHandler().rollback();
	}

	/**
	 * Retries current transaction.
	 */
	public static void retry()
	{
		getHandler().retry();
	}

	public static BaseTransactionObject getTargetObjectForRead(BaseTransactionObject o)
	{
		return getHandler().getTargetObjectForRead(o);
	}

	public static BaseTransactionObject getTargetObjectForWrite(BaseTransactionObject o)
	{
		return getHandler().getTargetObjectForWrite(o);
	}

	public static BaseTransactionObject getTargetObjectForCall(BaseTransactionObject o)
	{
		return getHandler().getTargetObjectForCall(o);
	}

	/**
	 * Registers a newly created shared object.
	 * 
	 * @param id
	 *            the id of the shared object.
	 * @param o
	 *            the shared object.
	 */
	public static void registerNewObject(BaseTransactionObject o)
	{
		getHandler().registerNewObject(o);
	}

	public static void notifyEnd()
	{
		getHandler().notifyEnd();
	}

	public static TransactionOutcome unexpected()
	{
		return getHandler().unexpected();
	}

	private static TransactionHandler getHandler()
	{
		TransactionHandler h = handler.get();
		if (h == null)
			throw new NoTransactionInProgressException();
		return h;
	}
}
