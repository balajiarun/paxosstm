package soa.paxosstm.dstm.internal.local;

import soa.paxosstm.dstm.internal.exceptions.TransactionAbortedException;
import soa.paxosstm.dstm.internal.exceptions.TransactionCommittedException;
import soa.paxosstm.dstm.internal.exceptions.TransactionRolledbackException;
import soa.paxosstm.dstm.internal.global.UpdateCursor;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager.TransactionOutcome;

public abstract class TransactionHandler
{
	protected UpdateCursor updateCursor;
	
	/**
	 * Starts new nested transaction.
	 */
	public abstract void newTransaction(boolean readOnly);

	/**
	 * Commits current (possibly nested) transaction.
	 * 
	 * @return the value indicating whether the commit was successful.
	 */
	public abstract boolean commit();

	/**
	 * Rollbacks current (possibly nested) transaction.
	 */
	public abstract void rollback();

	/**
	 * Retries current transaction.
	 */
	public abstract void retry();

	/**
	 * Registers a newly created shared object.
	 * 
	 * @param id
	 *            the id of the shared object.
	 * @param o
	 *            the shared object.
	 */
	public abstract void registerNewObject(BaseTransactionObject o);

	public abstract BaseTransactionObject getTargetObjectForRead(BaseTransactionObject o);

	public abstract BaseTransactionObject getTargetObjectForWrite(BaseTransactionObject o);

	public abstract BaseTransactionObject getTargetObjectForCall(BaseTransactionObject o);

	protected TransactionOutcome lastOperation = null;
	protected int notifyCount = 0;
	protected boolean clearFlag = false;

	public void notifyEnd()
	{
		if (--notifyCount <= 0)
		{
			lastOperation = null;
			if (clearFlag)
			{
				LocalTransactionManager.removeHandler();
				updateCursor.disable();
			}
		}
	}

	public TransactionOutcome unexpected()
	{
		TransactionOutcome result = lastOperation;

		if (result == null)
		{
			rollback();
			result = TransactionOutcome.Unexpected;
		}
		notifyEnd();

		return result;
	}

	protected void checkLastOp()
	{
		if (lastOperation != null)
		{
			switch (lastOperation)
			{
			case Aborted:
				throw new TransactionAbortedException();
			case Committed:
				throw new TransactionCommittedException();
			case Rolledback:
				throw new TransactionRolledbackException();
			case Unexpected:
				throw new RuntimeException();
			}
		}
	}
}
