package soa.paxosstm.dstm.internal.local;

import java.io.IOException;

import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.TransactionStatistics;
import soa.paxosstm.dstm.TransactionStatistics.State;
import soa.paxosstm.dstm.internal.TransactionState;
import soa.paxosstm.dstm.internal.exceptions.NoTransactionInProgressException;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager.TransactionOutcome;
import soa.paxosstm.utils.TCID;

public final class ReadWriteTransactionHandler extends TransactionHandler
{
	protected TCID id = TCID.randomTCID();
	protected TransactionState state;
	protected CommitDescriptor commitDescriptor;
	protected ShadowCopyMap shadowCopyMap;

	private long timerStart = 0;

	public ReadWriteTransactionHandler()
	{
		GlobalTransactionManager.getInstance().registerTransactionHandler(id, this);
		initTopLevelTransaction();
		PaxosSTM.ptlis.enable();
		PaxosSTM.ptbos.startLevel();
	}

	@Override
	public void newTransaction(boolean readOnly)
	{
		checkLastOp();
		shadowCopyMap.newLevel(readOnly);
		PaxosSTM.ptbos.startLevel();
	}

	@Override
	public boolean commit()
	{
		checkLastOp();

		if (shadowCopyMap == null) // should not happen
			throw new NoTransactionInProgressException();

		if (shadowCopyMap.getCurrentLevel() > 1)
		{
			shadowCopyMap.commitLevel();
			lastOperation = TransactionOutcome.Committed;
			notifyCount = 1;
			return true;
		}

		long abcastTime = 0;
		if (PaxosSTM.DEBUG)
		{
			long currentTime = System.currentTimeMillis();
			TransactionStatistics.getStatistics().setExecutionTime(currentTime - timerStart);
			abcastTime = currentTime;
			timerStart = currentTime;
		}

		commitDescriptor = new CommitDescriptor(id);
		if (!commitDescriptor.init(shadowCopyMap))
		{
			TransactionStatistics.getStatistics().setState(State.LocalAbort);
			abort();
			return false;
		}

		updateCursor.disable();
		if (commitDescriptor.isLocal())
		{
			state = TransactionState.Committed;
		}
		else
		{
			state = TransactionState.Committing;
			GlobalTransactionManager.getInstance().commitTransaction(commitDescriptor);
			synchronized (this)
			{
				while (state == TransactionState.Committing)
				{
					try
					{
						wait();
					}
					catch (InterruptedException e)
					{
						throw new RuntimeException(e);
					}
				}
			}

			if (PaxosSTM.DEBUG)
			{
				TransactionStatistics.getStatistics().setCommittingPhaseLatency(
						System.currentTimeMillis() - timerStart);
				timerStart = 0;

				TransactionStatistics.getStatistics().setAbcastDuration(
						commitDescriptor.getAbcastArrivalTime() - abcastTime);
				TransactionStatistics.getStatistics().setValidationTime(
						commitDescriptor.getValidationTime());
				TransactionStatistics.getStatistics().setUpdateTime(
						commitDescriptor.getUpdateTime());
			}
		}

		if (state == TransactionState.Committed)
		{
			TransactionStatistics.getStatistics().setState(State.Committed);
			TransactionStatistics.getStatistics().setReadSetSize(commitDescriptor.getReadSetSize());

			TransactionStatistics.getStatistics().setWriteSetSize(
					commitDescriptor.getWriteSetSize());
			TransactionStatistics.getStatistics().setNewSetSize(commitDescriptor.getNewSetSize());
			 TransactionStatistics.getStatistics()
					.setTypeSetSize(commitDescriptor.getTypesSetSize());
			TransactionStatistics.getStatistics().setPackageSize(commitDescriptor.getPackageSize());

			clearFlag = true;
			GlobalTransactionManager.getInstance().unregisterTransactionHandler(id);
			lastOperation = TransactionOutcome.Committed;
			notifyCount = 1;
			return true;
		}
		else
		{
			TransactionStatistics.getStatistics().setState(State.GlobalAbort);
			TransactionStatistics.getStatistics().setReadSetSize(commitDescriptor.getReadSetSize());
			TransactionStatistics.getStatistics().setWriteSetSize(
					commitDescriptor.getWriteSetSize());
			TransactionStatistics.getStatistics()
					.setTypeSetSize(commitDescriptor.getTypesSetSize());
			TransactionStatistics.getStatistics().setPackageSize(commitDescriptor.getPackageSize());
			abort();
			return false;
		}
	}

	@Override
	public void rollback()
	{
		checkLastOp();

		if (shadowCopyMap == null) // should not happen
			throw new NoTransactionInProgressException();

		if (shadowCopyMap.getCurrentLevel() > 1)
		{
			shadowCopyMap.rollbackLevel();
		}
		else
		{
			if (PaxosSTM.DEBUG)
			{

				TransactionStatistics.getStatistics().setExecutionTime(
						System.currentTimeMillis() - timerStart);

			}
			TransactionStatistics.getStatistics().setState(State.RolledBack);
			clearFlag = true;
			GlobalTransactionManager.getInstance().unregisterTransactionHandler(id);
		}

		lastOperation = TransactionOutcome.Rolledback;
		notifyCount = 1;
	}

	@Override
	public void retry()
	{
		checkLastOp();
		abort();
	}

	@Override
	public void registerNewObject(BaseTransactionObject o)
	{
		checkLastOp();
		shadowCopyMap.registerNewObject(o);
	}

	@Override
	public BaseTransactionObject getTargetObjectForRead(BaseTransactionObject o)
	{
		checkLastOp();
		return shadowCopyMap.getTargetObjectForRead(o);
	}

	@Override
	public BaseTransactionObject getTargetObjectForWrite(BaseTransactionObject o)
	{
		checkLastOp();
		return shadowCopyMap.getTargetObjectForWrite(o);
	}

	@Override
	public BaseTransactionObject getTargetObjectForCall(BaseTransactionObject o)
	{
		checkLastOp();
		return shadowCopyMap.getTargetObjectForCall(o);
	}

	@Override
	public void notifyEnd()
	{
		if (--notifyCount <= 0)
		{
			if (clearFlag)
			{
				LocalTransactionManager.removeHandler();
				updateCursor.disable();
				PaxosSTM.ptlis.disable();
			}

			switch (lastOperation)
			{
			case Aborted:
				PaxosSTM.ptbos.reset();
				break;
			case Committed:
				try
				{
					PaxosSTM.ptbos.commitLevel();
				}
				catch (IOException e)
				{
					e.printStackTrace();
					System.exit(1);
				}
				break;
			case Rolledback:
				PaxosSTM.ptbos.rollbackLevel();
				break;
			case Unexpected:
				break;
			}
			lastOperation = null;
		}
	}

	public void transactionFinished(TransactionState state)
	{
		synchronized (this)
		{
			notifyAll();
			this.state = state;
		}
	}

	public CommitDescriptor getCommitDescriptor()
	{
		return commitDescriptor;
	}

	protected void abort()
	{
		int depth = shadowCopyMap.getCurrentLevel();
		initTopLevelTransaction();

		lastOperation = TransactionOutcome.Aborted;
		notifyCount = depth;
	}

	private void initTopLevelTransaction()
	{
		if (PaxosSTM.DEBUG)
		{
			long currentTime = System.currentTimeMillis();
			if (timerStart != 0)
			{
				TransactionStatistics.getStatistics().setExecutionTime(currentTime - timerStart);
				TransactionStatistics.getStatistics().setCommittingPhaseLatency(
						currentTime - timerStart);
			}
			timerStart = currentTime;
		}

		updateCursor = GlobalTransactionManager.getInstance().getUpdateCursor();
		state = TransactionState.Executing;
//		shadowCopyMap = new ShadowCopyMap(this, updateCursor);
		shadowCopyMap = new ShadowCopyMap(this, updateCursor.clone());
	}

	public void localCommit()
	{
		commitDescriptor = new CommitDescriptor(id);
		commitDescriptor.init(shadowCopyMap);
		updateCursor.disable();
		GlobalTransactionManager.getInstance().getSharedObjectLibrary().localApply(commitDescriptor, 0);
		state = TransactionState.Committed;
		
		clearFlag = true;
		GlobalTransactionManager.getInstance().unregisterTransactionHandler(id);
		lastOperation = TransactionOutcome.Committed;
		notifyCount = 1;
	}
}
