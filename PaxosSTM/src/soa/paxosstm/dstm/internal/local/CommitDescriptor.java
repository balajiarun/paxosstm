package soa.paxosstm.dstm.internal.local;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.utils.TCID;

public final class CommitDescriptor
{
	private TCID id;
	private long highestVersion;

	private int readSetSize;
	private int writeSetSize;
	private int newSetSize;
	private ShadowCopyMap.Entry readSet;
	private ShadowCopyMap.Entry writeSet;
	private ShadowCopyMap.Entry newSet;
	private ArrayList<String> typesSet;

	private int packageSize;
	private long validationTime;
	private long updateTime;
	private long abcastArrivalTime;

	public CommitDescriptor(TCID id)
	{
		this.id = id;
	}

	// private static class RWNStat
	// {
	// int r, w, n;
	// }

	public boolean init(ShadowCopyMap shadowCopyMap)
	{
		highestVersion = shadowCopyMap.getHighestVersion();
		readSetSize = 0;
		writeSetSize = 0;
		newSetSize = 0;
		readSet = null;
		writeSet = null;
		newSet = null;
		typesSet = null;

		// HashMap<Class<?>, RWNStat> stats = new HashMap<Class<?>, RWNStat>();

		for (ShadowCopyMap.Entry entry = shadowCopyMap.header, n; entry != null; entry = n)
		{
			n = entry.after;
			entry.purge();

			BaseTransactionObject value = entry.getValue();
			BaseTransactionObject newest = entry.handle.__tc_retrieveNewest();

			if (newest == null)
			{
				entry.after = newSet;
				newSet = entry;
				newSetSize++;
				// RWNStat s = stats.get(entry.handle.getClass());
				// if (s == null)
				// {
				// s = new RWNStat();
				// stats.put(entry.handle.getClass(), s);
				// }
				// s.n++;
				continue;
			}

			if (newest.__tc_getVersion() > value.__tc_getVersion())
				return false;

			if (value.__tc_isWritten())
			{
				entry.after = writeSet;
				writeSet = entry;
				writeSetSize++;
				// RWNStat s = stats.get(entry.handle.getClass());
				// if (s == null)
				// {
				// s = new RWNStat();
				// stats.put(entry.handle.getClass(), s);
				// }
				// s.w++;
			}
			else
			{
				entry.after = readSet;
				readSet = entry;
				readSetSize++;
				// RWNStat s = stats.get(entry.handle.getClass());
				// if (s == null)
				// {
				// s = new RWNStat();
				// stats.put(entry.handle.getClass(), s);
				// }
				// s.r++;
			}
		}

		// System.err
		// .println("======================================================================================");
		// for (Map.Entry<Class<?>, RWNStat> entry : stats.entrySet())
		// {
		// String name = entry.getKey().getName();
		// RWNStat stat = entry.getValue();
		// System.err.format("%-65s %6d %6d %6d\n", name, stat.r, stat.w,
		// stat.n);
		// }
		// System.err.println();
		// System.err.println();
		// System.err.println();
		// System.err.println();

		if (isLocal())
			return true;

		if (newSet != null)
		{
			Map<String, Integer> typesRegistry = GlobalTransactionManager.getInstance()
					.getSharedObjectLibrary().getTypesRegistry();
			int typesCount = 0;
			Map<String, Integer> typeToIntMap = new HashMap<String, Integer>();

			for (ShadowCopyMap.Entry entry = newSet; entry != null; entry = entry.after)
			{
				String type = entry.getValue().getClass().getName();
				Integer typeId = typeToIntMap.get(type);
				if (typeId == null)
				{
					typeId = typesRegistry.get(type);
					if (typeId == null)
					{
						typesCount++;
						typeId = -typesCount;
						typeToIntMap.put(type, typeId);
						if (typesSet == null)
						{
							typesSet = new ArrayList<String>();
						}
						typesSet.add(type);
					}
				}

				entry.setTypeId(typeId);
			}
		}

		return true;
	}

	public void sortWriteSet()
	{
		ShadowCopyMap.Entry[] writeSetArray = new ShadowCopyMap.Entry[writeSetSize];
		
		int counter = 0;
		for (ShadowCopyMap.Entry entry = writeSet; entry != null; entry = entry.getNext())
		{
			writeSetArray[counter++] = entry;
		}
		Arrays.sort(writeSetArray);
		
		writeSet = writeSetArray[0];
		for (int i = 0; i < writeSetSize - 1; i++)
			writeSetArray[i].after = writeSetArray[i + 1];
		writeSetArray[writeSetSize - 1].after = null;
	}
	
	public boolean validate()
	{
		for (ShadowCopyMap.Entry entry = readSet; entry != null; entry = entry.after)
		{
			BaseTransactionObject value = entry.getValue();
			BaseTransactionObject newest = entry.handle.__tc_retrieveNewest();
			if (newest.__tc_getVersion() > value.__tc_getVersion())
				return false;
		}
		for (ShadowCopyMap.Entry entry = writeSet; entry != null; entry = entry.after)
		{
			BaseTransactionObject value = entry.getValue();
			BaseTransactionObject newest = entry.handle.__tc_retrieveNewest();
			if (newest.__tc_getVersion() > value.__tc_getVersion())
				return false;
		}
		return true;
	}

	public TCID getId()
	{
		return id;
	}

	public long getHighestVersion()
	{
		return highestVersion;
	}

	public int getReadSetSize()
	{
		return readSetSize;
	}

	public int getWriteSetSize()
	{
		return writeSetSize;
	}

	public int getNewSetSize()
	{
		return newSetSize;
	}

	public int getTypesSetSize()
	{
		if (typesSet == null)
			return 0;
		return typesSet.size();
	}

	public ShadowCopyMap.Entry getReadSet()
	{
		return readSet;
	}

	public ShadowCopyMap.Entry getWriteSet()
	{
		return writeSet;
	}

	public ShadowCopyMap.Entry getNewSet()
	{
		return newSet;
	}

	public List<String> getTypesSet()
	{
		return typesSet;
	}

	public boolean isLocal()
	{
		return writeSetSize == 0 && newSetSize == 0;
	}

	public int[] getUpdateWriteSet()
	{
		int[] set = new int[writeSetSize];
		int counter = 0;
		for (ShadowCopyMap.Entry entry = writeSet; entry != null; entry = entry.after)
		{
			set[counter++] = entry.getId();
		}
		// Arrays.sort(set);
		return set;
	}

	public int getPackageSize()
	{
		return packageSize;
	}

	public void setPackageSize(int packageSize)
	{
		this.packageSize = packageSize;
	}

	public void setValidationTime(long validationTime)
	{
		this.validationTime = validationTime;
	}

	public long getValidationTime()
	{
		return validationTime;
	}

	public void setUpdateTime(long updateTime)
	{
		this.updateTime = updateTime;
	}

	public long getUpdateTime()
	{
		return updateTime;
	}

	public void setAbcastArrivalTime(long abcastArrivalTime)
	{
		this.abcastArrivalTime = abcastArrivalTime;
	}

	public long getAbcastArrivalTime()
	{
		return this.abcastArrivalTime;
	}
}
