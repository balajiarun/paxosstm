package soa.paxosstm.dstm.internal;

import java.util.Map;

import soa.paxosstm.dstm.internal.instrumentation.SharedObjectInputStream;
import soa.paxosstm.utils.Pair;
import soa.paxosstm.utils.TCID;

public class NewSetEntry implements Map.Entry<TCID, Pair<Integer, SharedObjectInputStream>>
{
	private TCID key;
	private int typeId;
	private SharedObjectInputStream data;

	public NewSetEntry(TCID key, int typeId, SharedObjectInputStream data)
	{
		this.key = key;
		this.typeId = typeId;
		this.data = data;
	}

	@Override
	public TCID getKey()
	{
		return key;
	}
	
	public int getTypeId()
	{
		return typeId;
	}
	
	public SharedObjectInputStream getData()
	{
		return data;
	}

	@Override
	public Pair<Integer, SharedObjectInputStream> getValue()
	{
		return new Pair<Integer, SharedObjectInputStream>(typeId, data);
	}

	@Override
	public Pair<Integer, SharedObjectInputStream> setValue(Pair<Integer, SharedObjectInputStream> value)
	{
		throw new UnsupportedOperationException();
	}

}
