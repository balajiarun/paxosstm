package soa.paxosstm.dstm.internal;

import java.io.IOException;
import java.util.Arrays;

import soa.paxosstm.dstm.internal.instrumentation.SharedObjectOutputStream;

public class PackageOutputStream extends SharedObjectOutputStream
{
	private int uncompletedByte;
	private int freeBits;

	public PackageOutputStream()
	{
		super();
		uncompletedByte = -1;
	}

	public PackageOutputStream(int capacity)
	{
		super(capacity);
		uncompletedByte = -1;
	}

	public final void writeBits(int v, int bits) throws IOException
	{
		if (uncompletedByte != count - 1)
		{
			freeBits = 0;
		}
		ensureCapacity(count + ((bits - freeBits + 7) / 8));

		v &= (-1 >>> (32 - bits));

		if (freeBits > 0)
		{
			if (freeBits >= bits)
			{
				freeBits -= bits;
				buf[uncompletedByte] |= (byte) ((v << freeBits) & 0xFF);
				return;
			}

			int u = v >>> (bits - freeBits);
			buf[uncompletedByte] |= (byte) (u & 0xFF);
			bits -= freeBits;
			freeBits = 0;
		}

		while (bits >= 8)
		{
			buf[count++] |= (byte) ((v >>> (bits - 8)) & 0xFF);
			bits -= 8;
		}

		if (bits > 0)
		{
			freeBits = 8 - bits;
			buf[uncompletedByte = count++] |= (byte) ((v << freeBits) & 0xFF);
		}
	}

	public final void writeLongBits(long v, int bits) throws IOException
	{
		if (uncompletedByte != count - 1)
		{
			freeBits = 0;
		}
		ensureCapacity(count + ((bits - freeBits + 7) / 8));

		v &= (-1L >>> (64 - bits));

		if (freeBits > 0)
		{
			if (freeBits >= bits)
			{
				freeBits -= bits;
				buf[uncompletedByte] |= (byte) ((v << freeBits) & 0xFF);
				return;
			}

			long u = v >>> (bits - freeBits);
			buf[uncompletedByte] |= (byte) (u & 0xFF);
			bits -= freeBits;
			freeBits = 0;
		}

		while (bits >= 8)
		{
			buf[count++] |= (byte) ((v >>> (bits - 8)) & 0xFF);
			bits -= 8;
		}

		if (bits > 0)
		{
			freeBits = 8 - bits;
			buf[uncompletedByte = count++] |= (byte) ((v << freeBits) & 0xFF);
		}
	}

	public final void fillZeroBits(int bits) throws IOException
	{
		if (uncompletedByte != count - 1)
		{
			freeBits = 0;
		}
		bits -= freeBits;
		int bytes = (bits + 7) / 8;
		ensureCapacity(count + bytes);
		count += bytes;
		uncompletedByte = count - 1;
		freeBits = bytes * 8 - bits;
	}

	public final void completeByte() throws IOException
	{
		freeBits = 0;
	}

	public final void writeIdSet(int tab[], int n) throws IOException
	{
		int max = tab[0];
		for (int i = 1; i < n; i++)
			if (tab[i] > max)
				max = tab[i];

		int bits = 32 - Integer.numberOfLeadingZeros(max);
		writeIdSet(tab, n, bits);
	}

	public final void writeIdSet(int tab[], int n, int bits) throws IOException
	{
		writeBits(bits - 1, 5);

		for (int i = 0; i < n; i++)
			writeBits(tab[i], bits);
	}

	public void writeIdSetWithExceptions(int[] tab, int n, int max) throws IOException
	{
		int rsize = max + 1;
		int[] ranks = new int[rsize];
		int[] order = new int[rsize];
		int[] reversedOrder = new int[rsize];

		for (int i = 0; i < rsize; i++)
			ranks[i] = i;
		for (int i = 0; i < n; i++)
			ranks[tab[i]] += rsize;
		Arrays.sort(ranks);
		for (int i = 0, j = rsize - 1; i < rsize / 2; i++, j--)
		{
			int tmp = ranks[i];
			ranks[i] = ranks[j];
			ranks[j] = tmp;
		}
		for (int i = 0; i < rsize; i++)
		{
			int x = ranks[i] % rsize;
			int y = ranks[i] / rsize;
			reversedOrder[i] = x;
			order[x] = i;
			ranks[i] = y;
		}

		int maxBits = 32 - Integer.numberOfLeadingZeros(max + 1);
		int[] f = new int[maxBits + 1];
		for (int i = 0; i < rsize; i++)
		{
			f[32 - Integer.numberOfLeadingZeros(i + 1)] += ranks[i];
		}
		int bits = computeBestBits2(f, n, maxBits);

		writeBits(rsize, 32);
		for (int i = 0; i < rsize; i++)
			writeBits(reversedOrder[i], maxBits);
		writeBits(bits - 1, 5);

		final int range = 1 << bits;

		for (int i = 0; i < n; i++)
		{
			int y = order[tab[i]] + 1;
			if (y >= range)
			{
				writeBits(0, bits);
				writeBits(order[tab[i]], maxBits);
			}
			else
			{
				writeBits(y, bits);
			}
		}
	}

	public final void writeIdSetSorted(int tab[], int n) throws IOException
	{
		for (int i = n - 1; i > 0; i--)
			tab[i] -= tab[i - 1];

		int max = tab[0];
		for (int i = 1; i < n; i++)
			if (tab[i] > max)
				max = tab[i];

		int bits = 32 - Integer.numberOfLeadingZeros(max);
		writeBits(bits - 1, 5);

		for (int i = 0; i < n; i++)
			writeBits(tab[i], bits);
	}

	public final void writeIdSetSortedWithExceptions(int tab[], int n) throws IOException
	{
		for (int i = n - 1; i > 0; i--)
			tab[i] -= tab[i - 1];

		int[] f = computeFrequencies(tab, n, 31);

		int maxBits = 32;
		while (maxBits > 0 && f[maxBits] == 0)
			maxBits--;

		int bits = computeBestBits(f, n, maxBits);

		if (bits <= 8)
		{
			computeFrequencies(tab, n, maxBits - bits - 1, f);
			bits = computeBestBits(f, n, maxBits);
		}

		final int diff = maxBits - bits - 1;
		final int mask = (1 << bits) - 1;

		writeBits(bits - 1, 5);
		writeBits(maxBits - 1, 5);
		for (int i = 0; i < n; i++)
		{
			int y = tab[i] + diff;
			if (y > mask)
			{
				int b = Math.max(32 - Integer.numberOfLeadingZeros(tab[i] - 1), bits + 1);
				writeBits(b - bits - 1, bits);
				writeBits(tab[i] - 1, b);
			}
			else
			{
				writeBits(y, bits);
			}
		}
	}

	public final void writeIdSetSortedWith3States(int tab[], int n) throws IOException
	{
		for (int i = n - 1; i > 0; i--)
			tab[i] -= tab[i - 1] + 1;
		tab[0]--;

		int[] f1 = computeFrequencies(tab, n, 1);

		int maxBits = 32;
		while (maxBits > 0 && f1[maxBits] == 0)
			maxBits--;

		int[] f2 = computeFrequencies(tab, n, maxBits);

		int bits1;
		int bits2;

		int bestTotalBits = Integer.MAX_VALUE;
		int bestBits = 0;

		int superValues = 0;
		for (bits1 = 1; bits1 < maxBits; bits1++)
		{
			superValues += f1[bits1];
			int oldBestBits = bestBits;

			int extraBits;
			for (bits2 = maxBits, extraBits = 0; bits2 >= bits1 && bits2 > 0; extraBits += f2[bits2]
					* bits2, bits2--)
			{
				if (1 << bits2 < maxBits - bits2)
					break;

				int totalBits = n * (bits1 + bits2) + extraBits - superValues * bits2;
				if (totalBits < bestTotalBits)
				{
					bestBits = bits1 * 256 + bits2;
					bestTotalBits = totalBits;
				}
			}

			if (oldBestBits != bestBits || oldBestBits / 256 == bits1 - 1)
			{
				int[] f = computeFrequencies(tab, n, maxBits - bits2 - (1 << bits1) + 1);
				for (bits2 = maxBits, extraBits = 0; bits2 >= bits1 && bits2 > 0; extraBits += f[bits2]
						* bits2, bits2--)
				{
					if (1 << bits2 < maxBits - bits2)
						break;

					int totalBits = n * (bits1 + bits2) + extraBits - superValues * bits2;
					if (totalBits < bestTotalBits)
					{
						bestBits = bits1 * 256 + bits2;
						bestTotalBits = totalBits;
					}
				}
			}
			else
			{
				break;
			}
		}

		if (n * maxBits <= bestTotalBits)
		{
			bits1 = bits2 = maxBits;
		}
		else
		{
			bits1 = bestBits / 256;
			bits2 = bestBits % 256;
		}

		final int range1 = (1 << bits1);
		final int range2 = (1 << bits2);
		final int bitDiff = maxBits - bits2;
		final int diff = range2 + range1 - bitDiff - 1;

		writeBits(bits1 - 1, 5);
		writeBits(bits2 - 1, 5);
		writeBits(maxBits - 1, 5);
		for (int i = 0; i < n; i++)
		{
			int y = tab[i] + 1;
			if (y >= range1)
			{
				y = tab[i] - (range1 - 1) + bitDiff;
				if (y >= range2)
				{
					int b = Math.max(32 - Integer.numberOfLeadingZeros(tab[i] - diff), bits2 + 1);
					writeBits(0, bits1);
					writeBits(b - bits2 - 1, bits2);
					writeBits(tab[i] - diff, b);
				}
				else
				{
					writeBits(0, bits1);
					writeBits(y, bits2);
				}
			}
			else
			{
				writeBits(y, bits1);
			}
		}
	}

	private void computeFrequencies(int tab[], int n, int diff, int[] f)
	{
		Arrays.fill(f, 0);

		for (int i = 0; i < n; i++)
			f[32 - Integer.numberOfLeadingZeros(tab[i] + diff)]++;
	}

	private int[] computeFrequencies(int tab[], int n, int diff)
	{
		int[] f = new int[33];

		for (int i = 0; i < n; i++)
			f[32 - Integer.numberOfLeadingZeros(tab[i] + diff)]++;
		return f;
	}

	private int computeBestBits(int f[], int n, int maxBits)
	{
		int bestTotalBits = Integer.MAX_VALUE;
		int bestBits = maxBits;
		for (int bits = maxBits, extraBits = 0; bits > 0; extraBits += f[bits] * bits, bits--)
		{
			if (1 << bits < maxBits - bits)
				break;

			int totalBits = n * bits + extraBits;
			if (totalBits < bestTotalBits)
			{
				bestBits = bits;
				bestTotalBits = totalBits;
			}
		}
		return bestBits;
	}

	private int computeBestBits2(int f[], int n, int maxBits)
	{
		int bestTotalBits = Integer.MAX_VALUE;
		int bestBits = maxBits;
		for (int bits = maxBits, extraBits = 0; bits > 0; extraBits += f[bits] * maxBits, bits--)
		{
			int totalBits = n * bits + extraBits;
			if (totalBits < bestTotalBits)
			{
				bestBits = bits;
				bestTotalBits = totalBits;
			}
		}
		return bestBits;
	}
}
