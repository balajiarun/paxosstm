/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.instrumentation;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtBehavior;
import javassist.CtClass;
import javassist.CtConstructor;
import javassist.CtField;
import javassist.CtMethod;
import javassist.CtNewMethod;
import javassist.Modifier;
import javassist.NotFoundException;
import soa.paxosstm.dstm.TransactionObject;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.global.SharedObjectLibrary;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager;
import soa.paxosstm.utils.TCID;

/**
 * This class exposes all the necessary methods which are used by the
 * {@link ObservableTranslator} to instrument classes marked with
 * {@link TransactionObject} and classes that reference them. This allows the
 * changes tracking mechanism (see {@link GlobalTransactionManager} to collect
 * all necessary information about modifications performed on objects of such
 * classes to qualify the objects for read- or write-sets of the current
 * transaction.
 * <p>
 * Instrumented classes can serve as so called <i>shared objects</i> or
 * <i>shadow copies</i>. Shared objects, identified by unique indentifier of the
 * {@link TCID} type are replicated on each site and stored inside the Shared
 * Object Library, SOL (see {@link SharedObjectLibrary}. <i>Shadow copies</i>
 * are used during the execution of the transactions, so no changes to the SOL
 * would be performed until it is known that transaction can commit.
 * <p>
 * All classes which objects can be processed within transactions have to
 * implement the Observable interface and possess four additional fields:
 * <ul>
 * <li><i>__tc_id</i> field of the {@link TCID} class - holds a unique
 * identification number used to identify each shared object,</li>
 * <li><i>_tc_parent</i> field of the same type as declaring class - in case of
 * shadow copy instances holds a reference to the original object; otherwise a
 * self reference is assigned to the field,</li>
 * <li><i>__tc_read</i> field - a flag indicating whether the object was read in
 * the current transaction,</li>
 * <li><i>__tc_written</i> field - a flag indicating whether the object was
 * written in the current transaction.</li>
 * </ul>
 * <p>
 * Each class loaded to the JVM by the Javassist loader (therefore also a class
 * that is non-transactional) is instrumented in order to automatically set read
 * and written flags on shadow copies used in current transaction once they are
 * accessed by a method of the class. Checking the read and written flags of the
 * shadow copies of the shared objects used in the current transaction enables
 * one to create minimal read and write sets that are used later for the
 * transaction certification.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class ClassAlterer
{
	public static final String NAMES_PREFIX = "__tc_";

	public static final String ID_FIELD_NAME = NAMES_PREFIX + "id";
	public static final String VERSION_NUMBER_FIELD_NAME = NAMES_PREFIX + "version";
	public static final String NEXT_FIELD_NAME = NAMES_PREFIX + "next";
	public static final String ATTR_FIELD_NAME = NAMES_PREFIX + "attr";

	public static final String NEW_INSTANCE_METHOD_NAME = NAMES_PREFIX + "newInstance";
	public static final String INTERNAL_COPY_METHOD_NAME = NAMES_PREFIX + "internalCopy";
	public static final String COPY_METHOD_NAME = NAMES_PREFIX + "copy";
	public static final String MAKE_COPY_METHOD_NAME = NAMES_PREFIX + "makeCopy";
	public static final String IS_VERSION_METHOD_NAME = NAMES_PREFIX + "isVersion";
	public static final String IS_SHADOWCOPY_METHOD_NAME = NAMES_PREFIX + "isShadowCopy";
	public static final String IS_HANDLE_METHOD_NAME = NAMES_PREFIX + "isHandle";
	public static final String IS_WRITTEN_METHOD_NAME = NAMES_PREFIX + "isWritten";
	public static final String SET_WRITTEN_METHOD_NAME = NAMES_PREFIX + "setWritten";
	public static final String GET_NEXT_METHOD_NAME = NAMES_PREFIX + "getNext";
	public static final String SET_NEXT_METHOD_NAME = NAMES_PREFIX + "setNext";
	public static final String GET_ID_METHOD_NAME = NAMES_PREFIX + "getId";
	public static final String GET_VERSION_METHOD_NAME = NAMES_PREFIX + "getVersion";

	public static final String SERIALIZE_METHOD_NAME = NAMES_PREFIX + "serialize";
	public static final String DESERIALIZE_METHOD_NAME = NAMES_PREFIX + "deserialize";

	public static final String NO_CHECK_SUFFIX = "_noCheck";
	public static final String WRITE_CHECK_SUFFIX = "_wCheck";
	public static final String SAFETY_CHECK_SUFFIX = "_sCheck";
	public static final String INTERNAL_SUFFIX = "_internal";

	private final ClassInfo classInfo;
	private final CtClass parameterClass;
	private final ClassPool pool;

	/**
	 * Constructor.
	 * 
	 * @param classInfo
	 *            ClassInfo object for the given class
	 * @param pool
	 *            ClassPool object
	 */
	public ClassAlterer(ClassInfo classInfo, ClassPool pool)
	{
		this.classInfo = classInfo;
		this.parameterClass = classInfo.parameterClass;
		this.pool = pool;
	}

	/**
	 * Starts the process of altering the class associated with this
	 * ClassAlterer object.
	 * 
	 * @throws CannotCompileException
	 * @throws NotFoundException
	 * @throws ClassNotFoundException
	 */
	public void alterThis() throws CannotCompileException, NotFoundException,
			ClassNotFoundException
	{
		if (classInfo.getHeadTransactionObject() == classInfo)
		{
			parameterClass.setSuperclass(pool.get(BaseTransactionObject.class.getName()));
		}
		else
		{
			ClassInfo parent = classInfo.getClasses().elementAt(1);
			if (!parent.isAltered())
			{
				ClassAlterer ca = new ClassAlterer(parent, pool);
				ca.alterThis();
			}
		}
		alter();
		classInfo.setAltered(true);
	}

	private void alter() throws CannotCompileException, NotFoundException
	{
		CtConstructor ctConstructor = new CtConstructor(new CtClass[] { pool
				.get(SpecialConstructorMarker.class.getName()) }, parameterClass);
		ctConstructor.setBody("{ super($1); }");
		ctConstructor.setModifiers(Modifier.setProtected(ctConstructor.getModifiers()));
		parameterClass.addConstructor(ctConstructor);

		addNewInstanceMethod();
		addInternalCopyMethod();
		addCopyMethod();
		addMakeCopyMethod();
		addSerializeMethod();
		addDeserializeMethod();
		createDispatcherAndCheckMethods();
	}

	public void addImplementation() throws CannotCompileException, NotFoundException,
			ClassNotFoundException
	{
		implementNewInstanceMethod();
		implementInternalCopyMethod();
		implementCopyMethod();
		implementMakeCopyMethod();
		implementSerializeMethod();
		implementDeserializeMethod();
	}

	private void addNewInstanceMethod() throws CannotCompileException
	{
		// String returnType = classInfo.getParameterClass().getName();
		String returnType = BaseTransactionObject.class.getName();

		String body = "return null; ";
		String sig = "public " + returnType + " " + NEW_INSTANCE_METHOD_NAME + "()";
		String src = sig + " { " + body + "}";

		CtMethod newInstanceMethod = CtNewMethod.make(src, parameterClass);
		parameterClass.addMethod(newInstanceMethod);
	}

	private void addInternalCopyMethod() throws CannotCompileException
	{
		String param = parameterClass.getName() + " target";
		String src = "public final void " + INTERNAL_COPY_METHOD_NAME + "(" + param + ") { }";

		CtMethod internalCopyMethod = CtNewMethod.make(src, parameterClass);
		parameterClass.addMethod(internalCopyMethod);
	}

	private void addCopyMethod() throws CannotCompileException
	{
		String src = "public void " + COPY_METHOD_NAME + "(Object target) { }";

		CtMethod copyMethod = CtNewMethod.make(src, parameterClass);
		parameterClass.addMethod(copyMethod);
	}

	private void addMakeCopyMethod() throws CannotCompileException
	{
		// String returnType = classInfo.getParameterClass().getName();
		String returnType = BaseTransactionObject.class.getName();

		String body = "return null; ";
		String sig = "protected " + returnType + " " + MAKE_COPY_METHOD_NAME + "()";
		String src = sig + " { " + body + "}";

		CtMethod makeCopyMethod = CtNewMethod.make(src, parameterClass);
		parameterClass.addMethod(makeCopyMethod);
	}

	private void addSerializeMethod() throws CannotCompileException
	{
		String param = SharedObjectOutputStream.class.getName() + " shoos";
		String src = "public void " + SERIALIZE_METHOD_NAME + "(" + param + ") { }";

		CtMethod serializeHelperMethod = CtNewMethod.make(src, parameterClass);
		parameterClass.addMethod(serializeHelperMethod);
	}

	private void addDeserializeMethod() throws CannotCompileException
	{
		String param = SharedObjectInputStream.class.getName() + " shois";
		String src = "public void " + DESERIALIZE_METHOD_NAME + "(" + param + ") { }";

		CtMethod deserializeHelperMethod = CtNewMethod.make(src, parameterClass);
		parameterClass.addMethod(deserializeHelperMethod);
	}

	private void implementNewInstanceMethod() throws CannotCompileException, NotFoundException
	{
		String newInstanceBody = "return " + getNewInstanceStmt() + "; ";

		CtMethod newInstanceMethod = parameterClass.getDeclaredMethod(NEW_INSTANCE_METHOD_NAME);
		newInstanceMethod.setBody("{ " + newInstanceBody + "}");
	}

	/**
	 * <code>
	 * // target = new object to be filled with data
	 * protected void __tc_copy(ParameterClass target)
	 * {
	 * 		// if this is not head class
	 * 		super.__tc_copy(target);
	 * 		
	 *      // for all declared fields (not static and which do not start with __tc_ 
	 *      target.field = this.field;
	 * }
	 * </code>
	 */
	private void implementInternalCopyMethod() throws CannotCompileException, NotFoundException
	{
		boolean head = (classInfo.getHeadTransactionObject() == classInfo);
		String superCall = head ? "" : "super." + INTERNAL_COPY_METHOD_NAME + "($1); ";

		StringBuilder builder = new StringBuilder();
		CtField[] fields = parameterClass.getDeclaredFields();
		for (CtField ctField : fields)
		{
			if (ctField.getName().startsWith(NAMES_PREFIX))
				continue;
			if (Modifier.isStatic(ctField.getModifiers()))
				continue;

			String fieldName = ctField.getName();
			builder.append("$1." + fieldName + " = this." + fieldName + "; ");
		}
		String body = superCall + builder.toString();

		CtMethod copyMethod = parameterClass.getDeclaredMethod(INTERNAL_COPY_METHOD_NAME);
		copyMethod.setBody("{ " + body + "}");
	}

	/**
	 * <code>
	 * // target = new object to be filled with data
	 * public void __tc_vcopy(Object target)
	 * {
	 * 		this.__tc_copy((ParameterClass) target);
	 * }
	 * </code>
	 */
	private void implementCopyMethod() throws CannotCompileException, NotFoundException
	{
		String param = "(" + parameterClass.getName() + ") $1";
		String body = "this." + INTERNAL_COPY_METHOD_NAME + "(" + param + "); ";

		CtMethod vcopyMethod = parameterClass.getDeclaredMethod(COPY_METHOD_NAME);
		vcopyMethod.setBody("{ " + body + "}");
	}

	private void implementMakeCopyMethod() throws CannotCompileException, NotFoundException
	{
		String copyDecl = parameterClass.getName() + " copy = " + getNewInstanceStmt() + "; ";

		String copyCall = "this." + INTERNAL_COPY_METHOD_NAME + "(copy); ";

		String setIdStmt = "copy." + ID_FIELD_NAME + " = this." + ID_FIELD_NAME + "; ";
		String setVersionStmt = "copy." + VERSION_NUMBER_FIELD_NAME + " = this."
				+ VERSION_NUMBER_FIELD_NAME + "; ";
		String setNextStmt = "copy." + NEXT_FIELD_NAME + " = this." + NEXT_FIELD_NAME + "; ";
		String setAttrStmt = "copy." + ATTR_FIELD_NAME + " = "
				+ BaseTransactionObject.IS_SHADOW_COPY_MASK + "; ";
		String setFieldsStmt = setIdStmt + setVersionStmt + setNextStmt + setAttrStmt;

		String returnStmt = "return copy; ";

		String body = copyDecl + copyCall + setFieldsStmt + returnStmt;

		CtMethod makeCopyMethod = parameterClass.getDeclaredMethod(MAKE_COPY_METHOD_NAME);
		makeCopyMethod.setBody("{ " + body + "}");
	}

	private void implementSerializeMethod() throws CannotCompileException, NotFoundException,
			ClassNotFoundException
	{
		boolean head = (classInfo.getHeadTransactionObject() == classInfo);
		String superCall = head ? "" : "super." + SERIALIZE_METHOD_NAME + "($1); ";

		StringBuilder builder = new StringBuilder();
		CtField[] fields = parameterClass.getDeclaredFields();
		for (CtField ctField : fields)
		{
			if (ctField.getName().startsWith(NAMES_PREFIX))
				continue;
			if (Modifier.isStatic(ctField.getModifiers()))
				continue;

			builder.append("{ " + getFieldSerializationStmt(ctField) + "} ");
		}
		String body = superCall + builder.toString();

		CtMethod serializeMethod = parameterClass
				.getDeclaredMethod(SERIALIZE_METHOD_NAME);
		serializeMethod.setBody("{ " + body + "}");
	}

	private String getFieldSerializationStmt(CtField field) throws NotFoundException,
			CannotCompileException, ClassNotFoundException
	{
		String fieldName = "this." + field.getName();
		CtClass type = field.getType();
		ClassInfo ci = ClassInfo.get(type);
		if (ci.isTransactionObject())
		{
			if (!ci.isAltered)
				ClassAlterer.alterClass(ci, pool);
		}

		if (type.isPrimitive())
		{
			if (type == CtClass.booleanType)
				return "$1.writeBoolean(" + fieldName + "); ";
			else if (type == CtClass.byteType)
				return "$1.writeByte(" + fieldName + "); ";
			else if (type == CtClass.charType)
				return "$1.writeChar(" + fieldName + "); ";
			else if (type == CtClass.doubleType)
				return "$1.writeDouble(" + fieldName + "); ";
			else if (type == CtClass.floatType)
				return "$1.writeFloat(" + fieldName + "); ";
			else if (type == CtClass.intType)
				return "$1.writeInt(" + fieldName + "); ";
			else if (type == CtClass.longType)
				return "$1.writeLong(" + fieldName + "); ";
			else if (type == CtClass.shortType)
				return "$1.writeShort(" + fieldName + "); ";
			else
				return null;
		}
		else if (type == pool.get(Boolean.class.getName()))
			return "$1.writeBoolean(" + fieldName + "); ";
		else if (type == pool.get(Byte.class.getName()))
			return "$1.writeByte(" + fieldName + "); ";
		else if (type == pool.get(Character.class.getName()))
			return "$1.writeChar(" + fieldName + "); ";
		else if (type == pool.get(Double.class.getName()))
			return "$1.writeDouble(" + fieldName + "); ";
		else if (type == pool.get(Float.class.getName()))
			return "$1.writeFloat(" + fieldName + "); ";
		else if (type == pool.get(Integer.class.getName()))
			return "$1.writeInt(" + fieldName + "); ";
		else if (type == pool.get(Long.class.getName()))
			return "$1.writeLong(" + fieldName + "); ";
		else if (type == pool.get(Short.class.getName()))
			return "$1.writeShort(" + fieldName + "); ";
		else if (type == pool.get(String.class.getName()))
			return "$1.writeString(" + fieldName + "); ";
		else if (type == pool.get(TCID.class.getName()))
			return "$1.writeTCID(" + fieldName + "); ";
		else if (type.subclassOf(pool.get(BaseTransactionObject.class.getName())))
			return "$1.writeSharedObjectReference(" + fieldName + "); ";
		else
			return "$1.writeObject(" + fieldName + "); ";
	}

	private void implementDeserializeMethod() throws CannotCompileException,
			NotFoundException, ClassNotFoundException
	{
		boolean head = (classInfo.getHeadTransactionObject() == classInfo);
		String superCall = head ? "" : "super." + DESERIALIZE_METHOD_NAME + "($1); ";

		StringBuilder builder = new StringBuilder();
		CtField[] fields = parameterClass.getDeclaredFields();
		for (CtField ctField : fields)
		{
			if (ctField.getName().startsWith(NAMES_PREFIX))
				continue;
			if (Modifier.isStatic(ctField.getModifiers()))
				continue;

			builder.append("{ " + getFieldDeserializationStmt(ctField) + "} ");
		}
		String body = superCall + builder.toString();

		CtMethod deserializeMethod = parameterClass
				.getDeclaredMethod(DESERIALIZE_METHOD_NAME);
		deserializeMethod.setBody("{ " + body + "}");
	}

	private String getFieldDeserializationStmt(CtField field) throws NotFoundException,
			CannotCompileException, ClassNotFoundException
	{
		String fieldName = "this." + field.getName();
		CtClass type = field.getType();
		ClassInfo ci = ClassInfo.get(type);
		if (ci.isTransactionObject())
		{
			if (!ci.isAltered)
				ClassAlterer.alterClass(ci, pool);
		}

		if (type.isPrimitive())
		{
			if (type == CtClass.booleanType)
				return fieldName + " = $1.readBoolean(); ";
			else if (type == CtClass.byteType)
				return fieldName + " = $1.readByte(); ";
			else if (type == CtClass.charType)
				return fieldName + " = $1.readChar(); ";
			else if (type == CtClass.doubleType)
				return fieldName + " = $1.readDouble(); ";
			else if (type == CtClass.floatType)
				return fieldName + " = $1.readFloat(); ";
			else if (type == CtClass.intType)
				return fieldName + " = $1.readInt(); ";
			else if (type == CtClass.longType)
				return fieldName + " = $1.readLong(); ";
			else if (type == CtClass.shortType)
				return fieldName + " = $1.readShort(); ";
			else
				return null;
		}
		else if (type == pool.get(Boolean.class.getName()))
			return fieldName + " = $1.readBooleanBox(); ";
		else if (type == pool.get(Byte.class.getName()))
			return fieldName + " = $1.readByteBox(); ";
		else if (type == pool.get(Character.class.getName()))
			return fieldName + " = $1.readCharBox(); ";
		else if (type == pool.get(Double.class.getName()))
			return fieldName + " = $1.readDoubleBox(); ";
		else if (type == pool.get(Float.class.getName()))
			return fieldName + " = $1.readFloatBox(); ";
		else if (type == pool.get(Integer.class.getName()))
			return fieldName + " = $1.readIntBox(); ";
		else if (type == pool.get(Long.class.getName()))
			return fieldName + " = $1.readLongBox(); ";
		else if (type == pool.get(Short.class.getName()))
			return fieldName + " = $1.readShortBox(); ";
		else if (type == pool.get(String.class.getName()))
			return fieldName + " = $1.readString(); ";
		else if (type == pool.get(TCID.class.getName()))
			return fieldName + " = $1.readTCID(); ";
		else if (type.subclassOf(pool.get(BaseTransactionObject.class.getName())))
			return fieldName + " = (" + type.getName() + ") $1.readSharedObjectReference(); ";
		else
			return fieldName + " = (" + type.getName() + ") $1.readObject(); ";
	}

	private void createDispatcherAndCheckMethods() throws CannotCompileException, NotFoundException
	{
		CtMethod[] methods = parameterClass.getDeclaredMethods();
		for (CtMethod ctMethod : methods)
		{
			if (ctMethod.getName().startsWith(NAMES_PREFIX))
				continue;
			if (Modifier.isStatic(ctMethod.getModifiers()))
				continue;

			String name = getNewMethodName(ctMethod);
			boolean returnsValue = ctMethod.getReturnType() != CtClass.voidType;

			CtMethod noCheckMethod = CtNewMethod.copy(ctMethod, name + NO_CHECK_SUFFIX,
					parameterClass, null);
			noCheckMethod.setModifiers(noCheckMethod.getModifiers() | Modifier.FINAL);
			parameterClass.addMethod(noCheckMethod);

			CtMethod wCheckMethod = CtNewMethod.copy(ctMethod, name + WRITE_CHECK_SUFFIX,
					parameterClass, null);
			wCheckMethod.setModifiers(wCheckMethod.getModifiers() | Modifier.FINAL);
			parameterClass.addMethod(wCheckMethod);

			CtMethod sCheckMethod = CtNewMethod.copy(ctMethod, name + SAFETY_CHECK_SUFFIX,
					parameterClass, null);
			sCheckMethod.setModifiers(sCheckMethod.getModifiers() | Modifier.FINAL);
			parameterClass.addMethod(sCheckMethod);

			CtMethod dispatcherMethod = CtNewMethod.copy(ctMethod, name + INTERNAL_SUFFIX,
					parameterClass, null);
			dispatcherMethod.setModifiers(dispatcherMethod.getModifiers() | Modifier.FINAL);
			dispatcherMethod.setBody(getInternalDispatcherBody(name, returnsValue));
			parameterClass.addMethod(dispatcherMethod);

			ctMethod.setBody(getDispatcherBody(parameterClass.getName(), name, returnsValue));
		}
	}

	private static String getInternalDispatcherBody(String name, boolean ret)
	{
		String mCall1 = "$0." + name + NO_CHECK_SUFFIX + "($$)";
		String mCall2 = "$0." + name + WRITE_CHECK_SUFFIX + "($$)";

		String noCheckStmt = (ret ? "return " : "") + mCall1 + "; ";
		String wCheckStmt = (ret ? "return " : "") + mCall2 + "; ";

		String ifWriteStmt = "if (($0." + ATTR_FIELD_NAME + " & "
				+ BaseTransactionObject.IS_WRITTEN_MASK + ") != 0) ";

		return "{ " + ifWriteStmt + noCheckStmt + "else " + wCheckStmt + "}";
	}

	private static String getDispatcherBody(String className, String name, boolean ret)
	{
		String targetName = "target";

		String stmt1 = getTargetCallStmt(targetName, className);
		String stmt2 = getDispatchStmt(name, ret, targetName);

		return "{ " + stmt1 + stmt2 + "}";
	}

	private static String getClassPrefix(CtClass clazz)
	{
		return NAMES_PREFIX + clazz.getName().replace('.', '_').replace('$', '_') + "_";
	}

	protected static String getNewMethodName(CtMethod ctMethod)
	{
		return getClassPrefix(ctMethod.getDeclaringClass()) + ctMethod.getName();
	}

	protected static String getTargetReadStmt(String targetName, String className)
	{
		String targetDecl = className + " " + targetName + "; ";

		int targetMask = BaseTransactionObject.IS_SHADOW_COPY_MASK
				| BaseTransactionObject.IS_OBJECT_VERSION_MASK;
		String ifStmt = "if (($0." + ATTR_FIELD_NAME + " & " + targetMask + ") != 0) ";
		String stmt1 = ifStmt + targetName + " = $0; ";

		String stmt2 = "else " + getTargetObjectStmt(targetName, className, "Read");

		return targetDecl + stmt1 + stmt2;
	}

	protected static String getTargetWriteStmt(String targetName, String className)
	{
		String targetDecl = className + " " + targetName + "; ";

		String ifStmt = "if (($0." + ATTR_FIELD_NAME + " & "
				+ BaseTransactionObject.IS_SHADOW_COPY_MASK + ") != 0) ";
		String stmt1 = ifStmt + targetName + " = $0; ";

		String stmt2 = "else " + getTargetObjectStmt(targetName, className, "Write");

		return targetDecl + stmt1 + stmt2;
	}

	protected static String getTargetCallStmt(String targetName, String className)
	{
		String targetDecl = className + " " + targetName + "; ";

		int targetMask = BaseTransactionObject.IS_SHADOW_COPY_MASK
				| BaseTransactionObject.IS_OBJECT_VERSION_MASK;
		String ifStmt = "if (($0." + ATTR_FIELD_NAME + " & " + targetMask + ") != 0) ";
		String stmt1 = ifStmt + targetName + " = $0; ";

		String stmt2 = "else " + getTargetObjectStmt(targetName, className, "Call");

		return targetDecl + stmt1 + stmt2;
	}

	private static String getTargetObjectStmt(String targetName, String className, String purpose)
	{
		String getMethodName = LocalTransactionManager.class.getName() + ".getTargetObjectFor"
				+ purpose;
		String getCall = "(" + className + ")" + getMethodName + "($0)";
		String getStmt = targetName + " = " + getCall + "; ";

		return "{ " + getStmt + "}";
	}

	private String getNewInstanceStmt()
	{
		String className = parameterClass.getName();
		String marker = SpecialConstructorMarker.class.getName();
		return "new " + className + "(((" + marker + ") null))";
	}

	private static String getDispatchStmt(String name, boolean ret, String object)
	{
		String mCall1 = object + "." + name + NO_CHECK_SUFFIX + "($$)";
		String mCall2 = object + "." + name + WRITE_CHECK_SUFFIX + "($$)";
		String mCall3 = object + "." + name + SAFETY_CHECK_SUFFIX + "($$)";

		String noCheckStmt = (ret ? "return " : "") + mCall1 + "; ";
		String wCheckStmt = (ret ? "return " : "") + mCall2 + "; ";
		String sCheckStmt = (ret ? "return " : "") + mCall3 + "; ";

		int notWrittenMask = BaseTransactionObject.IS_SHADOW_COPY_MASK;
		int writtenMask = BaseTransactionObject.IS_SHADOW_COPY_MASK
				| BaseTransactionObject.IS_WRITTEN_MASK;

		String ifNotWrittenStmt = "if ((" + object + "." + ATTR_FIELD_NAME + " & " + writtenMask
				+ ") == " + notWrittenMask + ") ";
		String ifWrittenStmt = "if ((" + object + "." + ATTR_FIELD_NAME + " & " + writtenMask
				+ ") == " + writtenMask + ") ";

		return "{ " + ifNotWrittenStmt + wCheckStmt + "else " + ifWrittenStmt + noCheckStmt
				+ "else " + sCheckStmt + "}";
	}

	public static void alterClass(ClassInfo classInfo, ClassPool pool)
			throws CannotCompileException, NotFoundException, ClassNotFoundException
	{
		ClassAlterer ca = new ClassAlterer(classInfo, pool);
		ca.alterThis();
	}

	public static void instrumentClass(CtClass parameterClass, final ClassPool pool)
			throws CannotCompileException, NotFoundException, ClassNotFoundException
	{
		ClassInfo ci = ClassInfo.get(parameterClass);
		if (ci.isTransactionObject())
		{
			CtBehavior[] behaviors = parameterClass.getDeclaredBehaviors();
			for (CtBehavior ctBehavior : behaviors)
			{
				if (Modifier.isStatic(ctBehavior.getModifiers()))
					ctBehavior.instrument(new ObservableExprEditor(pool));
				else if (ctBehavior instanceof CtConstructor)
					ctBehavior.instrument(new ObservableExprEditor(pool));
				else
				{
					String name = ctBehavior.getName();
					if (name.startsWith(NAMES_PREFIX))
					{
						if (name.endsWith(NO_CHECK_SUFFIX))
						{
							ctBehavior.instrument(new TransactionObjectExprEditor(pool, false,
									false));
						}
						else if (name.endsWith(WRITE_CHECK_SUFFIX))
						{
							ctBehavior
									.instrument(new TransactionObjectExprEditor(pool, true, false));
						}
						else if (name.endsWith(SAFETY_CHECK_SUFFIX))
						{
							ctBehavior
									.instrument(new TransactionObjectExprEditor(pool, false, true));
						}
					}
				}
			}
		}
		else
		{
			CtBehavior[] behaviors = parameterClass.getDeclaredBehaviors();
			for (CtBehavior ctBehavior : behaviors)
			{
				ctBehavior.instrument(new ObservableExprEditor(pool));
			}
		}
	}
}
