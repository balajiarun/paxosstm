package soa.paxosstm.dstm.internal.instrumentation;

public interface SharedObjectSerializationConstants
{
	final static int NULL_TAG = 0x55;
	final static int REPEAT_TAG = 0x56;
	final static int DEFLATED_TAG = 0x57;
	final static int ID_TAG = 0x58;
	final static int ID3_TAG = 0x59;
	final static int ID2_TAG = 0x5A;
	final static int STRING_TAG = 0x5B;
	final static int BYTE_TAG = 0x5C;
	final static int SHORT_TAG = 0x5D;
	final static int INTEGER_TAG = 0x5E;
	final static int LONG_TAG = 0x5F;
	final static int FLOAT_TAG = 0x60;
	final static int DOUBLE_TAG = 0x61;
	final static int BOOLEAN_FALSE_TAG = 0x62;
	final static int BOOLEAN_TRUE_TAG = 0x63;
	final static int OTHER_TAG = 0x64;
	final static int ID_NULL_TAG = 0x80;
}
