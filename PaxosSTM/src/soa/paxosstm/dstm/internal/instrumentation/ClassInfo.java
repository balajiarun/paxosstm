/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.instrumentation;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javassist.CtClass;
import javassist.CtConstructor;
import javassist.NotFoundException;
import soa.paxosstm.dstm.TransactionObject;

/**
 * Class which objects are used to store the information useful during the
 * process of altering the classes, e.g. super-classes, top most superclass
 * marked with {@link TransactionObject} annotation, flag indicating whether the
 * class has been already altered or not, etc. This class has a static member -
 * ClassInfo cache, where all the ClassInfo objects are stored.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class ClassInfo
{
	protected CtClass parameterClass;
	protected boolean isAltered;

	protected Vector<ClassInfo> classes;

	protected boolean isTransactionObject;
	protected boolean hasSpecialConstructor;
	protected ClassInfo headTransactionObject;

	protected static Map<CtClass, ClassInfo> classInfoCache = new HashMap<CtClass, ClassInfo>();

	/**
	 * Returns from the ClassInfo cache the ClassInfo object for the given
	 * class.
	 * 
	 * @param parameterClass
	 *            class for which the appropriate ClassInfo object has to be
	 *            retrieved
	 * @return the ClassInfo object for the given class
	 * @throws NotFoundException
	 *             on Javassist error
	 * @throws ClassNotFoundException
	 *             if a class defined by the name passed by the argument is not
	 *             loaded to the JVM
	 */
	public static ClassInfo get(CtClass parameterClass) throws NotFoundException,
			ClassNotFoundException
	{
		if (parameterClass == null)
			return null;

		ClassInfo ret = classInfoCache.get(parameterClass);
		if (ret == null)
		{
			ret = new ClassInfo(parameterClass);
			classInfoCache.put(parameterClass, ret);
		}
		return ret;
	}

	protected ClassInfo(CtClass parameterClass) throws ClassNotFoundException, NotFoundException
	{
		this.parameterClass = parameterClass;

		initClasses();
		setTransactionObjectInfo();
		if (isTransactionObject)
			checkConstructors();
	}

	protected static boolean isSpecialConstructor(CtConstructor ct)
	{
		if (ct.isClassInitializer())
			return false;

		try
		{
			if (ct.isEmpty())
			{
				CtClass[] parameters = ct.getParameterTypes();
				if (parameters.length != 1)
					return false;
				if (parameters[0].getName().equals(SpecialConstructorMarker.class.getName()))
					return true;
			}
		}
		catch (NotFoundException e)
		{
		}
		return false;
	}

	private void checkConstructors()
	{
		for (CtConstructor ct : parameterClass.getDeclaredConstructors())
		{
			if (isSpecialConstructor(ct))
			{
				hasSpecialConstructor = true;
				break;
			}
		}
	}

	protected void setTransactionObjectInfo() throws ClassNotFoundException
	{
		if (classes.size() == 1)
		{
			if (parameterClass.getAnnotation(TransactionObject.class) != null)
			{
				isTransactionObject = true;
				headTransactionObject = this;
			}
			else
			{
				isTransactionObject = false;
				headTransactionObject = null;
			}
		}
		else
		{
			ClassInfo parent = classes.elementAt(1);
			if (parent.isTransactionObject)
			{
				isTransactionObject = true;
				headTransactionObject = parent.headTransactionObject;
			}
			else
			{
				if (parameterClass.getAnnotation(TransactionObject.class) != null)
				{
					// isTransactionObject = true;
					// headTransactionObject = this;
					System.err.println("Illegal definition of class " + parameterClass.getName());
					System.err
							.println("TransactionObject classes can only derive directly from Object.");
					System.exit(1);
				}
				else
				{
					isTransactionObject = false;
					headTransactionObject = null;
				}
			}
		}
	}

	private void initClasses() throws NotFoundException, ClassNotFoundException
	{
		classes = new Vector<ClassInfo>();
		classes.add(this);
		CtClass tmpClass = parameterClass.getSuperclass();
		while (tmpClass != null && !tmpClass.getName().equals("java.lang.Object"))
		{
			classes.add(get(tmpClass));
			tmpClass = tmpClass.getSuperclass();
		}
	}

	/**
	 * Returns the reference to the CtClass object for the class represented by
	 * this ClassInfo object.
	 * 
	 * @return the reference to the CtClass object for the class represented by
	 *         this ClassInfo object
	 */
	public CtClass getParameterClass()
	{
		return parameterClass;
	}

	/**
	 * Returns the {@link Vector} containing ClassInfo objects for all
	 * super-classes of the class represented by this ClassInfo object.
	 * 
	 * @return the {@link Vector} containing ClassInfo objects for all
	 *         super-classes
	 */
	public Vector<ClassInfo> getClasses()
	{
		return classes;
	}

	/**
	 * Returns true if the class represented by this ClassInfo object is marked
	 * with {@link TransactionObject} annotation; false otherwise.
	 * 
	 * @return true if the class represented by this ClassInfo object is marked
	 *         with {@link TransactionObject} annotation; false otherwise
	 */
	public boolean isTransactionObject()
	{
		return isTransactionObject;
	}

	/**
	 * Returns the most upper superclass marked with {@link TransactionObject}
	 * annotation of the class represented by this ClassInfo object.
	 * 
	 * @return the most upper superclass marked with {@link TransactionObject}
	 *         annotation of the class represented by this ClassInfo object
	 */
	public ClassInfo getHeadTransactionObject()
	{
		return headTransactionObject;
	}

	/**
	 * Sets the flag indicating whether the class represented by this ClassInfo
	 * object has been already altered.
	 * 
	 * @param isAltered
	 *            new value of the flag indicating whether the class represented
	 *            by this ClassInfo object has been already altered
	 */
	public void setAltered(boolean isAltered)
	{
		this.isAltered = isAltered;
	}

	/**
	 * Returns true if the class represented by this ClassInfo object has been
	 * already altered; false otherwise.
	 * 
	 * @return true if the class represented by this ClassInfo object has been
	 *         already altered; false otherwise
	 */
	public boolean isAltered()
	{
		return isAltered;
	}

	public boolean hasSpecialConstructor()
	{
		return hasSpecialConstructor;
	}
}
