package soa.paxosstm.dstm.internal.instrumentation;

import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.UTFDataFormatException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.zip.Deflater;

import soa.paxosstm.utils.TCID;

public class SharedObjectOutputStream extends OutputStream implements ObjectOutput,
		SharedObjectSerializationConstants
{
	private static final int DEFLATE_THRESHOLD = 16384;
	private static final int DEFLATER_BUFFER_SIZE = 512;

	protected byte buf[];
	protected int count;

	public SharedObjectOutputStream()
	{
		this(32);
	}

	public SharedObjectOutputStream(int capacity)
	{
		buf = new byte[capacity];
		count = 0;
	}

	protected final void ensureCapacity(int minCapacity)
	{
		if (minCapacity > buf.length)
			grow(minCapacity);
	}

	protected final void grow(int minCapacity)
	{
		int oldCapacity = buf.length;
		int newCapacity = oldCapacity << 1;
		if (newCapacity < minCapacity)
			newCapacity = minCapacity;
		if (newCapacity < 0)
		{
			if (minCapacity < 0) // overflow
				throw new OutOfMemoryError();
			newCapacity = Integer.MAX_VALUE;
		}
		buf = Arrays.copyOf(buf, newCapacity);
	}

	@Override
	public final void write(int b) throws IOException
	{
		ensureCapacity(count + 1);
		buf[count] = (byte) b;
		count += 1;
	}

	@Override
	public final void write(byte[] b) throws IOException
	{
		ensureCapacity(count + b.length);
		System.arraycopy(b, 0, buf, count, b.length);
		count += b.length;
	}

	@Override
	public final void write(byte[] b, int off, int len) throws IOException
	{
		if ((off < 0) || (off > b.length) || (len < 0) || ((off + len) > b.length))
		{
			throw new IndexOutOfBoundsException();
		}
		ensureCapacity(count + len);
		System.arraycopy(b, off, buf, count, len);
		count += len;
	}

	@Override
	public final void writeByte(int v) throws IOException
	{
		write(v);
	}

	public final void writeByte(Byte v) throws IOException
	{
		if (v == null)
		{
			write(NULL_TAG);
			return;
		}

		byte firstByte = v;

		if (firstByte == NULL_TAG || firstByte == REPEAT_TAG)
		{
			ensureCapacity(count + 2);
			buf[count] = REPEAT_TAG;
			buf[count + 1] = firstByte;
			count += 2;
		}
		else
		{
			ensureCapacity(count + 1);
			buf[count] = firstByte;
			count += 1;
		}
	}

	@Override
	public final void writeShort(int v) throws IOException
	{
		ensureCapacity(count + 2);
		buf[count] = (byte) ((v >>> 8) & 0xFF);
		buf[count + 1] = (byte) (v & 0xFF);
		count += 2;
	}

	public final void writeShort(Short v) throws IOException
	{
		if (v == null)
		{
			write(NULL_TAG);
			return;
		}

		int val = v.intValue();
		int firstByte = (val >>> 8) & 0xFF;
		int secondByte = val & 0xFF;

		if (firstByte == NULL_TAG || firstByte == REPEAT_TAG)
		{
			ensureCapacity(count + 3);
			buf[count] = REPEAT_TAG;
			buf[count + 1] = (byte) firstByte;
			buf[count + 2] = (byte) secondByte;
			count += 3;
		}
		else
		{
			ensureCapacity(count + 2);
			buf[count] = (byte) firstByte;
			buf[count + 1] = (byte) secondByte;
			count += 2;
		}
	}

	public final void write3byter(int v) throws IOException
	{
		ensureCapacity(count + 3);
		buf[count] = (byte) ((v >>> 16) & 0xFF);
		buf[count + 1] = (byte) ((v >>> 8) & 0xFF);
		buf[count + 2] = (byte) (v & 0xFF);
		count += 3;
	}

	@Override
	public final void writeInt(int v) throws IOException
	{
		ensureCapacity(count + 4);
		buf[count] = (byte) (v >>> 24);
		buf[count + 1] = (byte) ((v >>> 16) & 0xFF);
		buf[count + 2] = (byte) ((v >>> 8) & 0xFF);
		buf[count + 3] = (byte) (v & 0xFF);
		count += 4;
	}

	public final void writeInt(Integer v) throws IOException
	{
		if (v == null)
		{
			write(NULL_TAG);
			return;
		}

		int val = v;
		int firstByte = val >>> 24;
		int secondByte = (val >>> 16) & 0xFF;
		int thirdByte = (val >>> 8) & 0xFF;
		int fourthByte = val & 0xFF;

		if (firstByte == NULL_TAG || firstByte == REPEAT_TAG)
		{
			ensureCapacity(count + 5);
			buf[count] = REPEAT_TAG;
			buf[count + 1] = (byte) firstByte;
			buf[count + 2] = (byte) secondByte;
			buf[count + 3] = (byte) thirdByte;
			buf[count + 4] = (byte) fourthByte;
			count += 5;
		}
		else
		{
			ensureCapacity(count + 4);
			buf[count] = (byte) firstByte;
			buf[count + 1] = (byte) secondByte;
			buf[count + 2] = (byte) thirdByte;
			buf[count + 3] = (byte) fourthByte;
			count += 4;
		}
	}

	@Override
	public final void writeLong(long v) throws IOException
	{
		ensureCapacity(count + 8);
		buf[count] = (byte) (v >>> 56);
		buf[count + 1] = (byte) ((v >>> 48) & 0xFF);
		buf[count + 2] = (byte) ((v >>> 40) & 0xFF);
		buf[count + 3] = (byte) ((v >>> 32) & 0xFF);
		buf[count + 4] = (byte) ((v >>> 24) & 0xFF);
		buf[count + 5] = (byte) ((v >>> 16) & 0xFF);
		buf[count + 6] = (byte) ((v >>> 8) & 0xFF);
		buf[count + 7] = (byte) (v & 0xFF);
		count += 8;
	}

	public final void writeLong(Long v) throws IOException
	{
		if (v == null)
		{
			write(NULL_TAG);
			return;
		}

		long val = v;
		int firstByte = (int) (val >>> 56);
		int secondByte = (int) (val >>> 48) & 0xFF;
		int thirdByte = (int) (val >>> 40) & 0xFF;
		int fourthByte = (int) (val >>> 32) & 0xFF;
		int fifthByte = (int) (val >>> 24) & 0xFF;
		int sixthByte = (int) (val >>> 16) & 0xFF;
		int seventhByte = (int) (val >>> 8) & 0xFF;
		int eigthByte = (int) val & 0xFF;

		if (firstByte == NULL_TAG || firstByte == REPEAT_TAG)
		{
			ensureCapacity(count + 9);
			buf[count] = REPEAT_TAG;
			buf[count + 1] = (byte) firstByte;
			buf[count + 2] = (byte) secondByte;
			buf[count + 3] = (byte) thirdByte;
			buf[count + 4] = (byte) fourthByte;
			buf[count + 5] = (byte) fifthByte;
			buf[count + 6] = (byte) sixthByte;
			buf[count + 7] = (byte) seventhByte;
			buf[count + 8] = (byte) eigthByte;
			count += 9;
		}
		else
		{
			ensureCapacity(count + 8);
			buf[count] = (byte) firstByte;
			buf[count + 1] = (byte) secondByte;
			buf[count + 2] = (byte) thirdByte;
			buf[count + 3] = (byte) fourthByte;
			buf[count + 4] = (byte) fifthByte;
			buf[count + 5] = (byte) sixthByte;
			buf[count + 6] = (byte) seventhByte;
			buf[count + 7] = (byte) eigthByte;
			count += 8;
		}
	}

	@Override
	public final void writeFloat(float v) throws IOException
	{
		writeInt(Float.floatToIntBits(v));
	}

	public final void writeFloat(Float v) throws IOException
	{
		if (v == null)
		{
			write(NULL_TAG);
			return;
		}

		int val = Float.floatToIntBits(v);
		int firstByte = val >>> 24;
		int secondByte = (val >>> 16) & 0xFF;
		int thirdByte = (val >>> 8) & 0xFF;
		int fourthByte = val & 0xFF;

		if (firstByte == NULL_TAG || firstByte == REPEAT_TAG)
		{
			ensureCapacity(count + 5);
			buf[count] = REPEAT_TAG;
			buf[count + 1] = (byte) firstByte;
			buf[count + 2] = (byte) secondByte;
			buf[count + 3] = (byte) thirdByte;
			buf[count + 4] = (byte) fourthByte;
			count += 5;
		}
		else
		{
			ensureCapacity(count + 4);
			buf[count] = (byte) firstByte;
			buf[count + 1] = (byte) secondByte;
			buf[count + 2] = (byte) thirdByte;
			buf[count + 3] = (byte) fourthByte;
			count += 4;
		}
	}

	@Override
	public final void writeDouble(double v) throws IOException
	{
		writeLong(Double.doubleToLongBits(v));
	}

	public final void writeDouble(Double v) throws IOException
	{
		if (v == null)
		{
			write(NULL_TAG);
			return;
		}

		long val = Double.doubleToLongBits(v);
		int firstByte = (int) (val >>> 56);
		int secondByte = (int) (val >>> 48) & 0xFF;
		int thirdByte = (int) (val >>> 40) & 0xFF;
		int fourthByte = (int) (val >>> 32) & 0xFF;
		int fifthByte = (int) (val >>> 24) & 0xFF;
		int sixthByte = (int) (val >>> 16) & 0xFF;
		int seventhByte = (int) (val >>> 8) & 0xFF;
		int eigthByte = (int) val & 0xFF;

		if (firstByte == NULL_TAG || firstByte == REPEAT_TAG)
		{
			ensureCapacity(count + 9);
			buf[count] = REPEAT_TAG;
			buf[count + 1] = (byte) firstByte;
			buf[count + 2] = (byte) secondByte;
			buf[count + 3] = (byte) thirdByte;
			buf[count + 4] = (byte) fourthByte;
			buf[count + 5] = (byte) fifthByte;
			buf[count + 6] = (byte) sixthByte;
			buf[count + 7] = (byte) seventhByte;
			buf[count + 8] = (byte) eigthByte;
			count += 9;
		}
		else
		{
			ensureCapacity(count + 8);
			buf[count] = (byte) firstByte;
			buf[count + 1] = (byte) secondByte;
			buf[count + 2] = (byte) thirdByte;
			buf[count + 3] = (byte) fourthByte;
			buf[count + 4] = (byte) fifthByte;
			buf[count + 5] = (byte) sixthByte;
			buf[count + 6] = (byte) seventhByte;
			buf[count + 7] = (byte) eigthByte;
			count += 8;
		}
	}

	@Override
	public final void writeBoolean(boolean v) throws IOException
	{
		write(v ? 1 : 0);
	}

	public final void writeBoolean(Boolean v) throws IOException
	{
		if (v == null)
		{
			write(NULL_TAG);
			return;
		}

		write(v.booleanValue() ? 1 : 0);
	}

	@Override
	public final void writeChar(int c) throws IOException
	{
		writeShort(c);
	}

	public final void writeChar(Character c) throws IOException
	{
		if (c == null)
		{
			write(NULL_TAG);
			return;
		}

		int val = c;
		int firstByte = (val >>> 8) & 0xFF;
		int secondByte = val & 0xFF;

		if (firstByte == NULL_TAG || firstByte == REPEAT_TAG)
		{
			ensureCapacity(count + 3);
			buf[count] = REPEAT_TAG;
			buf[count + 1] = (byte) firstByte;
			buf[count + 2] = (byte) secondByte;
			count += 3;
		}
		else
		{
			ensureCapacity(count + 2);
			buf[count] = (byte) firstByte;
			buf[count + 1] = (byte) secondByte;
			count += 2;
		}
	}

	@Override
	public final void writeBytes(String s) throws IOException
	{
		final int len = s.length();
		ensureCapacity(count + len);
		for (int i = 0; i < len; i++)
		{
			buf[count + i] = (byte) s.charAt(i);
		}
		count += len;
	}

	@Override
	public final void writeChars(String s) throws IOException
	{
		final int len = s.length();
		ensureCapacity(count + 2 * len);
		for (int i = 0; i < len; i++)
		{
			int val = s.charAt(i);
			buf[count + 2 * i] = (byte) ((val >>> 8) & 0xFF);
			buf[count + 2 * i + 1] = (byte) (val & 0xFF);
		}
		count += 2 * len;
	}

	@Override
	public final void writeUTF(String str) throws IOException
	{
		final int len = str.length();
		ensureCapacity(count + len + 2);

		int base = count + 2;
		for (int i = 0; i < len; i++)
		{
			ensureCapacity(base + 3);
			base += putUTFChar(str.charAt(i), buf, base);
		}

		int utflen = base - count - 2;
		if (utflen > 65535)
			throw new UTFDataFormatException("encoded string too long: " + utflen + " bytes");

		buf[count] = (byte) (utflen >>> 8);
		buf[count + 1] = (byte) (utflen & 0xFF);

		count += utflen + 2;
	}

	public final void writeNormalString(String str) throws IOException
	{
		final int len = str.length();
		ensureCapacity(count + len + 4);

		int base = count + 4;
		for (int i = 0; i < len; i++)
		{
			ensureCapacity(base + 3);
			base += putUTFChar(str.charAt(i), buf, base);
		}

		int utflen = base - count - 4;

		buf[count] = (byte) (utflen >>> 24);
		buf[count + 1] = (byte) ((utflen >>> 16) & 0xFF);
		buf[count + 2] = (byte) ((utflen >>> 8) & 0xFF);
		buf[count + 3] = (byte) (utflen & 0xFF);

		count += utflen + 4;
	}

	public final void writeDeflatedString(String str) throws IOException
	{
		final int len = str.length();
		final int base = count + 9;
		int utflen = 0;
		int compressedSize = 0;

		Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
		byte[] bytes = new byte[DEFLATER_BUFFER_SIZE];

		int idx = 0;
		while (idx < len)
		{
			int encoded = 0;
			while (idx < len && encoded <= DEFLATER_BUFFER_SIZE - 3)
			{
				encoded += putUTFChar(str.charAt(idx++), bytes, encoded);
			}
			utflen += encoded;

			deflater.setInput(bytes, 0, encoded);
			if (idx == len)
				deflater.finish();

			while (true)
			{
				ensureCapacity(base + compressedSize + 4);
				int space = buf.length - (base + compressedSize);
				int x = deflater.deflate(buf, base + compressedSize, space);
				compressedSize += x;
				if (x < space)
					break;
			}
		}
		deflater.end();

		buf[count] = DEFLATED_TAG;
		buf[count + 1] = (byte) (utflen >>> 24);
		buf[count + 2] = (byte) ((utflen >>> 16) & 0xFF);
		buf[count + 3] = (byte) ((utflen >>> 8) & 0xFF);
		buf[count + 4] = (byte) (utflen & 0xFF);
		buf[count + 5] = (byte) (compressedSize >>> 24);
		buf[count + 6] = (byte) ((compressedSize >>> 16) & 0xFF);
		buf[count + 7] = (byte) ((compressedSize >>> 8) & 0xFF);
		buf[count + 8] = (byte) (compressedSize & 0xFF);

		count += compressedSize + 9;
	}

	public final void writeString(String str) throws IOException
	{
		if (str == null)
		{
			write(NULL_TAG);
			return;
		}

		if (str.length() < DEFLATE_THRESHOLD)
		{
			if (3 * str.length() >= (NULL_TAG << 24))
				write(REPEAT_TAG);
			writeNormalString(str);
		}
		else
			writeDeflatedString(str);
	}

	private int putUTFChar(int character, byte[] buf, int idx)
	{
		if ((character >= 0x0001) && (character <= 0x007F))
		{
			buf[idx] = (byte) character;
			return 1;
		}
		else if (character > 0x07FF)
		{
			buf[idx] = (byte) (0xE0 | ((character >> 12) & 0x0F));
			buf[idx + 1] = (byte) (0x80 | ((character >> 6) & 0x3F));
			buf[idx + 2] = (byte) (0x80 | ((character >> 0) & 0x3F));
			return 3;
		}
		else
		{
			buf[idx] = (byte) (0xC0 | ((character >> 6) & 0x1F));
			buf[idx + 1] = (byte) (0x80 | ((character >> 0) & 0x3F));
			return 2;
		}
	}

	public final void writeTCID(TCID id) throws IOException
	{
		if (id == null)
		{
			write(NULL_TAG);
			return;
		}

		long msb = id.getMostSignificantBits();
		long lsb = id.getLeastSignificantBits();

		int firstByte = (int) (msb >>> 56);
		if (firstByte == NULL_TAG || firstByte == REPEAT_TAG)
			write(REPEAT_TAG);

		writeLong(msb);
		writeLong(lsb);
	}

	public final void writeTCIDNotNull(TCID id) throws IOException
	{
		writeLong(id.getMostSignificantBits());
		writeLong(id.getLeastSignificantBits());
	}

	// static final long[] negativeThresholds = new long[10];
	// static final long[] positiveThresholds = new long[10];
	// static
	// {
	// negativeThresholds[1] = 127;
	// positiveThresholds[1] = 0;
	// long span = 16384;
	// for (int i = 2; i <= 9; i++)
	// {
	// negativeThresholds[i] = negativeThresholds[i - 1] + (span >>> 4);
	// positiveThresholds[i] = positiveThresholds[i - 1] + span - (span >>> 4);
	// span <<= 7;
	// }
	// }
	//
	// public final void writeId(long id) throws IOException
	// {
	// // 00000000 1 byte null
	// // 0xxxxxxx 1 byte all negative
	// // 10xxxxxx 2 bytes top 1/16 negative
	// // 110xxxxx 3 bytes top 1/16 negative
	// // 1110xxxx 4 bytes top 1/16 negative
	// // 11110xxx 5 bytes top 1/16 negative
	// // 111110xx 6 bytes top 1/16 negative
	// // 1111110x 7 bytes top 1/16 negative
	// // 11111110 8 bytes top 1/16 negative
	// // 11111111 9 bytes top 1/16 negative
	//
	// if (id == 0)
	// {
	// write(0);
	// return;
	// }
	//
	// int bytes;
	// if (id > 0)
	// {
	// bytes = 2;
	// while (id > positiveThresholds[bytes])
	// bytes++;
	// id -= positiveThresholds[bytes - 1];
	// }
	// else
	// {
	// id = -id;
	// bytes = 1;
	// while (id > negativeThresholds[bytes])
	// bytes++;
	// id -= negativeThresholds[bytes - 1];
	// if (bytes == 1)
	// {
	// write((int) id);
	// return;
	// }
	// id |= 0xFL << (7 * bytes - 4);
	// }
	// ensureCapacity(count + bytes);
	//
	// int prefix = ~(0xFF >> (bytes - 1)) & 0xFF;
	// id |= (long) prefix << (8 * (bytes - 1));
	//
	// for (int i = bytes - 1; i >= 0; i--)
	// {
	// buf[count + i] = (byte) (id & 0xFF);
	// id >>>= 8;
	// }
	//
	// count += bytes;
	// }

	public final void writeId(int id) throws IOException
	{
		if (id == 0)
		{
			write(ID_NULL_TAG);
			return;
		}

		writeInt(id);
	}

	public final void writeSharedObjectReference(BaseTransactionObject o) throws IOException
	{
		if (o == null)
		{
			write(ID_NULL_TAG);
			return;
		}

		writeInt(o.__tc_getId());
	}

	public final void writeSharedObject(BaseTransactionObject shadowCopy) throws IOException
	{
		// int pos = count;
		// ensureCapacity(count + 4);
		// count += 4;

		shadowCopy.__tc_serialize(this);
		// int len = count - pos - 4;
		//
		// buf[pos] = (byte) (len >>> 24);
		// buf[pos + 1] = (byte) ((len >>> 16) & 0xFF);
		// buf[pos + 2] = (byte) ((len >>> 8) & 0xFF);
		// buf[pos + 3] = (byte) (len & 0xFF);
	}

	@Override
	public final void writeObject(Object obj) throws IOException
	{
		if (obj == null)
		{
			write(NULL_TAG);
			return;
		}

		if (obj instanceof BaseTransactionObject)
		{
			int id = ((BaseTransactionObject) obj).__tc_getId();
			int absId = Math.abs(id);
			if (absId <= Short.MAX_VALUE)
			{
				write(ID2_TAG);
				writeShort(id);
			}
			else if (absId <= Short.MAX_VALUE << 8)
			{
				write(ID3_TAG);
				write3byter(id);
			}
			else
			{
				write(ID_TAG);
				writeInt(id);
			}
		}
		else
		{
			TypeHandler typeHandler = typeHandlers.get(obj.getClass());
			if (typeHandler != null)
			{
				typeHandler.write(this, obj);
			}
			else
			{
				write(OTHER_TAG);
				ObjectOutputStream oos = new ObjectOutputStream(this);
				oos.writeObject(obj);
				oos.flush();
				oos.close();
			}
		}
	}

	public final int size()
	{
		return count;
	}

	public final byte[] toByteArray()
	{
		return Arrays.copyOf(buf, count);
	}

	public final void writeTo(ByteBuffer bb)
	{
		bb.put(buf, 0, count);
	}

	private static final HashMap<Class<?>, TypeHandler> typeHandlers;
	static
	{
		typeHandlers = new HashMap<Class<?>, TypeHandler>();
		typeHandlers.put(String.class, new StringHandler());
		typeHandlers.put(Byte.class, new ByteHandler());
		typeHandlers.put(Short.class, new ShortHandler());
		typeHandlers.put(Integer.class, new IntegerHandler());
		typeHandlers.put(Long.class, new LongHandler());
		typeHandlers.put(Float.class, new FloatHandler());
		typeHandlers.put(Double.class, new DoubleHandler());
		typeHandlers.put(Boolean.class, new BooleanHandler());
	}

	private static abstract class TypeHandler
	{
		public abstract void write(SharedObjectOutputStream out, Object o) throws IOException;
	}

	private final static class StringHandler extends TypeHandler
	{
		public void write(SharedObjectOutputStream out, Object o) throws IOException
		{
			out.write(STRING_TAG);
			out.writeNormalString((String) o);
		}
	}

	private final static class ByteHandler extends TypeHandler
	{
		public void write(SharedObjectOutputStream out, Object o) throws IOException
		{
			out.write(BYTE_TAG);
			out.writeByte(((Byte) o).byteValue());
		}
	}

	private final static class ShortHandler extends TypeHandler
	{
		public void write(SharedObjectOutputStream out, Object o) throws IOException
		{
			out.write(SHORT_TAG);
			out.writeShort(((Short) o).shortValue());
		}
	}

	private final static class IntegerHandler extends TypeHandler
	{
		public void write(SharedObjectOutputStream out, Object o) throws IOException
		{
			out.write(INTEGER_TAG);
			out.writeInt(((Integer) o).intValue());
		}
	}

	private final static class LongHandler extends TypeHandler
	{
		public void write(SharedObjectOutputStream out, Object o) throws IOException
		{
			out.write(LONG_TAG);
			out.writeLong(((Long) o).longValue());
		}
	}

	private final static class FloatHandler extends TypeHandler
	{
		public void write(SharedObjectOutputStream out, Object o) throws IOException
		{
			out.write(FLOAT_TAG);
			out.writeFloat(((Float) o).floatValue());
		}
	}

	private final static class DoubleHandler extends TypeHandler
	{
		public void write(SharedObjectOutputStream out, Object o) throws IOException
		{
			out.write(DOUBLE_TAG);
			out.writeDouble(((Double) o).doubleValue());
		}
	}

	private final static class BooleanHandler extends TypeHandler
	{
		public void write(SharedObjectOutputStream out, Object o) throws IOException
		{
			boolean b = (Boolean) o;
			if (b)
				out.write(BOOLEAN_TRUE_TAG);
			else
				out.write(BOOLEAN_FALSE_TAG);
		}
	}
}
