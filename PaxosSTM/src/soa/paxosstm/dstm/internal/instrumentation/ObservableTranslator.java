/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.instrumentation;

import java.io.IOException;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.Translator;
import soa.paxosstm.dstm.PaxosSTM;

/**
 * ObservableTranslator modifies the definitions of classes loaded to the JVM by
 * the Javassist loader. There are two types of modifications done by the
 * translator. The first, called by the authors altering, applies only to
 * transactional classes. The second, called instrumentation, is performed on
 * all classes passing through the translator. The process of translating can be
 * summarized in these few simple steps:
 * <ul>
 * <li>check whether the given class is transactional or not,</li>
 * <li>if the class is transactional perform following two additional steps:<br>
 * <ul>
 * <li>alter the class if it was not altered before,</li>
 * <li>implement methods added during the alteration,</li>
 * </ul>
 * </li>
 * <li>instrument all the methods declared in the class.</li>
 * </ul>
 * <p>
 * There is also a special modification done to the static void main(String[]
 * args) method from the application. The body of the method is changed so the
 * first invoked method is the {@link PaxosSTM#init(String[])} which initializes
 * the Paxos STM framework.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class ObservableTranslator implements Translator
{
	private final String mainClassname;

	public ObservableTranslator(String mainClassname)
	{
		this.mainClassname = mainClassname;
	}

	public void start(ClassPool pool) throws NotFoundException, CannotCompileException
	{
		CtClass mainClass = pool.get(mainClassname);
		CtMethod mainMethod = mainClass.getDeclaredMethod("main", new CtClass[] { pool
				.get(String[].class.getName()) });
		mainMethod.insertBefore("$1 = " + PaxosSTM.class.getName() + ".init($1); ");
	}

	public void onLoad(ClassPool pool, String classname) throws NotFoundException,
			CannotCompileException
	{
		// System.out.println("onLoad: " + classname);
		CtClass cc = pool.get(classname);

		if (cc.isAnnotation() || cc.isEnum() || cc.isArray() || cc.isInterface())
			return;

		try
		{
			ClassInfo classInfo = ClassInfo.get(cc);
			if (classInfo.isTransactionObject())
			{
				ClassAlterer ca = new ClassAlterer(classInfo, pool);
				if (!classInfo.isAltered)
					ca.alterThis();
				ca.addImplementation();
			}
			ClassAlterer.instrumentClass(classInfo.getParameterClass(), pool);
			 try
			 {
			 cc.writeFile("xmieci");
			 }
			 catch (IOException e)
			 {
			 e.printStackTrace();
			 }
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}
}
