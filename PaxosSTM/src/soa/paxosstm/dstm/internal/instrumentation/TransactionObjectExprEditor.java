/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.instrumentation;

import java.lang.reflect.Modifier;
import java.util.HashSet;
import java.util.Set;

import soa.paxosstm.dstm.internal.exceptions.WriteOnReadOnlyException;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import javassist.bytecode.BadBytecode;
import javassist.bytecode.CodeAttribute;
import javassist.bytecode.CodeIterator;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.Opcode;
import javassist.expr.FieldAccess;
import javassist.expr.MethodCall;

/**
 * A class which provides methods used during the process of class
 * instrumentation and alteration (see {@link ClassAlterer}).
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class TransactionObjectExprEditor extends ObservableExprEditor
{
	protected Set<Integer> aload0InstructionSet;

	protected MethodInfo minfo;

	final boolean writeCheck;

	final boolean safetyCheck;

	/**
	 * Constructor.
	 * 
	 * @param pool
	 *            {@link ClassPool} object
	 * @param safetyCheck
	 *            flag indicating whether field reads should be instrumented or
	 *            not
	 * @param writeCheck
	 *            flag indicating whether field writes should be instrumented or
	 *            not
	 */
	public TransactionObjectExprEditor(ClassPool pool, boolean writeCheck, boolean safetyCheck)
	{
		super(pool);
		this.writeCheck = writeCheck;
		this.safetyCheck = safetyCheck;
	}

	@Override
	public boolean doit(CtClass clazz, MethodInfo minfo) throws CannotCompileException
	{
		this.parameterClass = clazz;
		this.minfo = minfo;
		return super.doit(clazz, minfo);
	}

	protected void retrieveSelfAccesses() throws BadBytecode
	{
		aload0InstructionSet = new HashSet<Integer>();

		CodeAttribute ca = minfo.getCodeAttribute();
		CodeIterator ci = ca.iterator();

		while (ci.hasNext())
		{
			int index = ci.next();
			int op = ci.byteAt(index);
			if (op == Opcode.ALOAD_0)
				aload0InstructionSet.add(index);
			// System.out.println(index + "\t" + Mnemonic.OPCODE[op] +
			// "(" + op + ")");
		}
	}

	protected boolean isOwnFieldAccess(FieldAccess f) throws NotFoundException
	{
		if (f.isReader())
			return aload0InstructionSet.contains(f.indexOfBytecode() - 1);
		if (!f.getField().getType().equals(CtClass.longType))
		{
			// TODO
			// make sure that previous instruction is a load
			return aload0InstructionSet.contains(f.indexOfBytecode() - 2);
			
		}
		// TODO
		// make sure that previous instruction is a long load
		return aload0InstructionSet.contains(f.indexOfBytecode() - 3);
	}

	protected boolean isOwnMethodCall(MethodCall m) throws NotFoundException
	{
		if (Modifier.isStatic(m.getMethod().getModifiers()))
			return false;
		
		CtClass[] parameters = m.getMethod().getParameterTypes();

		if (parameters.length == 0)
			return aload0InstructionSet.contains(m.indexOfBytecode() - 1);
		
		// TODO
		// make sure all previous instructions are respective loads
//		return aload0InstructionSet.contains(m.indexOfBytecode() - (parameters.length + 1));
		
		return false;
	}

	/**
	 * Instruments read and write accesses to the given field, which belongs to
	 * the current instance, so they can be traced by the changes tracking
	 * mechanism.
	 * 
	 * @param f
	 *            {@link FieldAccess} object associated with the given field
	 * @throws CannotCompileException
	 */
	protected void processInternal(FieldAccess f) throws CannotCompileException
	{
		if (f.isWriter())
		{
			String wCheckStmt = ClassAlterer.ATTR_FIELD_NAME + " |= "
					+ BaseTransactionObject.IS_WRITTEN_MASK + "; ";
			String exception = WriteOnReadOnlyException.class.getName();
			String sCheckStmt = "throw new " + exception + "(); ";
			String accessStmt = (writeCheck ? wCheckStmt : "") + "$proceed($1); ";
			if (safetyCheck)
				f.replace(sCheckStmt);
			else
				f.replace(accessStmt);
		}
	}

	public void edit(FieldAccess f) throws CannotCompileException
	{
		if (f.isStatic())
			return;

		try
		{
			ClassInfo ci = ClassInfo.get(f.getField().getDeclaringClass());
			if (ci.isTransactionObject())
			{
				if (!ci.isAltered)
					ClassAlterer.alterClass(ci, pool);
				if (f.getFieldName().startsWith(ClassAlterer.NAMES_PREFIX))
					return;
			}
			else
				return;

			retrieveSelfAccesses();
			boolean ownField = isOwnFieldAccess(f);

			if (ownField)
				processInternal(f);
			else
				processExternal(f);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}

	public void edit(MethodCall m) throws CannotCompileException
	{
		try
		{
			if (Modifier.isStatic(m.getMethod().getModifiers()))
				return;

			ClassInfo ci = ClassInfo.get(m.getMethod().getDeclaringClass());
			if (ci.isTransactionObject())
			{
				if (!ci.isAltered)
					ClassAlterer.alterClass(ci, pool);
				if (m.getMethodName().startsWith(ClassAlterer.NAMES_PREFIX))
					return;
			}
			else
				return;

			retrieveSelfAccesses();
			boolean ownMethod = isOwnMethodCall(m);

			if (ownMethod)
			{
				String newName = ClassAlterer.getNewMethodName(m.getMethod());
				boolean returnsValue = m.getMethod().getReturnType() != CtClass.voidType;

				if (safetyCheck)
				{
					// replace with safetyCheck method call
					String call = "$0." + newName + ClassAlterer.SAFETY_CHECK_SUFFIX + "($$); ";
					String stmt = (returnsValue ? "$_ = " : "") + call;
					m.replace(stmt);
				}
				else if (writeCheck)
				{
					// replace with internal dispatcher call
					String call = "$0." + newName + ClassAlterer.INTERNAL_SUFFIX + "($$); ";
					String stmt = (returnsValue ? "$_ = " : "") + call;
					m.replace(stmt);
				}
				else if (!writeCheck && !safetyCheck)
				{
					// replace with noCheck method call
					String call = "$0." + newName + ClassAlterer.NO_CHECK_SUFFIX + "($$); ";
					String stmt = (returnsValue ? "$_ = " : "") + call;
					m.replace(stmt);
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}
}
