/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.instrumentation;

import soa.paxosstm.dstm.ArrayWrapper;
import soa.paxosstm.dstm.ObjectWrapper;
import soa.paxosstm.dstm.TransactionObject;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import javassist.ClassPool;
import javassist.Translator;

/**
 * The transaction certification algorithms implemented in
 * {@link GlobalTransactionManager} rely on comparing read and write sets of
 * different transactions. Loader class is the head of the family of classes
 * responsible for automatic tracking of changes performed on objects inside the
 * transactions. The <i>changes tracking mechanism</i> enables Paxos STM to
 * determine the minimal read and write sets that are used later for the
 * transaction certification.
 * <p>
 * Monitoring of accesses to objects in Paxos STM is based on three different
 * types of shared objects:
 * <ul>
 * <li>custom shared objects - Classes marked with the {@link TransactionObject}
 * annotation and the ones that derive from them are called transactional
 * classes and are specially instrumented when loaded to the JVM by the
 * Javassist loader. Instances of these classes are called custom shared
 * objects. The instrumentation enables automatic monitoring of read and write
 * accesses to all fields and as a result enables to determine whether these
 * objects were read or written in the current transaction. In the current
 * version of Paxos STM changes to the static fields of transactional classes
 * are not tracked as they do not directly affect the state of a shared object.
 * In the future the ability to process static fields in transactions might be
 * added. Of course to mark a class with the {@link TransactionObject}
 * annotation the programmer needs to have a full control over it.</li>
 * <li>generic objects of the {@link ObjectWrapper} class - Classes which cannot
 * be controlled by the programmer, e.g. system classes, classes from external
 * libraries, etc., require different monitoring mechanism. Objects of such
 * classes need to be wrapped inside an instance of the {@link ObjectWrapper}
 * class. Upon the first usage of the wrapper in a given transaction a copy of
 * the original object is made and used in place of the original object. Changes
 * to the state of an object are checked upon the commit by comparing the
 * original object with the copy modified by the transaction.</li>
 * <li>generic object of the {@link ArrayWrapper} class - Accesses to arrays
 * cannot be easily monitored as in the case of for example variables of
 * primitive types. Therefore using arrays inside transactions requires wrapping
 * them inside an instance of the {@link ArrayWrapper} class. Once an array is
 * placed inside an ArrayWrapper reference to the array can be temporarily
 * released.</li>
 * </ul>
 * <p>
 * Let us assume there is an application utilizing Paxos STM framework. The
 * static void main(String[] args) method of the application is not the first
 * one to be invoked. Instead Paxos STM framework is started to prepare a loader
 * provided by the Javassist so that the application classes can be loaded a
 * while later (this is the role of the {@link #run(String, String[])} method).
 * The Javassist loader is customized with a so called translator (see
 * {@link ObservableTranslator} which describes all the modifications to be
 * applied to the applications classes. When the loader is ready, the class
 * containing the static void main(String[] args) method from the application is
 * loaded and the very method is invoked using the Java reflection mechanism
 * (see {@link ObservableTranslator#start(ClassPool)}. Subsequently, every class
 * used by the application which is not already loaded to the JVM is passed to
 * the translator which modifies its structure and recompiles it; now the
 * Javassist loader can load the modified class to the JVM.
 * 
 * @see ClassAlterer
 * @see BaseTransactionObject
 * @see ObservableTranslator
 * @see TransactionObject
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class Loader
{
	public static void run(String classname, String[] args) throws Throwable
	{
		Translator t = new ObservableTranslator(classname);
		ClassPool pool = ClassPool.getDefault();
		javassist.Loader cl = new javassist.Loader();
		cl.addTranslator(pool, t);
		cl.run(classname, args);
		System.exit(0);
	}
}
