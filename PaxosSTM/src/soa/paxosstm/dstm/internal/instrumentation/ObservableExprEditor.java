/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.instrumentation;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.bytecode.MethodInfo;
import javassist.expr.ExprEditor;
import javassist.expr.FieldAccess;

/**
 * A class which provides methods used during the process of class
 * instrumentation (see {@link ClassAlterer}). It is a base for the
 * {@link TransactionObjectExprEditor} class.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class ObservableExprEditor extends ExprEditor
{
	protected CtClass parameterClass;

	protected final ClassPool pool;

	/**
	 * Constructor.
	 * 
	 * @param pool
	 *            {@link ClassPool} object
	 */
	public ObservableExprEditor(ClassPool pool)
	{
		this.pool = pool;
	}

	@Override
	public boolean doit(CtClass clazz, MethodInfo minfo) throws CannotCompileException
	{
		this.parameterClass = clazz;
		return super.doit(clazz, minfo);
	}

	/**
	 * Instruments read and write accesses to the given field so they can be
	 * traced by the changes tracking mechanism.
	 * 
	 * @param f
	 *            {@link FieldAccess} object associated with the given field
	 * @throws CannotCompileException
	 */
	protected void processExternal(FieldAccess f) throws CannotCompileException
	{
		String targetName = "target";
		String className = f.getClassName();

		String attrField = targetName + "." + ClassAlterer.ATTR_FIELD_NAME;
		if (f.isReader())
		{
			String targetStmt = ClassAlterer.getTargetReadStmt(targetName, className);
			String accessStmt = "$_ = " + targetName + "." + f.getFieldName() + "; ";
			//String xxx = "System.err.println(\"xxx: " + f.getFieldName() + " \" + $0 + \" \" + $0.__tc_attr + \" \" + $0." + f.getFieldName() + " + \" \" + target + \" \" + target.__tc_attr + \" \" + target." + f.getFieldName() + "); ";
			f.replace(targetStmt + accessStmt);
		}
		else if (f.isWriter())
		{
			String targetStmt = ClassAlterer.getTargetWriteStmt(targetName, className);
			String setWrittenStmt = attrField + " |= " + BaseTransactionObject.IS_WRITTEN_MASK + "; ";
			String accessStmt = targetName + "." + f.getFieldName() + " = $1; ";
			// String ddd = "System.err.println(\"ddd: " + f.getFieldName() + " \" + $0 + \" \" + $0.__tc_attr + \" \" + $0." + f.getFieldName() + " + \" \" + target + \" \" + target.__tc_attr + \" \" + target." + f.getFieldName() + "); ";
			f.replace(targetStmt + setWrittenStmt + accessStmt);
		}
	}

	@Override
	public void edit(FieldAccess f) throws CannotCompileException
	{
		if (f.isStatic())
			return;

		try
		{
			ClassInfo ci = ClassInfo.get(f.getField().getDeclaringClass());
			if (ci.isTransactionObject())
			{
				if (!ci.isAltered)
					ClassAlterer.alterClass(ci, pool);
				if (f.getFieldName().startsWith(ClassAlterer.NAMES_PREFIX))
					return;
			}
			else
				return;

			processExternal(f);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			System.exit(1);
		}
	}
}
