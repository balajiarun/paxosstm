/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.instrumentation;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.io.ObjectStreamException;

import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager;

/**
 * The Observable interface consists of methods allowing retrieval of values of
 * those variables and manual marking the object as read or written. The
 * Observable interface also defines serialize and deserialize methods used when
 * a given object was modified in the current transaction and has to be
 * broadcast to other replicas together with the transaction's read and write
 * sets. Although the Java native serialization is reliable and guarantees
 * correctness, the use of a customized solution is preferential in our case. It
 * is much faster and serialized objects require less space which helps to keep
 * the size of messages exchanged between replicas relatively small.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public abstract class BaseTransactionObject implements Externalizable
{
	public static long MAX_VERSION_NUMBER = Long.MAX_VALUE;

	public static final int IS_WRITTEN_MASK = 0x1;
	public static final int IS_OBJECT_HANDLE_MASK = 0x2;
	public static final int IS_OBJECT_VERSION_MASK = 0x4;
	public static final int IS_SHADOW_COPY_MASK = 0x8;

	public int __tc_id;
	public long __tc_version;
	public BaseTransactionObject __tc_next;
	public int __tc_attr;

	public BaseTransactionObject()
	{
		LocalTransactionManager.registerNewObject(this);
		__tc_version = -1;
	}

	protected BaseTransactionObject(SpecialConstructorMarker marker)
	{
	}

	public abstract BaseTransactionObject __tc_newInstance();

	public final void __tc_initHandle(int id)
	{
		__tc_id = id;
		__tc_attr = IS_OBJECT_HANDLE_MASK;
	}

	public final void __tc_initShadowCopy(int id)
	{
		__tc_id = id;
		__tc_attr = IS_SHADOW_COPY_MASK;
	}

	public final int __tc_getId()
	{
		return __tc_id;
	}

	public final void __tc_setId(int id)
	{
		__tc_id = id;
	}

	public final long __tc_getVersion()
	{
		return __tc_version;
	}

	public final void __tc_setVersion(long version)
	{
		__tc_version = version;
	}

	public final BaseTransactionObject __tc_getNext()
	{
		return __tc_next;
	}

	public final void __tc_setNext(BaseTransactionObject next)
	{
		__tc_next = next;
	}

	public final boolean __tc_isVersion()
	{
		return (__tc_attr & IS_OBJECT_VERSION_MASK) != 0;
	}

	public final boolean __tc_isShadowCopy()
	{
		return (__tc_attr & IS_SHADOW_COPY_MASK) != 0;
	}

	public final boolean __tc_isHandle()
	{
		return (__tc_attr & IS_OBJECT_HANDLE_MASK) != 0;
	}

	public final boolean __tc_isWritten()
	{
		return (__tc_attr & IS_WRITTEN_MASK) > 0;
	}

	public final void __tc_setWritten(boolean written)
	{
		if (written)
			__tc_attr |= IS_WRITTEN_MASK;
		else
			__tc_attr &= ~IS_WRITTEN_MASK;
	}

	public final BaseTransactionObject __tc_retrieve(long version)
	{
		// BaseTransactionObject v = this;
		// while (v != null && (!v.__tc_isVersion() || v.__tc_version >
		// version))
		// v = v.__tc_next;
		BaseTransactionObject v = __tc_next;
		while (v != null && v.__tc_version > version)
			v = v.__tc_next;
		return v;
	}

	public final BaseTransactionObject __tc_retrieveNewest()
	{
		// BaseTransactionObject v = this;
		// while (v != null && !v.__tc_isVersion())
		// v = v.__tc_next;
		// return v;
		return __tc_next;
	}

	public final void __tc_release(long version)
	{
		// ensure there is at least one version object of number <= version
		BaseTransactionObject prev = this;
		BaseTransactionObject next = prev.__tc_next;
		while (next != null && next.__tc_version > version)
		{
			prev = next;
			next = next.__tc_next;
		}
		if (next == null)
			return;

		next.__tc_next = null;
	}

	public final void __tc_addNewVersion(long version, SharedObjectInputStream data)
			throws IOException, ClassNotFoundException
	{
		BaseTransactionObject obj = __tc_newInstance();
		obj.__tc_deserialize(data);
		BaseTransactionObject nextVersion = this.__tc_next;
		obj.__tc_id = this.__tc_id;
		obj.__tc_version = version;
		obj.__tc_next = nextVersion;
		obj.__tc_attr = IS_OBJECT_VERSION_MASK;
		__tc_next = obj;
	}

	public final void __tc_addNewVersion(long version, BaseTransactionObject obj)
			throws IOException, ClassNotFoundException
	{
		BaseTransactionObject nextVersion = this.__tc_next;
		obj.__tc_id = this.__tc_id;
		obj.__tc_version = version;
		obj.__tc_next = nextVersion;
		obj.__tc_attr = IS_OBJECT_VERSION_MASK;
		__tc_next = obj;
	}

	public abstract void __tc_serialize(SharedObjectOutputStream shoos) throws IOException;

	public abstract void __tc_deserialize(SharedObjectInputStream shois) throws IOException,
			ClassNotFoundException;

	public abstract void __tc_copy(Object target);

	public abstract BaseTransactionObject __tc_makeCopy();

	@Override
	public final void writeExternal(ObjectOutput out) throws IOException
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public final void readExternal(ObjectInput in) throws IOException
	{
		throw new UnsupportedOperationException();
	}

	protected final Object writeReplace() throws ObjectStreamException
	{
		return new SerializationProxy(__tc_id);
	}

	private static final class SerializationProxy implements Externalizable
	{
		int id;
		BaseTransactionObject handle;

		public SerializationProxy()
		{
		}

		public SerializationProxy(int id)
		{
			this.id = id;
		}

		@Override
		public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
		{
			id = in.readInt();
			if (id >= 0)
				handle = GlobalTransactionManager.getInstance().getSharedObjectLibrary().getHandle(id);
			else
				handle = GlobalTransactionManager.getInstance().getSharedObjectLibrary().getNewHandle(id);
		}

		@Override
		public void writeExternal(ObjectOutput out) throws IOException
		{
			out.writeInt(id);
		}

		private Object readResolve() throws ObjectStreamException
		{
			return handle;
		}
	}
}
