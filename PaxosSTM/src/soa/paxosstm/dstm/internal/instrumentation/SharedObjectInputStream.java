package soa.paxosstm.dstm.internal.instrumentation;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.utils.TCID;

public class SharedObjectInputStream extends InputStream implements ObjectInput,
		SharedObjectSerializationConstants
{
	final protected byte buf[];
	// FIXME
	public int pos;
	final protected int count;
	protected int mark;

	public SharedObjectInputStream(byte[] buf)
	{
		this(buf, 0, buf.length);
	}

	public SharedObjectInputStream(byte[] buf, int off, int len)
	{
		this.buf = buf;
		count = off + len;
		pos = off;
	}

	public SharedObjectInputStream(SharedObjectInputStream in, int len)
	{
		this.buf = in.buf;
		count = Math.min(in.pos + len, in.count);
		pos = in.pos;
	}

	@Override
	public final int read() throws IOException
	{
		return (pos < count) ? (buf[pos++] & 0xff) : -1;
	}

	@Override
	public final int read(byte[] b) throws IOException
	{
		return read(b, 0, b.length);
	}

	@Override
	public final int read(byte[] b, int off, int len) throws IOException
	{
		if (b == null)
			throw new NullPointerException();
		else if (off < 0 || len < 0 || len > b.length - off)
			throw new IndexOutOfBoundsException();

		if (pos >= count)
			return -1;

		int avail = count - pos;
		if (len > avail)
		{
			len = avail;
		}
		if (len <= 0)
		{
			return 0;
		}

		System.arraycopy(buf, pos, b, off, len);
		pos += len;
		return len;
	}

	@Override
	public final void readFully(byte[] b) throws IOException
	{
		read(b);
	}

	@Override
	public final void readFully(byte[] b, int off, int len) throws IOException
	{
		read(b, off, len);
	}

	@Override
	public final byte readByte() throws IOException
	{
		if (pos >= count)
			raiseEOF();

		return buf[pos++];
	}

	public final Byte readByteBox() throws IOException
	{
		byte b = readByte();

		if (b == NULL_TAG)
			return null;

		if (b == REPEAT_TAG)
			b = readByte();

		return Byte.valueOf(b);
	}

	@Override
	public final int readUnsignedByte() throws IOException
	{
		return readByte() & 0xFF;
	}

	@Override
	public final short readShort() throws IOException
	{
		if (pos + 2 > count)
			raiseEOF();

		int firstByte = buf[pos] & 0xFF;
		int secondByte = buf[pos + 1] & 0xFF;

		pos += 2;
		return (short) ((firstByte << 8) + secondByte);
	}

	public final Short readShortBox() throws IOException
	{
		int firstByte = readUnsignedByte();
		if (firstByte == NULL_TAG)
			return null;

		if (firstByte == REPEAT_TAG)
			return readShort();

		int secondByte = readUnsignedByte();

		return (short) ((firstByte << 8) + secondByte);
	}

	@Override
	public final int readUnsignedShort() throws IOException
	{
		return readShort() & 0xFFFF;
	}
	
	public final int read3byter() throws IOException
	{
		if (pos + 3 > count)
			raiseEOF();

		int firstByte = buf[pos] & 0xFF;
		int secondByte = buf[pos + 1] & 0xFF;
		int thirdByte = buf[pos + 2] & 0xFF;

		pos += 3;
		return (firstByte << 16) + (secondByte << 8) + thirdByte + (firstByte < 128 ? 0 : (-1 << 24));  
	}

	@Override
	public final int readInt() throws IOException
	{
		if (pos + 4 > count)
			raiseEOF();

		int firstByte = buf[pos] & 0xFF;
		int secondByte = buf[pos + 1] & 0xFF;
		int thirdByte = buf[pos + 2] & 0xFF;
		int fourthByte = buf[pos + 3] & 0xFF;

		pos += 4;
		return (firstByte << 24) + (secondByte << 16) + (thirdByte << 8) + fourthByte;
	}

	public final Integer readIntBox() throws IOException
	{
		int firstByte = readUnsignedByte();
		if (firstByte == NULL_TAG)
			return null;

		if (firstByte == REPEAT_TAG)
			return readInt();

		if (pos + 3 > count)
			raiseEOF();

		int secondByte = buf[pos] & 0xFF;
		int thirdByte = buf[pos + 1] & 0xFF;
		int fourthByte = buf[pos + 2] & 0xFF;
		pos += 3;

		return (firstByte << 24) + (secondByte << 16) + (thirdByte << 8) + fourthByte;
	}

	@Override
	public final long readLong() throws IOException
	{
		if (pos + 8 > count)
			raiseEOF();

		long firstByte = buf[pos] & 0xFF;
		long secondByte = buf[pos + 1] & 0xFF;
		long thirdByte = buf[pos + 2] & 0xFF;
		long fourthByte = buf[pos + 3] & 0xFF;
		long fifthByte = buf[pos + 4] & 0xFF;
		int sixthByte = buf[pos + 5] & 0xFF;
		int seventhByte = buf[pos + 6] & 0xFF;
		int eigthByte = buf[pos + 7] & 0xFF;

		pos += 8;
		return (firstByte << 56) + (secondByte << 48) + (thirdByte << 40) + (fourthByte << 32)
				+ (fifthByte << 24) + (sixthByte << 16) + (seventhByte << 8) + eigthByte;
	}

	public final Long readLongBox() throws IOException
	{
		int firstByte = readUnsignedByte();
		if (firstByte == NULL_TAG)
			return null;

		if (firstByte == REPEAT_TAG)
			return readLong();

		if (pos + 7 > count)
			raiseEOF();

		int secondByte = buf[pos] & 0xFF;
		int thirdByte = buf[pos + 1] & 0xFF;
		int fourthByte = buf[pos + 2] & 0xFF;
		int fifthByte = buf[pos + 3] & 0xFF;
		int sixthByte = buf[pos + 4] & 0xFF;
		int seventhByte = buf[pos + 5] & 0xFF;
		int eigthByte = buf[pos + 6] & 0xFF;
		pos += 7;

		return ((long) firstByte << 56) + ((long) secondByte << 48) + ((long) thirdByte << 40)
				+ ((long) fourthByte << 32) + ((long) fifthByte << 24) + (sixthByte << 16)
				+ (seventhByte << 8) + eigthByte;
	}

	@Override
	public final float readFloat() throws IOException
	{
		return Float.intBitsToFloat(readInt());
	}

	public final Float readFloatBox() throws IOException
	{
		if (pos >= count)
			raiseEOF();

		int firstByte = buf[pos] & 0xFF;
		if (firstByte == NULL_TAG)
		{
			pos++;
			return null;
		}

		if (firstByte == REPEAT_TAG)
			pos++;

		return Float.intBitsToFloat(readInt());
	}

	@Override
	public final double readDouble() throws IOException
	{
		return Double.longBitsToDouble(readLong());
	}

	public final Double readDoubleBox() throws IOException
	{
		if (pos >= count)
			raiseEOF();

		int firstByte = buf[pos] & 0xFF;
		if (firstByte == NULL_TAG)
		{
			pos++;
			return null;
		}

		if (firstByte == REPEAT_TAG)
			pos++;

		return Double.longBitsToDouble(readLong());
	}

	@Override
	public final boolean readBoolean() throws IOException
	{
		return readUnsignedByte() != 0;
	}

	public final Boolean readBooleanBox() throws IOException
	{
		int firstByte = readUnsignedByte();
		if (firstByte == NULL_TAG)
			return null;
		return firstByte != 0 ? Boolean.TRUE : Boolean.FALSE;
	}

	@Override
	public final char readChar() throws IOException
	{
		return (char) readUnsignedShort();
	}

	public final Character readCharBox() throws IOException
	{
		int firstByte = readUnsignedByte();
		if (firstByte == NULL_TAG)
			return null;

		if (firstByte == REPEAT_TAG)
			return readChar();

		int secondByte = readUnsignedByte();

		char ret = (char) ((firstByte << 8) + secondByte);
		return Character.valueOf(ret);
	}

	@Override
	public final String readLine() throws IOException
	{
		int start = pos;
		int end;

		loop: while (true)
		{
			switch (read())
			{
			case -1:
				if (start == pos)
					return null;
				end = pos;
				break loop;

			case '\n':
				end = pos - 1;
				break loop;

			case '\r':
				end = pos - 1;
				if (pos + 1 < count)
					if ((buf[pos] & 0xFF) == '\n')
						pos++;
				break loop;
			}
		}

		char[] v = new char[end - start];
		for (int i = start; i < end; i++)
			v[i] = (char) (buf[i] & 0xFF);
		return new String(v);
	}

	@Override
	public final String readUTF() throws IOException
	{
		int utflen = readUnsignedShort();
		if (pos + utflen > count)
			raiseEOF();

		int start = pos;
		pos += utflen;
		return new String(buf, start, utflen, "UTF-8");
	}

	public final String readNormalString() throws IOException
	{
		int utflen = readInt();
		if (pos + utflen > count)
			raiseEOF();

		int start = pos;
		pos += utflen;
		return new String(buf, start, utflen, "UTF-8");
	}

	public final String readDeflatedString() throws IOException
	{
		pos++;
		int utflen = readInt();
		int compressedSize = readInt();

		if (pos + compressedSize > count)
			raiseEOF();

		try
		{
			byte[] bytes = new byte[utflen];
			Inflater inflater = new Inflater(true);
			inflater.setInput(buf, pos, compressedSize);

			int uncompressed = inflater.inflate(bytes);
			inflater.end();
			if (uncompressed != utflen)
				throw new IOException();

			pos += compressedSize;
			return new String(bytes, "UTF-8");
		}
		catch (DataFormatException e)
		{
			throw new IOException(e);
		}
	}

	public final String readString() throws IOException
	{
		if (pos >= count)
			raiseEOF();

		int firstByte = buf[pos] & 0xFF;
		if (firstByte == NULL_TAG)
		{
			pos++;
			return null;
		}

		if (firstByte == DEFLATED_TAG)
		{
			return readDeflatedString();
		}

		if (firstByte == REPEAT_TAG)
			pos++;

		return readNormalString();
	}

	public final TCID readTCID() throws IOException
	{
		if (pos >= count)
			raiseEOF();

		int firstByte = buf[pos] & 0xFF;
		if (firstByte == NULL_TAG)
		{
			pos++;
			return null;
		}

		if (firstByte == REPEAT_TAG)
			pos++;

		long msb = readLong();
		long lsb = readLong();

		return new TCID(msb, lsb);
	}

	public final TCID readTCIDNotNull() throws IOException
	{
		long msb = readLong();
		long lsb = readLong();
		return new TCID(msb, lsb);
	}

	// public final long readId() throws IOException
	// {
	// int firstByte = readUnsignedByte();
	// if (firstByte == 0)
	// {
	// return 0;
	// }
	//
	// if ((firstByte & 0x80) == 0)
	// return -firstByte;
	//
	// int bytes = Integer.numberOfLeadingZeros(~firstByte) + 1;
	// long bits = firstByte & (0xFF >> bytes);
	//
	// for (int i = 0; i < bytes - 1; i++)
	// {
	// bits <<= 8;
	// bits |= readUnsignedByte();
	// }
	//
	// int highestFourBits = (int) (bits >>> (7 * bytes - 4));
	// if (highestFourBits == 0xF)
	// {
	// // negative
	// bits &= ~(-1L << (7 * bytes - 4));
	// return -(bits + SharedObjectOutputStream.negativeThresholds[bytes] + 1);
	// }
	// else
	// {
	// // positive
	// return bits + SharedObjectOutputStream.positiveThresholds[bytes] + 1;
	// }
	// }

	public final int readId() throws IOException
	{
		int firstByte = readUnsignedByte();
		if (firstByte == ID_NULL_TAG)
			return 0;

		if (pos + 3 > count)
			raiseEOF();

		int secondByte = buf[pos] & 0xFF;
		int thirdByte = buf[pos + 1] & 0xFF;
		int fourthByte = buf[pos + 2] & 0xFF;
		pos += 3;

		return (firstByte << 24) + (secondByte << 16) + (thirdByte << 8) + fourthByte;
	}

	public final BaseTransactionObject readSharedObjectReference() throws IOException
	{
		int id = readId();
		if (id == 0)
			return null;

		return getHandle(id);
	}

	public final BaseTransactionObject readSharedObjectReferenceNotNull() throws IOException
	{
		int id = readInt();

		return getHandle(id);
	}

	public final BaseTransactionObject readSharedObjectReference3byterNotNull() throws IOException
	{
		int id = read3byter();

		return getHandle(id);
	}

	public final BaseTransactionObject readSharedObjectReferenceShortNotNull() throws IOException
	{
		int id = readShort();

		return getHandle(id);
	}

	private BaseTransactionObject getHandle(int id)
	{
		if (id >= 0)
			return GlobalTransactionManager.getInstance().getSharedObjectLibrary().getHandle(id);
		else
			return GlobalTransactionManager.getInstance().getSharedObjectLibrary().getNewHandle(id);
	}
	
	public final void correctId(int id)
	{
		buf[pos - 4] = (byte) (id >>> 24);
		buf[pos - 3] = (byte) ((id >>> 16) & 0xFF);
		buf[pos - 2] = (byte) ((id >>> 8) & 0xFF);
		buf[pos - 1] = (byte) (id & 0xFF);
	}

	@Override
	public final Object readObject() throws ClassNotFoundException, IOException
	{
		int firstByte = readUnsignedByte();
		switch (firstByte)
		{
		case NULL_TAG:
			return null;

		case ID_TAG:
			return readSharedObjectReferenceNotNull();

		case ID3_TAG:
			return readSharedObjectReference3byterNotNull();

		case ID2_TAG:
			return readSharedObjectReferenceShortNotNull();

		case STRING_TAG:
			return readNormalString();
			
		case BYTE_TAG:
			return readByte();

		case SHORT_TAG:
			return readShort();

		case INTEGER_TAG:
			return readInt();

		case LONG_TAG:
			return readLong();

		case FLOAT_TAG:
			return readFloat();

		case DOUBLE_TAG:
			return readDouble();

		case BOOLEAN_TRUE_TAG:
			return Boolean.TRUE;

		case BOOLEAN_FALSE_TAG:
			return Boolean.FALSE;

		case OTHER_TAG:
			ObjectInputStream ois = new ObjectInputStream(this);
			Object ret = ois.readObject();
			ois.close();
			return ret;

		default:
			throw new IOException("unexpected tag");
		}
	}

	@Override
	public final int available()
	{
		return count - pos;
	}

	@Override
	public final long skip(long n) throws IOException
	{
		long k = count - pos;
		if (n < k)
		{
			k = n < 0 ? 0 : n;
		}

		pos += k;
		return k;
	}

	@Override
	public final int skipBytes(int n) throws IOException
	{
		return (int) skip(n);
	}

	public final void mark()
	{
		mark = pos;
	}

	public final byte[] getBytesFromMark(byte[] b)
	{
		int n = pos - mark;
		byte[] ret = (b != null && b.length >= n) ? b : new byte[n];
		System.arraycopy(buf, mark, ret, 0, n);
		return ret;
	}

	protected final void raiseEOF() throws EOFException
	{
		pos = count;
		throw new EOFException();
	}
}
