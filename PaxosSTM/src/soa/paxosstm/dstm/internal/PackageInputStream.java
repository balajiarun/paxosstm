package soa.paxosstm.dstm.internal;

import java.io.IOException;

import soa.paxosstm.dstm.internal.instrumentation.SharedObjectInputStream;

public class PackageInputStream extends SharedObjectInputStream
{
	private int uncompletedByte;
	// FIXME
	public int freeBits;

	public PackageInputStream(byte[] buf)
	{
		super(buf);
		uncompletedByte = -1;
	}

	public PackageInputStream(byte[] buf, int off, int len)
	{
		super(buf, off, len);
		uncompletedByte = off - 1;
	}

	public PackageInputStream(PackageInputStream in, int len)
	{
		super(in.buf, in.pos, Math.min(in.pos + len, in.count));
		uncompletedByte = in.uncompletedByte;
		freeBits = in.freeBits;
	}

	public final int readBits(int bits) throws IOException
	{
		if (uncompletedByte != pos - 1)
		{
			freeBits = 0;
		}
		if (pos + ((bits - freeBits + 7) / 8) > count)
			raiseEOF();

		int v = 0;

		if (freeBits > 0)
		{
			int u = buf[uncompletedByte] & (-1 >>> (32 - freeBits));
			if (freeBits >= bits)
			{
				freeBits -= bits;
				return u >>> freeBits;
			}

			v |= u;
			bits -= freeBits;
			freeBits = 0;
		}

		while (bits >= 8)
		{
			v <<= 8;
			v |= buf[pos++] & 0xFF;
			bits -= 8;
		}

		if (bits > 0)
		{
			freeBits = 8 - bits;
			v <<= bits;
			v |= (buf[uncompletedByte = pos++] & 0xFF) >>> freeBits;
		}

		return v;
	}

	public final long readLongBits(int bits) throws IOException
	{
		if (uncompletedByte != pos - 1)
		{
			freeBits = 0;
		}
		if (pos + ((bits - freeBits + 7) / 8) > count)
			raiseEOF();

		long v = 0;

		if (freeBits > 0)
		{
			int u = buf[uncompletedByte] & (-1 >>> (32 - freeBits));
			if (freeBits >= bits)
			{
				freeBits -= bits;
				return u >>> freeBits;
			}

			v |= u;
			bits -= freeBits;
			freeBits = 0;
		}

		while (bits >= 8)
		{
			v <<= 8;
			v |= buf[pos++] & 0xFF;
			bits -= 8;
		}

		if (bits > 0)
		{
			freeBits = 8 - bits;
			v <<= bits;
			v |= (buf[uncompletedByte = pos++] << freeBits) & 0xFF;
		}

		return v;
	}

	public final void completeByte() throws IOException
	{
		freeBits = 0;
	}

	public final void readIdSet(int tab[]) throws IOException
	{
		int bits = (readBits(5) + 1) & 31;

		for (int i = 0; i < tab.length; i++)
			tab[i] = readBits(bits);
	}
	
	public final void readIdSetWithExceptions(int tab[]) throws IOException
	{
		int n = tab.length;
		
		int rsize = readBits(32);
		int maxBits = 32 - Integer.numberOfLeadingZeros(rsize);
		
		int[] order = new int[rsize];
		for (int i=0; i<rsize; i++)
			order[i] = readBits(maxBits);
		int bits = (readBits(5) + 1) & 31;

		for (int i = 0; i < n; i++)
		{
			int x = readBits(bits);
			if (x == 0)
			{
				x = readBits(maxBits);
			}
			else
			{
				x--;
			}
			tab[i] = order[x];
		}
	}

	public final void readIdSetSorted(int tab[]) throws IOException
	{
		int bits = (readBits(5) + 1) & 31;

		int acc = 0;
		for (int i = 0; i < tab.length; i++)
			tab[i] = acc += readBits(bits);
	}

	public final void readIdSetSortedWithExceptions(int tab[]) throws IOException
	{
		int bits = (readBits(5) + 1) & 31;
		int maxBits = (readBits(5) + 1) & 31;

		readIdSetSortedWithExceptions(tab, tab.length, 0, bits, maxBits);
	}

	public final int readIdSetSortedWithExceptions(int tab[], int n, int acc, int bits, int maxBits) throws IOException
	{
		final int diff = maxBits - bits - 1;

		for (int i = 0; i < n; i++)
		{
			int y;
			int x = readBits(bits);
			if (x <= diff)
			{
				y = readBits(bits + x + 1) + 1;
			}
			else
			{
				y = x - diff;
			}
			tab[i] = acc += y;
		}
		
		return acc;
	}

	public final void readIdSetSortedWith3States(int tab[]) throws IOException
	{
		int bits1 = (readBits(5) + 1) & 31;
		int bits2 = (readBits(5) + 1) & 31;
		int maxBits = (readBits(5) + 1) & 31;

		readIdSetSortedWith3States(tab, tab.length, 0, bits1, bits2, maxBits);
	}
	
	public final int readIdSetSortedWith3States(int tab[], int n, int acc, int bits1, int bits2, int maxBits) throws IOException
	{
		final int range1 = (1 << bits1);
		final int range2 = (1 << bits2);
		final int bitDiff = maxBits - bits2;
		final int diff = range2 + range1 - bitDiff - 1;

		for (int i = 0; i < n; i++)
		{
			int x = readBits(bits1);
			if (x == 0)
			{
				x = readBits(bits2);
				if (x < bitDiff)
				{
					x = readBits(bits2 + x + 1) + diff;
				}
				else
				{
					x -= bitDiff;
					x += range1 - 1;
				}
			}
			else
			{
				x--;
			}
			tab[i] = acc += x + 1;
		}
		
		return acc;
	}
}
