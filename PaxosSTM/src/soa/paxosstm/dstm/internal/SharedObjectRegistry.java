package soa.paxosstm.dstm.internal;

import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager;
import soa.paxosstm.utils.TransactionalHashTable;

public class SharedObjectRegistry
{
	private final static int HASHTABLE_CAPACITY = 1024;

	public static TransactionalHashTable<String, BaseTransactionObject> registry;

	public static void init()
	{
		new Transaction()
		{
			protected void atomic()
			{
				registry = new TransactionalHashTable<String, BaseTransactionObject>(
						HASHTABLE_CAPACITY);

				LocalTransactionManager.localCommit();
			}
		};
	}

	@SuppressWarnings("unchecked")
	public static void refresh()
	{
		registry = (TransactionalHashTable<String, BaseTransactionObject>) ((Object) GlobalTransactionManager
				.getInstance().getSharedObjectLibrary().getHandle(1));
	}
}
