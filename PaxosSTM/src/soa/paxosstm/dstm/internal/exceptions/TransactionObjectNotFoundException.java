package soa.paxosstm.dstm.internal.exceptions;



public class TransactionObjectNotFoundException extends
		TransactionCorruptedException
{
	public TransactionObjectNotFoundException()
	{
	}

	public TransactionObjectNotFoundException(String message)
	{
		super(message);
	}

	public TransactionObjectNotFoundException(Throwable cause)
	{
		super(cause);
	}

	public TransactionObjectNotFoundException(String message, Throwable cause)
	{
		super(message, cause);
	}

	private static final long serialVersionUID = 1L;
}
