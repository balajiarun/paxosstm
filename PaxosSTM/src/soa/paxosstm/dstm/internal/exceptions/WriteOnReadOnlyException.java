/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.exceptions;

/**
 * Exception thrown when an operation on a shared object is performed while
 * there is no transaction in progress.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class WriteOnReadOnlyException extends InvalidOperationException
{
	public WriteOnReadOnlyException()
	{
	}

	public WriteOnReadOnlyException(String message)
	{
		super(message);
	}

	public WriteOnReadOnlyException(Throwable cause)
	{
		super(cause);
	}

	public WriteOnReadOnlyException(String message, Throwable cause)
	{
		super(message, cause);
	}

	private static final long serialVersionUID = 9033569496743457903L;
}
