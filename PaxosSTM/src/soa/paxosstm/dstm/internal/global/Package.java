package soa.paxosstm.dstm.internal.global;

import java.io.IOException;

import soa.paxosstm.dstm.internal.PackageInputStream;
import soa.paxosstm.dstm.internal.PackageOutputStream;

public abstract class Package
{
	public enum PackageType
	{
		TRANSACTION, GARBAGE, BARRIER
	}

	public static PackageType getPackageType(PackageInputStream in) throws IOException
	{
		return PackageType.values()[in.readByte()];
	}

	public final byte[] serialize() throws IOException
	{
		PackageOutputStream pos = new PackageOutputStream();
		serialize(pos);
		return pos.toByteArray();
	}

	protected abstract void serialize(PackageOutputStream output) throws IOException;

	protected final void writePackageType(PackageOutputStream output, PackageType packageType) throws IOException
	{
		output.write(packageType.ordinal());
	}
}
