package soa.paxosstm.dstm.internal.global;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import soa.paxosstm.dstm.internal.PackageInputStream;
import soa.paxosstm.dstm.internal.PackageOutputStream;
import soa.paxosstm.dstm.internal.instrumentation.SharedObjectInputStream;
import soa.paxosstm.dstm.internal.local.CommitDescriptor;
import soa.paxosstm.dstm.internal.local.ShadowCopyMap;
import soa.paxosstm.utils.TCID;

/**
 * Class which objects are passed between GTMs and which contain all the
 * necessary information about transaction so each GTM can independently perform
 * a certification process.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public final class TransactionPackage extends Package
{
	private static final int READ_SET_NO_COMPRESSION_THRESHOLD = 4;
	private static final int READ_SET_EXCEPTIONS_COMPRESSION_THRESHOLD = 128;
	private static final int WRITE_SET_NO_COMPRESSION_THRESHOLD = 4;
	private static final int WRITE_SET_SIMPLE_COMPRESSION_THRESHOLD = 16;
	private static final int WRITE_SET_EXCEPTIONS_COMPRESSION_THRESHOLD = 128;
	private static final int NEW_SET_NO_COMPRESSION_THRESHOLD = 4;
	private static final int NEW_SET_SIMPLE_COMPRESSION_THRESHOLD = 128;

	public static final int WRITE_SET_SORT_THRESHOLD = WRITE_SET_SIMPLE_COMPRESSION_THRESHOLD;

	CommitDescriptor desc;

	int localId;
	TCID id;

	private long highestVersion;
	private int readSetSize;
	private int writeSetSize;
	private int newSetSize;
	private int typesSetSize;

	private int[] writeSet;

	private PackageInputStream in;
	private int readSetRead;

	private int readSetBits;
	private int readSetBits2;
	private int readSetMaxBits;
	private int readSetAcc;

	public TransactionPackage(CommitDescriptor desc, int localId)
	{
		this.desc = desc;
		this.localId = localId;
		id = desc.getId();
		highestVersion = desc.getHighestVersion();
		readSetSize = desc.getReadSetSize();
		writeSetSize = desc.getWriteSetSize();
		newSetSize = desc.getNewSetSize();
		typesSetSize = desc.getTypesSetSize();
	}

	public TransactionPackage(PackageInputStream in) throws IOException
	{
		this.in = in;
		localId = in.readUnsignedShort();
		id = in.readTCIDNotNull();
	}

	public void readHeader() throws IOException
	{
		highestVersion = in.readLong();
		readSetSize = in.readInt();
		writeSetSize = in.readInt();
		newSetSize = in.readInt();
		if (newSetSize > 0)
			typesSetSize = in.readInt();
		else
			typesSetSize = 0;
	}

	public long getHighestVersion()
	{
		return highestVersion;
	}

	public int getNumberOfUpdates()
	{
		return writeSetSize + newSetSize;
	}

	public int readReadSet(int[] buf) throws IOException
	{
		if (readSetSize <= READ_SET_NO_COMPRESSION_THRESHOLD)
		{
			int n = Math.min(buf.length, readSetSize - readSetRead);
			for (int i = 0; i < n; i++)
			{
				buf[i] = in.readInt();
			}
			readSetRead += n;
			return n;
		}
		else if (readSetSize <= READ_SET_EXCEPTIONS_COMPRESSION_THRESHOLD)
		{
			if (readSetRead == 0)
			{
				readSetAcc = 0;
				readSetBits = (in.readBits(5) + 1) & 31;
				readSetMaxBits = (in.readBits(5) + 1) & 31;
			}
			int n = Math.min(buf.length, readSetSize - readSetRead);
			readSetAcc = in.readIdSetSortedWithExceptions(buf, n, readSetAcc, readSetBits,
					readSetMaxBits);
			readSetRead += n;
			return n;
		}
		else
		{
			if (readSetRead == 0)
			{
				readSetAcc = 0;
				readSetBits = (in.readBits(5) + 1) & 31;
				readSetBits2 = (in.readBits(5) + 1) & 31;
				readSetMaxBits = (in.readBits(5) + 1) & 31;
			}
			int n = Math.min(buf.length, readSetSize - readSetRead);
			readSetAcc = in.readIdSetSortedWith3States(buf, n, readSetAcc, readSetBits,
					readSetBits2, readSetMaxBits);
			readSetRead += n;
			return n;
		}
	}

	public int[] readReadSet() throws IOException
	{
		int[] ret = new int[readSetSize];
		readReadSet(ret);
		return ret;
	}

	public int[] readWriteSet() throws IOException
	{
		if (writeSetSize <= WRITE_SET_NO_COMPRESSION_THRESHOLD)
		{
			int[] ret = new int[writeSetSize];
			for (int i = 0; i < writeSetSize; i++)
				ret[i] = in.readInt();
			return writeSet = ret;
		}
		else
		{
			int[] ret = new int[writeSetSize];
			if (writeSetSize <= WRITE_SET_SIMPLE_COMPRESSION_THRESHOLD)
			{
				in.readIdSet(ret);
			}
			else if (writeSetSize <= WRITE_SET_EXCEPTIONS_COMPRESSION_THRESHOLD)
			{
				in.readIdSetSortedWithExceptions(ret);
			}
			else
			{
				in.readIdSetSortedWith3States(ret);
			}
			return writeSet = ret;
		}
	}

	public int[] getWriteSet()
	{
		return writeSet;
	}

	public int[] readTypeIds() throws IOException
	{
		if (newSetSize <= NEW_SET_NO_COMPRESSION_THRESHOLD)
		{
			int[] ret = new int[newSetSize];
			for (int i = 0; i < newSetSize; i++)
				ret[i] = in.readInt();
			return ret;
		}
		else if (newSetSize <= NEW_SET_SIMPLE_COMPRESSION_THRESHOLD)
		{
			int[] ret = new int[newSetSize];
			in.readIdSet(ret);
			if (typesSetSize > 0)
				for (int i = 0; i < newSetSize; i++)
					ret[i] -= typesSetSize;
			return ret;
		}
		else
		{
			int[] ret = new int[newSetSize];
			in.readIdSetWithExceptions(ret);
			if (typesSetSize > 0)
				for (int i = 0; i < newSetSize; i++)
					ret[i] -= typesSetSize;
			return ret;
		}
	}

	public String[] readTypesSet() throws IOException
	{
		if (typesSetSize > 0)
		{
			String[] ret = new String[typesSetSize];
			for (int i = 0; i < typesSetSize; i++)
				ret[i] = in.readUTF();
			return ret;
		}
		return null;
	}

	public SharedObjectInputStream getUpdates()
	{
		return in;
	}

	@Override
	protected void serialize(PackageOutputStream out) throws IOException
	{
		writePackageType(out, PackageType.TRANSACTION);

		out.writeShort(localId);
		out.writeTCIDNotNull(id);

		out.writeLong(highestVersion);

		out.writeInt(readSetSize);
		out.writeInt(writeSetSize);
		out.writeInt(newSetSize);
		if (newSetSize > 0)
			out.writeInt(typesSetSize);

		int[] array = null;

		if (readSetSize <= READ_SET_NO_COMPRESSION_THRESHOLD)
		{
			for (ShadowCopyMap.Entry entry = desc.getReadSet(); entry != null; entry = entry
					.getNext())
			{
				out.writeInt(entry.getId());
			}
		}
		else
		{
			array = new int[Math.max(readSetSize, writeSetSize)];
			int counter = 0;
			for (ShadowCopyMap.Entry entry = desc.getReadSet(); entry != null; entry = entry
					.getNext())
			{
				array[counter++] = entry.getId();
			}
			Arrays.sort(array, 0, counter);

			if (readSetSize <= READ_SET_EXCEPTIONS_COMPRESSION_THRESHOLD)
				out.writeIdSetSortedWithExceptions(array, counter);
			else
				out.writeIdSetSortedWith3States(array, counter);
		}

		if (writeSetSize <= WRITE_SET_NO_COMPRESSION_THRESHOLD)
		{
			for (ShadowCopyMap.Entry entry = desc.getWriteSet(); entry != null; entry = entry
					.getNext())
			{
				out.writeInt(entry.getId());
			}
		}
		else if (writeSetSize <= WRITE_SET_SIMPLE_COMPRESSION_THRESHOLD)
		{
			if (array == null)
				array = new int[writeSetSize];

			int counter = 0;
			int max = 0;
			for (ShadowCopyMap.Entry entry = desc.getWriteSet(); entry != null; entry = entry
					.getNext())
			{
				int id = entry.getId();
				array[counter++] = id;
				if (id > max)
					max = id;
			}
			out.writeIdSet(array, counter, 32 - Integer.numberOfLeadingZeros(max));
		}
		else
		{
			if (array == null)
				array = new int[writeSetSize];

			desc.sortWriteSet();

			int counter = 0;
			for (ShadowCopyMap.Entry entry = desc.getWriteSet(); entry != null; entry = entry
					.getNext())
			{
				array[counter++] = entry.getId();
			}

			if (writeSetSize <= WRITE_SET_EXCEPTIONS_COMPRESSION_THRESHOLD)
				out.writeIdSetSortedWithExceptions(array, counter);
			else
				out.writeIdSetSortedWith3States(array, counter);
		}

		if (newSetSize <= NEW_SET_NO_COMPRESSION_THRESHOLD)
		{
			for (ShadowCopyMap.Entry entry = desc.getNewSet(); entry != null; entry = entry
					.getNext())
			{
				out.writeInt(entry.getTypeId());
			}
		}
		else if (newSetSize <= NEW_SET_SIMPLE_COMPRESSION_THRESHOLD)
		{
			int max = 0;
			for (ShadowCopyMap.Entry entry = desc.getNewSet(); entry != null; entry = entry
					.getNext())
			{
				int id = entry.getTypeId() + typesSetSize;
				if (id > max)
					max = id;
			}
			int maxBits = 32 - Integer.numberOfLeadingZeros(max);
			out.writeBits(maxBits - 1, 5);
			for (ShadowCopyMap.Entry entry = desc.getNewSet(); entry != null; entry = entry
					.getNext())
			{
				int id = entry.getTypeId() + typesSetSize;
				out.writeBits(id, maxBits);
			}
		}
		else
		{
			if (array == null || array.length < newSetSize)
				array = new int[newSetSize];

			int counter = 0;
			int max = 0;
			for (ShadowCopyMap.Entry entry = desc.getNewSet(); entry != null; entry = entry
					.getNext())
			{
				int id = entry.getTypeId() + typesSetSize;
				array[counter++] = id;
				if (id > max)
					max = id;
			}

			out.writeIdSetWithExceptions(array, counter, max);
		}

		List<String> typesSet = desc.getTypesSet();
		if (typesSet != null)
		{
			for (String type : typesSet)
			{
				out.writeUTF(type);
			}
		}

		for (ShadowCopyMap.Entry entry = desc.getNewSet(); entry != null; entry = entry.getNext())
		{
			out.writeSharedObject(entry.getValue());
		}

		for (ShadowCopyMap.Entry entry = desc.getWriteSet(); entry != null; entry = entry.getNext())
		{
			out.writeSharedObject(entry.getValue());
		}
	}
}
