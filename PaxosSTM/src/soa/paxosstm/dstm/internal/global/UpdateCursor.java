package soa.paxosstm.dstm.internal.global;

import java.util.Iterator;

public interface UpdateCursor extends Iterator<int[]>
{
	UpdateCursor clone();
	long getVersion();
	void setVersion(long version);
	void disable();
}
