/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.global;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import soa.paxosstm.abcast.AtomicBroadcast;
import soa.paxosstm.abcast.AtomicDeliverListener;
import soa.paxosstm.common.ByteArrayStorage;
import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.Checkpointable;
import soa.paxosstm.common.RecoveryListener;
import soa.paxosstm.common.Storage;
import soa.paxosstm.common.StorageException;
import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.internal.PackageInputStream;
import soa.paxosstm.dstm.internal.SharedObjectRegistry;
import soa.paxosstm.dstm.internal.TransactionState;
import soa.paxosstm.dstm.internal.global.Package.PackageType;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.dstm.internal.local.CommitDescriptor;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager;
import soa.paxosstm.dstm.internal.local.ReadWriteTransactionHandler;
import soa.paxosstm.utils.Pair;
import soa.paxosstm.utils.TCID;

/**
 * GlobalTransactionManager (GTM) is the central class of the Paxos STM system.
 * It processes incoming messages, dispatches new messages, manages
 * {@link SharedObjectLibrary}, <i>Shared Object Registry</i>, {@link Storage}
 * and all the {@link LocalTransactionManager}s.
 * <p>
 * A part of the GTM is so called Certification Unit (CU), where transaction
 * certification algorithms are confined. Here also is implemented the Shared
 * Object Registry (SOR). It is a replicated, fault tolerant mechanism built
 * entirely on top of the ABcast mechanism which allows associating names (of
 * the type of {@link String} with shared objects. It allows easier access to
 * often accessed shared objects as well as synchronization of computing on
 * different nodes.
 * <p>
 * Reorder List is an optimization in CU which aims at decreasing the number of
 * transactions which have to be restarted due to conflicts between
 * transactions. This optimization is based on a observation that sometimes
 * changing the order in which transactions would be normally committed (due to
 * for example the order in which certification requests arrived) may result in
 * no need for restarting either of the transactions. Therefore, before a
 * transaction is committed it first waits for a while in the so called reorder
 * list so it may be 'overtaken' by other transaction so either of them would
 * not be restarted. In order to guarantee that a transaction would not wait
 * forever on the reorder list a special message can be issued to force the
 * transaction off the reorder list. Also the size of the reorder list can be
 * changed in runtime.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public final class GlobalTransactionManager implements CheckpointListener, RecoveryListener,
		AtomicDeliverListener, Checkpointable
{
	private static final int GARBAGE_PROPOSAL_THRESHOLD = 128;

	private static final String INNER = "inner";
	private static final String CURRENT_BAR = "currentBar";
	private static final String SHARED_OBJECT_LIBRARY = "sharedObjectLibrary";

	private static GlobalTransactionManager instance = null;

	private volatile boolean mgrReady = false;
	private final Object lock = new Object();
	private final int localId;
	// private final int numberOfReplicas;

	private final AtomicBroadcast abcast;
	private final CopyOnWriteArrayList<CheckpointListener> checkpointListeners = new CopyOnWriteArrayList<CheckpointListener>();
	private final CopyOnWriteArrayList<RecoveryListener> recoveryListeners = new CopyOnWriteArrayList<RecoveryListener>();
	private final Map<TCID, ReadWriteTransactionHandler> localHandlers = new ConcurrentHashMap<TCID, ReadWriteTransactionHandler>();

	private long currentBar;
	private SharedObjectLibrary sharedObjectLibrary = new SharedObjectLibrary();
	private UpdateManager updateManager;

	private final int[] readSetBuffer = new int[512];

	// TODO: add to recovery
	private Map<String, Pair<Integer, Integer>> barrierMap = new HashMap<String, Pair<Integer, Integer>>();

	/**
	 * Returns the instance of the GlobalTransactionManager.
	 * 
	 * @return the instance of Global Transaction Manager
	 */
	public static GlobalTransactionManager getInstance()
	{
		return instance;
	}

	/**
	 * Sets the GlobalTransactionManager instance.
	 * 
	 * @param instance
	 *            reference to the new GlobalTransactionManager to act as the
	 *            instance of the GlobalTransactionManager for the process
	 */
	public static void setInstance(GlobalTransactionManager instance)
	{
		GlobalTransactionManager.instance = instance;
	}

	/**
	 * Constructor.
	 * 
	 * @param localId
	 *            the replica number
	 * @param numberOfReplicas
	 *            number of replicas
	 * @param abcast
	 *            reference to the Atomic Broadcast module main object
	 * @param storage
	 *            reference to the Storage object
	 */
	public GlobalTransactionManager(int localId, int numberOfReplicas, AtomicBroadcast abcast)
	{
		this.localId = localId;
		// this.numberOfReplicas = numberOfReplicas;
		this.abcast = abcast;

		this.currentBar = 0;
		this.updateManager = new UpdateManager(currentBar);
		// updateManager.initGarbageThread();

		abcast.addAtomicDeliverListener(this);
		abcast.addCheckpointListener(this);
		abcast.addRecoveryListener(this);
	}

	/**
	 * Starts the module implementing the interface. This method is necessary
	 * since the process of starting a stack of protocols has to be properly
	 * coordinated.
	 * 
	 * @throws StorageException
	 *             when the storage is unoperational
	 */
	public void start() throws StorageException
	{
		abcast.start();
	}

	private void waitForMgrReady()
	{
		if (!mgrReady)
		{
			synchronized (lock)
			{
				while (!mgrReady)
				{
					try
					{
						lock.wait();
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		}
	}

	@Override
	public void adeliver(byte[] data)
	{
		try
		{
			PackageInputStream batch = new PackageInputStream(data);
			while (batch.available() > 0)
			{
				int n = batch.readInt();
//				System.err.println("HGW gtm " + n);
				PackageInputStream pis = new PackageInputStream(batch, n);
				adeliverSingle(pis);
				batch.skip(n);
			}
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	private void adeliverSingle(PackageInputStream in) throws IOException
	{
		PackageType packageType = Package.getPackageType(in);

		switch (packageType)
		{
		case TRANSACTION:
			processTransactionPackage(new TransactionPackage(in));
			break;
		case GARBAGE:
			processGarbagePackage(new GarbagePackage(in));
			break;
		case BARRIER:
			processBarrierPackage(new BarrierPackage(in));
			break;
		default:
			throw new RuntimeException("Unknown package type.");
		}

		Set<Integer> garbageProposals = sharedObjectLibrary.getGcProposal();
		if (garbageProposals.size() > GARBAGE_PROPOSAL_THRESHOLD)
		{
			GarbagePackage garbagePackage = new GarbagePackage(garbageProposals, localId);
			abcast.abcast(garbagePackage.serialize());
			garbageProposals.clear();
		}
	}

	private void processGarbagePackage(GarbagePackage garbagePackage)
	{
		int replicaId = garbagePackage.getReplicaId();
		int[] garbageProposals = garbagePackage.getGarbageProposals();
		sharedObjectLibrary.applyNewGarbageProposals(replicaId, garbageProposals);
	}

	private void processBarrierPackage(BarrierPackage barrierPackage)
	{
		synchronized (barrierMap)
		{
			Pair<Integer, Integer> pair = barrierMap.get(barrierPackage.barrierName);
			if (pair == null)
			{
				pair = new Pair<Integer, Integer>(barrierPackage.n, 0);
				barrierMap.put(barrierPackage.barrierName, pair);
			}

			Integer alreadyIn = pair.second + 1;
			pair.second = alreadyIn;
			if (alreadyIn == barrierPackage.n)
			{
				synchronized (pair)
				{
					pair.notifyAll();
				}
				barrierMap.remove(barrierPackage.barrierName);
			}
		}
	}

	public void enterBarrier(String name, int n)
	{
		if (n <= 1)
			return;

		BarrierPackage barrierPackage = new BarrierPackage(name, n);
		Pair<Integer, Integer> pair;
		synchronized (barrierMap)
		{
			pair = barrierMap.get(barrierPackage.barrierName);
			if (pair == null)
			{
				pair = new Pair<Integer, Integer>(barrierPackage.n, 0);
				barrierMap.put(barrierPackage.barrierName, pair);
			}
		}

		synchronized (pair)
		{
			try
			{
				abcast.abcast(barrierPackage.serialize());
				while (pair.first != pair.second)
					pair.wait();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
				System.exit(1);
			}
			catch (IOException e)
			{
				throw new RuntimeException(e);
			}
		}
	}

	private void processTransactionPackage(TransactionPackage transactionPackage)
			throws IOException
	{
		final boolean local = transactionPackage.localId == localId;
		ReadWriteTransactionHandler handler = local ? getTransactionHandler(transactionPackage.id)
				: null;
		CommitDescriptor descriptor = local ? handler.getCommitDescriptor() : null;

		// if (local && !PaxosSTM.DEBUG)
		// {
		// if (descriptor.validate())
		// {
		// BaseTransactionObject[] handles;
		// handles = sharedObjectLibrary.localApply(descriptor, ++currentBar);
		// int[] writeSet = descriptor.getUpdateWriteSet();
		// updateManager.add(writeSet, handles);
		// handler.transactionFinished(TransactionState.Committed);
		// }
		// else
		// {
		// handler.transactionFinished(TransactionState.Aborted);
		// }
		// return;
		// }

		long certStartTime = 0;
		long certStopTime = 0;

		if (local)
		{
			certStartTime = System.currentTimeMillis();
		}

		transactionPackage.readHeader();
		boolean validated = validate(transactionPackage);
		int[] writeSet = transactionPackage.getWriteSet();

		if (local && PaxosSTM.DEBUG)
		{
			certStopTime = System.currentTimeMillis();
		}

		if (!validated)
		{
			if (local)
			{
				if (PaxosSTM.DEBUG)
				{
					descriptor.setAbcastArrivalTime(certStartTime);
					descriptor.setValidationTime(certStopTime - certStartTime);
				}
				handler.transactionFinished(TransactionState.Aborted);
			}
			return;
		}

		int[] typeIds = transactionPackage.readTypeIds();
		String[] typesSet = transactionPackage.readTypesSet();

		BaseTransactionObject[] handles = new BaseTransactionObject[transactionPackage.getNumberOfUpdates()];
		int[] newSet;
		if (local)
		{
			newSet = sharedObjectLibrary.registerNew(descriptor, handles);
		}
		else
		{
			int[] newTypeIds = sharedObjectLibrary.registerTypes(typesSet);
			newSet = sharedObjectLibrary.createNew(typeIds, newTypeIds, handles);
		}

		sharedObjectLibrary.update(writeSet, newSet, handles, transactionPackage.getUpdates(),
				++currentBar);

		updateManager.add(writeSet, handles);

		if (local)
		{
			if (PaxosSTM.DEBUG)
			{
				descriptor.setAbcastArrivalTime(certStartTime);
				descriptor.setValidationTime(certStopTime - certStartTime);
				descriptor.setUpdateTime(System.currentTimeMillis() - certStopTime);
			}
			handler.transactionFinished(TransactionState.Committed);
		}
	}

	private boolean validate(TransactionPackage transactionPackage) throws IOException
	{
		long version = transactionPackage.getHighestVersion();

		for (int n; (n = transactionPackage.readReadSet(readSetBuffer)) != 0;)
		{
			if (!sharedObjectLibrary.validate(readSetBuffer, n, version))
			{
				return false;
			}
		}

		int[] writeSet = transactionPackage.readWriteSet();
		if (!sharedObjectLibrary.validate(writeSet, version))
		{
			return false;
		}
		return true;
	}

	/**
	 * Method called by the LTMs when attempting to commit a transaction.
	 * 
	 * @param desc
	 *            {@link TransactionDescriptorOLD} of the transaction to be
	 *            committed
	 */
	public void commitTransaction(CommitDescriptor desc)
	{
		waitForMgrReady();

		try
		{
			TransactionPackage transactionPackage = new TransactionPackage(desc, localId);
			byte[] data = transactionPackage.serialize();
			desc.setPackageSize(data.length);

			abcast.blockingAbcast(data);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	public void registerTransactionHandler(TCID id, ReadWriteTransactionHandler handler)
	{
		localHandlers.put(id, handler);
	}

	public void unregisterTransactionHandler(TCID id)
	{
		localHandlers.remove(id);
	}

	private ReadWriteTransactionHandler getTransactionHandler(TCID id)
	{
		return localHandlers.get(id);
	}

	public UpdateCursor getUpdateCursor()
	{
		return updateManager.getUpdateCursor();
	}

	public SharedObjectLibrary getSharedObjectLibrary()
	{
		return sharedObjectLibrary;
	}

	@Override
	public void onCheckpoint(int seqNumber, Storage storage)
	{
		// TODO
		// System.out.println("DSTM - onCommit");

		ByteArrayStorage innerStorage = new ByteArrayStorage();
		for (CheckpointListener listener : checkpointListeners)
			listener.onCheckpoint(seqNumber, innerStorage);

		// int commitNumber = (Integer) commitData;
		// int logVersion = commitNumber % 2;
		// int logVersion = 0;

		// try
		// {
		// storage.log(INNER + "_" + logVersion, innerStorage);
		// storage.log(CURRENT_BAR + "_" + logVersion, currentBar);
		// storage.log(SHARED_OBJECT_LIBRARY + "_" + logVersion,
		// serializeSharedObjectLibrary());
		// }
		// catch (StorageException e)
		// {
		// e.printStackTrace();
		// }
		// catch (IOException e)
		// {
		// e.printStackTrace();
		// }
	}

	@Override
	public void onCheckpointFinished(int seqNumber)
	{
		for (CheckpointListener listener : checkpointListeners)
			listener.onCheckpointFinished(seqNumber);
	}

	static class SerializedObjectInfo implements Serializable
	{
		private static final long serialVersionUID = -7798225907075761382L;

		public String classname;
		public byte[] data;

		public SerializedObjectInfo(String cn, byte[] d)
		{
			classname = cn;
			data = d;
		}
	}

	// private HashMap<TCID, SerializedObjectInfo>
	// serializeSharedObjectLibrary() throws IOException
	// {
	// HashMap<TCID, SerializedObjectInfo> ret = new HashMap<TCID,
	// SerializedObjectInfo>();
	// // for (Map.Entry<TCID, SharedObjectLibrary.LibEntry> entry :
	// // sharedObjectLibrary.sharedObjectMap
	// // .entrySet())
	// // {
	// // ret.put(entry.getKey(), new
	// // SerializedObjectInfo(entry.getValue().newestVersion
	// // .getClass().getName(),
	// // entry.getValue().newestVersion.__tc_serialize()));
	// // }
	// return ret;
	// }

	private void deserializeSharedObjectLibrary(Map<TCID, SerializedObjectInfo> arg0)
			throws ClassNotFoundException, IOException
	{
		// sharedObjectLibrary.sharedObjectMap = new HashMap<TCID,
		// SharedObjectLibrary.LibEntry>();
		// Objenesis obj = SharedObjectLibrary.objenesis;
		//
		// for (Map.Entry<TCID, SerializedObjectInfo> entry : arg0.entrySet())
		// {
		// String className = entry.getValue().classname;
		// ObjectInstantiator inst =
		// obj.getInstantiatorOf(Class.forName(className));
		// Observable o = (Observable) inst.newInstance();
		//
		// sharedObjectLibrary.sharedObjectMap.put(entry.getKey(), new
		// Pair<Observable, Lock>(o,
		// new ReentrantLock()));
		// }
		// for (Map.Entry<TCID, SerializedObjectInfo> entry : arg0.entrySet())
		// {
		// byte[] data = entry.getValue().data;
		// sharedObjectLibrary.sharedObjectMap.get(entry.getKey()).newestVersion
		// = wtf.__tc_deserialize(data);
		// }
	}

	@Override
	public void recoverFromCheckpoint(Storage storage)
	{
		// TODO
		Storage innerStorage = null;
		try
		{
			// int commitNumber = (Integer) commitData;
			// int logVersion = commitNumber % 2;
			int logVersion = 0;

			currentBar = (Integer) storage.retrieve(CURRENT_BAR + "_" + logVersion);
			@SuppressWarnings("unchecked")
			Map<TCID, SerializedObjectInfo> dummy = (Map<TCID, SerializedObjectInfo>) storage
					.retrieve(SHARED_OBJECT_LIBRARY + "_" + logVersion);
			deserializeSharedObjectLibrary(dummy);
			innerStorage = (Storage) storage.retrieve(INNER + "_" + logVersion);

			SharedObjectRegistry.refresh();
		}
		catch (StorageException ex)
		{
			System.out.println("Cannot recover the abcast layer, exiting...");
			ex.printStackTrace();
			System.exit(1);
		}
		catch (ClassNotFoundException e)
		{
			System.out.println("Cannot recover the abcast layer, exiting...");
			e.printStackTrace();
			System.exit(1);
		}
		catch (IOException e)
		{
			System.out.println("Cannot recover the abcast layer, exiting...");
			e.printStackTrace();
			System.exit(1);
		}

		System.out.println("DSTM - recoverFromCommit");

		for (RecoveryListener listener : recoveryListeners)
			listener.recoverFromCheckpoint(innerStorage);

	}

	@Override
	public void recoveryFinished()
	{
		synchronized (lock)
		{
			mgrReady = true;
			lock.notifyAll();
		}

		for (RecoveryListener listener : recoveryListeners)
			listener.recoveryFinished();

		System.out.println("DSTM - recoveryFinished");
	}

	@Override
	public boolean addCheckpointListener(CheckpointListener listener)
	{
		checkpointListeners.add(listener);
		return true;
	}

	@Override
	public boolean addRecoveryListener(RecoveryListener listener)
	{
		recoveryListeners.add(listener);
		return true;
	}

	@Override
	public void scheduleCheckpoint()
	{
		abcast.scheduleCheckpoint();
	}

	@Override
	public boolean removeCheckpointListener(CheckpointListener listener)
	{
		checkpointListeners.remove(listener);
		return true;
	}

	@Override
	public boolean removeRecoveryListener(RecoveryListener listener)
	{
		recoveryListeners.remove(listener);
		return true;
	}

	public void RRR()
	{
		updateManager.subscribers.remove(updateManager.localCursor.get());
	}
}
