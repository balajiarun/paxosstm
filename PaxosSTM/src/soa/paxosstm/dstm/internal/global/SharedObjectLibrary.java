/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm.internal.global;

import java.io.IOException;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;
import org.objenesis.instantiator.ObjectInstantiator;

import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.dstm.internal.instrumentation.SharedObjectInputStream;
import soa.paxosstm.dstm.internal.local.CommitDescriptor;
import soa.paxosstm.dstm.internal.local.ShadowCopyMap;

/**
 * Shared Object Library (SOL) is a place where the references to all shared
 * objects are stored. SOL is replicated on each site. SOL is a part of
 * {@link GlobalTransactionManager}
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public final class SharedObjectLibrary
{
	private static final long serialVersionUID = 7276390771651276425L;

	private static final class HandleReference extends WeakReference<BaseTransactionObject>
	{
		private int id;

		// public String className;
		// public int hash;

		public HandleReference(BaseTransactionObject referent,
				ReferenceQueue<? super BaseTransactionObject> referenceQueue, int id)
		{
			super(referent, referenceQueue);
			this.id = id;
			// className = referent.getClass().getName();
			// hash = System.identityHashCode(referent);
		}

		public int getId()
		{
			return id;
		}
	}

	private static final class LibEntry
	{
		private static final int numberOfBits = 64;

		private static long fullMask;
		private static int numberOfReplicas;

		public static void setMaximumNumberOfReplicas(int numberOfReplicas)
		{
			if (numberOfReplicas > numberOfBits)
			{
				System.err.println("The maximum number of replicas is exceeded (max is "
						+ numberOfBits + ").");
				System.exit(1);
			}
			LibEntry.numberOfReplicas = numberOfReplicas;
			fullMask = (-1L) >>> (numberOfBits - numberOfReplicas);
		}

		protected HandleReference handleReference;
		protected long version;
		protected byte[] newestVersion;
		protected int typeId;
		private long mask;

		public LibEntry(HandleReference handleReference, int typeId)
		{
			this.handleReference = handleReference;
			this.typeId = typeId;
		}

		public BaseTransactionObject getHandle()
		{
			if (handleReference != null)
				return handleReference.get();
			return null;
		}

		public void clearMask()
		{
			mask = 0;
		}

		public boolean setMask(int n)
		{
			if (n >= numberOfReplicas)
				throw new RuntimeException();
			mask |= 1 << n;
			return mask == fullMask;
		}
	}

	private static final int INITIAL_SHARED_OBJECTS_SIZE = 8192;

	private LibEntry[] sharedObjects = new LibEntry[INITIAL_SHARED_OBJECTS_SIZE];
	public IdPool idPool = new IdPool();
	private final Objenesis objenesis = new ObjenesisStd();
	private volatile Map<String, Integer> typesRegistry = new HashMap<String, Integer>();
	private List<Class<?>> types = new ArrayList<Class<?>>();
	private List<ObjectInstantiator> instantiators = new ArrayList<ObjectInstantiator>();
	private ReferenceQueue<BaseTransactionObject> handleDisposalQueue = new ReferenceQueue<BaseTransactionObject>();
	private Set<Integer> gcProposal = new LinkedHashSet<Integer>();
	private int[] temporaryNewSet = null;
	private SharedObjectInputStream temporaryUpdateStream = null;
	private Deque<LibEntry> entriesToBeRecreated = new ArrayDeque<LibEntry>();

	public SharedObjectLibrary()
	{
		LibEntry.setMaximumNumberOfReplicas(PaxosSTM.numberOfNodes);
	}

	public boolean validate(int[] ids, long version)
	{
		return validate(ids, ids.length, version);
	}

	public boolean validate(int[] ids, final int n, long version)
	{
//		boolean fail = false;
//		for (int i = 0; i < n; i++)
//			if (ids[i] >= sharedObjects.length || sharedObjects[ids[i]] == null)
//			{
//				System.err.println(ids[i]);
//				fail = true;
//			}
//		if (fail)
//			System.exit(1);

		int i = 0;
		for (; i <= n - 8; i += 8)
		{
			boolean invalid = false;
			invalid |= sharedObjects[ids[i + 0]].version > version;
			invalid |= sharedObjects[ids[i + 1]].version > version;
			invalid |= sharedObjects[ids[i + 2]].version > version;
			invalid |= sharedObjects[ids[i + 3]].version > version;
			invalid |= sharedObjects[ids[i + 4]].version > version;
			invalid |= sharedObjects[ids[i + 5]].version > version;
			invalid |= sharedObjects[ids[i + 6]].version > version;
			invalid |= sharedObjects[ids[i + 7]].version > version;
			if (invalid)
				return false;
		}
		for (; i < n; i++)
			if (sharedObjects[ids[i]].version > version)
				return false;
		return true;
	}

	public BaseTransactionObject[] localApply(CommitDescriptor desc, long version)
	{
		BaseTransactionObject[] handles = new BaseTransactionObject[desc.getNewSetSize()
				+ desc.getWriteSetSize()];
		registerNew(desc, handles);
		return localUpdate(desc, version, handles);
	}

	private void addType(Map<String, Integer> temporaryTypesRegistry, String type)
	{
		temporaryTypesRegistry.put(type, instantiators.size());
		try
		{
			Class<?> clazz = Class.forName(type);
			types.add(clazz);
			instantiators.add(objenesis.getInstantiatorOf(clazz));
		}
		catch (ClassNotFoundException e)
		{
			throw new RuntimeException(e);
		}
	}

	public int[] registerNew(CommitDescriptor desc, BaseTransactionObject[] handles)
	{
		checkHandleDisposalQueue();

		int[] newSet = new int[desc.getNewSetSize()];
		idPool.getNewIds(newSet);
		ensureCapacity(idPool.totalIds() + 1);

		List<String> typesSet = desc.getTypesSet();
		int[] newTypeIds = null;
		if (typesSet != null)
			newTypeIds = registerTypes(typesSet.toArray(new String[typesSet.size()]));

		ShadowCopyMap.Entry entry = desc.getNewSet();
		for (int i = 0; i < newSet.length; i++)
		{
			int id = newSet[i];
			int typeId = entry.getTypeId();
			typeId = typeId >= 0 ? typeId : newTypeIds[~typeId];

			BaseTransactionObject o = entry.getHandle();
			o.__tc_setId(id);
			handles[i] = o;

			HandleReference ref = new HandleReference(o, handleDisposalQueue, id);
			sharedObjects[id] = new LibEntry(ref, typeId);

			entry = entry.getNext();
		}

		return newSet;
	}

	private BaseTransactionObject[] localUpdate(CommitDescriptor desc, long version,
			BaseTransactionObject[] handles)
	{
		try
		{
			int counter = 0;
			for (ShadowCopyMap.Entry entry = desc.getNewSet(); entry != null; entry = entry
					.getNext())
			{
				addNewVersion(entry.getId(), entry.getValue(), version);
				counter++;
			}
			for (ShadowCopyMap.Entry entry = desc.getWriteSet(); entry != null; entry = entry
					.getNext())
			{
				handles[counter++] = addNewVersion(entry.getId(), entry.getValue(), version);
			}
		}
		catch (IOException e)
		{
			System.err.println("System in unconsistent state!");
			e.printStackTrace();
			System.exit(1);
		}
		catch (ClassNotFoundException e)
		{
			System.err.println("System in unconsistent state!!");
			e.printStackTrace();
			System.exit(1);
		}
		catch (Throwable e)
		{
			System.err.println("System in unconsistent state!!!");
			e.printStackTrace();
			System.exit(1);
		}

		return handles;
	}

	public int[] registerTypes(String[] typesSet)
	{
		if (typesSet == null)
			return null;

		HashMap<String, Integer> newTypesRegistry = new HashMap<String, Integer>(typesRegistry);
		int[] newTypeIds = new int[typesSet.length];

		int counter = 0;
		for (String type : typesSet)
		{
			newTypeIds[counter++] = instantiators.size();
			addType(newTypesRegistry, type);
		}

		typesRegistry = newTypesRegistry;
		return newTypeIds;
	}

	public int registerType(String type)
	{
		Map<String, Integer> reg = typesRegistry;
		Integer typeId = reg.get(type);
		if (typeId != null)
			return typeId;

		int ret = instantiators.size();
		addType(reg, type);
		typesRegistry = reg; // necessary because of volatile write
		return ret;
	}

	public int[] createNew(int[] typeIds, int[] newTypeIds, BaseTransactionObject[] handles)
	{
		checkHandleDisposalQueue();

		int[] newSet = new int[typeIds.length];
		idPool.getNewIds(newSet);
		ensureCapacity(idPool.totalIds() + 1);

		for (int i = 0; i < newSet.length; i++)
		{
			int id = newSet[i];
			int typeId = typeIds[i];
			typeId = typeId >= 0 ? typeId : newTypeIds[~typeId];

			ObjectInstantiator objectInstantiator = instantiators.get(typeId);
			BaseTransactionObject o = (BaseTransactionObject) objectInstantiator.newInstance();
			o.__tc_initHandle(id);
			// putting handle reference to prevent GC from collecting it before
			// deserialization
			handles[i] = o;

			HandleReference ref = new HandleReference(o, handleDisposalQueue, id);
			sharedObjects[id] = new LibEntry(ref, typeId);
		}

		return newSet;
	}

	public BaseTransactionObject[] update(int[] writeSet, int[] newSet,
			BaseTransactionObject[] handles, SharedObjectInputStream input, long version)
	{
		temporaryNewSet = newSet;
		temporaryUpdateStream = input;

		try
		{
			int counter = 0;
			for (int id : newSet)
			{
				addNewVersion(id, input, version);
				counter++;
			}
			for (int id : writeSet)
			{
				handles[counter++] = addNewVersion(id, input, version);
			}
			LibEntry entry;
			while ((entry = entriesToBeRecreated.pollLast()) != null)
			{
				BaseTransactionObject handle = entry.handleReference.get();
				SharedObjectInputStream data = new SharedObjectInputStream(entry.newestVersion);
				handle.__tc_addNewVersion(entry.version, data);
			}
		}
		catch (IOException e)
		{
			System.err.println("System in unconsistent state!");
			e.printStackTrace();
			System.exit(1);
		}
		catch (ClassNotFoundException e)
		{
			System.err.println("System in unconsistent state!!");
			e.printStackTrace();
			System.exit(1);
		}
		catch (Throwable e)
		{
			System.err.println("System in unconsistent state!!!");
			e.printStackTrace();
			System.exit(1);
		}

		temporaryUpdateStream = null;
		temporaryNewSet = null;
		return handles;
	}

	private BaseTransactionObject addNewVersion(int id, BaseTransactionObject shadowCopy,
			long version) throws IOException, ClassNotFoundException
	{
		LibEntry libEntry = sharedObjects[id];
		BaseTransactionObject handle = getHandle(libEntry, id);

		libEntry.version = version;
		handle.__tc_addNewVersion(version, shadowCopy);
		return handle;
	}

	private BaseTransactionObject addNewVersion(int id, SharedObjectInputStream data, long version)
			throws IOException, ClassNotFoundException
	{
		LibEntry libEntry = sharedObjects[id];
		BaseTransactionObject handle = getHandle(libEntry, id);

		libEntry.version = version;
		data.mark();
		handle.__tc_addNewVersion(version, data);
		libEntry.newestVersion = data.getBytesFromMark(libEntry.newestVersion);

		return handle;
	}

	public BaseTransactionObject getNewHandle(int id)
	{
		id = temporaryNewSet[~id];
		temporaryUpdateStream.correctId(id);
		return getHandle(id);
	}

	public BaseTransactionObject getHandle(int id)
	{
		LibEntry entry = sharedObjects[id];
		return getHandle(entry, id);
	}

	private BaseTransactionObject getHandle(LibEntry entry, int id)
	{
		BaseTransactionObject handle = entry.getHandle();

		entry.clearMask();
		gcProposal.remove(id);

		if (handle == null)
		{
			ObjectInstantiator objectInstantiator = instantiators.get(entry.typeId);
			handle = (BaseTransactionObject) objectInstantiator.newInstance();
			handle.__tc_initHandle(id);

			entry.handleReference = new HandleReference(handle, handleDisposalQueue, id);
			entriesToBeRecreated.addLast(entry);
		}

		return handle;
	}

	private void checkHandleDisposalQueue()
	{
		while (true)
		{
			HandleReference ref = (HandleReference) handleDisposalQueue.poll();
			if (ref == null)
				break;
			// System.err.println("checkHandleDisposalQueue() cl=" +
			// ref.className + " h=" + ref.hash + " id=" + ref.getId());

			int id = ref.getId();
			LibEntry entry = sharedObjects[id];
			if (entry == null)
				continue;
			if (entry.handleReference == ref)
			{
				// sharedObjects[id] = null; // fastTrack :)
				gcProposal.add(id);
				entry.handleReference = null;
			}
		}
	}

	public void applyNewGarbageProposals(int n, int[] set)
	{
		// System.err.println("applyNewGarbageProposals() n=" + n + " l=" +
		// set.length);
		int i, j;
		for (i = 0, j = 0; i < set.length; i++)
		{
			int id = set[i];
			LibEntry entry = sharedObjects[id];

			if (entry.setMask(n))
			{
				sharedObjects[id] = null;
				set[j++] = id;
				// gcProposal.remove(id); ???? TODO
			}
		}
		idPool.freeIds(set, j);
	}

	public Set<Integer> getGcProposal()
	{
		return gcProposal;
	}

	public Map<String, Integer> getTypesRegistry()
	{
		return typesRegistry;
	}

	public Class<?> getType(int typeId)
	{
		return types.get(typeId);
	}

	public int getSize()
	{
		return idPool.usedIds();
	}

	private void ensureCapacity(int minCapacity)
	{
		if (minCapacity > sharedObjects.length)
			grow(minCapacity);
	}

	private void grow(int minCapacity)
	{
		int oldCapacity = sharedObjects.length;
		int newCapacity = oldCapacity << 1;
		if (newCapacity < minCapacity)
			newCapacity = minCapacity;
		if (newCapacity < 0)
		{
			if (minCapacity < 0) // overflow
				throw new OutOfMemoryError();
			newCapacity = Integer.MAX_VALUE;
		}
		sharedObjects = Arrays.copyOf(sharedObjects, newCapacity);
	}
	
	@SuppressWarnings("unchecked")
	public Map<Class<?>, Integer> getSharedObjectsTypeCount()
	{
		Map<Class<?>, Integer> map = new HashMap<Class<?>, Integer>();
		for (int i = 0; i < sharedObjects.length; i++)
		{
			LibEntry entry = sharedObjects[i];
			if (entry == null)
				continue;
			Class<?> clazz = types.get(entry.typeId);
			Integer currentValue = map.get(clazz);
			if (currentValue == null)
				map.put(clazz, 1);
			else
				map.put(clazz, currentValue + 1);
		}

		Map.Entry<Class<?>, Integer>[] array = map.entrySet().toArray(
				new Map.Entry[0]);
		Arrays.sort(array, new Comparator<Map.Entry<Class<?>, Integer>>()
		{
			public int compare(Map.Entry<Class<?>, Integer> a,
					Map.Entry<Class<?>, Integer> b)
			{
				return b.getValue() - a.getValue();
			}
		});

		map = new LinkedHashMap<Class<?>, Integer>();
		for (Map.Entry<Class<?>, Integer> entry : array)
		{
			map.put(entry.getKey(), entry.getValue());
		}

		return map;
	}

}
