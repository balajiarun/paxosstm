package soa.paxosstm.dstm.internal.global;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;

import soa.paxosstm.dstm.internal.PackageInputStream;
import soa.paxosstm.dstm.internal.PackageOutputStream;

public final class GarbagePackage extends Package
{
	private static final int NO_COMPRESSION_THRESHOLD = 256;
	private static final int EXCEPTIONS_COMPRESSION_THRESHOLD = 1024;

	private int[] garbageProposals;
	private Set<Integer> garbageProposalsSet;
	private int replicaId;

	public GarbagePackage(Set<Integer> garbageProposals, int replicaId)
	{
		this.garbageProposalsSet = garbageProposals;
		this.replicaId = replicaId;
	}

	public GarbagePackage(PackageInputStream in) throws IOException
	{
		deserialize(in);
	}

	public int[] getGarbageProposals()
	{
		return garbageProposals;
	}

	public int getReplicaId()
	{
		return replicaId;
	}

	@Override
	protected void serialize(PackageOutputStream out) throws IOException
	{
		writePackageType(out, PackageType.GARBAGE);

		out.writeInt(replicaId);
		int size = garbageProposalsSet.size();
		out.writeInt(size);
		if (size <= NO_COMPRESSION_THRESHOLD)
		{
			for (int id : garbageProposalsSet)
			{
				out.writeInt(id);
			}
		}
		else
		{
			int[] ids = new int[size];
			int counter = 0;
			for (int id : garbageProposalsSet)
			{
				ids[counter++] = id;
			}
			Arrays.sort(ids);

			if (size <= EXCEPTIONS_COMPRESSION_THRESHOLD)
				out.writeIdSetSortedWithExceptions(ids, size);
			else
				out.writeIdSetSortedWith3States(ids, size);
		}
	}

	private void deserialize(PackageInputStream in) throws IOException
	{
		replicaId = in.readInt();

		int size = in.readInt();
		garbageProposals = new int[size];

		if (size <= NO_COMPRESSION_THRESHOLD)
		{
			for (int i = 0; i < size; i++)
			{
				garbageProposals[i] = in.readInt();
			}
		}
		else if (size <= EXCEPTIONS_COMPRESSION_THRESHOLD)
			in.readIdSetSortedWithExceptions(garbageProposals);
		else
			in.readIdSetSortedWith3States(garbageProposals);
	}
}
