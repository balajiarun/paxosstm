package soa.paxosstm.dstm.internal.global;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.LockSupport;

import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;

public final class UpdateManager
{
	private static final int INITIAL_BUFFER_SIZE = 1;
	private volatile Node[] buffer;

	private Sequence updateCursor;
	private Sequence garbageCursor;

	// FIXME
	public ThreadLocal<VolatileIter> localCursor = new ThreadLocal<VolatileIter>()
	{
		@Override
		public VolatileIter get()
		{
			VolatileIter it = super.get();
			it.version = updateCursor.version;
			it.used = true;
			it.version = updateCursor.version;
			return it;
		}

		@Override
		protected VolatileIter initialValue()
		{
			VolatileIter it = new VolatileIter(updateCursor.version);
			subscribers.add(it);
			return it;
		}
	};

	// FIXME
	public CopyOnWriteArrayList<VolatileIter> subscribers = new CopyOnWriteArrayList<VolatileIter>();

	public UpdateManager()
	{
		this(0);
	}

	public UpdateManager(long version)
	{
		buffer = new Node[INITIAL_BUFFER_SIZE];
		Node[] tab = buffer;
		for (int i = 0; i < tab.length; i++)
			tab[i] = new Node();

		// Node n = tab[(int) version];
		// n.update = new int[0];
		// n.handles = new BaseTransactionObject[0];

		updateCursor = new Sequence(version);
		garbageCursor = new Sequence(version);
	}
	
	public void add(int[] update, BaseTransactionObject[] handles)
	{
		long newVersion = updateCursor.version + 1;
		long oldestVersion = garbageCursor.version;
		Node[] tab = buffer;
		int tail = (int) (newVersion % tab.length);
		int head = (int) (oldestVersion % tab.length);

		if (tail == head)
		{
			grow(oldestVersion);
			tab = buffer;
			tail = (int) (newVersion % tab.length);
			head = (int) (oldestVersion % tab.length);
		}

		Node n = tab[tail];
		n.update = update;
		n.handles = handles;

		updateCursor.version = newVersion;

		collectGarbage();
	}

	private void grow(long head)
	{
		Node[] oldBuffer = buffer;
		int n = oldBuffer.length;
		int m = n << 1;
		Node[] newBuffer = new Node[m];

		// int oldIdx = (int) (head % n);
		int newIdx = (int) (head % m);
		// if (oldIdx == newIdx)
		// {
		// for (int i = 0; i < oldIdx; i++)
		// {
		// newBuffer[i] = new Node();
		// }
		// System.arraycopy(oldBuffer, oldIdx, newBuffer, oldIdx, n - oldIdx);
		// System.arraycopy(oldBuffer, 0, newBuffer, n, oldIdx);
		// for (int i = n + oldIdx; i < m; i++)
		// {
		// newBuffer[i] = new Node();
		// }
		// }
		// else
		// {
		// System.arraycopy(oldBuffer, 0, newBuffer, 0, oldIdx);
		// System.arraycopy(oldBuffer, oldIdx, newBuffer, newIdx, n - oldIdx);
		// for (int i = oldIdx; i < newIdx; i++)
		// {
		// newBuffer[i] = new Node();
		// }
		// }
		for (int i = 0; i < m; i++)
		{
			if ((i < newIdx && i >= newIdx - n) || i >= newIdx + n)
				newBuffer[i] = new Node();
			else
			{
				Node oldNode = oldBuffer[i % n];
				Node node = new Node();
				node.update = oldNode.update;
				node.handles = oldNode.handles;
				newBuffer[i] = node;
			}
		}

		buffer = newBuffer;

		// System.err.println("GROW " + newBuffer.length);
	}

	// private void shrink(long head, int exponent)
	// {
	// Node[] oldBuffer = buffer;
	// int n = oldBuffer.length;
	// int m = n >> exponent;
	// Node[] newBuffer = new Node[m];
	//
	// // int oldIdx = (int) (head % n);
	// // int newIdx = (int) (head % m);
	// // if (oldIdx == newIdx)
	// // {
	// // System.arraycopy(oldBuffer, oldIdx, newBuffer, oldIdx, m - newIdx);
	// // System.arraycopy(oldBuffer, m, newBuffer, 0, newIdx);
	// // }
	// // else
	// // {
	// // System.arraycopy(oldBuffer, oldIdx, newBuffer, newIdx, m - newIdx);
	// // System.arraycopy(oldBuffer, oldIdx + m - newIdx, newBuffer, 0, n -
	// oldIdx - (m - newIdx));
	// // System.arraycopy(oldBuffer, 0, newBuffer, n - oldIdx - (m - newIdx), m
	// - (n - oldIdx));
	// // }
	// for (long h=head; h<head+m; h++)
	// {
	// int src = (int)(h % n);
	// int dst = (int)(h % m);
	// newBuffer[dst] = oldBuffer[src];
	// }
	//		
	// buffer = newBuffer;
	//
	// System.err.println("SHRINK " + oldBuffer.length + " -> " +
	// newBuffer.length);
	// }

	public UpdateCursor getUpdateCursor()
	{
		return localCursor.get();
	}

	private void collectGarbage()
	{
		// System.err.println(subscribers.size());

		long minVersion = updateCursor.version;
		for (VolatileIter it : subscribers)
		{
			if (it.used)
			{
				long v = it.version;
				if (v < minVersion)
					minVersion = v;
			}
		}

		long oldestVersion = garbageCursor.version;
		if (oldestVersion < minVersion)
		{
			Node[] tab = buffer;
			do
			{
				oldestVersion++;

				int idx = (int) (oldestVersion % tab.length);
				Node n = tab[idx];

				n.clear(minVersion);
				n.update = null;
				n.handles = null;
			} while (oldestVersion < minVersion);
			garbageCursor.version = oldestVersion;
		}
	}

	private static final class Node
	{
		private int[] update;
		private BaseTransactionObject[] handles;

		public void clear(long version)
		{
			for (BaseTransactionObject o : handles)
			{
				o.__tc_release(version);
			}
		}
	}

	private static final class Sequence
	{
		long p0, p1, p2, p3, p4, p5, p6;
		volatile long version;
		long p7, p8, p9, p10, p11, p12, p13;

		public Sequence(long version)
		{
			this.version = version;
		}

		public long getPadding()
		{
			return p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9 + p10 + p11 + p12 + p13;
		}

		public void setPadding(long value)
		{
			p0 = p1 = p2 = p3 = p4 = p5 = p6 = p7 = p8 = p9 = p10 = p11 = p12 = p13 = value;
		}
	}

	private final class VolatileIter implements UpdateCursor
	{
		long p0, p1, p2, p3, p4, p5, p6;
		volatile long version;
		volatile boolean used;
		long p7, p8, p9, p10, p11, p12, p13;

		public VolatileIter(long version)
		{
			this.version = version;
			this.used = true;
		}

		@Override
		public boolean hasNext()
		{
			return true;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public Iter clone()
		{
			return new Iter(version);
		}

		@Override
		public int[] next()
		{
			long newVersion = version + 1;
			while (newVersion > updateCursor.version)
				LockSupport.parkNanos(1);
			version = newVersion;

			Node[] tab = buffer;
			int idx = (int) (version % tab.length);
			return tab[idx].update;
		}

		@Override
		public long getVersion()
		{
			return version;
		}

		@Override
		public void setVersion(long version)
		{
			this.version = version;
		}

		@Override
		public void disable()
		{
			used = false;
		}

		public long getPadding()
		{
			return p0 + p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8 + p9 + p10 + p11 + p12 + p13;
		}

		public void setPadding(long value)
		{
			p0 = p1 = p2 = p3 = p4 = p5 = p6 = p7 = p8 = p9 = p10 = p11 = p12 = p13 = value;
		}
	}

	private final class Iter implements UpdateCursor
	{
		long version;

		public Iter(long version)
		{
			this.version = version;
		}

		@Override
		public boolean hasNext()
		{
			return true;
		}

		@Override
		public void remove()
		{
			throw new UnsupportedOperationException();
		}

		@Override
		public Iter clone()
		{
			return new Iter(version);
		}

		@Override
		public int[] next()
		{
			long newVersion = version + 1;
			while (newVersion > updateCursor.version)
				LockSupport.parkNanos(1);
			version = newVersion;

			Node[] tab = buffer;
			int idx = (int) (version % tab.length);
			return tab[idx].update;
		}

		@Override
		public long getVersion()
		{
			return version;
		}

		@Override
		public void setVersion(long version)
		{
			this.version = version;
		}

		@Override
		public void disable()
		{
		}
	}
}
