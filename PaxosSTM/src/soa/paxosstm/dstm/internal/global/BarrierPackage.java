package soa.paxosstm.dstm.internal.global;

import java.io.IOException;

import soa.paxosstm.dstm.internal.PackageInputStream;
import soa.paxosstm.dstm.internal.PackageOutputStream;

/**
 * Class which objects are passed between nodes and which represent barrier
 * requests.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class BarrierPackage extends Package
{
	protected String barrierName;
	protected int n;

	public BarrierPackage(String barrierName, int n)
	{
		this.barrierName = barrierName;
		this.n = n;
	}

	public BarrierPackage(PackageInputStream in) throws IOException
	{
		deserialize(in);
	}

	@Override
	protected void serialize(PackageOutputStream out) throws IOException
	{
		writePackageType(out, PackageType.BARRIER);
		
		out.writeUTF(barrierName);
		out.writeInt(n);
	}

	private void deserialize(PackageInputStream in) throws IOException
	{
		barrierName = in.readUTF();
		n = in.readInt();
	}
}
