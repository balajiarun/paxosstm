package soa.paxosstm.dstm.internal.global;

import java.util.Arrays;

public final class IdPool
{
	private int[] freeIds = new int[128];
	private int numberOfFreeIds = 0;
	private int highestId = 0;

	public int getNewId()
	{
		if (numberOfFreeIds > 0)
		{
			return freeIds[--numberOfFreeIds];
		}
		return ++highestId;
	}

	public void getNewIds(int[] ids)
	{
		int len = Math.min(ids.length, numberOfFreeIds);
		if (len > 0)
			System.arraycopy(freeIds, numberOfFreeIds - len, ids, 0, len);
		numberOfFreeIds -= len;
		for (int i = len; i < ids.length; i++)
		{
			ids[i] = ++highestId;
		}
	}
	
	public void freeId(int id)
	{
		ensureCapacity(numberOfFreeIds + 1);
		freeIds[numberOfFreeIds++] = id;
	}
	
	public void freeIds(int[] ids)
	{
		freeIds(ids, ids.length);
	}
	
	public void freeIds(int[] ids, int n)
	{
		ensureCapacity(numberOfFreeIds + n);
		
		System.arraycopy(ids, 0, freeIds, numberOfFreeIds, n);
		numberOfFreeIds += n;
	}
	
	public int totalIds()
	{
		return highestId;
	}
	
	public int usedIds()
	{
		return highestId - numberOfFreeIds;
	}
	
	private void ensureCapacity(int minCapacity)
	{
		if (minCapacity > freeIds.length)
			grow(minCapacity);
	}

	private void grow(int minCapacity)
	{
		int oldCapacity = freeIds.length;
		int newCapacity = oldCapacity << 1;
		if (newCapacity < minCapacity)
			newCapacity = minCapacity;
		if (newCapacity < 0)
		{
			if (minCapacity < 0) // overflow
				throw new OutOfMemoryError();
			newCapacity = Integer.MAX_VALUE;
		}
		freeIds = Arrays.copyOf(freeIds, newCapacity);
	}
}
