package soa.paxosstm.dstm.internal.global;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import soa.paxosstm.consensus.BatchDelegateProposer;

public class ClientPool
{
	private static final int NUMBER_OF_PROPOSERS = 1;
	private final BatchDelegateProposer[] proposers = new BatchDelegateProposer[NUMBER_OF_PROPOSERS];
	private final BlockingQueue<byte[]> requestsQueue = new LinkedBlockingQueue<byte[]>();

	// private static ThreadLocal<Client> localClient = new
	// ThreadLocal<Client>()
	// {
	// @Override
	// public Client initialValue()
	// {
	// Client client = null;
	// try
	// {
	// client = new Client();
	// client.connect();
	// }
	// catch (IOException e)
	// {
	// e.printStackTrace();
	// }
	//
	// return client;
	// }
	// };

	public ClientPool()
	{
		try
		{
			for (int i = 0; i < NUMBER_OF_PROPOSERS; i++)
				proposers[i] = new BatchDelegateProposer(requestsQueue);
		}
		catch (IOException e)
		{
			throw new RuntimeException(e);
		}
	}

	public void execute(byte[] bytes)
	{
		// long start = System.currentTimeMillis();
		//
		// Client client = localClient.get();
		//
		// try
		// {
		// client.execute(Collections.singletonList(bytes));
		// }
		// catch (ReplicationException e)
		// {
		// throw new RuntimeException(e);
		// }
		// long stop = System.currentTimeMillis() - start;
		// TestCounter.getInstance().addLatency(stop);
		if (!requestsQueue.offer(bytes))
			throw new RuntimeException();
	}
}
