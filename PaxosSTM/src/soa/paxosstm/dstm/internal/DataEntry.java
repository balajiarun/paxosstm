package soa.paxosstm.dstm.internal;

import java.util.Map;

import soa.paxosstm.dstm.internal.instrumentation.SharedObjectInputStream;
import soa.paxosstm.utils.TCID;

public class DataEntry implements Map.Entry<TCID, SharedObjectInputStream>
{
	private TCID key;

	private SharedObjectInputStream value;

	public DataEntry(TCID key, SharedObjectInputStream value)
	{
		this.key = key;
		this.value = value;
	}

	@Override
	public TCID getKey()
	{
		return key;
	}

	@Override
	public SharedObjectInputStream getValue()
	{
		return value;
	}

	@Override
	public SharedObjectInputStream setValue(SharedObjectInputStream value)
	{
		throw new UnsupportedOperationException();
	}
}