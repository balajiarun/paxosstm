/**
 * 
 */
package soa.paxosstm.dstm.internal;

public enum TransactionState
{
	Executing, Committing, Committed, RolledBack, Aborted
}