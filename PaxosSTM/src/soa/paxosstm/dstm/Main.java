/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm;

import soa.paxosstm.dstm.internal.instrumentation.Loader;

/**
 * The main class of the Paxos STM framework.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 */
public class Main
{
	/**
	 * The main method of the Paxos STM framework. It takes several arguments,
	 * two are obligatory: the name of the application that utilizes Paxos STM
	 * and the number of the consensus replica the current Paxos STM instance is
	 * running on. The arguments specified by the application follow.
	 * 
	 * @param args
	 *            \<application\> \<replica number\> [\<application argument\>]
	 *            ...
	 * @throws Throwable
	 */
	public static void main(String[] args) throws Throwable
	{
		int n = args.length;
		if (n < 2)
		{
			System.out
					.println("invalid arguments, try <application name> <replica_number> ...");
			System.exit(1);
		}

		Loader.run(args[0], args);
	}
}
