/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Arrays;

import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.dstm.internal.instrumentation.SharedObjectInputStream;
import soa.paxosstm.dstm.internal.instrumentation.SharedObjectOutputStream;
import soa.paxosstm.dstm.internal.instrumentation.SpecialConstructorMarker;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager;

// TODO: define array wrapper classes for primitive types i.e. int, long etc.
/**
 * Since changes performed on arrays cannot be easily traced in order to use
 * arrays as transactional objects they should be wrapped inside ArrayWrapper.
 * Normally all operations performed on the array stored within the ArrayWrapper
 * (copy of the array given as the argument to the ArrayWrapper constructor)
 * should be carried out though the interface: {@link #get(int)} and
 * {@link #set(int, Object)} methods. Although sometimes it might be more
 * convenient to operate on the array itself. Therefore there is the possibility
 * to temporarily release (through the {@link #release()} and {@link #hold()}
 * methods) the array. Once the {@link #release()} method is invoked the copy of
 * the array is created in the ArrayWrapper. By comparing the copy with the
 * released array the mechanism tracing the changes performed to the
 * transactional object can determine whether the ArrayWrapper should be a part
 * of the write-set of the current transaction. Since read operations cannot be
 * followed, once the {@link #release()} method is called, the ArrayWrapper
 * automatically becomes a part of the read-set of the current transaction.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public final class ArrayWrapper<T> extends BaseTransactionObject
{
	private T[] data;

	private ArrayWrapper()
	{
		super((SpecialConstructorMarker) null);
	}

	/**
	 * Constructor. The array passed by the argument is not the same one that
	 * will be stored within the ArrayWrapper. Instead, in the ArrayWrapper the
	 * copy of the array passed by the argument is stored. Therefore no one can
	 * modify the array from outside the object and thus surpass the changes
	 * tracking mechanism.
	 * 
	 * @param array
	 *            array which copy is stored within the ArrayWrapper
	 */
	public ArrayWrapper(T[] array)
	{
		ArrayWrapper<T> target = getTargetForWrite();
		target.data = Arrays.copyOf(array, array.length);
	}

	/**
	 * Returns the i-th element of the array.
	 * 
	 * @param i
	 *            number of the array element to be retrieved
	 * @return retrieved element
	 */
	public T get(int i)
	{
		ArrayWrapper<T> target = getTargetForRead();
		return target.data[i];
	}

	/**
	 * Sets the i-th element of the array.
	 * 
	 * @param i
	 *            number of the array element to be changed
	 * @param value
	 *            new value of the i-th element of the array
	 */
	public void set(int i, T value)
	{
		ArrayWrapper<T> target = getTargetForWrite();
		target.__tc_attr |= BaseTransactionObject.IS_WRITTEN_MASK;
		target.data[i] = value;
	}

	/**
	 * Returns the length of the array stored within the ArrayWrapper.
	 * 
	 * @return the length of the array stored within the ArrayWrapper.
	 */
	public int length()
	{
		ArrayWrapper<T> target = getTargetForRead();
		return target.data.length;
	}

	/**
	 * Returns the copy of the array stored within the ArrayWrapper.
	 * 
	 * @return the copy of the array stored within the ArrayWrapper.
	 */
	public T[] toArray()
	{
		ArrayWrapper<T> target = getTargetForRead();
		return Arrays.copyOf(target.data, target.data.length);
	}

	@Override
	public void __tc_serialize(SharedObjectOutputStream shoos) throws IOException
	{
		String componentType = data.getClass().getComponentType().getName();
		Integer typeId = GlobalTransactionManager.getInstance().getSharedObjectLibrary()
				.getTypesRegistry().get(componentType);
		if (typeId != null)
		{
			shoos.writeInt(typeId.intValue());
		}
		else
		{
			shoos.writeInt(-1);
			shoos.writeUTF(componentType);
		}
		shoos.writeInt(data.length);
		if (BaseTransactionObject.class.isAssignableFrom(data.getClass().getComponentType()))
		{
			for (int i = 0; i < data.length; i++)
			{
				shoos.writeSharedObjectReference((BaseTransactionObject) data[i]);
			}
		}
		else
		{
			for (int i = 0; i < data.length; i++)
			{
				shoos.writeObject(data[i]);
			}
		}
	}

	@Override
	public void __tc_deserialize(SharedObjectInputStream shois) throws IOException,
			ClassNotFoundException
	{
		Class<?> componentType;
		int typeId = shois.readInt();
		if (typeId == -1)
		{
			String typeName = shois.readUTF();
			GlobalTransactionManager.getInstance().getSharedObjectLibrary().registerType(typeName);
			componentType = Class.forName(typeName);
		}
		else
		{
			componentType = GlobalTransactionManager.getInstance().getSharedObjectLibrary()
					.getType(typeId);
		}
		int n = shois.readInt();

		@SuppressWarnings("unchecked")
		T[] dummy = (T[]) Array.newInstance(componentType, n);
		data = dummy;

		Object[] tab = data;
		if (BaseTransactionObject.class.isAssignableFrom(componentType))
		{
			for (int i = 0; i < n; i++)
			{
				tab[i] = shois.readSharedObjectReference();
			}
		}
		else
		{
			for (int i = 0; i < n; i++)
			{
				tab[i] = shois.readObject();
			}
		}
	}

	private ArrayWrapper<T> getTargetForRead()
	{
		if ((this.__tc_attr & (BaseTransactionObject.IS_SHADOW_COPY_MASK | BaseTransactionObject.IS_OBJECT_VERSION_MASK)) != 0)
			return this;

		@SuppressWarnings("unchecked")
		ArrayWrapper<T> target = (ArrayWrapper<T>) LocalTransactionManager
				.getTargetObjectForRead(this);
		return target;
	}

	private ArrayWrapper<T> getTargetForWrite()
	{
		if ((this.__tc_attr & BaseTransactionObject.IS_SHADOW_COPY_MASK) != 0)
			return this;

		@SuppressWarnings("unchecked")
		ArrayWrapper<T> target = (ArrayWrapper<T>) LocalTransactionManager
				.getTargetObjectForWrite(this);
		return target;
	}

	@Override
	public void __tc_copy(Object target)
	{
		@SuppressWarnings("unchecked")
		ArrayWrapper<T> dest = (ArrayWrapper<T>) target;

		// dest.exposed = false;
		// dest.data = data.clone();
		dest.data = Arrays.copyOf(data, data.length);
		// dest.copy = null;
	}

	@Override
	public BaseTransactionObject __tc_makeCopy()
	{
		ArrayWrapper<T> target = new ArrayWrapper<T>();

		target.data = Arrays.copyOf(data, data.length);
		target.__tc_id = __tc_id;
		target.__tc_version = __tc_version;
		target.__tc_next = this.__tc_next;
		target.__tc_attr = BaseTransactionObject.IS_SHADOW_COPY_MASK;

		return target;
	}

	@Override
	public ArrayWrapper<T> __tc_newInstance()
	{
		return new ArrayWrapper<T>();
	}

	@SuppressWarnings("unchecked")
	public static <T> ArrayWrapper<ArrayWrapper<T>> create2DMatrix(int m, int n, Class<T> c)
	{
		ArrayWrapper<T>[] array = new ArrayWrapper[m];
		for (int i = 0; i < m; i++)
		{
			T inArray[] = (T[]) Array.newInstance(c, n);
			array[i] = new ArrayWrapper<T>(inArray);
		}
		ArrayWrapper<ArrayWrapper<T>> matrix = new ArrayWrapper<ArrayWrapper<T>>(array);
		return matrix;
	}
}
