/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.dstm;

import java.io.IOException;

import soa.paxosstm.dstm.internal.instrumentation.BaseTransactionObject;
import soa.paxosstm.dstm.internal.instrumentation.ClassAlterer;
import soa.paxosstm.dstm.internal.instrumentation.SharedObjectInputStream;
import soa.paxosstm.dstm.internal.instrumentation.SharedObjectOutputStream;

/**
 * Normally, in order to enable using objects of a given class in transaction,
 * the <i>@TransactionalObject</i> annotation has to be added to the definition
 * of the class. However, it can only be done to classes which can be altered by
 * the programmer. This results in difficulty in using transaction objects of
 * system classes and classes from third-party libraries. Why is it so is
 * discussed in the documentation to {@link ClassAlterer} class. Needless to
 * say, if the programmer wants to use an object of system class or class from
 * external library, it has to be wrapped inside ObjectWrapper, similarly to
 * arrays which have to be encapsulated in {@link ArrayWrapper}s. In
 * ObjectWrapper tracing the changes performed on the object stored inside the
 * ObjectWrapper is accomplished by comparing the object originally put/created
 * inside the ObjectWrapper and the current state of the object. In order to
 * retrieve the reference to the stored object one has to call {@link #get()}
 * method. The programmer might optimize the process of resolving the read- and
 * write-sets for the current transactions by manually marking the object as
 * read/written by calling {@link #__tc_setRead(boolean)}/
 * {@link #__tc_setWritten(boolean)} manually.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 * @param <T>
 *            type of the object stored within the ObjectWrapper
 */
public final class ObjectWrapper<T> extends BaseTransactionObject
{

	@Override
	public void __tc_copy(Object target)
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public void __tc_deserialize(SharedObjectInputStream shois) throws IOException,
			ClassNotFoundException
	{
		// TODO Auto-generated method stub
		
	}

	@Override
	public BaseTransactionObject __tc_makeCopy()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BaseTransactionObject __tc_newInstance()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void __tc_serialize(SharedObjectOutputStream shoos) throws IOException
	{
		// TODO Auto-generated method stub
		
	}
//	private T original;
//
//	/**
//	 * Default constructor. For creating the object stored inside the
//	 * ObjectWrapper the default constructor is called.
//	 */
//	public ObjectWrapper()
//	{
//	}
//
//	/**
//	 * Constructor. The object passed by the argument is not the same one that
//	 * will be stored within the ObjectWrapper. Instead, in the ObjectWrapper
//	 * the copy of the object passed by the argument is stored. Therefore no one
//	 * can modify the object from outside the ObjectWrapper and thus surpass the
//	 * changes tracking mechanism.
//	 * 
//	 * @param object
//	 *            object which copy is stored within the ObjectWrapper
//	 */
//	public ObjectWrapper(T object)
//	{
//		original = clone(object);
//		LocalTransactionManager.registerNewObject(this.__tc_id, this);
//	}
//
//	/**
//	 * Returns the reference to the object stored within the ObjectWrapper and
//	 * adds the ObjectWrapper to the read-set of the current transaction.
//	 * 
//	 * @return the reference to the object stored within the ObjectWrapper
//	 */
//	public T get()
//	{
//		ObjectWrapper<T> target = getTargetForRead();
//		return target.original;
//	}
//
//	@Override
//	public boolean __tc_isWritten()
//	{
////		ObjectWrapper<T> target = getTargetForRead();
////		if (!target.__tc_written)
////			target.__tc_written = !target.original.equals(target.copy);
//		return true;
//	}
//
//	@Override
//	public void __tc_setWritten(boolean written)
//	{
//		if (written)
//		{
//			ObjectWrapper<T> target = getTargetForWrite();
//			target.__tc_attr |= ClassAlterer.IS_WRITTEN_MASK;
//		}
//	}
//
//	@Override
//	public final TCID __tc_getId()
//	{
//		return __tc_id;
//	}
//
//	@Override
//	public byte[] __tc_serialize() throws IOException
//	{
//		ByteArrayOutputStream baos = new ByteArrayOutputStream();
//		ObjectOutputStream oos = new ObjectOutputStream(baos);
//
//		// System.out.println(this);
//		// System.out.println(this.__tc_isProxyCopy);
//		// System.out.println(this.__tc_id);
//		oos.writeLong(this.__tc_id.getLeastSignificantBits());
//		oos.writeLong(this.__tc_id.getMostSignificantBits());
//		oos.writeObject(original);
//
//		oos.close();
//		byte[] ret = baos.toByteArray();
//
//		return ret;
//	}
//
//	@SuppressWarnings("unchecked")
//	@Override
//	public void __tc_deserialize(byte[] data) throws IOException, ClassNotFoundException
//	{
//		ByteArrayInputStream bais = new ByteArrayInputStream(data);
//		ObjectInputStream ois = new ObjectInputStream(bais);
//
//		long lsb = ois.readLong();
//		long msb = ois.readLong();
//		TCID id = new TCID(msb, lsb);
//		this.__tc_id = id;
//		// System.out.println("$$ " + this);
//		// System.out.println("$$ " + this.__tc_isProxyCopy);
//		// System.out.println("$$ " + this.__tc_id);
//		this.original = (T) ois.readObject();
//
//		ois.close();
//	}
//
//	private ObjectWrapper<T> getTargetForRead()
//	{
//
//		if ((this.__tc_attr & (ClassAlterer.IS_SHADOW_COPY_MASK | ClassAlterer.IS_OBJECT_VERSION_MASK)) != 0)
//			return this;
//
//		@SuppressWarnings("unchecked")
//		ObjectWrapper<T> target = (ObjectWrapper<T>) LocalTransactionManager.getTargetObject(
//				this.__tc_id, this, false);
//		return target;
//	}
//
//	private ObjectWrapper<T> getTargetForWrite()
//	{
//		if ((this.__tc_attr & ClassAlterer.IS_SHADOW_COPY_MASK) != 0)
//			return this;
//
//		@SuppressWarnings("unchecked")
//		ObjectWrapper<T> target = (ObjectWrapper<T>) LocalTransactionManager.getTargetObject(
//				this.__tc_id, this, true);
//		return target;
//	}
//
//
//	@SuppressWarnings("unchecked")
//	private T clone(T object)
//	{
//		try
//		{
//			ByteArrayOutputStream baos = new ByteArrayOutputStream();
//			ObjectOutputStream oos = new ObjectOutputStream(baos);
//			oos.writeObject(object);
//			oos.close();
//			byte[] data = baos.toByteArray();
//
//			ByteArrayInputStream bais = new ByteArrayInputStream(data);
//			ObjectInputStream ois = new ObjectInputStream(bais);
//			T ret = (T) ois.readObject();
//			ois.close();
//
//			return ret;
//		}
//		catch (IOException e)
//		{
//			throw new RuntimeException(e);
//		}
//		catch (ClassNotFoundException e)
//		{
//			throw new RuntimeException(e);
//		}
//	}
//
//	@Override
//	public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException
//	{
//		LocalTransactionManager.unregisterNewObject(__tc_id);
//
//		long lsb = in.readLong();
//		long msb = in.readLong();
//		__tc_id = new TCID(msb, lsb);
//
//		@SuppressWarnings("unchecked")
//		ObjectWrapper<T> dummy = (ObjectWrapper<T>) GlobalTransactionManager.getInstance()
//				.getSharedObjectLibrary().getHandle(__tc_id);
//		__tc_next = dummy;
//	}
//	
//	@Override
//	public void writeExternal(ObjectOutput out) throws IOException
//	{
//		out.writeLong(__tc_id.getLeastSignificantBits());
//		out.writeLong(__tc_id.getMostSignificantBits());
//	}
//
//	@Override
//	public void __tc_vcopy(Object target)
//	{
//		@SuppressWarnings("unchecked")
//		ObjectWrapper<T> dest = (ObjectWrapper<T>) target;
//
//		dest.__tc_id = __tc_id;
//		dest.__tc_next = __tc_next;
//		dest.original = clone(original);
//		// dest.copy = clone(original);
//	}
//
//	@Override
//	public BaseTransactionObject __tc_vmakeCopy()
//	{
//		ObjectWrapper<T> target = new ObjectWrapper<T>();
//		__tc_vcopy(target);
//		target.__tc_id = __tc_id;
//		target.__tc_version = __tc_version;
//		target.__tc_next = this.__tc_next;
//		target.__tc_attr = ClassAlterer.IS_SHADOW_COPY_MASK;
//		return target;
//	}
//
//	@Override
//	public final BaseTransactionObject __tc_getNext()
//	{
//		return __tc_next;
//	}
//
//	@Override
//	public final void __tc_setNext(Object obj)
//	{
//		@SuppressWarnings("unchecked")
//		ObjectWrapper<T> dummy = (ObjectWrapper<T>) obj;
//		__tc_next = dummy;
//	}
//	
//	@Override
//	public long __tc_getVersion()
//	{
//		return __tc_version;
//	}
//
//	@Override
//	public void __tc_initHandle(TCID id)
//	{
//		__tc_id = id;
//		__tc_attr = ClassAlterer.IS_OBJECT_HANDLE_MASK;
//	}
//
//	@Override
//	public void __tc_initShadowCopy(TCID id)
//	{
//		__tc_id = id;
//		__tc_attr = ClassAlterer.IS_SHADOW_COPY_MASK;
//	}
//	
//	@Override
//	public boolean __tc_isHandle()
//	{
//		return (__tc_attr & ClassAlterer.IS_OBJECT_HANDLE_MASK) != 0;
//	}
//
//	@Override
//	public boolean __tc_isShadowCopy()
//	{
//		return (__tc_attr & ClassAlterer.IS_SHADOW_COPY_MASK) != 0;
//	}
//
//	@Override
//	public boolean __tc_isVersion()
//	{
//		return (__tc_attr & ClassAlterer.IS_OBJECT_VERSION_MASK) != 0;
//	}
//
//	@Override
//	public void __tc_release(long version)
//	{
//		ObjectWrapper<T> prev = this;
//		ObjectWrapper<T> next = prev.__tc_next;
//		while (next != null && next.__tc_version != version)
//		{
//			prev = next;
//			next = prev.__tc_next;
//		}
//		if (next == null)
//			return;
//		if ((prev.__tc_attr & ClassAlterer.IS_OBJECT_VERSION_MASK) == 0)
//			next.__tc_attr |= ClassAlterer.TO_BE_REPLACED_MASK;
//		else
//			prev.__tc_next = null;
//	}
//
//	@Override
//	public BaseTransactionObject __tc_retrieve(long version)
//	{
//		ObjectWrapper<T> v = this;
//		while (v != null
//				&& ((v.__tc_attr & ClassAlterer.IS_OBJECT_VERSION_MASK) == 0 || v.__tc_version > version))
//			v = v.__tc_next;
//		return v;
//	}
//
//	@Override
//	public BaseTransactionObject __tc_addNewVersion(long version, byte[] data) throws IOException,
//			ClassNotFoundException
//	{
//		ObjectWrapper<T> obj = new ObjectWrapper<T>();
//		obj.__tc_deserialize(data);
//		ObjectWrapper<T> nextVersion = this.__tc_next;
//		if (nextVersion != null && (nextVersion.__tc_attr & ClassAlterer.TO_BE_REPLACED_MASK) != 0)
//			nextVersion = null;
//		obj.__tc_id = this.__tc_id;
//		obj.__tc_version = version;
//		obj.__tc_attr = ClassAlterer.IS_OBJECT_VERSION_MASK;
//		obj.__tc_next = nextVersion;
//		__tc_next = obj;
//		return obj;
//	}
}
