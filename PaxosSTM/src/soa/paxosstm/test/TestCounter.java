/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.test;

import java.lang.management.ThreadMXBean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

/**
 * This class can be used for gathering simple statistics about the Paxos STM
 * performance, e.g. number of global and local conflicts that occured during
 * the execution of the program. Statistics are gathered per thread. In order to
 * use this class one has to retrieve the instance of the class by calling the
 * static method {@link TestCounter#getInstance()} and then the method
 * incrementing the chosen parameter. At any time one can retrieve the current
 * value of a given parameter by calling one of the get methods.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class TestCounter
{
	private AtomicInteger globalConflicts;
	private AtomicInteger globalDeleteConflicts;
	private AtomicInteger globalPreviousConflicts;
	private AtomicInteger globalReorderConflicts;
	private AtomicInteger localConflicts;

	private AtomicLong totalReadSetSize;
	private AtomicLong totalWriteSetSize;
	private AtomicLong totalNewSetSize;
	private AtomicLong totalTypeSetSize;
	private AtomicLong totalPackageSize;
	private AtomicInteger totalPackages;

	private AtomicLong totalLatency;
	private AtomicLong totalLocalRequests;
	
	private AtomicLong totalROAtomicLength;
	private AtomicInteger totalROTransactions;

	// private AtomicLong timeSpentOnGlobalConflictChecks;
	// private AtomicLong timeSpentOnLocalConflictChecks;
	// private AtomicLong timeSpentOnUpdating;

	private final ThreadMXBean mxBean = java.lang.management.ManagementFactory
			.getThreadMXBean();
	private final static TestCounter instance = new TestCounter();

	private TestCounter()
	{
		reset();
	}

	/**
	 * Returns the singleton instance of the class.
	 * 
	 * @return instance of the class.
	 */
	public static TestCounter getInstance()
	{
		return instance;
	}

	/**
	 * Returns current CPU time.
	 * 
	 * @return current CPU time
	 */
	public long getCPUTime()
	{
		return mxBean.getCurrentThreadCpuTime();
	}

	/**
	 * Resets all counters.
	 */
	public void reset()
	{
		globalConflicts = new AtomicInteger();
		globalPreviousConflicts = new AtomicInteger();
		globalDeleteConflicts = new AtomicInteger();
		globalReorderConflicts = new AtomicInteger();
		localConflicts = new AtomicInteger();

		totalReadSetSize = new AtomicLong();
		totalWriteSetSize = new AtomicLong();
		totalNewSetSize = new AtomicLong();
		totalTypeSetSize = new AtomicLong();
		totalPackageSize = new AtomicLong();
		totalPackages = new AtomicInteger();

		totalLatency = new AtomicLong();
		totalLocalRequests = new AtomicLong();
		
		totalROAtomicLength = new AtomicLong();
		totalROTransactions = new AtomicInteger();
		
		// timeSpentOnGlobalConflictChecks = new AtomicLong();
		// timeSpentOnLocalConflictChecks = new AtomicLong();
		// timeSpentOnUpdating = new AtomicLong();
	}

	/**
	 * Increment the global conflict counter.
	 */
	public void addGlobalConflict()
	{
		globalConflicts.incrementAndGet();
	}

	/**
	 * Returns the current number of global conflicts.
	 * 
	 * @return the current number of global conflicts
	 */
	public int getGlobalConflicts()
	{
		return globalConflicts.get();
	}

	/**
	 * Increment the global conflict counter.
	 */
	public void addGlobalDeleteConflict()
	{
		globalDeleteConflicts.incrementAndGet();
	}

	/**
	 * Returns the current number of global conflicts.
	 * 
	 * @return the current number of global conflicts
	 */
	public int getGlobalDeleteConflicts()
	{
		return globalDeleteConflicts.get();
	}

	/**
	 * Increment the global conflict counter.
	 */
	public void addGlobalPreviousConflict()
	{
		globalPreviousConflicts.incrementAndGet();
	}

	/**
	 * Returns the current number of global conflicts.
	 * 
	 * @return the current number of global conflicts
	 */
	public int getGlobalPreviousConflicts()
	{
		return globalPreviousConflicts.get();
	}

	/**
	 * Increment the global conflict counter.
	 */
	public void addGlobalReorderConflict()
	{
		globalReorderConflicts.incrementAndGet();
	}

	/**
	 * Returns the current number of global conflicts.
	 * 
	 * @return the current number of global conflicts
	 */
	public int getGlobalReorderConflicts()
	{
		return globalReorderConflicts.get();
	}

	/**
	 * Increment the local conflict counter.
	 */
	public void addLocalConflict()
	{
		localConflicts.incrementAndGet();
	}

	/**
	 * Returns the current number of local conflicts.
	 * 
	 * @return the current number of local conflicts
	 */
	public int getLocalConflicts()
	{
		return localConflicts.get();
	}

	public double getAvgReadSetSize()
	{
		return (double) totalReadSetSize.get() / totalPackages.get();
	}

	public double getAvgWriteSetSize()
	{
		return (double) totalWriteSetSize.get() / totalPackages.get();
	}

	public double getAvgNewSetSize()
	{
		return (double) totalNewSetSize.get() / totalPackages.get();
	}

	public double getAvgTypeSetSize()
	{
		return (double) totalTypeSetSize.get() / totalPackages.get();
	}

	public double getAvgPackageSize()
	{
		return (double) totalPackageSize.get() / totalPackages.get();
	}

	public int getPackages()
	{
		return totalPackages.get();
	}

	public void addPackage(int readSetSize, int writeSetSize, int newSetSize,
			int typeSetSize, int packageSize)
	{
		totalReadSetSize.addAndGet(readSetSize);
		totalWriteSetSize.addAndGet(writeSetSize);
		totalNewSetSize.addAndGet(newSetSize);
		totalTypeSetSize.addAndGet(typeSetSize);
		totalPackageSize.addAndGet(packageSize);
		totalPackages.incrementAndGet();
	}

	public void addLatency(long stop)
	{
		totalLatency.addAndGet(stop);
		totalLocalRequests.incrementAndGet();
	}

	public double getAvgLatency()
	{
		return (double)totalLatency.get()/totalLocalRequests.get();
	}

	public void addROTransaction(long time)
	{
		totalROAtomicLength.addAndGet(time);
		totalROTransactions.incrementAndGet();
	}
	
	public double getAvgROAtomicLength()
	{
		return (double)totalROAtomicLength.get() / totalROTransactions.get();
	}
	
	public double getAvgRWAtomicLength()
	{
		return 0;
	}

	public void printAvg()
	{
		System.out.println("avgReadSetSize:\t"
				+ TestCounter.getInstance().getAvgReadSetSize());
		System.out.println("avgWriteSetSize:\t"
				+ TestCounter.getInstance().getAvgWriteSetSize());
		System.out.println("avgNewSetSize:\t"
				+ TestCounter.getInstance().getAvgNewSetSize());
		System.out.println("avgTypeSetSize:\t"
				+ TestCounter.getInstance().getAvgTypeSetSize());
		System.out.println("avgPackageSize:\t"
				+ TestCounter.getInstance().getAvgPackageSize());
	}
	
	// /**
	// * Adds given time to the counter measuring the total time spent on global
	// * conflict checks.
	// *
	// * @param time
	// * value to be added to the counter measuring the total time
	// * spent on global conflict checks
	// */
	// public void addGlobalConflictCheckTime(long time)
	// {
	// timeSpentOnGlobalConflictChecks.addAndGet(time);
	// }
	//
	// /**
	// * Returns the current value of the counter measuring the total time spent
	// * on global conflict checks.
	// *
	// * @return the current value of the counter measuring the total time spent
	// * on global conflict checks
	// */
	// public long getTimeSpentOnGlobalConflictChecks()
	// {
	// return timeSpentOnGlobalConflictChecks.get();
	// }
	//
	// /**
	// * Adds given time to the counter measuring the total time spent on local
	// * conflict checks.
	// *
	// * @param time
	// * value to be added to the counter measuring the total time
	// * spent on local conflict checks
	// */
	// public void addLocalConflictCheckTime(long time)
	// {
	// timeSpentOnLocalConflictChecks.addAndGet(time);
	// }
	//
	// /**
	// * Returns the current value of the counter measuring the total time spent
	// * on local conflict checks.
	// *
	// * @return the current value of the counter measuring the total time spent
	// * on local conflict checks
	// */
	// public long getTimeSpentOnLocalConflictChecks()
	// {
	// return timeSpentOnLocalConflictChecks.get();
	// }
	//
	// /**
	// * Adds given time to the counter measuring the total time spent on
	// updating
	// * the shared object library.
	// *
	// * @param time
	// * value to be added to the counter measuring the total time
	// * spent on updating the shared object library
	// * @see SharedObjectLibrary
	// */
	// public void addUpdatingTime(long time)
	// {
	// timeSpentOnUpdating.addAndGet(time);
	// }
	//
	// /**
	// * Returns the current value of the counter measuring the total time spent
	// * on updating shared object library.
	// *
	// * @return the current value of the counter measuring the total time spent
	// * on updating shared object library
	// * @see SharedObjectLibrary
	// */
	// public long getTimeSpentOnUpdating()
	// {
	// return timeSpentOnUpdating.get();
	// }
}
