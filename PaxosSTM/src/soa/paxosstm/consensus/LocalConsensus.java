/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.consensus;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.LinkedBlockingQueue;

import soa.paxosstm.abcast.AtomicBroadcast;
import soa.paxosstm.abcast.AtomicDeliverListener;
import soa.paxosstm.common.ByteArrayStorage;
import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.RecoveryListener;

/**
 * LocalConsensus imitates the behaviour of the JPaxos library. Should be used
 * only for testing purposes.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public final class LocalConsensus implements CommitableConsensus, ConsensusDelegateProposer,
		AtomicBroadcast
{
	private final BlockingQueue<byte[]> propositions = new LinkedBlockingQueue<byte[]>();
	private final byte[] scheduleCheckpointProposition = new byte[0];

	private final CopyOnWriteArrayList<ConsensusListener> consensusListeners = new CopyOnWriteArrayList<ConsensusListener>();
	private final List<RecoveryListener> recoveryListeners = new Vector<RecoveryListener>();
	private final List<CheckpointListener> checkpointListeners = new Vector<CheckpointListener>();

	int counter = 0;

	@Override
	public void addConsensusListener(ConsensusListener listener)
	{
		consensusListeners.add(listener);
	}

	@Override
	public void propose(byte[] obj)
	{
		// try
		// {
		// Thread.sleep(0);
		// }
		// catch (InterruptedException e)
		// {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		byte[] prop = new byte[obj.length + 4];
		ByteBuffer bb = ByteBuffer.wrap(prop);
		bb.putInt(obj.length);
		bb.put(obj);
		// prop = bb.array();
		propositions.add(prop);
		// propositions.add(obj);
	}

	@Override
	public void removeConsensusListener(ConsensusListener listener)
	{
		consensusListeners.remove(listener);
	}

	@Override
	public void start()
	{
		for (RecoveryListener listener : recoveryListeners)
		{
			listener.recoveryFinished();
		}

		new Thread()
		{
			@Override
			public void run()
			{
				while (true)
				{
					try
					{
						byte[] prop = propositions.take();
						if (prop == scheduleCheckpointProposition)
						{
							ByteArrayStorage bas = new ByteArrayStorage();
							for (CheckpointListener checkpointListener : checkpointListeners)
							{
								checkpointListener.onCheckpoint(0, bas);
							}
							for (CheckpointListener commitListener : checkpointListeners)
							{
								commitListener.onCheckpointFinished(0);
							}
						}
						else
						{
							for (ConsensusListener listener : consensusListeners)
							{
								listener.decide(prop);
							}
						}
					}
					catch (InterruptedException e)
					{
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	@Override
	public boolean addCheckpointListener(CheckpointListener listener)
	{
		return checkpointListeners.add(listener);
	}

	@Override
	public boolean removeCheckpointListener(CheckpointListener listener)
	{
		return checkpointListeners.remove(listener);
	}

	@Override
	public boolean addRecoveryListener(RecoveryListener listener)
	{
		return recoveryListeners.add(listener);
	}

	@Override
	public boolean removeRecoveryListener(RecoveryListener listener)
	{
		return recoveryListeners.remove(listener);
	}

	@Override
	public void scheduleCheckpoint()
	{
		propositions.add(scheduleCheckpointProposition);
	}

	@Override
	public ConsensusDelegateProposer getNewDelegateProposer() throws IOException
	{
		return this;
	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub
	}

	@Override
	public void abcast(byte[] obj)
	{
		propose(obj);
	}

	@Override
	public void addAtomicDeliverListener(final AtomicDeliverListener listener)
	{
		addConsensusListener(new ConsensusListener()
		{
			@Override
			public void decide(byte[] obj)
			{
				listener.adeliver(obj);
			}
		});
	}

	@Override
	public void blockingAbcast(byte[] obj)
	{
		propose(obj);
	}

	@Override
	public void removeAtomicDeliverListener(AtomicDeliverListener listener)
	{
		// TODO Auto-generated method stub

	}
}
