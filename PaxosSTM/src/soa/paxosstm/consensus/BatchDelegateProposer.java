package soa.paxosstm.consensus;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.concurrent.BlockingQueue;

import lsr.paxos.client.Client;

public class BatchDelegateProposer implements ConsensusDelegateProposer
{
	private final BlockingQueue<byte[]> requestsQueue;
	private final ArrayDeque<byte[]> myRequests = new ArrayDeque<byte[]>(64);
	private final Client client;

	public BatchDelegateProposer(BlockingQueue<byte[]> queue) throws IOException
	{
		requestsQueue = queue;
		client = new Client();
		client.connect();

		// Starting thread for new proposals
		Thread thread = new Thread()
		{
			public void run()
			{
				try
				{
					while (true)
					{
						requestsQueue.drainTo(myRequests);
						if (myRequests.isEmpty())
						{
							myRequests.add(requestsQueue.take());
							requestsQueue.drainTo(myRequests);
						}
						// long start = System.currentTimeMillis();
						client.execute(myRequests);
						// long latency = System.currentTimeMillis() - start;
						// TestCounter.getInstance().addLatency(latency);
						myRequests.clear();
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					return;
				}
			}
		};
		thread.start();
	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void propose(byte[] obj)
	{
		if (!requestsQueue.offer(obj))
			throw new RuntimeException();
	}

}
