package soa.paxosstm.consensus;

import java.io.IOException;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import javax.naming.OperationNotSupportedException;

import lsr.common.Configuration;
import lsr.paxos.replica.Replica;
import lsr.service.AbstractService;
import soa.paxosstm.abcast.AtomicBroadcast;
import soa.paxosstm.abcast.AtomicDeliverListener;
import soa.paxosstm.common.ByteArrayStorage;
import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.RecoveryListener;
import soa.paxosstm.common.StorageException;
import soa.paxosstm.dstm.internal.global.ClientPool;

public final class PaxosConsensus extends AbstractService implements CommitableConsensus, AtomicBroadcast
{
	private Replica replica;
	private BlockingQueue<Runnable> operationsToBeDone = new LinkedBlockingQueue<Runnable>();
	private List<RecoveryListener> recoveryListeners = new Vector<RecoveryListener>();
	private List<CheckpointListener> checkpointListeners = new Vector<CheckpointListener>();
	private AtomicDeliverListener adeliverListener = null;
	private int lastDeliveredRequest = -1;

	private static final int numberOfProposers = 0;
	// private int lastProposerUsed = 0;
	private ConsensusDelegateProposer[] proposers = new ConsensusDelegateProposer[numberOfProposers];
	private ClientPool clientPool;

	public PaxosConsensus(Configuration configuration, int localId) throws IOException
	{
		replica = new Replica(configuration, localId, this);
	}

	@Override
	public final void start() throws StorageException
	{
		try
		{
			replica.start();

			for (int i = 0; i < numberOfProposers; i++)
			{
				try
				{
					proposers[i] = getNewDelegateProposer();
				}
				catch (IOException e)
				{
					throw new StorageException(e);
				}
			}

			clientPool = new ClientPool();
			startThreads();
		}
		catch (IOException e)
		{
			throw new StorageException(e);
		}
	}

	private final void startThreads()
	{
		// Starting thread for all actions
		Thread thread = new Thread()
		{
			public void run()
			{
				try
				{
					while (true)
						operationsToBeDone.take().run();
				}
				catch (InterruptedException e)
				{
					throw new RuntimeException(e);
				}
			}
		};
		thread.start();
	}

	@Override
	public final byte[] execute(final byte[] value, final int seqNo)
	{
		operationsToBeDone.add(new Runnable()
		{
			public void run()
			{
				synchronized (adeliverListener)
				{
					adeliverListener.adeliver(value);
				}
				lastDeliveredRequest = seqNo;
			}
		});
		return new byte[0];
	}

	@Override
	public final void propose(byte[] obj)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public final void scheduleCheckpoint()
	{
		operationsToBeDone.add(new Runnable()
		{
			public void run()
			{
				// long start = System.currentTimeMillis();

				ByteArrayStorage bas = new ByteArrayStorage();
				for (CheckpointListener listener : checkpointListeners)
					listener.onCheckpoint(lastDeliveredRequest, bas);
				byte[] snapshot = bas.toByteArray();
				fireSnapshotMade(lastDeliveredRequest, snapshot, null);
				for (CheckpointListener listener : checkpointListeners)
					listener.onCheckpointFinished(lastDeliveredRequest);

				// long time = System.currentTimeMillis() - start;
				// System.out.println("checkpoint - seq num: " +
				// lastDeliveredRequest + " size: "
				// + snapshot.length + " creation time: " + time / 1000.0 +
				// "s");
			}
		});
	}

	@Override
	public final void updateToSnapshot(final int instanceId, final byte[] snapshot)
	{
		operationsToBeDone.add(new Runnable()
		{
			public void run()
			{
				lastDeliveredRequest = instanceId;
				for (RecoveryListener listener : recoveryListeners)
					listener.recoverFromCheckpoint(new ByteArrayStorage(snapshot));
			}
		});
	}

	@Override
	public final void recoveryFinished()
	{
		super.recoveryFinished();
		operationsToBeDone.add(new Runnable()
		{
			public void run()
			{
				for (RecoveryListener listener : recoveryListeners)
					listener.recoveryFinished();
			}
		});
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	@Override
	public final void addConsensusListener(ConsensusListener listener)
	{
	}

	@Override
	public final void removeConsensusListener(ConsensusListener listener)
	{
	}

	public final void setSingleAdeliverListener(AtomicDeliverListener listener)
	{
		this.adeliverListener = listener;
	}

	@Override
	public final boolean addCheckpointListener(CheckpointListener listener)
	{
		return checkpointListeners.add(listener);
	}

	@Override
	public final boolean removeCheckpointListener(CheckpointListener listener)
	{
		return checkpointListeners.remove(listener);
	}

	@Override
	public final boolean addRecoveryListener(RecoveryListener listener)
	{
		return recoveryListeners.add(listener);
	}

	@Override
	public final boolean removeRecoveryListener(RecoveryListener listener)
	{
		return recoveryListeners.remove(listener);
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	@Override
	public final void askForSnapshot(int lastSnapshotInstance)
	{
	}

	@Override
	public final void forceSnapshot(int lastSnapshotInstance)
	{
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	@Override
	public ConsensusDelegateProposer getNewDelegateProposer() throws IOException
	{
		return new ConsensusDelegateProposerImpl();
	}

	/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

	public final int getHighestExecuteSeqNo()
	{
		return lastDeliveredRequest;
	}

	@Override
	public void abcast(byte[] obj)
	{
//		ConsensusDelegateProposer proposer;
//		synchronized (proposers)
//		{
//			proposer = proposers[lastProposerUsed++];
//			lastProposerUsed %= numberOfProposers;
//		}
//		proposer.propose(obj);
		clientPool.execute(obj);
	}
	
	@Override
	public void blockingAbcast(byte[] obj)
	{
		clientPool.execute(obj);
	}

	@Override
	public void addAtomicDeliverListener(AtomicDeliverListener listener)
	{
	}

	@Override
	public void removeAtomicDeliverListener(AtomicDeliverListener listener)
	{
	}
}
