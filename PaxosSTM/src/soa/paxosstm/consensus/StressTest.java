/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.consensus;

import java.io.IOException;
import java.nio.ByteBuffer;

import lsr.common.Configuration;
import lsr.paxos.test.utils.BarrierClient;
import soa.paxosstm.common.StorageException;

/**
 * The class can be used to test the module solving the distributed consensus
 * problem. Since this class is not used normally by Paxos STM it is not
 * properly secured (some exceptions are thrown by the
 * {@link StressTest#main(String[])} method instead of being handled.
 * 
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
public class StressTest
{
	/**
	 * The main method, used for invoking test methods. The brief description of
	 * the tests is presented upon starting the application.
	 * 
	 * @param args
	 *            determines which test to be run
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws StorageException 
	 */
	public static void main(String[] args) throws IOException, InterruptedException, StorageException
	{
		CommitableConsensus consensus;

		if (args.length < 1)
		{
			System.out.println("usage: replica_id [threads [repeats [size [rounds]]]]");
			System.out.println("replica_id = -1   -> LocalConsensus test");
			System.out.println("threads <= 0      -> no test run");
			System.exit(1);
		}

		int localId = 0;
		try
		{
			localId = Integer.parseInt(args[0]);
		}
		catch (NumberFormatException e)
		{
			System.err.println("Need replica number");
			System.exit(1);
		}

		if (localId == -1)
		{
			consensus = new LocalConsensus();
			localId = 0;
		}
		else
		{
			Configuration conf = new Configuration();
			consensus = new SerializablePaxosConsensus(conf, localId);
			//consensus = new PaxosConsensus(conf, localId);
		}

		int threads = 20;
		int size = 1;
		long mills = 10000;
		int rounds = 30;
		String host = "localhost";
		int port = 1234;
		try
		{
			threads = Integer.parseInt(args[1]);
			size = Integer.parseInt(args[2]);
			mills = Long.parseLong(args[3]);
			rounds = Integer.parseInt(args[4]);
			host = args[5];
			port = Integer.parseInt(args[6]);
		}
		catch (Exception e)
		{
		}

		if (threads > 0)
		{
			stressTest(consensus, localId, threads, size, mills, rounds, host, port);
			System.exit(0);
		}
		else
		{
			consensus.start();
		}
	}

	private static void stressTest(final CommitableConsensus consensus, final int localId,
			final int numberOfThreads, final int packetSize, final long mills,
			final int numberOfRounds, final String host, final int port)
			throws InterruptedException, IOException, StorageException
	{
		long totalTime = 0;
		int totalRequests = 0;

		consensus.start();

		final int numberOfProposers = numberOfThreads;
		ConsensusDelegateProposer[] proposers = new ConsensusDelegateProposer[numberOfProposers];
		for (int i = 0; i < numberOfProposers; i++)
			proposers[i] = consensus.getNewDelegateProposer();

		class TestRunner implements Runnable
		{
			private int v;
			private ConsensusDelegateProposer proposer;

			public long start;
			public long end;
			public long mills;
			public int requests;

			public TestRunner(int v, ConsensusDelegateProposer proposer, long mills)
					throws IOException
			{
				this.v = v;
				this.proposer = proposer;
				this.mills = mills;
			}

			private void makeProposition()
			{
				byte[] proposition = new byte[packetSize];
				ByteBuffer bb = ByteBuffer.wrap(proposition);
				bb.asIntBuffer().put(0, v);
				proposer.propose(proposition);
			}

			public void run()
			{
				requests = 0;
				start = System.currentTimeMillis();
				while ((end = System.currentTimeMillis()) - start <= mills)
				{
					synchronized (this)
					{
						makeProposition();
						try
						{
							wait();
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
					requests++;
				}
			}
		}

		final TestRunner[] runners = new TestRunner[numberOfThreads];
		for (int i = 0; i < numberOfThreads; i++)
		{
			runners[i] = new TestRunner(localId * 1000 + i, proposers[i % numberOfProposers], mills);
			consensus.addConsensusListener(new ConsensusListener()
			{
				@Override
				public void decide(byte[] obj)
				{
					ByteBuffer bb = ByteBuffer.wrap(obj);
					int v = bb.asIntBuffer().get(0);
					if (v / 1000 != localId)
						return;
					int r = v % 1000;
					synchronized (runners[r])
					{
						runners[r].notifyAll();
					}
				}
			});
		}

		System.out
				.println("      round     threads           total requests       avg time     requests per second");
		for (int round = 1; round <= numberOfRounds; round++)
		{
			totalTime = 0;
			totalRequests = 0;
			BarrierClient barrierClient = new BarrierClient();

			Thread[] threads = new Thread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new Thread(runners[i]);
			}
			barrierClient.enterBarrier(host, port, 0, 0, numberOfThreads);
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				totalTime += runners[i].end - runners[i].start;
				totalRequests += runners[i].requests;
				threads[i] = null;
			}
			barrierClient.enterBarrier(host, port, totalRequests, totalTime, numberOfThreads);
			consensus.scheduleCheckpoint();
			threads = null;

			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			double instPerSec = totalRequests / avgTime;
			String format = "%d %d %d %3d       %3d                 %10d        %8.3f           %10.2f\n";
			System.out.format(format, 0, 0, 0, round, numberOfThreads, totalRequests, avgTime,
					instPerSec);
			consensus.scheduleCheckpoint();
		}
	}
}
