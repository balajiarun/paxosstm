package soa.paxosstm.consensus;

public interface ConsensusDelegateProposer {
    void propose(byte[] obj);

    void dispose();
}
