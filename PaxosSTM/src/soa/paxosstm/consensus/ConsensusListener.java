package soa.paxosstm.consensus;

public interface ConsensusListener {
    /**
     * Executed when a decision has been taken. Includes decisions proposed by
     * self.
     */
    void decide(byte[] obj);
}
