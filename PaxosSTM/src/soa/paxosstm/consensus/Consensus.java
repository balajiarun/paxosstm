package soa.paxosstm.consensus;

import java.io.IOException;

import soa.paxosstm.common.StorageException;

public interface Consensus {
    /**
     * Blocking method. Gets the object proposed and decided. Executed on
     * default ConsensusDelegateProposer.
     */
    void propose(byte[] obj);

    ConsensusDelegateProposer getNewDelegateProposer() throws IOException;

    void start() throws IOException, StorageException;

    void addConsensusListener(ConsensusListener listener);

    void removeConsensusListener(ConsensusListener listener);
}
