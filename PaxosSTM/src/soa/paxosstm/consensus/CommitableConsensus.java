package soa.paxosstm.consensus;

import soa.paxosstm.common.Checkpointable;

public interface CommitableConsensus extends Checkpointable, Consensus {

}
