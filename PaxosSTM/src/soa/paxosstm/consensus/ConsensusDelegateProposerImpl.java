package soa.paxosstm.consensus;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import lsr.paxos.client.Client;

public class ConsensusDelegateProposerImpl implements ConsensusDelegateProposer {

    private Client client;

    private BlockingQueue<byte[]> objectsToPropose = new LinkedBlockingQueue<byte[]>();

    public ConsensusDelegateProposerImpl() throws IOException 
    {
     	client = new Client();
        client.connect();

        // Starting thread for new proposals
        Thread thread = new Thread() {
            public void run() {
                try {
                    while (true)
                        client.execute(Collections.singletonList(objectsToPropose.take()));
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        };
        thread.start();
    }

    public void dispose() {
    }

    public void propose(byte[] obj) {
        objectsToPropose.add(obj);
    }

}
