package soa.paxosstm.consensus;

import java.io.IOException;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import lsr.common.Configuration;
import lsr.paxos.replica.Replica;
import lsr.service.AbstractService;
import soa.paxosstm.common.ByteArrayStorage;
import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.RecoveryListener;

public class SerializablePaxosConsensus extends AbstractService implements CommitableConsensus {

    private Replica replica;
    private ConsensusDelegateProposer client;
    private BlockingQueue<Runnable> operationsToBeDone = new LinkedBlockingQueue<Runnable>();
    private List<ConsensusListener> consensusListeners = new Vector<ConsensusListener>();
    private List<RecoveryListener> recoveryListeners = new Vector<RecoveryListener>();
    private List<CheckpointListener> commitListeners = new Vector<CheckpointListener>();
    private int lastDeliveredRequest = -1;

    public SerializablePaxosConsensus(Configuration configuration, int localId)
            throws IOException {
        replica = new Replica(configuration, localId, this);
    }

    public final void start() throws IOException {
        replica.start();

        client = new ConsensusDelegateProposerImpl();

        startThreads();
    }

    private final void startThreads() {
        // Starting thread for all actions
        Thread thread = new Thread() {
            public void run() {
                try {
                    while (true)
                        operationsToBeDone.take().run();
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
        };
        thread.start();
    }

    public final byte[] execute(final byte[] value, final int seqNo) {
        operationsToBeDone.add(new Runnable() {
            public void run() {
                synchronized (consensusListeners) {
                    for (ConsensusListener l : consensusListeners)
                        l.decide(value);
                }
                lastDeliveredRequest = seqNo;
            }
        });
        return new byte[0];
    }

    public final void propose(byte[] obj) {
        client.propose(obj);
    }

    public final void scheduleCheckpoint() {
        operationsToBeDone.add(new Runnable() {
            public void run() {
            	ByteArrayStorage bas = new ByteArrayStorage();
                for (CheckpointListener listener : commitListeners)
                    listener.onCheckpoint(lastDeliveredRequest, bas);
                fireSnapshotMade(lastDeliveredRequest, bas.toByteArray(), null);
                for (CheckpointListener listener : commitListeners)
                    listener.onCheckpointFinished(lastDeliveredRequest);
            }
        });
    }

    public final void updateToSnapshot(final int instanceId, final byte[] snapshot) {
        operationsToBeDone.add(new Runnable() {
            public void run() {
                lastDeliveredRequest = instanceId;
                for (RecoveryListener listener : recoveryListeners)
                    listener.recoverFromCheckpoint(new ByteArrayStorage(snapshot));
            }
        });
    }

    public final void recoveryFinished() {
        super.recoveryFinished();
        operationsToBeDone.add(new Runnable() {
            public void run() {
                for (RecoveryListener listener : recoveryListeners)
                    listener.recoveryFinished();
            }
        });
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    public final void addConsensusListener(ConsensusListener listener) {
        synchronized (consensusListeners) {
            consensusListeners.add(listener);
        }
    }

    public final void removeConsensusListener(ConsensusListener listener) {
        synchronized (consensusListeners) {
            consensusListeners.remove(listener);
        }
    }

    public final boolean addCheckpointListener(CheckpointListener listener) {
        return commitListeners.add(listener);
    }

    public final boolean removeCheckpointListener(CheckpointListener listener) {
        return commitListeners.remove(listener);
    }

    public final boolean addRecoveryListener(RecoveryListener listener) {
        return recoveryListeners.add(listener);
    }

    public final boolean removeRecoveryListener(RecoveryListener listener) {
        return recoveryListeners.remove(listener);
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    public final void askForSnapshot(int lastSnapshotInstance) {
    }

    public final void forceSnapshot(int lastSnapshotInstance) {
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    public ConsensusDelegateProposer getNewDelegateProposer() throws IOException {
        return new ConsensusDelegateProposerImpl();
    }

    /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

    public final int getHighestExecuteSeqNo() {
        return lastDeliveredRequest;
    }

}