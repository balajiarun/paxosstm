package soa.paxosstm.common;

public interface CheckpointListener
{
	void onCheckpoint(int seqNumber, Storage storage);

	void onCheckpointFinished(int seqNumber);
}
