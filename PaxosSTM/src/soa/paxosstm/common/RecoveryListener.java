package soa.paxosstm.common;

public interface RecoveryListener
{
	void recoverFromCheckpoint(Storage storage);

	void recoveryFinished();
}
