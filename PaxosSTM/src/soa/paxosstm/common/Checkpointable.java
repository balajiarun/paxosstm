package soa.paxosstm.common;


public interface Checkpointable {
    void scheduleCheckpoint();

    boolean addCheckpointListener(CheckpointListener listener);

    boolean removeCheckpointListener(CheckpointListener listener);

    boolean addRecoveryListener(RecoveryListener listener);

    boolean removeRecoveryListener(RecoveryListener listener);
}
