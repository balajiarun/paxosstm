package soa.paxosstm.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


public class StorageImpl implements Storage
{
	private String directory;

	public StorageImpl(String directory) throws StorageException
	{
		if (directory.endsWith("/")) 
			this.directory = directory;
		else
			this.directory = directory + "/";
		
		File d = new File(directory);
		if (!d.exists() && !d.mkdir())
		{
			throw new StorageException("Cannot create directory " + directory, null);
		}
		d.setExecutable(true);
		d.setWritable(true);
	}

	@Override
	public void log(Serializable key, Serializable value) throws StorageException
	{
		if (value == null)
		{
			File f = new File(directory + key.toString());
			if (!f.delete())
				throw new StorageException("Cannot remove file " + key.toString(), null);
		}

		try
		{
			FileOutputStream fout = new FileOutputStream(directory + key.toString());
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(value);
			oos.close();
		}
		catch (Exception e)
		{
			throw new StorageException(e);
		}
	}

	@Override
	public Object retrieve(Serializable key) throws StorageException
	{
		Object ret = null;
		try
		{
			FileInputStream fin = new FileInputStream(directory + key.toString());
			ObjectInputStream ois = new ObjectInputStream(fin);
			ret = ois.readObject();
			ois.close();
		}
		catch (Exception e)
		{
			throw new StorageException(e);
		}

		return ret;
	}

}
