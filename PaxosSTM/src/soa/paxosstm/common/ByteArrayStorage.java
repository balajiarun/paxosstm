package soa.paxosstm.common;

import java.io.Serializable;
import java.util.HashMap;

import soa.paxosstm.utils.SerializationHelper;

public class ByteArrayStorage implements Storage, Serializable
{
	private static final long serialVersionUID = 1L;
	private final HashMap<Serializable, Serializable> data;

	public ByteArrayStorage()
	{
		data = new HashMap<Serializable, Serializable>();
	}

	@SuppressWarnings("unchecked")
	public ByteArrayStorage(byte[] bytes)
	{
		data = (HashMap<Serializable, Serializable>) SerializationHelper.byteArrayToObject(bytes);
	}

	@Override
	public void log(Serializable key, Serializable value) throws StorageException
	{
		data.put(key, value);
	}

	@Override
	public Object retrieve(Serializable key) throws StorageException
	{
		return data.get(key);
	}

	public byte[] toByteArray()
	{
		return SerializationHelper.byteArrayFromObject(data);
	}
}
