package soa.paxosstm.utils;

import java.util.AbstractList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;

import soa.paxosstm.dstm.TransactionObject;

//http://www.docjar.com/html/api/org/apache/commons/collections/list/TreeList.java.html
//http://svn.apache.org/repos/asf/directory/apacheds/trunk/core-avl/src/main/java/org/apache/directory/server/core/avltree/LinkedAvlNode.java

//FIXME SubLists i final fields, methods, arguments

@TransactionObject
public class TransactionalTreeList<E> implements List<E>, Set<E>
{
	private AVLNode<E> root;

	public TransactionalTreeList()
	{
		super();
	}

	public TransactionalTreeList(Collection<E> coll)
	{
		super();
		addAll(coll);
	}

	@Override
	public boolean add(E e)
	{
		if (e == null)
			return false;
		
		if (root == null)
		{
			root = new AVLNode<E>(0, e, null, null);
		}
		else
		{
			AVLNode<E> max = root.max();
			max.insertOnRight(1, e);
		}
		return true;
	}

	@Override
	public void add(int index, E obj)
	{
		if (obj == null)
			return;
		
		if (root == null)
		{
			root = new AVLNode<E>(index, obj, null, null);
		}
		else
		{
			root = root.insert(index, obj);
		}
	}

	@Override
	public boolean addAll(Collection<? extends E> c)
	{
		if (c == null)
			return false;
		
		Iterator<? extends E> iter = c.iterator();
		E current = iter.next();
		AVLNode<E> currentNode;
		if (root == null)
		{
			root = new AVLNode<E>(0, current, null, null);
			currentNode = root;
		}
		else
		{
			currentNode = root.max();
		}
		
		while (iter.hasNext())
		{
			currentNode.insertOnRight(1, iter.next());
			currentNode = currentNode.max();
		}
		
		return true;
	}

	@Override
	public boolean addAll(int index, Collection<? extends E> c)
	{
		boolean modified = false;
		for (E e : c)
		{
			add(index++, e);
			modified = true;
		}
		return modified;
	}

	@Override
	public void clear()
	{
		root = null;
	}

	@Override
	public boolean contains(Object object)
	{
		return (indexOf(object) >= 0);
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		for (Object object : c)
		{
			if (!contains(object))
				return false;
		}
		return true;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean equals(Object o)
	{
		if (o == this)
			return true;
		if (!(o instanceof List))
			return false;

		ListIterator<E> e1 = listIterator();
		ListIterator e2 = ((List) o).listIterator();
		while (e1.hasNext() && e2.hasNext())
		{
			E o1 = e1.next();
			Object o2 = e2.next();
			if (!(o1 == null ? o2 == null : o1.equals(o2)))
				return false;
		}
		return !(e1.hasNext() || e2.hasNext());
	}

	@Override
	public E get(int index)
	{
		AVLNode<E> node = root.get(index);
		if (node == null)
			throw new IndexOutOfBoundsException();
		return node.getValue();
	}

	@Override
	public int hashCode()
	{
		int hashCode = 1;
		for (E e : this)
			hashCode = 31 * hashCode + (e == null ? 0 : e.hashCode());
		return hashCode;
	}

	@Override
	public int indexOf(Object object)
	{
		if (root == null)
		{
			return -1;
		}			
		return root.indexOf(object, root.relativePosition);
	}

	@Override
	public boolean isEmpty()
	{
		return root != null;
	}

	@Override
	public Iterator<E> iterator()
	{
		return listIterator(0);
	}

	@Override
	public int lastIndexOf(Object o)
	{	
		//FIXME
		ListIterator<E> it = listIterator(size());
		
		
		if (o == null)
		{
			while (it.hasPrevious())
				if (it.previous() == null)
					return it.nextIndex();
		}
		else
		{
			while (it.hasPrevious())
				if (o.equals(it.previous()))
					return it.nextIndex();
		}
		return -1;
	}

	@Override
	public ListIterator<E> listIterator()
	{
		return listIterator(0);
	}

	@Override
	public ListIterator<E> listIterator(int fromIndex)
	{
		// FIXME co jak index za duzy?
		return new TreeListIterator<E>(this, fromIndex);
	}

	@Override
	public E remove(int index)
	{
		E result = get(index);
		if (result == null)
			throw new IndexOutOfBoundsException();
		root = root.remove(index);
		return result;
	}

	@Override
	public boolean remove(Object o)
	{
		if (root == null)
		{
			return false;
		}			
		int index = root.indexOf(o, root.relativePosition);
		if (index == -1)
			return false;
		root.remove(index);
		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		for (Object object : c)
		{
			remove(object);
		}
		return true;
	}

	protected void removeRange(int fromIndex, int toIndex)
	{
		ListIterator<E> it = listIterator(fromIndex);
		for (int i = 0, n = toIndex - fromIndex; i < n; i++)
		{
			it.next();
			it.remove();
		}
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		// FIXME
		return false;
	}

	@Override
	public E set(int index, E obj)
	{
		// checkInterval(index, 0, size() - 1);
		AVLNode<E> node = root.get(index);
		if (node == null)
			throw new IndexOutOfBoundsException();
		E result = node.value;
		node.setValue(obj);
		return result;
	}

	@Override
	public int size()
	{
		return root.getLeftSubTree().size() + 1 + root.getRightSubTree().size();
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex)
	{
		return (this instanceof RandomAccess ? new RandomAccessSubList<E>(this,
				fromIndex, toIndex) : new SubList<E>(this, fromIndex, toIndex));
	}

	@Override
	public Object[] toArray()
	{
		// override to go 20% faster
		Object[] array = new Object[size()];
		if (root != null)
		{
			root.toArray(array, root.relativePosition);
		}
		return array;
	}

	@Override
	public <T> T[] toArray(T[] a)
	{
		if (root != null)
		{
			root.toArray(a, root.relativePosition);
		}
		return a;
	}

	@TransactionObject
	static class AVLNode<E>
	{
		private AVLNode<E> left;
		private boolean leftIsPrevious;
		private AVLNode<E> right;
		private boolean rightIsNext;
		private int height;
		private int relativePosition;
		private E value;

		private AVLNode(int relativePosition, E obj, AVLNode<E> rightFollower,
				AVLNode<E> leftFollower)
		{
			this.relativePosition = relativePosition;
			value = obj;
			rightIsNext = true;
			leftIsPrevious = true;
			right = rightFollower;
			left = leftFollower;
		}

		E getValue()
		{
			return value;
		}

		void setValue(E obj)
		{
			this.value = obj;
		}

		AVLNode<E> get(int index)
		{
			int indexRelativeToMe = index - relativePosition;

			if (indexRelativeToMe == 0)
			{
				return this;
			}

			AVLNode<E> nextNode = ((indexRelativeToMe < 0) ? getLeftSubTree()
					: getRightSubTree());
			if (nextNode == null)
			{
				return null;
			}
			return nextNode.get(indexRelativeToMe);
		}

		int indexOf(Object object, int index)
		{
			if (getLeftSubTree() != null)
			{
				int result = left
						.indexOf(object, index + left.relativePosition);
				if (result != -1)
				{
					return result;
				}
			}
			if (value == null ? value == object : value.equals(object))
			{
				return index;
			}
			if (getRightSubTree() != null)
			{
				return right.indexOf(object, index + right.relativePosition);
			}
			return -1;
		}

		void toArray(Object[] array, int index)
		{
			array[index] = value;
			if (getLeftSubTree() != null)
			{
				left.toArray(array, index + left.relativePosition);
			}
			if (getRightSubTree() != null)
			{
				right.toArray(array, index + right.relativePosition);
			}
		}

		AVLNode<E> next()
		{
			if (rightIsNext || right == null)
			{
				return right;
			}
			return right.min();
		}

		AVLNode<E> previous()
		{
			if (leftIsPrevious || left == null)
			{
				return left;
			}
			return left.max();
		}

		AVLNode<E> insert(int index, E obj)
		{
			int indexRelativeToMe = index - relativePosition;

			if (indexRelativeToMe <= 0)
			{
				return insertOnLeft(indexRelativeToMe, obj);
			}
			else
			{
				return insertOnRight(indexRelativeToMe, obj);
			}
		}

		private AVLNode<E> insertOnLeft(int indexRelativeToMe, E obj)
		{
			AVLNode<E> ret = this;

			if (getLeftSubTree() == null)
			{
				setLeft(new AVLNode<E>(-1, obj, this, left), null);
			}
			else
			{
				setLeft(left.insert(indexRelativeToMe, obj), null);
			}

			if (relativePosition >= 0)
			{
				relativePosition++;
			}
			ret = balance();
			recalcHeight();
			return ret;
		}

		private AVLNode<E> insertOnRight(int indexRelativeToMe, E obj)
		{
			AVLNode<E> ret = this;

			if (getRightSubTree() == null)
			{
				setRight(new AVLNode<E>(+1, obj, right, this), null);
			}
			else
			{
				setRight(right.insert(indexRelativeToMe, obj), null);
			}
			if (relativePosition < 0)
			{
				relativePosition--;
			}
			ret = balance();
			recalcHeight();
			return ret;
		}

		private AVLNode<E> getLeftSubTree()
		{
			return (leftIsPrevious ? null : left);
		}

		private AVLNode<E> getRightSubTree()
		{
			return (rightIsNext ? null : right);
		}

		private AVLNode<E> max()
		{
			return (getRightSubTree() == null) ? this : right.max();
		}

		private AVLNode<E> min()
		{
			return (getLeftSubTree() == null) ? this : left.min();
		}

		AVLNode<E> remove(int index)
		{
			int indexRelativeToMe = index - relativePosition;

			if (indexRelativeToMe == 0)
			{
				return removeSelf();
			}
			if (indexRelativeToMe > 0)
			{
				setRight(right.remove(indexRelativeToMe), right.right);
				if (relativePosition < 0)
				{
					relativePosition++;
				}
			}
			else
			{
				setLeft(left.remove(indexRelativeToMe), left.left);
				if (relativePosition > 0)
				{
					relativePosition--;
				}
			}
			recalcHeight();
			return balance();
		}

		private AVLNode<E> removeMax()
		{
			if (getRightSubTree() == null)
			{
				return removeSelf();
			}
			setRight(right.removeMax(), right.right);
			if (relativePosition < 0)
			{
				relativePosition++;
			}
			recalcHeight();
			return balance();
		}

		private AVLNode<E> removeMin()
		{
			if (getLeftSubTree() == null)
			{
				return removeSelf();
			}
			setLeft(left.removeMin(), left.left);
			if (relativePosition > 0)
			{
				relativePosition--;
			}
			recalcHeight();
			return balance();
		}

		private AVLNode<E> removeSelf()
		{
			if (getRightSubTree() == null && getLeftSubTree() == null)
			{
				return null;
			}
			if (getRightSubTree() == null)
			{
				if (relativePosition > 0)
				{
					left.relativePosition += relativePosition
							+ (relativePosition > 0 ? 0 : 1);
				}
				left.max().setRight(null, right);
				return left;
			}
			if (getLeftSubTree() == null)
			{
				right.relativePosition += relativePosition
						- (relativePosition < 0 ? 0 : 1);
				right.min().setLeft(null, left);
				return right;
			}

			if (heightRightMinusLeft() > 0)
			{
				// more on the right, so delete from the right
				AVLNode<E> rightMin = right.min();
				value = rightMin.value;
				if (leftIsPrevious)
				{
					left = rightMin.left;
				}
				right = right.removeMin();
				if (relativePosition < 0)
				{
					relativePosition++;
				}
			}
			else
			{
				// more on the left or equal, so delete from the left
				AVLNode<E> leftMax = left.max();
				value = leftMax.value;
				if (rightIsNext)
				{
					right = leftMax.right;
				}
				AVLNode<E> leftPrevious = left.left;
				left = left.removeMax();
				if (left == null)
				{
					// special case where left that was deleted was a double
					// link
					// only occurs when height difference is equal
					left = leftPrevious;
					leftIsPrevious = true;
				}
				if (relativePosition > 0)
				{
					relativePosition--;
				}
			}
			recalcHeight();
			return this;
		}

		private AVLNode<E> balance()
		{
			switch (heightRightMinusLeft())
			{
			case 1:
			case 0:
			case -1:
				return this;
			case -2:
				if (left.heightRightMinusLeft() > 0)
				{
					setLeft(left.rotateLeft(), null);
				}
				return rotateRight();
			case 2:
				if (right.heightRightMinusLeft() < 0)
				{
					setRight(right.rotateRight(), null);
				}
				return rotateLeft();
			default:
				throw new RuntimeException("tree inconsistent!");
			}
		}

		private int getOffset(AVLNode<E> node)
		{
			if (node == null)
			{
				return 0;
			}
			return node.relativePosition;
		}

		private int setOffset(AVLNode<E> node, int newOffest)
		{
			if (node == null)
			{
				return 0;
			}
			int oldOffset = getOffset(node);
			node.relativePosition = newOffest;
			return oldOffset;
		}

		private void recalcHeight()
		{
			height = Math.max(getLeftSubTree() == null ? -1
					: getLeftSubTree().height, getRightSubTree() == null ? -1
					: getRightSubTree().height) + 1;
		}

		private int getHeight(AVLNode<E> node)
		{
			return (node == null ? -1 : node.height);
		}

		private int heightRightMinusLeft()
		{
			return getHeight(getRightSubTree()) - getHeight(getLeftSubTree());
		}

		private AVLNode<E> rotateLeft()
		{
			AVLNode<E> newTop = right; // can't be faedelung!
			AVLNode<E> movedNode = getRightSubTree().getLeftSubTree();

			int newTopPosition = relativePosition + getOffset(newTop);
			int myNewPosition = -newTop.relativePosition;
			int movedPosition = getOffset(newTop) + getOffset(movedNode);

			setRight(movedNode, newTop);
			newTop.setLeft(this, null);

			setOffset(newTop, newTopPosition);
			setOffset(this, myNewPosition);
			setOffset(movedNode, movedPosition);
			return newTop;
		}

		private AVLNode<E> rotateRight()
		{
			AVLNode<E> newTop = left; // can't be faedelung
			AVLNode<E> movedNode = getLeftSubTree().getRightSubTree();

			int newTopPosition = relativePosition + getOffset(newTop);
			int myNewPosition = -newTop.relativePosition;
			int movedPosition = getOffset(newTop) + getOffset(movedNode);

			setLeft(movedNode, newTop);
			newTop.setRight(this, null);

			setOffset(newTop, newTopPosition);
			setOffset(this, myNewPosition);
			setOffset(movedNode, movedPosition);
			return newTop;
		}

		private void setLeft(AVLNode<E> node, AVLNode<E> previous)
		{
			leftIsPrevious = (node == null);
			left = (leftIsPrevious ? previous : node);
			recalcHeight();
		}

		private void setRight(AVLNode<E> node, AVLNode<E> next)
		{
			rightIsNext = (node == null);
			right = (rightIsNext ? next : node);
			recalcHeight();
		}

		// private void checkFaedelung() {
		// AVLNode maxNode = left.max();
		// if (!maxNode.rightIsFaedelung || maxNode.right != this) {
		// throw new RuntimeException(maxNode + " should right-faedel to " +
		// this);
		// }
		// AVLNode minNode = right.min();
		// if (!minNode.leftIsFaedelung || minNode.left != this) {
		// throw new RuntimeException(maxNode + " should left-faedel to " +
		// this);
		// }
		// }
		//
		// private int checkTreeDepth() {
		// int hright = (getRightSubTree() == null ? -1 :
		// getRightSubTree().checkTreeDepth());
		// // System.out.print("checkTreeDepth");
		// // System.out.print(this);
		// // System.out.print(" left: ");
		// // System.out.print(_left);
		// // System.out.print(" right: ");
		// // System.out.println(_right);
		//
		// int hleft = (left == null ? -1 : left.checkTreeDepth());
		// if (height != Math.max(hright, hleft) + 1) {
		// throw new RuntimeException(
		// "height should be max" + hleft + "," + hright + " but is " + height);
		// }
		// return height;
		// }
		//
		// private int checkLeftSubNode() {
		// if (getLeftSubTree() == null) {
		// return 0;
		// }
		// int count = 1 + left.checkRightSubNode();
		// if (left.relativePosition != -count) {
		// throw new RuntimeException();
		// }
		// return count + left.checkLeftSubNode();
		// }
		//
		// private int checkRightSubNode() {
		// AVLNode right = getRightSubTree();
		// if (right == null) {
		// return 0;
		// }
		// int count = 1;
		// count += right.checkLeftSubNode();
		// if (right.relativePosition != count) {
		// throw new RuntimeException();
		// }
		// return count + right.checkRightSubNode();
		// }

		public String toString()
		{
			return "AVLNode(" + relativePosition + "," + (left != null) + ","
					+ value + "," + (getRightSubTree() != null)
					+ ", faedelung " + rightIsNext + " )";
		}

		public int size()
		{
			return ((left != null) ? left.size() : 0) + 1
					+ ((right != null) ? right.size() : 0);
		}
	}

	static class TreeListIterator<E> implements ListIterator<E>
	{
		protected final TransactionalTreeList<E> parent;

		protected AVLNode<E> next;

		protected int nextIndex;

		protected AVLNode<E> current;

		protected int currentIndex;

		// protected int expectedModCount;

		protected TreeListIterator(TransactionalTreeList<E> parent,
				int fromIndex) throws IndexOutOfBoundsException
		{
			super();
			this.parent = parent;
			// this.expectedModCount = parent.modCount;
			this.next = (parent.root == null ? null : parent.root
					.get(fromIndex));
			this.nextIndex = fromIndex;
			this.currentIndex = -1;
		}

		// protected void checkModCount()
		// {
		// if (parent.modCount != expectedModCount)
		// {
		// throw new ConcurrentModificationException();
		// }
		// }

		public boolean hasNext()
		{
			// MAJOR FIXME
			return (nextIndex < parent.size());
		}

		public E next()
		{
			if (!hasNext())
			{
				throw new NoSuchElementException("No element at index "
						+ nextIndex + ".");
			}
			if (next == null)
			{
				next = parent.root.get(nextIndex);
			}
			E value = next.getValue();
			current = next;
			currentIndex = nextIndex++;
			next = next.next();
			return value;
		}

		public boolean hasPrevious()
		{
			return (nextIndex > 0);
		}

		public E previous()
		{
			if (!hasPrevious())
			{
				throw new NoSuchElementException("Already at start of list.");
			}
			if (next == null)
			{
				next = parent.root.get(nextIndex - 1);
			}
			else
			{
				next = next.previous();
			}
			E value = next.getValue();
			current = next;
			currentIndex = --nextIndex;
			return value;
		}

		public int nextIndex()
		{
			return nextIndex;
		}

		public int previousIndex()
		{
			return nextIndex() - 1;
		}

		public void remove()
		{
			if (currentIndex == -1)
			{
				throw new IllegalStateException();
			}
			if (nextIndex == currentIndex)
			{
				// remove() following previous()
				next = next.next();
				parent.remove(currentIndex);
			}
			else
			{
				// remove() following next()
				parent.remove(currentIndex);
				nextIndex--;
			}
			current = null;
			currentIndex = -1;
			// expectedModCount++;
		}

		public void set(E obj)
		{
			// checkModCount();
			if (current == null)
			{
				throw new IllegalStateException();
			}
			current.setValue(obj);
		}

		public void add(E obj)
		{
			// checkModCount();
			parent.add(nextIndex, obj);
			current = null;
			currentIndex = -1;
			nextIndex++;
			// expectedModCount++;
		}

	}

	class SubList<E> extends AbstractList<E>
	{
		private final List<E> l;
		private final int offset;
		private int size;

		SubList(List<E> list, int fromIndex, int toIndex)
		{
			if (fromIndex < 0)
				throw new IndexOutOfBoundsException("fromIndex = " + fromIndex);
			if (toIndex > list.size())
				throw new IndexOutOfBoundsException("toIndex = " + toIndex);
			if (fromIndex > toIndex)
				throw new IllegalArgumentException("fromIndex(" + fromIndex
						+ ") > toIndex(" + toIndex + ")");
			l = list;
			offset = fromIndex;
		}

		public E set(int index, E element)
		{
			rangeCheck(index);
			return l.set(index + offset, element);
		}

		public E get(int index)
		{
			rangeCheck(index);
			return l.get(index + offset);
		}

		public int size()
		{
			return 0;
		}

		public void add(int index, E element)
		{
			rangeCheckForAdd(index);
			l.add(index + offset, element);
		}

		public E remove(int index)
		{
			rangeCheck(index);
			E result = l.remove(index + offset);
			return result;
		}

		protected void removeRange(int fromIndex, int toIndex)
		{
			// FIXME
			// l.removeRange(fromIndex + offset, toIndex + offset);
			size -= (toIndex - fromIndex);
		}

		public boolean addAll(Collection<? extends E> c)
		{
			return addAll(size, c);
		}

		public boolean addAll(int index, Collection<? extends E> c)
		{
			rangeCheckForAdd(index);
			int cSize = c.size();
			if (cSize == 0)
				return false;

			l.addAll(offset + index, c);
			return true;
		}

		public Iterator<E> iterator()
		{
			return listIterator();
		}

		public ListIterator<E> listIterator(final int index)
		{
			rangeCheckForAdd(index);

			return new ListIterator<E>()
			{
				private final ListIterator<E> i = l
						.listIterator(index + offset);

				public boolean hasNext()
				{
					return nextIndex() < size;
				}

				public E next()
				{
					if (hasNext())
						return i.next();
					else
						throw new NoSuchElementException();
				}

				public boolean hasPrevious()
				{
					return previousIndex() >= 0;
				}

				public E previous()
				{
					if (hasPrevious())
						return i.previous();
					else
						throw new NoSuchElementException();
				}

				public int nextIndex()
				{
					return i.nextIndex() - offset;
				}

				public int previousIndex()
				{
					return i.previousIndex() - offset;
				}

				public void remove()
				{
					i.remove();
				}

				public void set(E e)
				{
					i.set(e);
				}

				public void add(E e)
				{
					i.add(e);
				}
			};
		}

		public List<E> subList(int fromIndex, int toIndex)
		{
			return new SubList<E>(this, fromIndex, toIndex);
		}

		private void rangeCheck(int index)
		{
			if (index < 0 || index >= size)
				throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
		}

		private void rangeCheckForAdd(int index)
		{
			if (index < 0 || index > size)
				throw new IndexOutOfBoundsException(outOfBoundsMsg(index));
		}

		private String outOfBoundsMsg(int index)
		{
			return "Index: " + index + ", Size: " + size;
		}
	}

	class RandomAccessSubList<E> extends SubList<E> implements RandomAccess
	{
		RandomAccessSubList(TransactionalTreeList<E> list, int fromIndex,
				int toIndex)
		{
			super(list, fromIndex, toIndex);
		}

		public List<E> subList(int fromIndex, int toIndex)
		{
			// FIXME
			// return new RandomAccessSubList<E>(this, fromIndex, toIndex);
			return null;
		}
	}
}