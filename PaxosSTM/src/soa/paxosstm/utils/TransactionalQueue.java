package soa.paxosstm.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalQueue<E> implements java.util.Queue<E>
{
	@TransactionObject
	static class Node<E>
	{
		public E value;
		public Node<E> nextNode;

		public Node()
		{
		}

		public Node(E value)
		{
			this.value = value;
		}

		public Node(E value, Node<E> nextNode)
		{
			this.value = value;
			this.nextNode = nextNode;
		}
	}

	private Node<E> head;
	private Node<E> tail;

	@Override
	public boolean add(E value)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public E element()
	{
		if (head == null)
			throw new NoSuchElementException();
		return head.value;
	}

	@Override
	public boolean offer(E value)
	{
		if (tail == null)
		{
			head = tail = new Node<E>(value);
		}
		else
		{
			tail = tail.nextNode = new Node<E>(value);
		}
		return true;
	}

	@Override
	public E peek()
	{
		if (head == null)
			return null;
		return head.value;
	}

	@Override
	public E poll()
	{
		if (head == null)
			return null;
		if (head == tail)
			tail = null;
		E ret = head.value;
		head = head.nextNode;
		return ret;
	}

	@Override
	public E remove()
	{
		if (head == null)
			throw new NoSuchElementException();
		if (head == tail)
			tail = null;
		E ret = head.value;
		head = head.nextNode;
		return ret;
	}

	@Override
	public boolean addAll(Collection<? extends E> arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear()
	{
		while (poll() != null)
			;
	}

	@Override
	public boolean contains(Object arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isEmpty()
	{
		return tail == null;
	}

	@Override
	public Iterator<E> iterator()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int size()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Object[] toArray()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T[] toArray(T[] arg0)
	{
		throw new UnsupportedOperationException();
	}
}
