package soa.paxosstm.utils;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalBoolean
{
	private boolean bool;

	public TransactionalBoolean()
	{
	}

	public TransactionalBoolean(boolean bool)
	{
		this.setBool(bool);
	}

	public void setBool(boolean bool)
	{
		this.bool = bool;
	}

	public boolean getBool()
	{
		return bool;
	}
}
