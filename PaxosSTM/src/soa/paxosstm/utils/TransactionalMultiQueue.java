package soa.paxosstm.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;

import soa.paxosstm.dstm.ArrayWrapper;
import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalMultiQueue<E> implements java.util.Queue<E>
{
	private int numberOfQueues;
	private ArrayWrapper<TransactionalQueue<E>> queues;

	private static Random random = new Random();

	public TransactionalMultiQueue()
	{
		this(1);
	}

	public TransactionalMultiQueue(int numberOfQueues)
	{
		this.numberOfQueues = numberOfQueues;
		@SuppressWarnings("unchecked")
		TransactionalQueue<E>[] array = new TransactionalQueue[numberOfQueues];
		
		queues = new ArrayWrapper<TransactionalQueue<E>>(array);
		for (int i = 0; i < numberOfQueues; i++)
		{
			queues.set(i, new TransactionalQueue<E>());
		}
	}

	@Override
	public boolean add(E value)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public E element()
	{
		E value = peek();
		if (value == null)
			throw new NoSuchElementException();
		return value;
	}

	@Override
	public boolean offer(E value)
	{
		return queues.get(random.nextInt(numberOfQueues)).offer(value);
	}

	@Override
	public E peek()
	{
		int start = random.nextInt(numberOfQueues);
		E value = null;
		for (int i = 0; i < numberOfQueues; i++)
		{
			value = queues.get((i + start) % numberOfQueues).peek();
			if (value != null)
				break;
		}
		return value;
	}

	@Override
	public E poll()
	{
		int start = random.nextInt(numberOfQueues);
		E value = null;
		for (int i = 0; i < numberOfQueues; i++)
		{
			value = queues.get((i + start) % numberOfQueues).poll();
			if (value != null)
				break;
		}
		return value;
	}

	@Override
	public E remove()
	{
		E value = poll();
		if (value == null)
			throw new NoSuchElementException();
		return value;
	}

	@Override
	public boolean addAll(Collection<? extends E> arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public void clear()
	{
		for (int i = 0; i < numberOfQueues; i++)
			queues.get(i).clear();
	}

	@Override
	public boolean contains(Object arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsAll(Collection<?> arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean isEmpty()
	{
		for (int i = 0; i < numberOfQueues; i++)
			if (!queues.get(i).isEmpty())
				return false;
		return true;
	}

	@Override
	public Iterator<E> iterator()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean remove(Object arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeAll(Collection<?> arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean retainAll(Collection<?> arg0)
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public int size()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Object[] toArray()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public <T> T[] toArray(T[] arg0)
	{
		throw new UnsupportedOperationException();
	}
}
