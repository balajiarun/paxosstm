package soa.paxosstm.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalLinkedListInt implements List<Integer>, Cloneable, Deque<Integer>
{
	@TransactionObject
	private static class Node
	{
		public int value;
		public Node nextNode;
		public Node prevNode;

		public Node()
		{
		}

		public Node(int value)
		{
			this.value = value;
		}

		public Node(int value, Node nextNode, Node prevNode)
		{
			this.value = value;
			this.nextNode = nextNode;
			this.prevNode = prevNode;
		}
	}

	private Node firstNode;
	private Node lastNode;

	public TransactionalLinkedListInt()
	{
		firstNode = lastNode = null;
	}

	@SuppressWarnings("unchecked")
	public TransactionalLinkedListInt(Collection c)
	{
		for (Object object : c)
		{
			linkLast((Integer) object);
		}
	}

	@Override
	public boolean add(Integer value)
	{
		return add((int) value);
	}

	public boolean add(int value)
	{
		linkLast(value);
		return true;
	}

	@Override
	public void add(int index, Integer element)
	{
		add(index, (int) element);
	}

	public void add(int index, int element)
	{
		if (index < 0)
			throw new IndexOutOfBoundsException();

		if (index == 0)
		{
			linkFirst(element);
		}
		else
		{
			Node successor = node(index);
			if (successor == null)
				throw new IndexOutOfBoundsException();
			linkBefore(element, successor);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean addAll(Collection<? extends Integer> c)
	{
		Iterator<Integer> iter = (Iterator<Integer>) c.iterator();

		if (iter.hasNext() == false)
			return false;

		Node localFirstNode = new Node(iter.next());

		Node currentNode = localFirstNode;
		while (iter.hasNext())
		{
			currentNode.nextNode = new Node(iter.next());
			currentNode.nextNode.prevNode = currentNode;
			currentNode = currentNode.nextNode;
		}

		if (firstNode == null)
			firstNode = localFirstNode;
		else
		{
			lastNode.nextNode = localFirstNode;
			localFirstNode.prevNode = lastNode;
		}

		lastNode = currentNode;

		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean addAll(int index, Collection<? extends Integer> c)
	{
		if (index < 0)
			throw new IndexOutOfBoundsException();

		Iterator<Integer> iter = (Iterator<Integer>) c.iterator();

		if (iter.hasNext() == false)
			return false;

		Node successor = node(index);
		if (successor == null)
			throw new IndexOutOfBoundsException();

		Node predecessor = successor.prevNode;

		Node localFirstNode = new Node(iter.next());

		Node currentNode = localFirstNode;
		while (iter.hasNext())
		{
			currentNode.nextNode = new Node(iter.next());
			currentNode.nextNode.prevNode = currentNode;
			currentNode = currentNode.nextNode;
		}

		if (index == 0)
		{
			firstNode = localFirstNode;
		}
		else
		{
			localFirstNode.prevNode = predecessor;
			predecessor.nextNode = localFirstNode;
		}
		currentNode.nextNode = successor;
		successor.prevNode = currentNode;

		return true;
	}

	@Override
	public void clear()
	{
		if (firstNode == null)
			return;

		Node currentNode = firstNode;
		while (currentNode.nextNode != null)
		{
			// ((Observable) currentNode).__tc_delete();
			currentNode = currentNode.nextNode;
		}
		firstNode = lastNode = null;
	}

	@Override
	public boolean contains(Object o)
	{
		return indexOf(o) >= 0;
	}

	public boolean contains(int x)
	{
		return indexOf(x) >= 0;
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		if (firstNode == null)
			return false;

		Map<Object, Boolean> map = new HashMap<Object, Boolean>();
		for (Object object : c)
		{
			map.put(object, false);
		}
		int counter = map.size();

		Node currentNode = firstNode;
		while (currentNode != null)
		{
			int o = currentNode.value;
			Boolean b = map.get(o);
			if (b == false)
			{
				map.put(o, true);
				if (--counter == 0)
					break;
			}
			currentNode = currentNode.nextNode;
		}

		return counter == 0;
	}

	@Override
	public boolean equals(Object o)
	{
		if (o == this)
			return true;
		if (!(o instanceof List))
			return false;

		ListIterator<Integer> e1 = listIterator();
		ListIterator<?> e2 = ((List<?>) o).listIterator();
		while (e1.hasNext() && e2.hasNext())
		{
			Integer o1 = e1.next();
			Object o2 = e2.next();
			if (!o1.equals(o2))
				return false;
		}
		return !(e1.hasNext() || e2.hasNext());
	}

	@Override
	public Integer get(int index)
	{
		return getInt(index);
	}

	public int getInt(int index)
	{
		Node node = node(index);
		if (node == null)
			throw new IndexOutOfBoundsException();

		return node.value;
	}

	@Override
	public int indexOf(Object o)
	{
		int i = 0;
		Node currentNode = firstNode;
		while (currentNode != null)
		{
			if (o.equals(currentNode.value))
				return i;
			i++;
			currentNode = currentNode.nextNode;
		}
		return -1;
	}

	public int indexOf(int x)
	{
		int i = 0;
		Node currentNode = firstNode;
		while (currentNode != null)
		{
			if (currentNode.value == x)
				return i;
			i++;
			currentNode = currentNode.nextNode;
		}
		return -1;
	}

	@Override
	public boolean isEmpty()
	{
		return firstNode == null;
	}

	@Override
	public Iterator<Integer> iterator()
	{
		return new ListIter();
	}

	@Override
	public int lastIndexOf(Object o)
	{
		int lastFound = -1;
		int i = 0;
		Node currentNode = firstNode;
		while (currentNode != null)
		{
			if (o.equals(currentNode.value))
				lastFound = i;
			i++;
			currentNode = currentNode.nextNode;
		}
		return lastFound;
	}

	public int lastIndexOf(int x)
	{
		int lastFound = -1;
		int i = 0;
		Node currentNode = firstNode;
		while (currentNode != null)
		{
			if (currentNode.value == x)
				lastFound = i;
			i++;
			currentNode = currentNode.nextNode;
		}
		return lastFound;
	}

	@Override
	public ListIterator<Integer> listIterator()
	{
		return new ListIter();
	}

	@Override
	public ListIterator<Integer> listIterator(int index)
	{
		return new ListIter(index);
	}

	@Override
	public boolean remove(Object o)
	{
		if (o == null)
		{
			return false;
		}
		for (Node node = firstNode; node != null; node = node.nextNode)
		{
			if (o.equals(node.value))
			{
				unlink(node);
				return true;
			}
		}
		return false;
	}

	public boolean removeInt(int x)
	{
		for (Node node = firstNode; node != null; node = node.nextNode)
		{
			if (node.value == x)
			{
				unlink(node);
				return true;
			}
		}
		return false;
	}

	@Override
	public Integer remove(int index)
	{
		Node node = node(index);
		if (node == null)
			throw new IndexOutOfBoundsException();

		return unlink(node);
	}

	public int removeIndex(int index)
	{
		Node node = node(index);
		if (node == null)
			throw new IndexOutOfBoundsException();

		return unlink(node);
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		Set<Object> map = new HashSet<Object>(c);
		boolean listModified = false;

		Node currentNode = firstNode;
		while (currentNode != null)
		{
			Node localCurrentNode = currentNode;
			currentNode = currentNode.nextNode;
			if (map.contains(localCurrentNode.value))
			{
				unlink(localCurrentNode);
				listModified = true;
			}
		}

		return listModified;
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		Set<Object> map = new HashSet<Object>(c);
		boolean listModified = false;

		Node currentNode = firstNode;
		while (currentNode != null)
		{
			Node localCurrentNode = currentNode;
			currentNode = currentNode.nextNode;
			if (!map.contains(localCurrentNode.value))
			{
				unlink(localCurrentNode);
				listModified = true;
			}
		}

		return listModified;
	}

	@Override
	public Integer set(int index, Integer element)
	{
		Node node = node(index);
		if (node == null)
			throw new IndexOutOfBoundsException();

		int ret = node.value;
		node.value = element;

		return ret;
	}

	public int set(int index, int element)
	{
		Node node = node(index);
		if (node == null)
			throw new IndexOutOfBoundsException();

		int ret = node.value;
		node.value = element;

		return ret;
	}

	@Override
	public int size()
	{
		int i = 0;
		Node currentNode = firstNode;

		while (currentNode != null)
		{
			i++;
			currentNode = currentNode.nextNode;
		}

		return i;
	}

	@Override
	public Object[] toArray()
	{
		Object[] array = new Object[size()];
		int i = 0;
		Node currentNode = firstNode;
		while (currentNode != null)
		{
			array[i++] = currentNode.value;
			currentNode = currentNode.nextNode;
		}

		return array;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a)
	{
		int size = size();
		if (a.length < size)
		{
			a = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
		}

		Node currentNode = firstNode;
		int i = 0;
		while (currentNode != null)
		{
			a[i++] = (T) (Integer) currentNode.value;
			currentNode = currentNode.nextNode;
		}

		return a;
	}

	@Override
	public TransactionalLinkedListInt clone()
	{
		TransactionalLinkedListInt ret = new TransactionalLinkedListInt();

		Node currentNode = firstNode;
		while (currentNode != null)
		{
			ret.add(currentNode.value);
			currentNode = currentNode.nextNode;
		}

		return ret;
	}

	private void linkFirst(int e)
	{
		final Node f = firstNode;
		final Node newNode = new Node(e, f, null);
		firstNode = newNode;
		if (f == null)
			lastNode = newNode;
		else
			f.prevNode = newNode;
	}

	void linkLast(int e)
	{
		final Node l = lastNode;
		final Node newNode = new Node(e, null, l);
		lastNode = newNode;
		if (l == null)
			firstNode = newNode;
		else
			l.nextNode = newNode;
	}

	void linkBefore(int e, Node successor)
	{
		final Node predecessor = successor.prevNode;
		final Node newNode = new Node(e, successor, predecessor);
		successor.prevNode = newNode;
		if (predecessor == null)
			firstNode = newNode;
		else
			predecessor.nextNode = newNode;
	}

	private int unlinkFirst(Node f)
	{
		final int element = f.value;
		final Node next = f.nextNode;
		// ((Observable) f).__tc_delete();
		firstNode = next;
		if (next == null)
			lastNode = null;
		else
			next.prevNode = null;
		return element;
	}

	private int unlinkLast(Node l)
	{
		final int element = l.value;
		final Node prev = l.prevNode;
		// ((Observable) l).__tc_delete();
		lastNode = prev;
		if (prev == null)
			firstNode = null;
		else
			prev.nextNode = null;

		return element;
	}

	int unlink(Node node)
	{
		final int element = node.value;
		final Node next = node.nextNode;
		final Node prev = node.prevNode;

		if (prev == null)
			firstNode = next;
		else
			prev.nextNode = next;

		if (next == null)
			lastNode = prev;
		else
			next.prevNode = prev;

		// ((Observable) node).__tc_delete();

		return element;
	}

	Node node(int index)
	{
		if (index < 0)
			return null;

		int i = 0;
		Node currentNode = firstNode;
		while (currentNode != null && i < index)
		{
			i++;
			currentNode = currentNode.nextNode;
		}

		if (currentNode == null)
			return null;

		return currentNode;
	}

	@Override
	public void addFirst(Integer e)
	{
		linkFirst(e);
	}

	public void addFirst(int e)
	{
		linkFirst(e);
	}

	@Override
	public void addLast(Integer e)
	{
		linkLast(e);
	}

	public void addLast(int e)
	{
		linkLast(e);
	}

	@Override
	public boolean offerFirst(Integer e)
	{
		addFirst(e);
		return true;
	}

	public boolean offerFirst(int e)
	{
		addFirst(e);
		return true;
	}

	@Override
	public boolean offerLast(Integer e)
	{
		addLast(e);
		return true;
	}

	public boolean offerLast(int e)
	{
		addLast(e);
		return true;
	}

	@Override
	public Integer removeFirst()
	{
		if (firstNode == null)
			throw new NoSuchElementException();

		return unlinkFirst(firstNode);
	}

	public int removeFirstInt()
	{
		if (firstNode == null)
			throw new NoSuchElementException();

		return unlinkFirst(firstNode);
	}

	@Override
	public Integer removeLast()
	{
		if (lastNode == null)
			throw new NoSuchElementException();

		return unlinkLast(lastNode);
	}

	public int removeLastInt()
	{
		if (lastNode == null)
			throw new NoSuchElementException();

		return unlinkLast(lastNode);
	}

	@Override
	public Integer pollFirst()
	{
		return (firstNode == null) ? null : unlinkFirst(firstNode);
	}

	@Override
	public Integer pollLast()
	{
		return (lastNode == null) ? null : unlinkLast(lastNode);
	}

	@Override
	public Integer getFirst()
	{
		if (firstNode == null)
			throw new NoSuchElementException();

		return firstNode.value;
	}

	public int getFirstInt()
	{
		if (firstNode == null)
			throw new NoSuchElementException();

		return firstNode.value;
	}

	@Override
	public Integer getLast()
	{
		if (lastNode == null)
			throw new NoSuchElementException();

		return lastNode.value;
	}

	public int getLastInt()
	{
		if (lastNode == null)
			throw new NoSuchElementException();

		return lastNode.value;
	}

	@Override
	public Integer peekFirst()
	{
		return (firstNode == null) ? null : firstNode.value;
	}

	@Override
	public Integer peekLast()
	{
		return (lastNode == null) ? null : lastNode.value;
	}

	@Override
	public boolean removeFirstOccurrence(Object o)
	{
		return remove(o);
	}

	@Override
	public boolean removeLastOccurrence(Object o)
	{
		for (Node node = lastNode; node != null; node = node.prevNode)
		{
			if (o.equals(node.value))
			{
				unlink(node);
				return true;
			}
		}
		return false;
	}

	public boolean removeLastOccurrence(int x)
	{
		for (Node node = lastNode; node != null; node = node.prevNode)
		{
			if (node.value == x)
			{
				unlink(node);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean offer(Integer e)
	{
		return add(e);
	}

	public boolean offer(int e)
	{
		return add(e);
	}

	@Override
	public Integer remove()
	{
		return removeFirst();
	}

	public int removeInt()
	{
		return removeFirst();
	}

	@Override
	public Integer poll()
	{
		return (firstNode == null) ? null : unlinkFirst(firstNode);
	}

	@Override
	public Integer element()
	{
		return getFirst();
	}

	public int elementInt()
	{
		return getFirst();
	}

	@Override
	public Integer peek()
	{
		return (firstNode == null) ? null : firstNode.value;
	}

	@Override
	public void push(Integer e)
	{
		addFirst(e);
	}

	public void push(int e)
	{
		addFirst(e);
	}

	@Override
	public Integer pop()
	{
		return removeFirst();
	}

	public int popInt()
	{
		return removeFirst();
	}

	@Override
	public Iterator<Integer> descendingIterator()
	{
		return new DescendingIter();
	}

	private class ListIter implements ListIterator<Integer>
	{
		Node next;
		Node prev;
		int currentIndex = 0;
		Node lastElement = null;

		public ListIter()
		{
			next = (Node) TransactionalLinkedListInt.this.firstNode;
			if (next != null)
				prev = next.prevNode;
		}

		public ListIter(int index)
		{
			int i = 0;
			Node currentNode = (Node) TransactionalLinkedListInt.this.firstNode;
			while (currentNode != null && i < index)
			{
				i++;
				currentNode = currentNode.nextNode;
			}

			if (currentNode == null)
				throw new IndexOutOfBoundsException();

			next = currentNode;
			currentIndex = i;
		}

		@Override
		public boolean hasNext()
		{
			return next != null;
		}

		@Override
		public Integer next()
		{
			if (next == null)
				throw new NoSuchElementException();
			currentIndex++;
			lastElement = prev = next;
			next = lastElement.nextNode;

			return lastElement.value;
		}

		@Override
		public boolean hasPrevious()
		{
			return prev != null;
		}

		@Override
		public Integer previous()
		{
			if (prev == null)
				throw new NoSuchElementException();
			currentIndex--;
			lastElement = next = prev;
			prev = lastElement.prevNode;

			return lastElement.value;
		}

		@Override
		public int nextIndex()
		{
			return currentIndex;
		}

		@Override
		public int previousIndex()
		{
			return currentIndex - 1;
		}

		@Override
		public void remove()
		{
			if (lastElement == null)
				throw new IllegalStateException();

			if (lastElement == prev)
				currentIndex--;

			next = lastElement.nextNode;
			prev = lastElement.prevNode;
			unlink((Node) lastElement);

			lastElement = null;
		}

		@Override
		public void set(Integer e)
		{
			if (lastElement == null)
				throw new IllegalStateException();
			lastElement.value = e;

		}

		@Override
		public void add(Integer o)
		{
			currentIndex++;
			Node e = new Node(o);
			e.prevNode = prev;
			e.nextNode = next;

			if (prev != null)
				prev.nextNode = e;
			else
				firstNode = e;

			if (next != null)
				next.prevNode = e;
			else
				lastNode = e;

			prev = e;
			lastElement = null;
		}
	}

	private class DescendingIter implements Iterator<Integer>
	{
		private final ListIter itr = new ListIter(size());

		public boolean hasNext()
		{
			return itr.hasPrevious();
		}

		public Integer next()
		{
			return itr.previous();
		}

		public void remove()
		{
			itr.remove();
		}
	}

	@Override
	public int hashCode()
	{
		int hashCode = 1;
		for (int e : this)
			hashCode = 31 * hashCode + e;
		return hashCode;
	}

	@Override
	public List<Integer> subList(int fromIndex, int toIndex)
	{
		throw new UnsupportedOperationException();
	}

	protected void removeRange(int fromIndex, int toIndex)
	{
		ListIterator<Integer> it = listIterator(fromIndex);
		for (int i = 0, n = toIndex - fromIndex; i < n; i++)
		{
			it.next();
			it.remove();
		}
	}
}
