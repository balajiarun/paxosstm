/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.utils;

import soa.paxosstm.dstm.ArrayWrapper;
import soa.paxosstm.dstm.TransactionObject;

/**
 * Custom implementation of integer:integer hash-table that can be used as
 * transactional object.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 */
@TransactionObject
public class TransactionalHashTableIntInt
{
	private static final int DEFAULT_CAPACITY = 65536;

	@TransactionObject
	static class TransactionalList
	{
		@TransactionObject
		public static class Node
		{
			public int key;
			public int value;
			public Node nextNode;
		}

		public Node firstNode;
	}

	private ArrayWrapper<TransactionalList> table;
	private int capacity;
	
	/**
	 * Default constructor.
	 */
	public TransactionalHashTableIntInt()
	{
		this(DEFAULT_CAPACITY);
	}

	/**
	 * Constructor.
	 * 
	 * @param capacity
	 *            the size of the hash-table
	 */
	public TransactionalHashTableIntInt(Integer capacity)
	{
		this.capacity = capacity;
		table = new ArrayWrapper<TransactionalList>(
				new TransactionalList[capacity]);
		for (int i = 0; i < capacity; i++)
		{
			table.set(i, new TransactionalList());
		}
	}
	
	/**
	 * Returns the value for the given key. Returns null if there is no such
	 * key in the hash-table.
	 * 
	 * @param key
	 *            key to the value to be retrieved
	 * @return the value for the given key if such key exists in the hash-table;
	 *         null otherwise
	 */
	public Integer get(int key)
	{
		int hash = Math.abs(key);
		TransactionalList list = table.get(hash % capacity);

		TransactionalList.Node node = list.firstNode;
		while (node != null)
		{
			if (node.key == key)
				return node.value;
			node = node.nextNode;
		}
		return null;
	}

	/**
	 * Removes value for the given key from the hash-table.
	 * 
	 * @param key
	 *            key for the value to be removed from the hash-table
	 * @return value that was removed or null if it was not present
	 */
	public Integer remove(int key)
	{
		int hash = Math.abs(key);
		TransactionalList list = table.get(hash % capacity);

		TransactionalList.Node prev = null;
		TransactionalList.Node node = list.firstNode;
		while (node != null)
		{
			if (node.key == key)
				break;
			prev = node;
			node = node.nextNode;
		}

		if (node == null)
			return null;

		if (prev == null)
		{
			list.firstNode = node.nextNode;
		}
		else
		{
			prev.nextNode = node.nextNode;
		}
		Integer oldValue = node.value;

		return oldValue;
	}

	/**
	 * Inserts a value for a given key to the hash-table.
	 * 
	 * @param key
	 * @param value
	 */
	public Integer put(int key, int value)
	{
		int hash = Math.abs(key);
		TransactionalList list = table.get(hash % capacity);

		TransactionalList.Node prev = null;
		TransactionalList.Node node = list.firstNode;
		while (node != null)
		{
			if (node.key == key)
				break;
			prev = node;
			node = node.nextNode;
		}

		if (node != null)
		{
			Integer oldValue = node.value;
			node.value = value;
			return oldValue;
		}

		node = new TransactionalList.Node();
		node.key = key;
		node.value = value;
		node.nextNode = null;

		if (prev == null)
		{
			list.firstNode = node;
		}
		else
		{
			prev.nextNode = node;
		}
		
		return null;
	}
}
