package soa.paxosstm.utils;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicReference;

public class ReferenceBag<E>
{
	private AtomicReference<Node<E>> first = new AtomicReference<Node<E>>();

	Node<E> linkFirst(E e)
	{
		Node<E> newNode = new Node<E>(null, e, null);
		while (true)
		{
			Node<E> f = first.get();
			newNode.next = f;
			if (f == null)
			{
				first.set(newNode);
				return newNode;
			}

			f.prev = newNode;

			if (first.compareAndSet(f, newNode))
				return newNode;
		}
	}

	private void unlink(Node<E> x)
	{
		Node<E> next = x.next;
		Node<E> prev = x.prev;

		if (prev == null)
		{
			if (!first.compareAndSet(x, next))
			{
				prev = x.prev;
				prev.next = next;
			}
		}
		else
		{
			prev.next = next;
		}
		x.prev = null;

		if (next != null)
		{
			next.prev = prev;
			x.next = null;
		}
	}

	public Iterator<E> add(E e)
	{
		return new Iter(linkFirst(e));
	}

	public E poll()
	{
		Node<E> n = first.get();
		if (n != null)
		{
			Node<E> n2 = n.next;
			if (n2 != null)
			{
				Node<E> n3 = n2.next;
				if (n3 != null)
				{
					unlink(n3);
					return n3.item;
				}
				unlink(n2);
				return n2.item;
			}
			unlink(n);
			return n.item;
		}
		return null;
	}

	private static class Node<E>
	{
		E item;
		Node<E> next;
		Node<E> prev;

		Node(Node<E> prev, E element, Node<E> next)
		{
			this.item = element;
			this.next = next;
			this.prev = prev;
		}
	}

	private class Iter implements Iterator<E>
	{
		private Node<E> node;

		public Iter(Node<E> node)
		{
			this.node = node;
		}

		@Override
		public boolean hasNext()
		{
			return false;
		}

		@Override
		public E next()
		{
			throw new NoSuchElementException();
		}

		@Override
		public void remove()
		{
			if (node != null)
			{
				unlink(node);
				node = null;
			}
			else
				throw new IllegalStateException();
		}
	}
}
