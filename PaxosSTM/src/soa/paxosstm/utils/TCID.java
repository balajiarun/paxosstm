package soa.paxosstm.utils;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Random;

public final class TCID implements Comparable<TCID>, Serializable
{
	private static final long serialVersionUID = 1L;

	private static final Random random = new Random();
	private final long leastSignificantBits;
	private final long mostSignificantBits;

	public TCID(long mostSignificantBits, long leastSignificantBits)
	{
		this.leastSignificantBits = leastSignificantBits;
		this.mostSignificantBits = mostSignificantBits;
	}

	public long getLeastSignificantBits()
	{
		return leastSignificantBits;
	}

	public long getMostSignificantBits()
	{
		return mostSignificantBits;
	}

	@Override
	public int compareTo(TCID arg0)
	{
		if (mostSignificantBits == arg0.mostSignificantBits)
			return Long.signum(leastSignificantBits - arg0.leastSignificantBits);
		else
			return Long.signum(mostSignificantBits - arg0.mostSignificantBits);
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (!(obj instanceof TCID))
			return false;
		TCID other = (TCID) obj;
		return (mostSignificantBits == other.mostSignificantBits)
				&& (leastSignificantBits == other.leastSignificantBits);
	}

	@Override
	public int hashCode()
	{
		return (int) ((mostSignificantBits >> 32) ^ (mostSignificantBits & 0xFFFFFFFF)
				^ (leastSignificantBits >> 32) ^ (leastSignificantBits & 0xFFFFFFFF));
	}

	// static final char[] hexCharacters = { '0', '1', '2', '3', '4', '5', '6',
	// '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	@Override
	public String toString()
	{
		// char[] array = new char[33];
		// long x = mostSignificantBits;
		// for (int i = 15; i>=0; i--) {
		// array[i] = hexCharacters[(int) (x % 16)];
		// x /= 16;
		// }
		// array[16] = '-';
		// x = leastSignificantBits;
		// for (int i = 32; i>=17; i--) {
		// array[i] = hexCharacters[(int) (x % 16)];
		// x /= 16;
		// }
		// return String.copyValueOf(array, 0, array.length);
		String m = Long.toHexString(mostSignificantBits);
		String l = Long.toHexString(leastSignificantBits);
		char array[] = new char[33];
		for (int i = 0; i < 16 - m.length(); i++)
			array[i] = '0';
		for (int i = 0; i < m.length(); i++)
			array[16 - m.length() + i] = m.charAt(i);
		array[16] = '-';
		for (int i = 17; i < 33 - l.length(); i++)
			array[i] = '0';
		for (int i = 0; i < l.length(); i++)
			array[33 - l.length() + i] = l.charAt(i);
		return String.copyValueOf(array);
	}

	private static long parseUnsignedLongHex(String s) throws ParseException
	{
		long x = 0;
		for (int i = 0; i < s.length(); i++)
		{
			char c = s.charAt(i);
			if (c >= '0' && c <= '9')
				c -= '0';
			else if (c >= 'a' && c <= 'f')
				c -= 'a' - 10;
			else if (c >= 'A' && c <= 'F')
				c -= 'A' - 10;
			else
				throw new ParseException("Illegal character: " + c, i);
			x *= 16;
			x += c;
		}
		return x;
	}

	public static TCID fromString(String name) throws IllegalArgumentException
	{
		try
		{
			// long m = Long.parseLong(name.substring(0, 16), 16);
			// long l = Long.parseLong(name.substring(17, 33), 16);
			//
			// return new TCID(m, l);
			long m = parseUnsignedLongHex(name.substring(0, 16));
			long l = parseUnsignedLongHex(name.substring(17, 33));
			return new TCID(m, l);
		}
		catch (Exception e)
		{
			throw new IllegalArgumentException(e);
		}
	}

	public static TCID randomTCID()
	{
		// TCID id = randomTCID(random);
		// System.err.println("NEW TCID " + id);
		// new Exception().printStackTrace();
		return randomTCID(random);
	}

	public static TCID randomTCID(Random random)
	{
		long m = random.nextLong();
		long l = random.nextLong();
		return new TCID(m, l);
		// return new TCID(random.nextInt() & 0xFFFFFFFFL, 0);
	}
}
