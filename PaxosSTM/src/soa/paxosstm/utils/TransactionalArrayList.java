package soa.paxosstm.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import java.util.Set;

import soa.paxosstm.dstm.ArrayWrapper;
import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalArrayList<E> implements Cloneable, Iterable<E>,
		Collection<E>, List<E>, RandomAccess
{
	@TransactionObject
	static class TransactionalBox<E>
	{
		public E value;

		public TransactionalBox()
		{
		}

		public TransactionalBox(E value)
		{
			this.value = value;
		}
	}

	private ArrayWrapper<TransactionalBox<E>> array;
	private TransactionalInteger size;

	public TransactionalArrayList()
	{
		this(10);
	}

	@SuppressWarnings("unchecked")
	public TransactionalArrayList(Collection<? extends E> c)
	{
		TransactionalBox<E>[] internalArray = new TransactionalBox[c.size()]; 
		int i = 0;
		for (E e : c)
		{
			internalArray[i++] = new TransactionalBox<E>(e);
		}
		array = new ArrayWrapper<TransactionalArrayList.TransactionalBox<E>>(
				internalArray);
		size = new TransactionalInteger(internalArray.length);
	}

	@SuppressWarnings("unchecked")
	public TransactionalArrayList(int initialCapacity)
	{
		TransactionalBox<E>[] internalArray = new TransactionalBox[initialCapacity];
		for (int i = 0; i < internalArray.length; i++)
			internalArray[i] = new TransactionalBox<E>();
		array = new ArrayWrapper<TransactionalArrayList.TransactionalBox<E>>(
				internalArray);
		size = new TransactionalInteger(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean addAll(int index, Collection<? extends E> c)
	{
		if (index < 0 || index > size())
			throw new IndexOutOfBoundsException();

		if (c.size() == 0)
			return false;

		if (size() + c.size() < array.length())
		{
			for (int i = size() - 1; i >= index; i--)
			{
				//array.set(i + c.size(), array.get(i));
				array.get(i + c.size()).value = array.get(i).value;
			}
			for (E e : c)
			{
				//array.set(index++, new TransactionalBox<E>(e));
				array.get(index++).value = e;
			}
			size.setValue(size.getValue() + c.size());
		}
		else
		{
			int newLength = array.length() * 2;
			while (newLength < size() + c.size())
			{
				newLength *= 2;
			}

			TransactionalBox<E>[] newInternalArray = new TransactionalBox[newLength];
			for (int i = 0; i < index; i++)
			{
				newInternalArray[i] = array.get(i);
			}
			for (E e : c)
			{
				newInternalArray[index++] = new TransactionalBox<E>(e);
			}
			int newSize = size() + c.size();
			for (int i = index; i < newSize; i++)
			{
				newInternalArray[i] = array.get(i - c.size());
			}
			for (int i = newSize; i < newInternalArray.length; i++)
			{
				newInternalArray[i] = new TransactionalBox<E>();
			}
			size.setValue(newSize);
			array = new ArrayWrapper<TransactionalBox<E>>(newInternalArray);
		}


		return true;
	}

	@Override
	public boolean equals(Object o)
	{
		if (o == this)
			return true;
		if (!(o instanceof List))
			return false;

		ListIterator<E> e1 = listIterator();
		ListIterator<?> e2 = ((List<?>) o).listIterator();
		while (e1.hasNext() && e2.hasNext())
		{
			E o1 = e1.next();
			Object o2 = e2.next();
			if (!(o1 == null ? o2 == null : o1.equals(o2)))
				return false;
		}
		return !(e1.hasNext() || e2.hasNext());
	}

	
	@Override
	public E get(int index) throws IndexOutOfBoundsException
	{
		if (index < 0 || index >= size())
			throw new IndexOutOfBoundsException();

		return array.get(index).value;
	}

	@Override
	public E set(int index, E element) throws IndexOutOfBoundsException
	{
		if (index < 0 || index >= size())
			throw new IndexOutOfBoundsException();

		E ret = array.get(index).value;
		array.get(index).value = element;
		return ret;
	}

	@Override
	@SuppressWarnings(
	{ "unchecked" })
	public void add(int index, E element) throws IndexOutOfBoundsException
	{
		if (index < 0 || index > size())
			throw new IndexOutOfBoundsException();

		if (index == array.length())
		{
			add(element);
		}
		else
		{
			if (size() == array.length())
			{
				TransactionalBox<E>[] newInternalArray = new TransactionalBox[array
						.length() * 2];
				for (int i = 0; i < index; i++)
					newInternalArray[i] = array.get(i);
				newInternalArray[index] = new TransactionalBox<E>(element);
				for (int i = index; i < array.length(); i++)
					newInternalArray[i + 1] = array.get(i);
				for (int i = array.length() + 1; i < newInternalArray.length; i++)
					newInternalArray[i] = new TransactionalBox<E>();
				array = new ArrayWrapper<TransactionalBox<E>>(newInternalArray);
			}
			else
			{
				for (int i = array.length() - 1; i >= index; i--)
				{
					array.get(i + 1).value = array.get(i).value;
				}
				array.get(index).value = element;
			}
			size.increment();
		}
	}

	@Override
	public E remove(int index) throws IndexOutOfBoundsException
	{
		if (index < 0 || index >= size())
			throw new IndexOutOfBoundsException();

		E ret = array.get(index).value;

		int n = size.decrement();
		for (int i = index; i < n; i++)
			array.get(i).value = array.get(i + 1).value;
		array.get(n).value = null;
		
		return ret;
	}

	@Override
	public int indexOf(Object o)
	{
		int size = size();
		if (size == 0)
			return -1;

		int i = 0;
		while (i < size && !o.equals(array.get(i).value))
		{
			i++;
		}

		if (i == size)
			return -1;
		return i;
	}

	@Override
	public int lastIndexOf(Object o)
	{
		int size = size();
		if (size == 0)
			return -1;

		int i = size - 1;
		while (i >= 0 && !o.equals(array.get(i).value))
		{
			i--;
		}

		return i;
	}

	@Override
	public ListIterator<E> listIterator()
	{
		return new ListIter();
	}

	@Override
	public ListIterator<E> listIterator(int index)
	{
		return new ListIter(index);
	}

	protected void removeRange(int fromIndex, int toIndex)
	{
		ListIterator<E> it = listIterator(fromIndex);
		for (int i = 0, n = toIndex - fromIndex; i < n; i++)
		{
			it.next();
			it.remove();
		}
	}
	
	@Override
	public int hashCode()
	{
		int hashCode = 1;
		for (E e : this)
			hashCode = 31 * hashCode + (e == null ? 0 : e.hashCode());
		return hashCode;
	}
	
	@Override
	public List<E> subList(int fromIndex, int toIndex)
	{
		//TODO
		throw new UnsupportedOperationException();
	}

	@Override
	public int size()
	{
		return size.getValue();
	}

	@Override
	public boolean isEmpty()
	{
		return size.getValue() == 0;
	}

	@Override
	public boolean contains(Object o)
	{
		if (indexOf(o) == -1)
			return false;
		else
			return true;
	}

	@Override
	public Object[] toArray()
	{
		Object[] ret = new Object[size()];
		for (int i = 0; i < size(); i++)
		{
			ret[i] = array.get(i).value;
		}
		return ret;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a)
	{
		if (a.length < size())
		{
			a = (T[]) Array
					.newInstance(a.getClass().getComponentType(), size());
		}

		for (int i = 0; i < size(); i++)
		{
			a[i] = (T) array.get(i).value;
		}
		return a;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean add(E e)
	{
		int n = size();
		if (n == array.length())
		{
			TransactionalBox<E>[] newInternalArray = new TransactionalBox[array
					.length() * 2];
			for (int i = 0; i < n; i++)
				newInternalArray[i] = array.get(i);
			newInternalArray[n] = new TransactionalBox<E>(e);
			for (int i = size() + 1; i < newInternalArray.length; i++)
				newInternalArray[i] = new TransactionalBox<E>();
			array = new ArrayWrapper<TransactionalBox<E>>(newInternalArray);
		}
		else
		{
			array.get(n).value = e;
		}
		size.increment();
		return true;
	}

	@Override
	public boolean remove(Object o)
	{
		int index = indexOf(o);
		if (index == -1)
			return false;
		remove(index);
		return true;
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		Map<Object, Boolean> map = new HashMap<Object, Boolean>();
		for (Object object : c)
		{
			map.put(object, false);
		}
		int counter = map.size();

		for (int i = 0; i < size(); i++)
		{
			E o = array.get(i).value;
			Boolean b = map.get(o);
			if (b == false)
			{
				map.put(o, true);
				if (--counter == 0)
					break;
			}
		}
		return counter == 0;
	}

	@Override
	public boolean addAll(Collection<? extends E> c)
	{
		if (c.size() == 0)
			return false;

		for (E e : c)
		{
			add(e);
		}

		return true;
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		Set<Object> map = new HashSet<Object>(c);
		int spacer = 0;

		for (int i = 0; i < size(); i++)
		{
			if (map.contains(array.get(i).value))
				spacer++;
			else
			{
				if (spacer != 0)
					array.get(i - spacer).value = array.get(i).value;
			}
		}

		size.setValue(size.getValue() - spacer);
		
		return spacer != 0;
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		Set<Object> map = new HashSet<Object>(c);
		int spacer = 0;

		for (int i = 0; i < size(); i++)
		{
			if (map.contains(array.get(i).value))
			{
				if (spacer != 0)
					array.get(i - spacer).value = array.get(i).value;
			}
			else
				spacer++;
		}

		size.setValue(size.getValue() - spacer);
		
		return spacer != 0;
	}

	@Override
	public void clear()
	{
		size.setValue(0);
	}

	@Override
	public Iterator<E> iterator()
	{
		return new ListIter();
	}

	@Override
	public TransactionalArrayList<E> clone()
	{
		int n = size();
		TransactionalArrayList<E> ret = new TransactionalArrayList<E>(n);
		for (int i = 0; i < n; i++)
		{
			ret.array.set(i, new TransactionalBox<E>(array.get(i).value));
		}
		ret.size.setValue(size());
		return ret;
	}
	
	public class ListIter implements ListIterator<E>
	{
		int currentIndex = 0;
		int lastElementIndex = -1;
		
		public ListIter()
		{
		}
		
		public ListIter(int index)
		{
			this.currentIndex = index;
		}
		
		@Override
		public boolean hasNext()
		{
			return currentIndex < size();
		}

		@Override
		public E next()
		{
			if (currentIndex < size())
			{
				lastElementIndex = currentIndex++;
				return array.get(lastElementIndex).value;
			}
			else
				throw new NoSuchElementException();
		}

		@Override
		public boolean hasPrevious()
		{
			return currentIndex > 0;
		}

		@Override
		public E previous()
		{
			if (currentIndex > 0)
			{
				lastElementIndex = currentIndex--;
				return array.get(lastElementIndex).value;
			}
			else
				throw new NoSuchElementException();
		}

		@Override
		public int nextIndex()
		{
			return currentIndex;
		}

		@Override
		public int previousIndex()
		{
			return currentIndex - 1;
		}

		@Override
		public void remove()
		{
			if (lastElementIndex == -1)
				throw new IllegalStateException();
			
			TransactionalArrayList.this.remove(lastElementIndex);
			if (currentIndex >= lastElementIndex)
				currentIndex--;
			
			lastElementIndex = -1;
		}

		@Override
		public void set(E e)
		{
			if (lastElementIndex == -1)
				throw new IllegalStateException();
		
			TransactionalArrayList.this.set(lastElementIndex, e);
		}

		@Override
		public void add(E e)
		{
			TransactionalArrayList.this.add(currentIndex, e);
			lastElementIndex = -1;
		}
	}
}