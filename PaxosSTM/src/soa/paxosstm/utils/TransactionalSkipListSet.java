package soa.paxosstm.utils;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedSet;
import java.util.Map.Entry;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalSkipListSet<E> implements NavigableSet<E>, Cloneable
{
	private TransactionalSkipListMap<E, Object> m;

	public TransactionalSkipListSet()
	{
		m = new TransactionalSkipListMap<E, Object>();
	}

	public TransactionalSkipListSet(Comparator<? super E> comparator)
	{
		m = new TransactionalSkipListMap<E, Object>(comparator);
	}

	public TransactionalSkipListSet(Collection<? extends E> c)
	{
		m = new TransactionalSkipListMap<E, Object>();
		addAll(c);
	}

	public TransactionalSkipListSet(final SortedSet<E> s)
	{
		m = new TransactionalSkipListMap<E, Object>(s.comparator());
		m.buildFromSorted(new Iterator<Map.Entry<E, Object>>()
		{
			private Iterator<E> it = s.iterator();

			@Override
			public boolean hasNext()
			{
				return it.hasNext();
			}

			@Override
			public Entry<E, Object> next()
			{
				return new AbstractMap.SimpleImmutableEntry<E, Object>(it.next(), Boolean.TRUE);
			}

			@Override
			public void remove()
			{
				it.remove();

			}
		});
	}

	@Override
	public TransactionalSkipListSet<E> clone()
	{
		try
		{
			@SuppressWarnings("unchecked")
			TransactionalSkipListSet<E> clone = (TransactionalSkipListSet<E>) super.clone();

			clone.m = new TransactionalSkipListMap<E, Object>(m);
			return clone;
		}
		catch (CloneNotSupportedException e)
		{
			throw new InternalError();
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if (o == this)
			return true;
		if (!(o instanceof Set))
			return false;
		Collection<?> c = (Collection<?>) o;
		try
		{
			return containsAll(c) && c.containsAll(this);
		}
		catch (ClassCastException unused)
		{
			return false;
		}
		catch (NullPointerException unused)
		{
			return false;
		}
	}

	@Override
	public int hashCode()
	{
		int h = 0;
		Iterator<E> i = iterator();
		while (i.hasNext())
		{
			E obj = i.next();
			if (obj != null)
				h += obj.hashCode();
		}
		return h;
	}

	/* ---------------- Set API methods ---------------- */

	@Override
	public int size()
	{
		return m.size();
	}

	@Override
	public boolean isEmpty()
	{
		return m.isEmpty();
	}

	@Override
	public boolean contains(Object o)
	{
		return m.containsKey(o);
	}

	@Override
	public boolean add(E e)
	{
		return m.put(e, Boolean.TRUE) == null;
	}

	@Override
	public boolean remove(Object o)
	{
		return m.remove(o) != null;
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		for (Object o : c)
		{
			if (!contains(o))
				return false;
		}
		return true;
	}

	@Override
	public boolean addAll(Collection<? extends E> c)
	{
		boolean ret = false;
		for (E e : c)
		{
			ret |= add(e);
		}
		return ret;
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		boolean modified = false;
		for (Iterator<?> i = c.iterator(); i.hasNext();)
			if (remove(i.next()))
				modified = true;
		return modified;
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		boolean modified = false;
		if (!(c instanceof Set<?>))
			c = new HashSet<Object>(c);
		for (Iterator<?> i = iterator(); i.hasNext();)
			if (!c.contains(i.next()))
			{
				i.remove();
				modified = true;
			}
		return modified;
	}

	@Override
	public void clear()
	{
		m.clear();
	}

	@Override
	public Iterator<E> iterator()
	{
		return m.keyIterator();
	}

	@Override
	public Object[] toArray()
	{
		return TransactionalSkipListMap.toList(this).toArray();
	}

	@Override
	public <T> T[] toArray(T[] a)
	{
		return TransactionalSkipListMap.toList(this).toArray(a);
	}

	/* ---------------- SortedSet API methods ---------------- */

	@Override
	public Comparator<? super E> comparator()
	{
		return m.comparator();
	}

	@Override
	public E first()
	{
		return m.firstKey();
	}

	@Override
	public E last()
	{
		return m.lastKey();
	}

	/* ---------------- NavigableSet API methods ---------------- */

	@Override
	public E lower(E e)
	{
		return m.lowerKey(e);
	}

	@Override
	public E higher(E e)
	{
		return m.higherKey(e);
	}

	@Override
	public E ceiling(E e)
	{
		return m.ceilingKey(e);
	}

	@Override
	public E floor(E e)
	{
		return m.floorKey(e);
	}

	@Override
	public E pollFirst()
	{
		Map.Entry<E, Object> e = m.pollFirstEntry();
		return e == null ? null : e.getKey();
	}

	@Override
	public E pollLast()
	{
		Map.Entry<E, Object> e = m.pollLastEntry();
		return e == null ? null : e.getKey();
	}

	@Override
	public Iterator<E> descendingIterator()
	{
		return m.descendingKeySet().iterator();
	}

	/* ---------------- view methods ---------------- */

	@Override
	public SortedSet<E> headSet(E toElement)
	{
		return headSet(toElement, true);
	}

	@Override
	public NavigableSet<E> headSet(E toElement, boolean inclusive)
	{
		return new TransactionalSkipListMap.KeySet<E>(m.headMap(toElement, inclusive), Boolean.TRUE);
	}

	@Override
	public SortedSet<E> subSet(E fromElement, E toElement)
	{
		return subSet(fromElement, true, toElement, true);
	}

	@Override
	public NavigableSet<E> subSet(E fromElement, boolean fromInclusive, E toElement,
			boolean toInclusive)
	{
		return new TransactionalSkipListMap.KeySet<E>(m.subMap(fromElement, fromInclusive,
				toElement, toInclusive), Boolean.TRUE);
	}

	@Override
	public SortedSet<E> tailSet(E fromElement)
	{
		return tailSet(fromElement, true);
	}

	@Override
	public NavigableSet<E> tailSet(E fromElement, boolean inclusive)
	{
		return new TransactionalSkipListMap.KeySet<E>(m.tailMap(fromElement, inclusive),
				Boolean.TRUE);
	}

	@Override
	public NavigableSet<E> descendingSet()
	{
		return new TransactionalSkipListMap.KeySet<E>(m.descendingMap(), Boolean.TRUE);
	}
}
