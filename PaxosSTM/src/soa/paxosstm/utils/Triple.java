package soa.paxosstm.utils;

/**
 * Simple generic class representing a triple of values.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 * @param <F>
 *            type of the first element of the triple
 * @param <S>
 *            type of the second element of the triple
 * @param <T>
 *            type of the third element of the triple
 */
public class Triple<F, S, T>
{
	public F first;
	public S second;
	public T third;

	/**
	 * Constructor.
	 * 
	 * @param first
	 *            first element of the triple
	 * @param second
	 *            second element of the triple
	 * @param third
	 *            third element of the triple
	 */
	public Triple(F first, S second, T third)
	{
		this.first = first;
		this.second = second;
		this.third = third;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj)
	{
		if (obj == null)
			return false;
		if (!(obj instanceof Triple))
			return false;
		Triple other = (Triple) obj;
		return ((first == null && other.first == null) || first.equals(other.first))
				&& ((second == null && other.second == null) || second.equals(other.second))
				&& ((third == null && other.third == null) || third.equals(other.third));
	}
}
