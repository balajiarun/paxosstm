package soa.paxosstm.utils;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalLinkedList<E> implements List<E>, Cloneable, Deque<E>
{
	@TransactionObject
	private static class Node<E>
	{
		public E value;
		public Node<E> nextNode;
		public Node<E> prevNode;

		public Node()
		{
		}

		public Node(E value)
		{
			this.value = value;
		}

		public Node(E value, Node<E> nextNode, Node<E> prevNode)
		{
			this.value = value;
			this.nextNode = nextNode;
			this.prevNode = prevNode;
		}
	}

	private Node<E> firstNode;
	private Node<E> lastNode;

	public TransactionalLinkedList()
	{
		firstNode = lastNode = null;
	}

	@SuppressWarnings("unchecked")
	public TransactionalLinkedList(Collection<E> c)
	{
		for (Object object : c)
		{
			linkLast((E) object);
		}
	}

	@Override
	public boolean add(E value)
	{
		linkLast(value);
		return true;
	}

	@Override
	public void add(int index, E element)
	{
		if (index < 0)
			throw new IndexOutOfBoundsException();

		if (index == 0)
		{
			linkFirst(element);
		}
		else
		{
			Node<E> successor = node(index);
			if (successor == null)
				throw new IndexOutOfBoundsException();
			linkBefore(element, successor);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean addAll(Collection<? extends E> c)
	{
		Iterator<E> iter = (Iterator<E>) c.iterator();

		if (iter.hasNext() == false)
			return false;

		Node<E> localFirstNode = new Node<E>(iter.next());

		Node<E> currentNode = localFirstNode;
		while (iter.hasNext())
		{
			currentNode.nextNode = new Node<E>(iter.next());
			currentNode.nextNode.prevNode = currentNode;
			currentNode = currentNode.nextNode;
		}

		if (firstNode == null)
			firstNode = localFirstNode;
		else
		{
			lastNode.nextNode = localFirstNode;
			localFirstNode.prevNode = lastNode;
		}

		lastNode = currentNode;

		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean addAll(int index, Collection<? extends E> c)
	{
		if (index < 0)
			throw new IndexOutOfBoundsException();

		Iterator<E> iter = (Iterator<E>) c.iterator();

		if (iter.hasNext() == false)
			return false;

		Node<E> successor = node(index);
		if (successor == null)
			throw new IndexOutOfBoundsException();

		Node<E> predecessor = successor.prevNode;

		Node<E> localFirstNode = new Node<E>(iter.next());

		Node<E> currentNode = localFirstNode;
		while (iter.hasNext())
		{
			currentNode.nextNode = new Node<E>(iter.next());
			currentNode.nextNode.prevNode = currentNode;
			currentNode = currentNode.nextNode;
		}

		if (index == 0)
		{
			firstNode = localFirstNode;
		}
		else
		{
			localFirstNode.prevNode = predecessor;
			predecessor.nextNode = localFirstNode;
		}
		currentNode.nextNode = successor;
		successor.prevNode = currentNode;

		return true;
	}

	@Override
	public void clear()
	{
		if (firstNode == null)
			return;

		Node<E> currentNode = firstNode;
		while (currentNode.nextNode != null)
		{
			// ((Observable) currentNode).__tc_delete();
			currentNode = currentNode.nextNode;
		}
		firstNode = lastNode = null;
	}

	@Override
	public boolean contains(Object o)
	{
		return indexOf(o) >= 0;
	}

	@Override
	public boolean containsAll(Collection<?> c)
	{
		if (firstNode == null)
			return false;

		Map<Object, Boolean> map = new HashMap<Object, Boolean>();
		for (Object object : c)
		{
			map.put(object, false);
		}
		int counter = map.size();

		Node<E> currentNode = firstNode;
		while (currentNode != null)
		{
			E o = currentNode.value;
			Boolean b = map.get(o);
			if (b == false)
			{
				map.put(o, true);
				if (--counter == 0)
					break;
			}
			currentNode = currentNode.nextNode;
		}

		return counter == 0;
	}

	@Override
	public boolean equals(Object o)
	{
		if (o == this)
			return true;
		if (!(o instanceof List))
			return false;

		ListIterator<E> e1 = listIterator();
		ListIterator<?> e2 = ((List<?>) o).listIterator();
		while (e1.hasNext() && e2.hasNext())
		{
			E o1 = e1.next();
			Object o2 = e2.next();
			if (!(o1 == null ? o2 == null : o1.equals(o2)))
				return false;
		}
		return !(e1.hasNext() || e2.hasNext());
	}

	@Override
	public E get(int index)
	{
		Node<E> node = node(index);
		if (node == null)
			throw new IndexOutOfBoundsException();

		return node.value;
	}

	@Override
	public int indexOf(Object o)
	{
		int i = 0;
		Node<E> currentNode = firstNode;
		while (currentNode != null)
		{
			if (currentNode.value.equals(o))
				return i;
			i++;
			currentNode = currentNode.nextNode;
		}
		return -1;
	}

	@Override
	public boolean isEmpty()
	{
		return firstNode == null;
	}

	@Override
	public Iterator<E> iterator()
	{
		return new ListIter();
	}

	@Override
	public int lastIndexOf(Object o)
	{
		int lastFound = -1;
		int i = 0;
		Node<E> currentNode = firstNode;
		while (currentNode != null)
		{
			if (currentNode.value.equals(o))
				lastFound = i;
			i++;
			currentNode = currentNode.nextNode;
		}
		return lastFound;
	}

	@Override
	public ListIterator<E> listIterator()
	{
		return new ListIter();
	}

	@Override
	public ListIterator<E> listIterator(int index)
	{
		return new ListIter(index);
	}

	@Override
	public boolean remove(Object o)
	{
		if (o == null)
		{
			for (Node<E> node = firstNode; node != null; node = node.nextNode)
			{
				if (node.value == null)
				{
					unlink(node);
					return true;
				}
			}
		}
		else
		{
			for (Node<E> node = firstNode; node != null; node = node.nextNode)
			{
				if (o.equals(node.value))
				{
					unlink(node);
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public E remove(int index)
	{
		Node<E> node = node(index);
		if (node == null)
			throw new IndexOutOfBoundsException();

		return unlink(node);
	}

	@Override
	public boolean removeAll(Collection<?> c)
	{
		Set<Object> map = new HashSet<Object>(c);
		boolean listModified = false;

		Node<E> currentNode = firstNode;
		while (currentNode != null)
		{
			Node<E> localCurrentNode = currentNode;
			currentNode = currentNode.nextNode;
			if (map.contains(localCurrentNode.value))
			{
				unlink(localCurrentNode);
				listModified = true;
			}
		}

		return listModified;
	}

	@Override
	public boolean retainAll(Collection<?> c)
	{
		Set<Object> map = new HashSet<Object>(c);
		boolean listModified = false;

		Node<E> currentNode = firstNode;
		while (currentNode != null)
		{
			Node<E> localCurrentNode = currentNode;
			currentNode = currentNode.nextNode;
			if (!map.contains(localCurrentNode.value))
			{
				unlink(localCurrentNode);
				listModified = true;
			}
		}

		return listModified;
	}

	@Override
	public E set(int index, E element)
	{
		Node<E> node = node(index);
		if (node == null)
			throw new IndexOutOfBoundsException();

		E ret = node.value;
		node.value = element;

		return ret;
	}

	@Override
	public int size()
	{
		int i = 0;
		Node<E> currentNode = firstNode;

		while (currentNode != null)
		{
			i++;
			currentNode = currentNode.nextNode;
		}

		return i;
	}

	@Override
	public Object[] toArray()
	{
		Object[] array = new Object[size()];
		int i = 0;
		Node<E> currentNode = firstNode;
		while (currentNode != null)
		{
			array[i++] = currentNode.value;
			currentNode = currentNode.nextNode;
		}

		return array;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T[] toArray(T[] a)
	{
		int size = size();
		if (a.length < size)
		{
			a = (T[]) Array.newInstance(a.getClass().getComponentType(), size);
		}

		Node<E> currentNode = firstNode;
		int i = 0;
		while (currentNode != null)
		{
			a[i++] = (T) currentNode.value;
			currentNode = currentNode.nextNode;
		}

		return a;
	}

	@Override
	public TransactionalLinkedList<E> clone()
	{
		TransactionalLinkedList<E> ret = new TransactionalLinkedList<E>();

		Node<E> currentNode = firstNode;
		while (currentNode != null)
		{
			ret.add(currentNode.value);
			currentNode = currentNode.nextNode;
		}

		return ret;
	}

	private void linkFirst(E e)
	{
		final Node<E> f = firstNode;
		final Node<E> newNode = new Node<E>(e, f, null);
		firstNode = newNode;
		if (f == null)
			lastNode = newNode;
		else
			f.prevNode = newNode;
	}

	void linkLast(E e)
	{
		final Node<E> l = lastNode;
		final Node<E> newNode = new Node<E>(e, null, l);
		lastNode = newNode;
		if (l == null)
			firstNode = newNode;
		else
			l.nextNode = newNode;
	}

	void linkBefore(E e, Node<E> successor)
	{
		final Node<E> predecessor = successor.prevNode;
		final Node<E> newNode = new Node<E>(e, successor, predecessor);
		successor.prevNode = newNode;
		if (predecessor == null)
			firstNode = newNode;
		else
			predecessor.nextNode = newNode;
	}

	private E unlinkFirst(Node<E> f)
	{
		final E element = f.value;
		final Node<E> next = f.nextNode;
		// ((Observable) f).__tc_delete();
		firstNode = next;
		if (next == null)
			lastNode = null;
		else
			next.prevNode = null;
		return element;
	}

	private E unlinkLast(Node<E> l)
	{
		final E element = l.value;
		final Node<E> prev = l.prevNode;
		// ((Observable) l).__tc_delete();
		lastNode = prev;
		if (prev == null)
			firstNode = null;
		else
			prev.nextNode = null;

		return element;
	}

	E unlink(Node<E> node)
	{
		final E element = node.value;
		final Node<E> next = node.nextNode;
		final Node<E> prev = node.prevNode;

		if (prev == null)
			firstNode = next;
		else
			prev.nextNode = next;

		if (next == null)
			lastNode = prev;
		else
			next.prevNode = prev;

		// ((Observable) node).__tc_delete();

		return element;
	}

	Node<E> node(int index)
	{
		if (index < 0)
			return null;

		int i = 0;
		Node<E> currentNode = firstNode;
		while (currentNode != null && i < index)
		{
			i++;
			currentNode = currentNode.nextNode;
		}

		if (currentNode == null)
			return null;

		return currentNode;
	}

	@Override
	public void addFirst(E e)
	{
		linkFirst(e);
	}

	@Override
	public void addLast(E e)
	{
		linkLast(e);
	}

	@Override
	public boolean offerFirst(E e)
	{
		addFirst(e);
		return true;
	}

	@Override
	public boolean offerLast(E e)
	{
		addLast(e);
		return true;
	}

	@Override
	public E removeFirst()
	{
		if (firstNode == null)
			throw new NoSuchElementException();

		return unlinkFirst(firstNode);
	}

	@Override
	public E removeLast()
	{
		if (lastNode == null)
			throw new NoSuchElementException();

		return unlinkLast(lastNode);
	}

	@Override
	public E pollFirst()
	{
		return (firstNode == null) ? null : unlinkFirst(firstNode);
	}

	@Override
	public E pollLast()
	{
		return (lastNode == null) ? null : unlinkLast(lastNode);
	}

	@Override
	public E getFirst()
	{
		if (firstNode == null)
			throw new NoSuchElementException();

		return firstNode.value;
	}

	@Override
	public E getLast()
	{
		if (lastNode == null)
			throw new NoSuchElementException();

		return lastNode.value;
	}

	@Override
	public E peekFirst()
	{
		return (firstNode == null) ? null : firstNode.value;
	}

	@Override
	public E peekLast()
	{
		return (lastNode == null) ? null : lastNode.value;
	}

	@Override
	public boolean removeFirstOccurrence(Object o)
	{
		return remove(o);
	}

	@Override
	public boolean removeLastOccurrence(Object o)
	{
		if (o == null)
		{
			for (Node<E> node = lastNode; node != null; node = node.prevNode)
			{
				if (node.value == null)
				{
					unlink(node);
					return true;
				}
			}
		}
		else
		{
			for (Node<E> node = lastNode; node != null; node = node.prevNode)
			{
				if (o.equals(node.value))
				{
					unlink(node);
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public boolean offer(E e)
	{
		return add(e);
	}

	@Override
	public E remove()
	{
		return removeFirst();
	}

	@Override
	public E poll()
	{
		return (firstNode == null) ? null : unlinkFirst(firstNode);
	}

	@Override
	public E element()
	{
		return getFirst();
	}

	@Override
	public E peek()
	{
		return (firstNode == null) ? null : firstNode.value;
	}

	@Override
	public void push(E e)
	{
		addFirst(e);
	}

	@Override
	public E pop()
	{
		return removeFirst();
	}

	@Override
	public Iterator<E> descendingIterator()
	{
		return new DescendingIter();
	}

	private class ListIter implements ListIterator<E>
	{
		Node<E> next;
		Node<E> prev;
		int currentIndex = 0;
		Node<E> lastElement = null;

		public ListIter()
		{
			next = (Node<E>) TransactionalLinkedList.this.firstNode;
			if (next != null)
				prev = next.prevNode;
		}

		public ListIter(int index)
		{
			int i = 0;
			Node<E> currentNode = (Node<E>) TransactionalLinkedList.this.firstNode;
			while (currentNode != null && i < index)
			{
				i++;
				currentNode = currentNode.nextNode;
			}

			if (currentNode == null)
				throw new IndexOutOfBoundsException();

			next = currentNode;
			currentIndex = i;
		}

		@Override
		public boolean hasNext()
		{
			return next != null;
		}

		@Override
		public E next()
		{
			if (next == null)
				throw new NoSuchElementException();
			currentIndex++;
			lastElement = prev = next;
			next = lastElement.nextNode;

			return lastElement.value;
		}

		@Override
		public boolean hasPrevious()
		{
			return prev != null;
		}

		@Override
		public E previous()
		{
			if (prev == null)
				throw new NoSuchElementException();
			currentIndex--;
			lastElement = next = prev;
			prev = lastElement.prevNode;

			return lastElement.value;
		}

		@Override
		public int nextIndex()
		{
			return currentIndex;
		}

		@Override
		public int previousIndex()
		{
			return currentIndex - 1;
		}

		@Override
		public void remove()
		{
			if (lastElement == null)
				throw new IllegalStateException();

			if (lastElement == prev)
				currentIndex--;

			next = lastElement.nextNode;
			prev = lastElement.prevNode;
			unlink((Node<E>) lastElement);

			lastElement = null;
		}

		@SuppressWarnings("unchecked")
		@Override
		public void set(Object e)
		{
			if (lastElement == null)
				throw new IllegalStateException();
			lastElement.value = (E) e;

		}

		@Override
		public void add(E o)
		{
			currentIndex++;
			Node<E> e = new Node<E>(o);
			e.prevNode = prev;
			e.nextNode = next;

			if (prev != null)
				prev.nextNode = e;
			else
				firstNode = e;

			if (next != null)
				next.prevNode = e;
			else
				lastNode = e;

			prev = e;
			lastElement = null;
		}
	}

	private class DescendingIter implements Iterator<E>
	{
		private final ListIter itr = new ListIter(size());

		public boolean hasNext()
		{
			return itr.hasPrevious();
		}

		public E next()
		{
			return itr.previous();
		}

		public void remove()
		{
			itr.remove();
		}
	}

	@Override
	public int hashCode()
	{
		int hashCode = 1;
		for (E e : this)
			hashCode = 31 * hashCode + (e == null ? 0 : e.hashCode());
		return hashCode;
	}

	@Override
	public List<E> subList(int fromIndex, int toIndex)
	{
		throw new UnsupportedOperationException();
	}

	protected void removeRange(int fromIndex, int toIndex)
	{
		ListIterator<E> it = listIterator(fromIndex);
		for (int i = 0, n = toIndex - fromIndex; i < n; i++)
		{
			it.next();
			it.remove();
		}
	}
}
