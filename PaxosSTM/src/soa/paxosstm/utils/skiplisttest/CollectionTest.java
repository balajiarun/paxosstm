package soa.paxosstm.utils.skiplisttest;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class CollectionTest<E>
{
	final Collection<E> referenceCollection;
	final Collection<E> testedCollection;
	final Generator<E> generator;

	final int op;
	final int longop;

	public CollectionTest(Collection<E> referenceCollection, Collection<E> testedCollection,
			int op, int longop, Generator<E> generator)
	{
		this.referenceCollection = referenceCollection;
		this.testedCollection = testedCollection;
		this.generator = generator;
		this.op = op;
		this.longop = longop;
	}

	public void run()
	{
		checkCollectionsEquality(referenceCollection, testedCollection);

		for (int i = 0; i < op; i++)
		{
			containsOp();
			addOp();
			modOp();
			removeOp();
			isEmptyOp();
			if (longop > 0 && i % (op / longop) == 0)
			{
				checkCollectionsEquality(referenceCollection, testedCollection);
				removeAllOp();
				addAllOp();
				retainAllOp();
				addAllOp();
				sizeOp();
				// hashCodeOp();
				// equalsTrueOp();
				// equalsFalseOp();
				iteratorOp();
				iteratorRemoveOp();
				addAllOp();
				toArrayOp();
			}
		}

		checkCollectionsEquality(referenceCollection, testedCollection);
	}

	void isEmptyOp()
	{
		boolean r1 = referenceCollection.isEmpty();
		boolean r2 = testedCollection.isEmpty();
		checkEquality(r1, r2);
	}

	void containsOp()
	{
		Object key = generator.next();
		Object r1 = referenceCollection.contains(key);
		Object r2 = testedCollection.contains(key);
		checkEquality(r1, r2);
	}

	void sizeOp()
	{
		int size1 = referenceCollection.size();
		int size2 = testedCollection.size();
		checkEquality(size1, size2);
	}

	void hashCodeOp()
	{
		int hash1 = referenceCollection.hashCode();
		int hash2 = testedCollection.hashCode();
		checkEquality(hash1, hash2);
	}

	void equalsTrueOp()
	{
		checkEquality(testedCollection, referenceCollection);
	}

	void equalsFalseOp()
	{
		HashSet<E> m = new HashSet<E>(referenceCollection);
		E key = generator.invalid();
		m.add(key);
		checkEquality(testedCollection.equals(m), false);
	}

	void addAllOp()
	{
		HashSet<E> m = new HashSet<E>();
		for (int i = 0; i < op / longop; i++)
			m.add(generator.next());
		Exception e1 = null;
		Exception e2 = null;
		try
		{
			referenceCollection.addAll(m);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			testedCollection.addAll(m);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
	}

	void removeAllOp()
	{
		HashSet<E> m = new HashSet<E>();
		for (int i = 0; i < op / longop; i++)
			m.add(generator.next());
		referenceCollection.removeAll(m);
		testedCollection.removeAll(m);
	}

	void retainAllOp()
	{
		HashSet<E> m = new HashSet<E>(referenceCollection);
		for (int i = 0; i < op / longop; i++)
			m.remove(generator.next());
		referenceCollection.retainAll(m);
		testedCollection.retainAll(m);
	}

	void iteratorOp()
	{
		Iterator<E> it1 = referenceCollection.iterator();
		Iterator<E> it2 = testedCollection.iterator();
		while (true)
		{
			checkEquality(it1.hasNext(), it2.hasNext());
			if (!it1.hasNext())
				break;
			E e1 = it1.next();
			E e2 = it2.next();
			checkEquality(e1, e2);
		}
	}

	void iteratorRemoveOp()
	{
		HashSet<E> m = new HashSet<E>();
		for (int i = 0; i < op / longop; i++)
			m.add(generator.next());

		Iterator<E> it1 = referenceCollection.iterator();
		Iterator<E> it2 = testedCollection.iterator();
		while (true)
		{
			checkEquality(it1.hasNext(), it2.hasNext());
			if (!it1.hasNext())
				break;
			E e1 = it1.next();
			E e2 = it2.next();
			checkEquality(e1, e2);

			if (m.contains(e1))
			{
				it1.remove();
				it2.remove();
			}
		}
	}

	void toArrayOp()
	{
		Object[] r1 = referenceCollection.toArray();
		Object[] r2 = testedCollection.toArray();
		checkEquality(Arrays.deepEquals(r1, r2), true);
	}

	void modOp()
	{
		E key = generator.next();
		if (!referenceCollection.contains(key))
		{
			addOp(key);
		}
		else
		{
			removeOp(key);
		}
	}

	void addOp()
	{
		addOp(generator.next());
	}

	void addOp(E key)
	{
		Exception e1 = null;
		Exception e2 = null;
		Object r1 = null;
		Object r2 = null;
		try
		{
			r1 = referenceCollection.add(key);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			r2 = testedCollection.add(key);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(r1, r2);
	}

	void removeOp()
	{
		removeOp(generator.next());
	}

	void removeOp(E key)
	{
		Object r1 = referenceCollection.remove(key);
		Object r2 = testedCollection.remove(key);
		checkEquality(r1, r2);
	}

	static void checkEquality(Object o1, Object o2)
	{
		if ((o1 != null && !o1.equals(o2)) || (o1 == null && o2 != null))
		{
			System.err.println("A: " + o1);
			System.err.println("B: " + o2);
			throw new RuntimeException("NOT EQUAL");
		}
	}

	static void checkCollectionsEquality(Collection<?> c1, Collection<?> c2)
	{
		Iterator<?> it1 = c1.iterator();
		Iterator<?> it2 = c2.iterator();
		while (true)
		{
			checkEquality(it1.hasNext(), it2.hasNext());
			if (!it1.hasNext())
				break;
			checkEquality(it1.next(), it2.next());
		}
	}
}
