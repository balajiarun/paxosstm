package soa.paxosstm.utils.skiplisttest;

import java.io.IOException;
import java.util.Comparator;
import java.util.Map;
import java.util.Random;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentSkipListMap;

import soa.paxosstm.utils.TransactionalSkipListMap;
import soa.paxosstm.utils.TransactionalSkipListSet;

public class SkipListTest
{
	static final int RANGE = 20000;
	static final int OP = 10000;
	static final int LONGOP = 10;

	public static void main(String[] args) throws IOException
	{
		ConcurrentSkipListMap<Integer, Integer> referenceMap = new ConcurrentSkipListMap<Integer, Integer>(
				new StrangeComparator());

		Generator<Integer> gen = new IntGenerator(0, RANGE, -1);

		// NavigableMap<Integer, Integer> tm = new TreeMap<Integer, Integer>();
		// tm.navigableKeySet().descendingSet().add(0);
		// System.out.println(tm.get(0));
		// for (Map.Entry<Integer, Integer> e : tm.entrySet())
		// System.out.println(e.getKey() + ": " + e.getValue());

		System.out.println("testing map...");
		init(referenceMap, gen, gen, RANGE / 2);
		TransactionalSkipListMap<Integer, Integer> testedMap = new TransactionalSkipListMap<Integer, Integer>(
				referenceMap);
		new NavigableMapTest<Integer, Integer>(referenceMap, testedMap, OP, LONGOP, gen, gen, true,
				true).run();

		System.out.println("testing set...");
		referenceMap.clear();
		init(referenceMap, gen, gen, RANGE / 2);
		TreeSet<Integer> refSet = new TreeSet<Integer>(referenceMap.keySet());
		TransactionalSkipListSet<Integer> testSet = new TransactionalSkipListSet<Integer>(refSet);
		new NavigableSetTest<Integer>(refSet, testSet, OP * 10, LONGOP, gen, true, true).run();

		System.out.println("finished.");
	}

	private static <K, V> void init(Map<K, V> map, Generator<K> keyGenerator,
			Generator<V> valueGenerator, int n)
	{
		for (int i = 0; i < n; i++)
		{
			while (map.put(keyGenerator.next(), valueGenerator.next()) != null)
				;
		}
	}

	static class IntGenerator implements Generator<Integer>
	{
		final int range1;
		final int range2;
		final int invalid;
		final Random random;

		public IntGenerator(int range1, int range2, int invalid)
		{
			this.range1 = range1;
			this.range2 = range2;
			this.invalid = invalid;
			this.random = new Random();
		}

		@Override
		public Integer next()
		{
			return random.nextInt(range2 - range1) + range1;
		}

		@Override
		public Integer invalid()
		{
			return invalid;
		}
	}

	static class StrangeComparator implements Comparator<Integer>
	{
		@Override
		public int compare(Integer a, Integer b)
		{
			int bc1 = Integer.bitCount(a);
			int bc2 = Integer.bitCount(b);
			if (bc1 != bc2)
				return bc1 - bc2;
			return a - b;
		}
	}
}
