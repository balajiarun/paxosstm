package soa.paxosstm.utils.skiplisttest;

import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;

public class NavigableMapTest<K, V> extends SortedMapTest<K, V>
{
	final NavigableMap<K, V> referenceMap;
	final NavigableMap<K, V> testedMap;
	final boolean testDescending;

	public NavigableMapTest(NavigableMap<K, V> referenceMap, NavigableMap<K, V> testedMap, int op,
			int longop, Generator<K> keyGenerator, Generator<V> valueGenerator, boolean testViews,
			boolean testDescending)
	{
		super(referenceMap, testedMap, op, longop, keyGenerator, valueGenerator, testViews);
		this.referenceMap = referenceMap;
		this.testedMap = testedMap;
		this.testDescending = testDescending;
	}

	public void run()
	{
		checkEquality(referenceMap, testedMap);

		for (int i = 0; i < op; i++)
		{
			containsKeyOp();
			getOp();
			firstKeyOp();
			lastKeyOp();
			putOp();
			modOp();
			removeOp();
			pollFirstEntryOp();
			pollLastEntryOp();
			putOp();
			putOp();
			putOp();
			putOp();
			firstEntryOp();
			lastEntryOp();
			isEmptyOp();
			lowerKeyOp();
			lowerEntryOp();
			ceilingKeyOp();
			ceilingEntryOp();
			floorKeyOp();
			floorEntryOp();
			higherKeyOp();
			higherEntryOp();
			if (longop > 0 && i % (op / longop) == 0)
			{
				checkEquality(referenceMap, testedMap);
				putAllOp();
				containsValueOp();
				sizeOp();
				hashCodeOp();
				equalsTrueOp();
				equalsFalseOp();
				if (testViews)
				{
					headMapOp((i / (op / longop)) % 2 == 0);
					tailMapOp((i / (op / longop)) % 2 == 1);
					subMapOp((i / (op / longop)) % 4 < 2, (i / (op / longop)) % 2 == 1);
					Map<K,V> m = new HashMap<K,V>(referenceMap);
					navigableKeySetOp();
					referenceMap.putAll(m);
					testedMap.putAll(m);
					checkEquality(referenceMap, testedMap);
					valuesOp();
					referenceMap.putAll(m);
					testedMap.putAll(m);
					checkEquality(referenceMap, testedMap);
					entrySetOp();
					clearOp();
					referenceMap.putAll(m);
					testedMap.putAll(m);
					checkEquality(referenceMap, testedMap);
				}
			}
		}
		if (testDescending)
			new NavigableMapTest<K, V>(referenceMap.descendingMap(), testedMap.descendingMap(), op,
					longop, keyGenerator, valueGenerator, testViews, false).run();

		checkEquality(referenceMap, testedMap);
	}

	void firstEntryOp()
	{
		Object r1 = referenceMap.firstEntry();
		Object r2 = testedMap.firstEntry();
		checkEquality(r1, r2);
	}

	void lastEntryOp()
	{
		Object r1 = referenceMap.lastEntry();
		Object r2 = testedMap.lastEntry();
		checkEquality(r1, r2);
	}

	void ceilingKeyOp()
	{
		K key = keyGenerator.next();
		Object r1 = referenceMap.ceilingKey(key);
		Object r2 = testedMap.ceilingKey(key);
		checkEquality(r1, r2);
	}

	void ceilingEntryOp()
	{
		K key = keyGenerator.next();
		Object r1 = referenceMap.ceilingEntry(key);
		Object r2 = testedMap.ceilingEntry(key);
		checkEquality(r1, r2);
	}

	void floorKeyOp()
	{
		K key = keyGenerator.next();
		Object r1 = referenceMap.floorKey(key);
		Object r2 = testedMap.floorKey(key);
		checkEquality(r1, r2);
	}

	void floorEntryOp()
	{
		K key = keyGenerator.next();
		Object r1 = referenceMap.floorEntry(key);
		Object r2 = testedMap.floorEntry(key);
		checkEquality(r1, r2);
	}

	void lowerKeyOp()
	{
		K key = keyGenerator.next();
		Object r1 = referenceMap.lowerKey(key);
		Object r2 = testedMap.lowerKey(key);
		checkEquality(r1, r2);
	}

	void lowerEntryOp()
	{
		K key = keyGenerator.next();
		Object r1 = referenceMap.lowerEntry(key);
		Object r2 = testedMap.lowerEntry(key);
		checkEquality(r1, r2);
	}

	void higherKeyOp()
	{
		K key = keyGenerator.next();
		Object r1 = referenceMap.higherKey(key);
		Object r2 = testedMap.higherKey(key);
		checkEquality(r1, r2);
	}

	void higherEntryOp()
	{
		K key = keyGenerator.next();
		Object r1 = referenceMap.higherEntry(key);
		Object r2 = testedMap.higherEntry(key);
		checkEquality(r1, r2);
	}

	void pollFirstEntryOp()
	{
		Object r1 = referenceMap.pollFirstEntry();
		Object r2 = testedMap.pollFirstEntry();
		checkEquality(r1, r2);

	}

	void pollLastEntryOp()
	{
		Object r1 = referenceMap.pollLastEntry();
		Object r2 = testedMap.pollLastEntry();
		checkEquality(r1, r2);

	}

	void headMapOp(boolean inclusive)
	{
		Exception e1 = null;
		Exception e2 = null;
		NavigableMap<K, V> map1 = null;
		NavigableMap<K, V> map2 = null;
		K to = keyGenerator.next();
		try
		{
			map1 = referenceMap.headMap(to, inclusive);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			map2 = testedMap.headMap(to, inclusive);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(map1, map2);
		if (e1 == null)
			new NavigableMapTest<K, V>(map1, map2, op / longop, longop > 1 ? 1 : 0, keyGenerator,
					valueGenerator, testViews, testDescending).run();
	}

	void tailMapOp(boolean inclusive)
	{
		Exception e1 = null;
		Exception e2 = null;
		NavigableMap<K, V> map1 = null;
		NavigableMap<K, V> map2 = null;
		K from = keyGenerator.next();
		try
		{
			map1 = referenceMap.tailMap(from, inclusive);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			map2 = testedMap.tailMap(from, inclusive);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(map1, map2);
		if (e1 == null)
			new NavigableMapTest<K, V>(map1, map2, op / longop, longop > 1 ? 1 : 0, keyGenerator,
					valueGenerator, testViews, testDescending).run();
	}

	void subMapOp(boolean fromInclusive, boolean toInclusive)
	{
		Exception e1 = null;
		Exception e2 = null;
		NavigableMap<K, V> map1 = null;
		NavigableMap<K, V> map2 = null;
		K from = keyGenerator.next();
		K to = keyGenerator.next();
		try
		{
			map1 = referenceMap.subMap(from, fromInclusive, to, toInclusive);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			map2 = testedMap.subMap(from, fromInclusive, to, toInclusive);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(map1, map2);
		if (e1 == null)
			new NavigableMapTest<K, V>(map1, map2, op / longop, longop > 1 ? 1 : 0, keyGenerator,
					valueGenerator, testViews, testDescending).run();
	}

	void navigableKeySetOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		NavigableSet<K> set1 = null;
		NavigableSet<K> set2 = null;
		try
		{
			set1 = referenceMap.navigableKeySet();
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			set2 = testedMap.navigableKeySet();
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(set1, set2);
		if (e1 == null)
			new NavigableSetTest<K>(set1, set2, op / longop, longop > 1 ? 1 : 0, keyGenerator,
					false, false).run();
	}
}
