package soa.paxosstm.utils.skiplisttest;

public interface Generator<T>
{
	T next();
	T invalid();
}
