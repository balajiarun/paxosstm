package soa.paxosstm.utils.skiplisttest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetTest<E>
{
	final Set<E> referenceSet;
	final Set<E> testedSet;
	final Generator<E> generator;

	final int op;
	final int longop;

	public SetTest(Set<E> referenceSet, Set<E> testedSet, int op, int longop,
			Generator<E> generator)
	{
		this.referenceSet = referenceSet;
		this.testedSet = testedSet;
		this.generator = generator;
		this.op = op;
		this.longop = longop;
	}

	public void run()
	{
		checkEquality(referenceSet, testedSet);

		for (int i = 0; i < op; i++)
		{
			containsOp();
			addOp();
			modOp();
			removeOp();
			isEmptyOp();
			if (longop > 0 && i % (op / longop) == 0)
			{
				checkEquality(referenceSet, testedSet);
				removeAllOp();
				addAllOp();
				retainAllOp();
				addAllOp();
				sizeOp();
				hashCodeOp();
				equalsTrueOp();
				equalsFalseOp();
				iteratorOp();
				iteratorRemoveOp();
				addAllOp();
				toArrayOp();
			}
		}

		checkEquality(referenceSet, testedSet);
	}

	void isEmptyOp()
	{
		boolean r1 = referenceSet.isEmpty();
		boolean r2 = testedSet.isEmpty();
		checkEquality(r1, r2);
	}

	void containsOp()
	{
		Object key = generator.next();
		Object r1 = referenceSet.contains(key);
		Object r2 = testedSet.contains(key);
		checkEquality(r1, r2);
	}

	void sizeOp()
	{
		int size1 = referenceSet.size();
		int size2 = testedSet.size();
		checkEquality(size1, size2);
	}

	void hashCodeOp()
	{
		int hash1 = referenceSet.hashCode();
		int hash2 = testedSet.hashCode();
		checkEquality(hash1, hash2);
	}

	void equalsTrueOp()
	{
		checkEquality(testedSet, referenceSet);
	}

	void equalsFalseOp()
	{
		HashSet<E> m = new HashSet<E>(referenceSet);
		E key = generator.invalid();
		m.add(key);
		checkEquality(testedSet.equals(m), false);
	}

	void addAllOp()
	{
		HashSet<E> m = new HashSet<E>();
		for (int i = 0; i < op / longop; i++)
			m.add(generator.next());
		Exception e1 = null;
		Exception e2 = null;
		try
		{
			referenceSet.addAll(m);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			testedSet.addAll(m);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
	}

	void removeAllOp()
	{
		HashSet<E> m = new HashSet<E>();
		for (int i = 0; i < op / longop; i++)
			m.add(generator.next());
		referenceSet.removeAll(m);
		testedSet.removeAll(m);
	}

	void retainAllOp()
	{
		HashSet<E> m = new HashSet<E>(referenceSet);
		for (int i = 0; i < op / longop; i++)
			m.remove(generator.next());
		referenceSet.retainAll(m);
		testedSet.retainAll(m);
	}

	void iteratorOp()
	{
		Iterator<E> it1 = referenceSet.iterator();
		Iterator<E> it2 = testedSet.iterator();
		while (true)
		{
			checkEquality(it1.hasNext(), it2.hasNext());
			if (!it1.hasNext())
				break;
			E e1 = it1.next();
			E e2 = it2.next();
			checkEquality(e1, e2);
		}
	}

	void iteratorRemoveOp()
	{
		HashSet<E> m = new HashSet<E>();
		for (int i = 0; i < op / longop; i++)
			m.add(generator.next());

		Iterator<E> it1 = referenceSet.iterator();
		Iterator<E> it2 = testedSet.iterator();
		while (true)
		{
			checkEquality(it1.hasNext(), it2.hasNext());
			if (!it1.hasNext())
				break;
			E e1 = it1.next();
			E e2 = it2.next();
			checkEquality(e1, e2);

			if (m.contains(e1))
			{
				it1.remove();
				it2.remove();
			}
		}
	}
	
	void toArrayOp()
	{
		Object[] r1 = referenceSet.toArray();
		Object[] r2 = testedSet.toArray();
		checkEquality(Arrays.deepEquals(r1, r2), true);
	}

	void modOp()
	{
		E key = generator.next();
		if (!referenceSet.contains(key))
		{
			addOp(key);
		}
		else
		{
			removeOp(key);
		}
	}
	
	void addOp()
	{
		addOp(generator.next());
	}
	
	void addOp(E key)
	{
		Exception e1 = null;
		Exception e2 = null;
		Object r1 = null;
		Object r2 = null;
		try
		{
			r1 = referenceSet.add(key);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			r2 = testedSet.add(key);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(r1, r2);
	}
	
	void removeOp()
	{
		removeOp(generator.next());
	}
	
	void removeOp(E key)
	{
		Object r1 = referenceSet.remove(key);
		Object r2 = testedSet.remove(key);
		checkEquality(r1, r2);
	}

	void clearOp()
	{
		checkEquality(referenceSet, testedSet);
		referenceSet.clear();
		testedSet.clear();
		checkEquality(referenceSet, testedSet);
	}

	static void checkEquality(Object o1, Object o2)
	{
		if ((o1 != null && !o1.equals(o2)) || (o1 == null && o2 != null))
		{
			System.err.println("A: " + o1);
			System.err.println("B: " + o2);
			throw new RuntimeException("NOT EQUAL");
		}
	}
}
