package soa.paxosstm.utils.skiplisttest;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MapTest<K, V>
{
	final Map<K, V> referenceMap;
	final Map<K, V> testedMap;
	final Generator<K> keyGenerator;
	final Generator<V> valueGenerator;

	final int op;
	final int longop;
	final boolean testViews;

	public MapTest(Map<K, V> referenceMap, Map<K, V> testedMap, int op, int longop,
			Generator<K> keyGenerator, Generator<V> valueGenerator, boolean testViews)
	{
		this.referenceMap = referenceMap;
		this.testedMap = testedMap;
		this.op = op;
		this.longop = longop;
		this.keyGenerator = keyGenerator;
		this.valueGenerator = valueGenerator;
		this.testViews = testViews;
	}

	public void run()
	{
		checkEquality(referenceMap, testedMap);

		for (int i = 0; i < op; i++)
		{
			containsKeyOp();
			getOp();
			putOp();
			modOp();
			removeOp();
			isEmptyOp();
			if (longop > 0 && i % (op / longop) == 0)
			{
				checkEquality(referenceMap, testedMap);
				putAllOp();
				containsValueOp();
				sizeOp();
				hashCodeOp();
				equalsTrueOp();
				equalsFalseOp();
				if (testViews)
				{
					Map<K,V> m = new HashMap<K,V>(referenceMap);
					keySetOp();
					referenceMap.putAll(m);
					testedMap.putAll(m);
					checkEquality(referenceMap, testedMap);
					valuesOp();
					referenceMap.putAll(m);
					testedMap.putAll(m);
					checkEquality(referenceMap, testedMap);
					entrySetOp();
					clearOp();
					referenceMap.putAll(m);
					testedMap.putAll(m);
					checkEquality(referenceMap, testedMap);
				}
			}
		}

		checkEquality(referenceMap, testedMap);
	}

	void getOp()
	{
		Object key = keyGenerator.next();
		Object r1 = referenceMap.get(key);
		Object r2 = testedMap.get(key);
		checkEquality(r1, r2);
	}

	void isEmptyOp()
	{
		boolean r1 = referenceMap.isEmpty();
		boolean r2 = testedMap.isEmpty();
		checkEquality(r1, r2);
	}

	void containsKeyOp()
	{
		Object key = keyGenerator.next();
		Object r1 = referenceMap.containsKey(key);
		Object r2 = testedMap.containsKey(key);
		checkEquality(r1, r2);
	}

	void containsValueOp()
	{
		Object key = keyGenerator.next();
		Object r1 = referenceMap.containsValue(key);
		Object r2 = testedMap.containsValue(key);
		checkEquality(r1, r2);
	}

	void sizeOp()
	{
		int size1 = referenceMap.size();
		int size2 = testedMap.size();
		checkEquality(size1, size2);
	}

	void hashCodeOp()
	{
		int hash1 = referenceMap.hashCode();
		int hash2 = testedMap.hashCode();
		checkEquality(hash1, hash2);
	}

	void equalsTrueOp()
	{
		checkEquality(testedMap, referenceMap);
	}

	void equalsFalseOp()
	{
		HashMap<K, V> m = new HashMap<K, V>(referenceMap);
		m.put(keyGenerator.next(), valueGenerator.invalid());
		checkEquality(testedMap.equals(m), false);
	}

	void putAllOp()
	{
		HashMap<K, V> m = new HashMap<K, V>();
		for (int i = 0; i < op / longop; i++)
			m.put(keyGenerator.next(), valueGenerator.next());
		
		Exception e1 = null;
		Exception e2 = null;
		try
		{
			referenceMap.putAll(m);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			testedMap.putAll(m);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
	}

	void modOp()
	{
		K key = keyGenerator.next();
		if (referenceMap.get(key) == null)
		{
			putOp(key);
		}
		else
		{
			Object r1 = referenceMap.remove(key);
			Object r2 = testedMap.remove(key);
			checkEquality(r1, r2);
		}
	}
	
	void putOp()
	{
		putOp(keyGenerator.next());
	}
	
	void putOp(K key)
	{
		Exception e1 = null;
		Exception e2 = null;
		Object r1 = null;
		Object r2 = null;
		V val = valueGenerator.next();
		try
		{
			r1 = referenceMap.put(key, val);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			r2 = testedMap.put(key, val);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(r1, r2);
	}
	
	void removeOp()
	{
		K key = keyGenerator.next();
		Object r1 = referenceMap.remove(key);
		Object r2 = testedMap.remove(key);
		checkEquality(r1, r2);
	}
	
	void clearOp()
	{
		checkEquality(referenceMap, testedMap);
		referenceMap.clear();
		testedMap.clear();
		checkEquality(referenceMap, testedMap);
	}

	void keySetOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		Set<K> set1 = null;
		Set<K> set2 = null;
		try
		{
			set1 = referenceMap.keySet();
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			set2 = testedMap.keySet();
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(set1, set2);
		if (e1 == null)
			new SetTest<K>(set1, set2, op / longop, longop > 1 ? 1 : 0, keyGenerator).run();
	}

	void entrySetOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		Set<Map.Entry<K, V>> set1 = null;
		Set<Map.Entry<K, V>> set2 = null;
		try
		{
			set1 = referenceMap.entrySet();
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			set2 = testedMap.entrySet();
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(set1, set2);
		if (e1 == null)
			new SetTest<Map.Entry<K, V>>(set1, set2, op / longop, longop > 1 ? 1 : 0, new EntryGenerator<K, V>(keyGenerator, valueGenerator)).run();
	}

	void valuesOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		Collection<V> c1 = null;
		Collection<V> c2 = null;
		try
		{
			c1 = referenceMap.values();
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			c2 = testedMap.values();
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		CollectionTest.checkCollectionsEquality(c1, c2);
		if (e1 == null)
			new CollectionTest<V>(c1, c2, op / longop, longop > 1 ? 1 : 0, valueGenerator).run();
	}

	static void checkEquality(Object o1, Object o2)
	{
		if ((o1 != null && !o1.equals(o2)) || (o1 == null && o2 != null))
		{
			throw new RuntimeException("NOT EQUAL");
		}
	}

	static class EntryGenerator<K, V> implements Generator<Map.Entry<K, V>>
	{
		final Generator<K> keyGenerator;
		final Generator<V> valueGenerator;

		public EntryGenerator(Generator<K> keyGenerator, Generator<V> valueGenerator)
		{
			this.keyGenerator = keyGenerator;
			this.valueGenerator = valueGenerator;
		}

		@Override
		public Map.Entry<K, V> next()
		{
			return new AbstractMap.SimpleImmutableEntry<K, V>(keyGenerator.next(), valueGenerator
					.next());
		}

		@Override
		public Map.Entry<K, V> invalid()
		{
			return new AbstractMap.SimpleImmutableEntry<K, V>(keyGenerator.invalid(),
					valueGenerator.invalid());
		}
	}
}
