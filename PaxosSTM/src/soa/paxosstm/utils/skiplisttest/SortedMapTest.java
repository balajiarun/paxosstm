package soa.paxosstm.utils.skiplisttest;

import java.util.HashMap;
import java.util.Map;
import java.util.SortedMap;

public class SortedMapTest<K, V> extends MapTest<K, V>
{
	final SortedMap<K, V> referenceMap;
	final SortedMap<K, V> testedMap;

	public SortedMapTest(SortedMap<K, V> referenceMap, SortedMap<K, V> testedMap, int op,
			int longop, Generator<K> keyGenerator, Generator<V> valueGenerator, boolean testViews)
	{
		super(referenceMap, testedMap, op, longop, keyGenerator, valueGenerator, testViews);
		this.referenceMap = referenceMap;
		this.testedMap = testedMap;
	}

	public void run()
	{
		checkEquality(referenceMap, testedMap);

		for (int i = 0; i < op; i++)
		{
			containsKeyOp();
			getOp();
			firstKeyOp();
			lastKeyOp();
			putOp();
			modOp();
			removeOp();
			isEmptyOp();
			if (longop > 0 && i % (op / longop) == 0)
			{
				checkEquality(referenceMap, testedMap);
				putAllOp();
				containsValueOp();
				sizeOp();
				hashCodeOp();
				equalsTrueOp();
				equalsFalseOp();
				if (testViews)
				{
					headMapOp();
					tailMapOp();
					subMapOp();
					Map<K,V> m = new HashMap<K,V>(referenceMap);
					keySetOp();
					referenceMap.putAll(m);
					testedMap.putAll(m);
					checkEquality(referenceMap, testedMap);
					valuesOp();
					referenceMap.putAll(m);
					testedMap.putAll(m);
					checkEquality(referenceMap, testedMap);
					entrySetOp();
					clearOp();
					referenceMap.putAll(m);
					testedMap.putAll(m);
					checkEquality(referenceMap, testedMap);
				}
			}
		}
		new SetTest<K>(referenceMap.keySet(), testedMap.keySet(), op, longop, keyGenerator).run();
		new SetTest<Map.Entry<K, V>>(referenceMap.entrySet(), testedMap.entrySet(), op, longop,
				new EntryGenerator<K, V>(keyGenerator, valueGenerator)).run();

		checkEquality(referenceMap, testedMap);
	}

	void firstKeyOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		Object r1 = null;
		Object r2 = null;
		try
		{
			r1 = referenceMap.firstKey();
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			r2 = testedMap.firstKey();
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(r1, r2);
	}

	void lastKeyOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		Object r1 = null;
		Object r2 = null;
		try
		{
			r1 = referenceMap.lastKey();
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			r2 = testedMap.lastKey();
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(r1, r2);
	}

	void headMapOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		SortedMap<K, V> map1 = null;
		SortedMap<K, V> map2 = null;
		K to = keyGenerator.next();
		try
		{
			map1 = referenceMap.headMap(to);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			map2 = testedMap.headMap(to);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(map1, map2);
		if (e1 == null)
			new SortedMapTest<K, V>(map1, map2, op / longop, longop > 1 ? 1 : 0, keyGenerator,
					valueGenerator, testViews).run();
	}

	void tailMapOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		SortedMap<K, V> map1 = null;
		SortedMap<K, V> map2 = null;
		K from = keyGenerator.next();
		try
		{
			map1 = referenceMap.tailMap(from);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			map2 = testedMap.tailMap(from);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(map1, map2);
		if (e1 == null)
			new SortedMapTest<K, V>(map1, map2, op / longop, longop > 1 ? 1 : 0, keyGenerator,
					valueGenerator, testViews).run();
	}

	void subMapOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		SortedMap<K, V> map1 = null;
		SortedMap<K, V> map2 = null;
		K from = keyGenerator.next();
		K to = keyGenerator.next();
		try
		{
			map1 = referenceMap.subMap(from, to);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			map2 = testedMap.subMap(from, to);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(map1, map2);
		if (e1 == null)
			new SortedMapTest<K, V>(map1, map2, op / longop, longop > 1 ? 1 : 0, keyGenerator,
					valueGenerator, testViews).run();
	}
}
