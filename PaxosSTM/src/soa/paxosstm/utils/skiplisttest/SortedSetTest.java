package soa.paxosstm.utils.skiplisttest;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedSet;

public class SortedSetTest<E> extends SetTest<E>
{
	final SortedSet<E> referenceSet;
	final SortedSet<E> testedSet;
	final boolean testViews;

	public SortedSetTest(SortedSet<E> referenceSet, SortedSet<E> testedSet, int op, int longop,
			Generator<E> generator, boolean testViews)
	{
		super(referenceSet, testedSet, op, longop, generator);
		this.referenceSet = referenceSet;
		this.testedSet = testedSet;
		this.testViews = testViews;
	}

	public void run()
	{
		checkEquality(referenceSet, testedSet);

		for (int i = 0; i < op; i++)
		{
			containsOp();
			firstOp();
			lastOp();
			addOp();
			modOp();
			removeOp();
			isEmptyOp();
			if (longop > 0 && i % (op / longop) == 0)
			{
				checkEquality(referenceSet, testedSet);
				removeAllOp();
				addAllOp();
				retainAllOp();
				addAllOp();
				sizeOp();
				hashCodeOp();
				equalsTrueOp();
				equalsFalseOp();
				iteratorOp();
				iteratorRemoveOp();
				addAllOp();
				toArrayOp();
				if (testViews)
				{
					headSetOp();
					tailSetOp();
					subSetOp();
					Set<E> s = new HashSet<E>(referenceSet);
					clearOp();
					boolean r1 = referenceSet.addAll(s);
					boolean r2 = testedSet.addAll(s);
					checkEquality(r1, r2);
					checkEquality(referenceSet, testedSet);
				}
			}
		}

		checkEquality(referenceSet, testedSet);
	}

	void firstOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		Object r1 = null;
		Object r2 = null;
		try
		{
			r1 = referenceSet.first();
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			r2 = testedSet.first();
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(r1, r2);
	}

	void lastOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		Object r1 = null;
		Object r2 = null;
		try
		{
			r1 = referenceSet.last();
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			r2 = testedSet.last();
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(r1, r2);
	}

	void headSetOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		SortedSet<E> set1 = null;
		SortedSet<E> set2 = null;
		E to = generator.next();
		try
		{
			set1 = referenceSet.headSet(to);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			set2 = testedSet.headSet(to);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(set1, set2);
		if (e1 == null)
			new SortedSetTest<E>(set1, set2, op / longop, longop > 1 ? 1 : 0, generator, testViews)
					.run();
	}

	void tailSetOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		SortedSet<E> set1 = null;
		SortedSet<E> set2 = null;
		E from = generator.next();
		try
		{
			set1 = referenceSet.tailSet(from);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			set2 = testedSet.tailSet(from);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(set1, set2);
		if (e1 == null)
			new SortedSetTest<E>(set1, set2, op / longop, longop > 1 ? 1 : 0, generator, testViews).run();
	}

	void subSetOp()
	{
		Exception e1 = null;
		Exception e2 = null;
		SortedSet<E> set1 = null;
		SortedSet<E> set2 = null;
		E from = generator.next();
		E to = generator.next();
		try
		{
			set1 = referenceSet.subSet(from, to);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			set2 = testedSet.subSet(from, to);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(set1, set2);
		if (e1 == null)
			new SortedSetTest<E>(set1, set2, op / longop, longop > 1 ? 1 : 0, generator, testViews).run();
	}
}
