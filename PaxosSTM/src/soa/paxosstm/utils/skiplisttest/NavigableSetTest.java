package soa.paxosstm.utils.skiplisttest;

import java.util.HashSet;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.Set;

public class NavigableSetTest<E> extends SortedSetTest<E>
{
	final NavigableSet<E> referenceSet;
	final NavigableSet<E> testedSet;
	final boolean testDescending;

	public NavigableSetTest(NavigableSet<E> referenceSet, NavigableSet<E> testedSet, int op,
			int longop, Generator<E> generator, boolean testViews, boolean testDescending)
	{
		super(referenceSet, testedSet, op, longop, generator, testViews);
		this.referenceSet = referenceSet;
		this.testedSet = testedSet;
		this.testDescending = testDescending;
	}

	public void run()
	{
		checkEquality(referenceSet, testedSet);

		for (int i = 0; i < op; i++)
		{
			containsOp();
			firstOp();
			lastOp();
			addOp();
			modOp();
			removeOp();
			pollFirstOp();
			pollLastOp();
			addOp();
			addOp();
			addOp();
			addOp();
			isEmptyOp();
			lowerOp();
			ceilingOp();
			floorOp();
			higherOp();
			if (longop > 0 && i % (op / longop) == 0)
			{
				checkEquality(referenceSet, testedSet);
				removeAllOp();
				addAllOp();
				retainAllOp();
				addAllOp();
				sizeOp();
				hashCodeOp();
				equalsTrueOp();
				equalsFalseOp();
				iteratorOp();
				iteratorRemoveOp();
				addAllOp();
				descendingIteratorOp();
				descendingIteratorRemoveOp();
				addAllOp();
				toArrayOp();
				if (testViews)
				{
					headSetOp((i / (op / longop)) % 2 == 0);
					tailSetOp((i / (op / longop)) % 2 == 1);
					subSetOp((i / (op / longop)) % 4 < 2, (i / (op / longop)) % 2 == 1);
					Set<E> s = new HashSet<E>(referenceSet);
					clearOp();
					boolean r1 = referenceSet.addAll(s);
					boolean r2 = testedSet.addAll(s);
					checkEquality(r1, r2);
					checkEquality(referenceSet, testedSet);
				}
			}
		}
		if (testDescending)
			new NavigableSetTest<E>(referenceSet.descendingSet(), testedSet.descendingSet(), op,
					longop, generator, false, false).run();

		checkEquality(referenceSet, testedSet);
	}

	void ceilingOp()
	{
		E key = generator.next();
		Object r1 = referenceSet.ceiling(key);
		Object r2 = testedSet.ceiling(key);
		checkEquality(r1, r2);
	}

	void floorOp()
	{
		E key = generator.next();
		Object r1 = referenceSet.floor(key);
		Object r2 = testedSet.floor(key);
		checkEquality(r1, r2);
	}

	void lowerOp()
	{
		E key = generator.next();
		Object r1 = referenceSet.lower(key);
		Object r2 = testedSet.lower(key);
		checkEquality(r1, r2);
	}

	void higherOp()
	{
		E key = generator.next();
		Object r1 = referenceSet.higher(key);
		Object r2 = testedSet.higher(key);
		checkEquality(r1, r2);
	}

	void pollFirstOp()
	{
		Object r1 = referenceSet.pollFirst();
		Object r2 = testedSet.pollFirst();
		checkEquality(r1, r2);

	}

	void pollLastOp()
	{
		Object r1 = referenceSet.pollLast();
		Object r2 = testedSet.pollLast();
		checkEquality(r1, r2);

	}

	void headSetOp(boolean inclusive)
	{
		Exception e1 = null;
		Exception e2 = null;
		NavigableSet<E> set1 = null;
		NavigableSet<E> set2 = null;
		E to = generator.next();
		try
		{
			set1 = referenceSet.headSet(to, inclusive);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			set2 = testedSet.headSet(to, inclusive);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(set1, set2);
		if (e1 == null)
			new NavigableSetTest<E>(set1, set2, op / longop, longop > 1 ? 1 : 0, generator, testViews,
					testDescending).run();
	}

	void tailSetOp(boolean inclusive)
	{
		Exception e1 = null;
		Exception e2 = null;
		NavigableSet<E> set1 = null;
		NavigableSet<E> set2 = null;
		E from = generator.next();
		try
		{
			set1 = referenceSet.tailSet(from, inclusive);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			set2 = testedSet.tailSet(from, inclusive);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(set1, set2);
		if (e1 == null)
			new NavigableSetTest<E>(set1, set2, op / longop, longop > 1 ? 1 : 0, generator, testViews,
					testDescending).run();
	}

	void subSetOp(boolean fromInclusive, boolean toInclusive)
	{
		Exception e1 = null;
		Exception e2 = null;
		NavigableSet<E> set1 = null;
		NavigableSet<E> set2 = null;
		E from = generator.next();
		E to = generator.next();
		try
		{
			set1 = referenceSet.subSet(from, fromInclusive, to, toInclusive);
		}
		catch (Exception e)
		{
			e1 = e;
		}
		try
		{
			set2 = testedSet.subSet(from, fromInclusive, to, toInclusive);
		}
		catch (Exception e)
		{
			e2 = e;
		}
		if (e1 != e2)
		{
			checkEquality(e1 != null && e2 != null, true);
			checkEquality(e1.getClass(), e2.getClass());
		}
		checkEquality(set1, set2);
		if (e1 == null)
			new NavigableSetTest<E>(set1, set2, op / longop, longop > 1 ? 1 : 0, generator, testViews,
					testDescending).run();
	}

	void descendingIteratorOp()
	{
		Iterator<E> it1 = referenceSet.descendingIterator();
		Iterator<E> it2 = testedSet.descendingIterator();
		while (true)
		{
			checkEquality(it1.hasNext(), it2.hasNext());
			if (!it1.hasNext())
				break;
			E e1 = it1.next();
			E e2 = it2.next();
			checkEquality(e1, e2);
		}
	}

	void descendingIteratorRemoveOp()
	{
		HashSet<E> m = new HashSet<E>();
		for (int i = 0; i < op / longop; i++)
			m.add(generator.next());

		Iterator<E> it1 = referenceSet.descendingIterator();
		Iterator<E> it2 = testedSet.descendingIterator();
		while (true)
		{
			checkEquality(it1.hasNext(), it2.hasNext());
			if (!it1.hasNext())
				break;
			E e1 = it1.next();
			E e2 = it2.next();
			checkEquality(e1, e2);

			if (m.contains(e1))
			{
				it1.remove();
				it2.remove();
			}
		}
	}
}
