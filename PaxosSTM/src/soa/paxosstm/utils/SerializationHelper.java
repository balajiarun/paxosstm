package soa.paxosstm.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SerializationHelper
{
	public static Object byteArrayToObject(byte[] bytes) {
	    try {
	        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
	        ObjectInputStream ois;
	        ois = new ObjectInputStream(bis);
	        return ois.readObject();
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}

	public static byte[] byteArrayFromObject(Object object) {
	    try {
	        ByteArrayOutputStream bos = new ByteArrayOutputStream();
	        new ObjectOutputStream(bos).writeObject(object);
	        return bos.toByteArray();
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}
