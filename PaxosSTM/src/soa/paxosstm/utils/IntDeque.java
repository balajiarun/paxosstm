package soa.paxosstm.utils;

import java.util.NoSuchElementException;

public final class IntDeque
{
	private static final int INITIAL_CAPACITY = 16;

	private int[] table;
	private int head, size;

	public IntDeque()
	{
		this(INITIAL_CAPACITY);
	}

	public IntDeque(int capacity)
	{
		table = new int[INITIAL_CAPACITY];
		head = size = 0;
	}

	public int size()
	{
		return size;
	}

	public boolean isEmpty()
	{
		return size == 0;
	}

	public void addLast(int v)
	{
		if (size + 1 == table.length)
			doubleCapacity();
		
		table[head + size++] = v;
	}

	public int removeFirst()
	{
		if (size <= 0)
			throw new NoSuchElementException();
		int ret = table[head++];
		head &= table.length-1;
		size--;
		return ret;
	}
	
	public void undoHeadRemoves(int n)
	{
		size += n;
		head -= n;
		head &= table.length - 1;
	}
	
	public void clear()
	{
		head = size = 0;
	}

	private void doubleCapacity()
	{
		int newCapacity = size << 1;
		if (newCapacity < 0)
			throw new IllegalStateException("Deque too big");
		
		int[] newTable = new int[newCapacity];
		System.arraycopy(table, head, newTable, 0, size - head);
		System.arraycopy(table, 0, newTable, size - head, head);
		
		table = newTable;
		head = 0;
	}
}
