package soa.paxosstm.utils;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalInteger
{
       private int value;

       public TransactionalInteger()
       {
       }

       public TransactionalInteger(int value)
       {
               this.value = value;
       }

       public void setValue(int value)
       {
               this.value = value;
       }

       public int getValue()
       {
               return value;
       }

       public int increment()
       {
               return ++value;
       }

       public int decrement()
       {
               return --value;
       }
}
