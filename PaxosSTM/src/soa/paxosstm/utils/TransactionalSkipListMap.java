package soa.paxosstm.utils;

import java.io.Serializable;
import java.util.AbstractCollection;
import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalSkipListMap<K, V> implements NavigableMap<K, V>, Cloneable
{
	// TOC:
	// initialization & constructors
	// Object overrides
	// comparison utilities
	// nodes & indexes
	// traversal
	// insertion
	// deletion
	// Map API methods
	// SortedMap API methods
	// NavigableMap API methods
	// view methods
	// iterators
	// view classes
	// SubMap
	// Object overrides
	// utilities
	// Map API methods
	// SortedMap API methods
	// NavigableMap API methods
	// view methods
	// iterators

	/* ---------------- initialization & constructors ---------------- */

	private final Comparator<? super K> comparator;

	private Node<K, V> headNode;

	private TransactionalReference<Index<K, V>> topIndex;

	private TransactionalInteger maxLevel;

	private void initialize()
	{
		headNode = new Node<K, V>(null, null, null, new Random().nextInt() | 0x0100);
		topIndex = new TransactionalReference<Index<K, V>>();
		maxLevel = new TransactionalInteger(1);
	}

	/**
	 * Constructs a new, empty map, sorted according to the
	 * {@linkplain Comparable natural ordering} of the keys.
	 */
	public TransactionalSkipListMap()
	{
		this.comparator = null;
		initialize();
	}

	/**
	 * Constructs a new, empty map, sorted according to the specified
	 * comparator.
	 * 
	 * @param comparator
	 *            the comparator that will be used to order this map. If
	 *            <tt>null</tt>, the {@linkplain Comparable natural ordering} of
	 *            the keys will be used.
	 */
	public TransactionalSkipListMap(Comparator<? super K> comparator)
	{
		this.comparator = comparator;
		initialize();
	}

	/**
	 * Constructs a new map containing the same mappings as the given map,
	 * sorted according to the {@linkplain Comparable natural ordering} of the
	 * keys.
	 * 
	 * @param m
	 *            the map whose mappings are to be placed in this map
	 * @throws ClassCastException
	 *             if the keys in <tt>m</tt> are not {@link Comparable}, or are
	 *             not mutually comparable
	 * @throws NullPointerException
	 *             if the specified map or any of its keys or values are null
	 */
	public TransactionalSkipListMap(Map<? extends K, ? extends V> m)
	{
		this.comparator = null;
		initialize();
		putAll(m);
	}

	/**
	 * Constructs a new map containing the same mappings and using the same
	 * ordering as the specified sorted map.
	 * 
	 * @param m
	 *            the sorted map whose mappings are to be placed in this map,
	 *            and whose comparator is to be used to sort this map
	 * @throws NullPointerException
	 *             if the specified sorted map or any of its keys or values are
	 *             null
	 */
	public TransactionalSkipListMap(SortedMap<K, ? extends V> m)
	{
		this.comparator = m.comparator();
		initialize();
		buildFromSorted(m);
	}

	/* ---------------- Object overrides --------------- */

	/**
	 * Returns the hash code value for this map. The hash code of a map is
	 * defined to be the sum of the hash codes of each entry in the map's
	 * <tt>entrySet()</tt> view. This ensures that <tt>m1.equals(m2)</tt>
	 * implies that <tt>m1.hashCode()==m2.hashCode()</tt> for any two maps
	 * <tt>m1</tt> and <tt>m2</tt>, as required by the general contract of
	 * {@link Object#hashCode}.
	 * 
	 * <p>
	 * This implementation iterates over <tt>entrySet()</tt>, calling
	 * {@link Map.Entry#hashCode hashCode()} on each element (entry) in the set,
	 * and adding up the results.
	 * 
	 * @return the hash code value for this map
	 * @see Map.Entry#hashCode()
	 * @see Object#equals(Object)
	 * @see Set#equals(Object)
	 */
	@Override
	public int hashCode()
	{
		int h = 0;
		Iterator<Entry<K, V>> i = entrySet().iterator();
		while (i.hasNext())
			h += i.next().hashCode();
		return h;
	}

	/**
	 * Returns a string representation of this map. The string representation
	 * consists of a list of key-value mappings in the order returned by the
	 * map's <tt>entrySet</tt> view's iterator, enclosed in braces (
	 * <tt>"{}"</tt>). Adjacent mappings are separated by the characters
	 * <tt>", "</tt> (comma and space). Each key-value mapping is rendered as
	 * the key followed by an equals sign (<tt>"="</tt>) followed by the
	 * associated value. Keys and values are converted to strings as by
	 * {@link String#valueOf(Object)}.
	 * 
	 * @return a string representation of this map
	 */
	@Override
	public String toString()
	{
		Iterator<Entry<K, V>> i = entrySet().iterator();
		if (!i.hasNext())
			return "{}";

		StringBuilder sb = new StringBuilder();
		sb.append('{');
		for (;;)
		{
			Entry<K, V> e = i.next();
			K key = e.getKey();
			V value = e.getValue();
			sb.append(key == this ? "(this Map)" : key);
			sb.append('=');
			sb.append(value == this ? "(this Map)" : value);
			if (!i.hasNext())
				return sb.append('}').toString();
			sb.append(',').append(' ');
		}
	}

	/**
	 * Returns a shallow copy of this <tt>ConcurrentSkipListMap</tt> instance.
	 * (The keys and values themselves are not cloned.)
	 * 
	 * @return a shallow copy of this map
	 */
	@Override
	public TransactionalSkipListMap<K, V> clone()
	{
		try
		{
			@SuppressWarnings("unchecked")
			TransactionalSkipListMap<K, V> clone = (TransactionalSkipListMap<K, V>) super.clone();

			clone.initialize();
			clone.buildFromSorted(this);
			return clone;
		}
		catch (CloneNotSupportedException e)
		{
			throw new InternalError();
		}
	}

	@Override
	public boolean equals(Object o)
	{
		if (o == this)
			return true;
		if (!(o instanceof Map))
			return false;
		Map<?, ?> m = (Map<?, ?>) o;
		try
		{
			for (Map.Entry<K, V> e : this.entrySet())
			{
				if (!e.getValue().equals(m.get(e.getKey())))
					return false;
			}
			for (Map.Entry<?, ?> e : m.entrySet())
			{
				Object k = e.getKey();
				Object v = e.getValue();
				if (k == null || v == null || !v.equals(get(k)))
					return false;
			}
			return true;
		}
		catch (ClassCastException unused)
		{
			return false;
		}
		catch (NullPointerException unused)
		{
			return false;
		}
	}

	/* ---------------- comparison utilities ---------------- */

	static final class ComparableUsingComparator<K> implements Comparable<K>
	{
		final K actualKey;
		final Comparator<? super K> cmp;

		ComparableUsingComparator(K key, Comparator<? super K> cmp)
		{
			this.actualKey = key;
			this.cmp = cmp;
		}

		public int compareTo(K k2)
		{
			return cmp.compare(actualKey, k2);
		}
	}

	/**
	 * If using comparator, return a ComparableUsingComparator, else cast key as
	 * Comparable, which may cause ClassCastException, which is propagated back
	 * to caller.
	 */
	private Comparable<? super K> comparable(Object key) throws ClassCastException
	{
		if (key == null)
			throw new NullPointerException();

		if (comparator != null)
		{
			@SuppressWarnings("unchecked")
			K kkey = (K) key;
			return new ComparableUsingComparator<K>(kkey, comparator);
		}
		else
		{
			@SuppressWarnings("unchecked")
			Comparable<? super K> compKey = (Comparable<? super K>) key;
			return compKey;
		}
	}

	/**
	 * Compares using comparator or natural ordering. Used when the
	 * ComparableUsingComparator approach doesn't apply.
	 */
	private int compare(K k1, K k2) throws ClassCastException
	{
		Comparator<? super K> cmp = comparator;
		if (cmp != null)
			return cmp.compare(k1, k2);
		else
		{
			@SuppressWarnings("unchecked")
			Comparable<? super K> compKey = (Comparable<? super K>) k1;
			return compKey.compareTo(k2);
		}
	}

	/* ---------------- nodes & indexes ---------------- */

	@TransactionObject
	static final class Node<K, V> implements Map.Entry<K, V>
	{
		final K key;
		V value;
		Node<K, V> next;
		int seed;

		Node(K key, V value, Node<K, V> next, int seed)
		{
			this.key = key;
			this.value = value;
			this.next = next;
			this.seed = seed;
		}

		/**
		 * This uses the simplest of the generators described in George
		 * Marsaglia's "Xorshift RNGs" paper. This is not a high-quality
		 * generator but is acceptable here.
		 */
		int random()
		{
			seed ^= seed << 13;
			seed ^= seed >>> 17;
			seed ^= seed << 5;
			return seed;
		}

		@Override
		public K getKey()
		{
			return key;
		}

		@Override
		public V getValue()
		{
			return value;
		}

		@Override
		public V setValue(V value)
		{
			if (value == null)
				throw new NullPointerException();

			V oldValue = this.value;
			this.value = value;
			return oldValue;
		}

		@Override
		public boolean equals(Object o)
		{
			if (!(o instanceof Map.Entry))
				return false;
			Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
			Object k1 = key;
			Object k2 = e.getKey();
			if (k1.equals(k2))
			{
				Object v1 = value;
				Object v2 = e.getValue();
				if (v1 == v2 || (v1 != null && v1.equals(v2)))
					return true;
			}
			return false;
		}

		@Override
		public int hashCode()
		{
			return (key == null ? 0 : key.hashCode()) ^ (value == null ? 0 : value.hashCode());
		}
	}

	@TransactionObject
	static final class Index<K, V>
	{
		final Node<K, V> node;
		final Index<K, V> down;
		Index<K, V> right;
		K rightKey;

		Index(Node<K, V> node, Index<K, V> down, Index<K, V> right, K rightKey)
		{
			this.node = node;
			this.down = down;
			this.right = right;
			this.rightKey = rightKey;
		}
	}

	/* ---------------- traversing ---------------- */

	private Node<K, V> findPredecessor(Comparable<? super K> key)
	{
		Node<K, V> node = headNode;
		Index<K, V> idx = topIndex.get();
		while (idx != null)
		{
			while (idx.right != null && key.compareTo(idx.rightKey) > 0)
			{
				idx = idx.right;
			}
			node = idx.node;
			idx = idx.down;
		}
		while (node.next != null && key.compareTo(node.next.key) > 0)
		{
			node = node.next;
		}
		return node;
	}

	private Node<K, V> findNode(Comparable<? super K> key)
	{
		Node<K, V> node = ceilingNode(key);
		if (node == null || (key.compareTo(node.key) != 0))
			return null;
		return node;
	}

	private Node<K, V> findFirstNode()
	{
		return headNode.next;
	}

	private Node<K, V> findLastNode()
	{
		Node<K, V> node = headNode;
		Index<K, V> idx = topIndex.get();
		while (idx != null)
		{
			while (idx.right != null)
			{
				idx = idx.right;
			}
			node = idx.node;
			idx = idx.down;
		}
		while (node.next != null)
		{
			node = node.next;
		}
		if (node == headNode)
			return null;
		return node;
	}

	private Node<K, V> ceilingNode(Comparable<? super K> key)
	{
		return findPredecessor(key).next;
	}

	private Node<K, V> lowerNode(Comparable<? super K> key)
	{
		Node<K, V> node = findPredecessor(key);
		if (node == headNode)
			return null;
		return node;
	}

	private Node<K, V> floorNode(Comparable<? super K> key)
	{
		Node<K, V> node = findPredecessor(key);
		if (node.next != null && key.compareTo(node.next.key) == 0)
			return node.next;
		if (node == headNode)
			return null;
		return node;
	}

	private Node<K, V> higherNode(Comparable<? super K> key)
	{
		Node<K, V> node = findPredecessor(key);
		if (node.next != null)
		{
			if (key.compareTo(node.next.key) < 0)
				return node.next;
			return node.next.next;
		}
		return null;
	}

	/* ---------------- insertion ---------------- */

	private V doPut(K kkey, V value)
	{
		Comparable<? super K> key = comparable(kkey);

		Node<K, V> before = findPredecessor(key);
		Node<K, V> after = before.next;

		if (after != null && key.compareTo(after.key) == 0)
		{
			V oldValue = after.value;
			after.value = value;
			return oldValue;
		}

		int level = insertNode(before, kkey, value);
		if (level > 1)
			insertIndex(before.next, key, level);
		return null;
	}

	private int insertNode(Node<K, V> before, K key, V value)
	{
		int firstSeed = before.seed;
		int secondSeed = before.random();

		int level = getNewLevel(secondSeed);
		before.next = new Node<K, V>(key, value, before.next, (firstSeed ^ secondSeed));
		return level;
	}

	private void insertIndex(Node<K, V> node, Comparable<? super K> key, int level)
	{
		Index<K, V> newIdx = null;
		for (int i = 0; i < level - 1; i++)
		{
			newIdx = new Index<K, V>(node, newIdx, null, null);
		}

		int currLevel = maxLevel.getValue();

		Index<K, V> idx = topIndex.get();
		while (idx != null)
		{
			while (idx.right != null && key.compareTo(idx.rightKey) > 0)
			{
				idx = idx.right;
			}
			if (currLevel <= level)
			{
				newIdx.right = idx.right;
				newIdx.rightKey = idx.rightKey;
				idx.right = newIdx;
				idx.rightKey = node.key;
				newIdx = newIdx.down;
			}
			idx = idx.down;
			--currLevel;
		}
	}

	/**
	 * Streamlined bulk insertion to initialize from elements of given sorted
	 * map. Call only from constructor or clone method.
	 */
	private void buildFromSorted(SortedMap<K, ? extends V> map)
	{
		if (map == null)
			throw new NullPointerException();

		buildFromSorted(map.entrySet().iterator());
	}

	final void buildFromSorted(Iterator<? extends Map.Entry<? extends K, ? extends V>> it)
	{
		ArrayList<Index<K, V>> preds = new ArrayList<Index<K, V>>(maxLevel.getValue() - 1);
		Index<K, V> idx = topIndex.get();
		for (int i = preds.size() - 1; i >= 0; --i)
		{
			preds.set(i, idx);
			idx = idx.down;
		}

		Node<K, V> lastNode = headNode;

		while (it.hasNext())
		{
			Map.Entry<? extends K, ? extends V> e = it.next();

			if (e.getValue() == null)
				throw new NullPointerException();

			int level = insertNode(lastNode, e.getKey(), e.getValue());
			lastNode = lastNode.next;
			while (level - 1 > preds.size())
				preds.add(null);

			Index<K, V> newIdx = null;
			for (int i = 0; i < level - 1; i++)
			{
				newIdx = new Index<K, V>(lastNode, newIdx, null, null);
				idx = preds.get(i);
				if (idx != null)
				{
					idx.right = newIdx;
					idx.rightKey = lastNode.key;
				}
				preds.set(i, newIdx);
			}
		}
	}

	private int getNewLevel(int seed)
	{
		int level = randomLevel(seed);
		if (level > maxLevel.getValue())
		{
			level = maxLevel.increment();
			Index<K, V> idx = new Index<K, V>(headNode, topIndex.get(), null, null);
			topIndex.set(idx);
		}
		return level;
	}

	/**
	 * Returns a random level for inserting a new node. Hardwired to k=0,
	 * p=0.25, max 16.
	 */
	private int randomLevel(int x)
	{
		int level = 1;
		while ((x & 0x11) == 0x11)
		{
			++level;
			x >>>= 2;
		}
		return level;
	}

	/* ---------------- deletion ---------------- */

	private V doRemove(Comparable<? super K> key)
	{
		Node<K, V> before = findPredecessor(key);
		Node<K, V> node = before.next;

		if (node == null || key.compareTo(node.key) != 0)
			return null;

		before.next = node.next;
		removeIndex(key, node);

		return node.value;
	}

	private void removeIndex(Comparable<? super K> key, Node<K, V> node)
	{
		Index<K, V> idx = topIndex.get();
		while (idx != null)
		{
			while (idx.right != null && key.compareTo(idx.rightKey) > 0)
			{
				idx = idx.right;
			}
			Index<K, V> right = idx.right;
			if (right != null && right.node == node)
			{
				idx.right = right.right;
				idx.rightKey = right.rightKey;
			}
			idx = idx.down;
		}
		trimMaxLevel();
	}

	private void removeNode(Node<K, V> node)
	{
		Comparable<? super K> key = comparable(node.key);
		Node<K, V> pred = headNode;

		Index<K, V> idx = topIndex.get();
		while (idx != null)
		{
			while (idx.right != null && key.compareTo(idx.rightKey) > 0)
			{
				idx = idx.right;
			}
			Index<K, V> right = idx.right;
			if (right != null && right.node == node)
			{
				idx.right = right.right;
				idx.rightKey = right.rightKey;
			}
			pred = idx.node;
			idx = idx.down;
		}
		while (pred.next != node)
			pred = pred.next;
		pred.next = node.next;
		trimMaxLevel();
	}

	private Node<K, V> removeFirstNode()
	{
		Node<K, V> node = headNode.next;
		if (node == null)
			return null;

		headNode.next = node.next;

		Index<K, V> idx = topIndex.get();
		while (idx != null)
		{
			Index<K, V> right = idx.right;
			if (right != null && right.node == node)
			{
				idx.right = right.right;
				idx.rightKey = right.rightKey;
			}
			idx = idx.down;
		}

		trimMaxLevel();
		return node;
	}

	private Node<K, V> removeLastNode()
	{
		Node<K, V> node = headNode;
		Index<K, V> idx = topIndex.get();
		while (idx != null)
		{
			while (idx.right != null)
			{
				if (idx.right.right != null)
				{
					idx = idx.right;
				}
				else if (idx.right.node.next != null)
				{
					idx = idx.right;
				}
				else
				{
					idx.right = null;
					break;
				}
			}
			node = idx.node;
			idx = idx.down;
		}
		while (node.next != null && node.next.next != null)
		{
			node = node.next;
		}

		Node<K, V> last = node.next;
		if (last == null)
			return null;

		node.next = null;

		trimMaxLevel();
		return last;
	}

	private void trimMaxLevel()
	{
		Index<K, V> idx = topIndex.get();
		while (idx != null && idx.right == null)
		{
			topIndex.set(idx = idx.down);
			maxLevel.decrement();
		}
	}

	/* ---------------- Map API methods ---------------- */

	@Override
	public boolean containsKey(Object key)
	{
		return findNode(comparable(key)) != null;
	}

	@Override
	public V get(Object key)
	{
		Node<K, V> node = findNode(comparable(key));
		if (node == null)
			return null;
		return node.value;
	}

	@Override
	public V put(K key, V value)
	{
		if (value == null)
			throw new NullPointerException();

		return doPut(key, value);
	}

	@Override
	public void putAll(Map<? extends K, ? extends V> m)
	{
		for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
			put(e.getKey(), e.getValue());
	}

	@Override
	public V remove(Object key)
	{
		return doRemove(comparable(key));
	}

	@Override
	public boolean containsValue(Object value)
	{
		if (value == null)
			throw new NullPointerException();
		for (Node<K, V> n = headNode.next; n != null; n = n.next)
			if (value.equals(n.value))
				return true;
		return false;
	}

	@Override
	public int size()
	{
		long count = 0;
		for (Node<K, V> n = headNode.next; n != null; n = n.next)
			count++;
		return (count >= Integer.MAX_VALUE) ? Integer.MAX_VALUE : (int) count;
	}

	@Override
	public boolean isEmpty()
	{
		return headNode.next == null;
	}

	@Override
	public void clear()
	{
		initialize();
	}

	/* ---------------- SortedMap API methods ---------------- */

	@Override
	public Comparator<? super K> comparator()
	{
		return comparator;
	}

	@Override
	public K firstKey()
	{
		Node<K, V> node = findFirstNode();
		if (node == null)
			throw new NoSuchElementException();
		return node.key;
	}

	@Override
	public K lastKey()
	{
		Node<K, V> node = findLastNode();
		if (node == null)
			throw new NoSuchElementException();
		return node.key;
	}

	/* ---------------- NavigableMap API methods ---------------- */

	@Override
	public K lowerKey(K kkey)
	{
		Comparable<? super K> key = comparable(kkey);
		Node<K, V> node = lowerNode(key);
		if (node == null)
			return null;
		return node.key;
	}

	@Override
	public Map.Entry<K, V> lowerEntry(K kkey)
	{
		Comparable<? super K> key = comparable(kkey);
		return lowerNode(key);
	}

	@Override
	public K higherKey(K kkey)
	{
		Comparable<? super K> key = comparable(kkey);
		Node<K, V> node = higherNode(key);
		return node == null ? null : node.key;
	}

	@Override
	public Map.Entry<K, V> higherEntry(K kkey)
	{
		Comparable<? super K> key = comparable(kkey);
		return higherNode(key);
	}

	@Override
	public K floorKey(K kkey)
	{
		Comparable<? super K> key = comparable(kkey);
		Node<K, V> node = floorNode(key);
		return node == null ? null : node.key;
	}

	@Override
	public Map.Entry<K, V> floorEntry(K kkey)
	{
		Comparable<? super K> key = comparable(kkey);
		return floorNode(key);
	}

	@Override
	public K ceilingKey(K kkey)
	{
		Comparable<? super K> key = comparable(kkey);
		Node<K, V> node = ceilingNode(key);
		if (node == null)
			return null;
		return node.key;
	}

	@Override
	public Map.Entry<K, V> ceilingEntry(K kkey)
	{
		Comparable<? super K> key = comparable(kkey);
		return ceilingNode(key);
	}

	@Override
	public Map.Entry<K, V> firstEntry()
	{
		return findFirstNode();
	}

	@Override
	public Map.Entry<K, V> lastEntry()
	{
		return findLastNode();
	}

	@Override
	public Map.Entry<K, V> pollFirstEntry()
	{
		return removeFirstNode();
	}

	@Override
	public Map.Entry<K, V> pollLastEntry()
	{
		return removeLastNode();
	}

	/* ---------------- view methods ---------------- */

	@Override
	public Set<K> keySet()
	{
		return new KeySet<K>(this);
	}

	@Override
	public Collection<V> values()
	{
		return new Values<V>(this);
	}

	@Override
	public Set<Map.Entry<K, V>> entrySet()
	{
		return new EntrySet<K, V>(this);
	}

	@Override
	public SortedMap<K, V> headMap(K toKey)
	{
		return headMap(toKey, false);
	}

	@Override
	public NavigableMap<K, V> headMap(K toKey, boolean inclusive)
	{
		if (toKey == null)
			throw new NullPointerException();

		return new SubMap<K, V>(this, null, false, toKey, inclusive, false);
	}

	@Override
	public SortedMap<K, V> subMap(K fromKey, K toKey)
	{
		return subMap(fromKey, true, toKey, false);
	}

	@Override
	public NavigableMap<K, V> subMap(K fromKey, boolean fromInclusive, K toKey, boolean toInclusive)
	{
		if (fromKey == null || toKey == null)
			throw new NullPointerException();

		return new SubMap<K, V>(this, fromKey, fromInclusive, toKey, toInclusive, false);
	}

	@Override
	public SortedMap<K, V> tailMap(K fromKey)
	{
		return tailMap(fromKey, true);
	}

	@Override
	public NavigableMap<K, V> tailMap(K fromKey, boolean inclusive)
	{
		if (fromKey == null)
			throw new NullPointerException();

		return new SubMap<K, V>(this, fromKey, inclusive, null, false, false);
	}

	@Override
	public NavigableSet<K> navigableKeySet()
	{
		return new KeySet<K>(this);
	}

	@Override
	public NavigableSet<K> descendingKeySet()
	{
		return descendingMap().navigableKeySet();
	}

	@Override
	public NavigableMap<K, V> descendingMap()
	{
		return new SubMap<K, V>(this, null, false, null, false, true);
	}

	/* ---------------- iterators ---------------- */

	/**
	 * Base of iterator classes:
	 */
	abstract class Iter<T> implements Iterator<T>
	{
		Node<K, V> lastReturned;
		Node<K, V> next;

		Iter()
		{
			next = headNode.next;
		}

		@Override
		public final boolean hasNext()
		{
			return next != null;
		}

		final void advance()
		{
			if (next == null)
				throw new NoSuchElementException();
			lastReturned = next;
			next = next.next;
		}

		@Override
		public void remove()
		{
			if (lastReturned == null)
				throw new IllegalStateException();
			removeNode(lastReturned);
			lastReturned = null;
		}
	}

	final class KeyIterator extends Iter<K>
	{
		@Override
		public K next()
		{
			advance();
			return lastReturned.key;
		}
	}

	final class ValueIterator extends Iter<V>
	{
		@Override
		public V next()
		{
			advance();
			return lastReturned.value;
		}
	}

	final class EntryIterator extends Iter<Map.Entry<K, V>>
	{
		@Override
		public Map.Entry<K, V> next()
		{
			advance();
			return lastReturned;
		}
	}

	// Factory methods for iterators needed by TransactionalSkipListSet etc

	final Iterator<K> keyIterator()
	{
		return new KeyIterator();
	}

	final Iterator<V> valueIterator()
	{
		return new ValueIterator();
	}

	final Iterator<Map.Entry<K, V>> entryIterator()
	{
		return new EntryIterator();
	}

	/* ---------------- view classes ---------------- */

	static <E> List<E> toList(Collection<E> c)
	{
		List<E> list = new ArrayList<E>();
		for (E e : c)
			list.add(e);
		return list;
	}

	static final class KeySet<E> extends AbstractSet<E> implements NavigableSet<E>, Cloneable,
			Serializable
	{
		private static final long serialVersionUID = 840501476671410627L;

		private final NavigableMap<E, ?> m;
		private final Object valueForAdd;

		KeySet(NavigableMap<E, ?> map)
		{
			m = map;
			valueForAdd = null;
		}

		KeySet(NavigableMap<E, ?> map, Object value)
		{
			m = map;
			valueForAdd = value;
		}

		@Override
		public Iterator<E> iterator()
		{
			if (m instanceof TransactionalSkipListMap)
				return ((TransactionalSkipListMap<E, ?>) m).keyIterator();
			else
				return ((SubMap<E, ?>) m).keyIterator();
		}

		@Override
		public int size()
		{
			return m.size();
		}

		@Override
		public boolean isEmpty()
		{
			return m.isEmpty();
		}

		@Override
		public boolean contains(Object o)
		{
			return m.containsKey(o);
		}

		@Override
		public boolean add(E e)
		{
			if (valueForAdd == null)
				throw new UnsupportedOperationException();

			@SuppressWarnings("unchecked")
			NavigableMap<E, Object> map = (NavigableMap<E, Object>) m;
			
			return map.put(e, valueForAdd) == null;
		}

		@Override
		public boolean remove(Object o)
		{
			return m.remove(o) != null;
		}

		@Override
		public boolean removeAll(Collection<?> c)
		{
			boolean modified = false;
			for (Iterator<?> i = c.iterator(); i.hasNext();)
				modified |= remove(i.next());
			return modified;
		}

		@Override
		public boolean retainAll(Collection<?> c)
		{
			boolean modified = false;
			if (!(c instanceof Set<?>))
				c = new HashSet<Object>(c);
			for (Iterator<?> i = iterator(); i.hasNext();)
			{
				Object o = i.next();
				if (!c.contains(o))
				{
					i.remove();
					modified = true;
				}
			}
			return modified;
		}

		@Override
		public void clear()
		{
			m.clear();
		}

		@Override
		public Comparator<? super E> comparator()
		{
			return m.comparator();
		}

		@Override
		public E lower(E e)
		{
			return m.lowerKey(e);
		}

		@Override
		public E higher(E e)
		{
			return m.higherKey(e);
		}

		@Override
		public E floor(E e)
		{
			return m.floorKey(e);
		}

		@Override
		public E ceiling(E e)
		{
			return m.ceilingKey(e);
		}

		@Override
		public E first()
		{
			return m.firstKey();
		}

		@Override
		public E last()
		{
			return m.lastKey();
		}

		@Override
		public E pollFirst()
		{
			Map.Entry<E, ?> e = m.pollFirstEntry();
			return (e == null) ? null : e.getKey();
		}

		@Override
		public E pollLast()
		{
			Map.Entry<E, ?> e = m.pollLastEntry();
			return (e == null) ? null : e.getKey();
		}

		@Override
		public boolean equals(Object o)
		{
			if (o == this)
				return true;
			if (!(o instanceof Set))
				return false;
			Collection<?> c = (Collection<?>) o;
			try
			{
				return containsAll(c) && c.containsAll(this);
			}
			catch (ClassCastException unused)
			{
				return false;
			}
			catch (NullPointerException unused)
			{
				return false;
			}
		}

		@Override
		public Object[] toArray()
		{
			return toList(this).toArray();
		}

		@Override
		public <T> T[] toArray(T[] a)
		{
			return toList(this).toArray(a);
		}

		@Override
		public Iterator<E> descendingIterator()
		{
			return descendingSet().iterator();
		}

		@Override
		public SortedSet<E> headSet(E toElement)
		{
			return headSet(toElement, false);
		}

		@Override
		public NavigableSet<E> headSet(E toElement, boolean inclusive)
		{
			return new KeySet<E>(m.headMap(toElement, inclusive), valueForAdd);
		}

		@Override
		public SortedSet<E> subSet(E fromElement, E toElement)
		{
			return subSet(fromElement, true, toElement, false);
		}

		@Override
		public NavigableSet<E> subSet(E fromElement, boolean fromInclusive, E toElement,
				boolean toInclusive)
		{
			return new KeySet<E>(m.subMap(fromElement, fromInclusive, toElement, toInclusive),
					valueForAdd);
		}

		@Override
		public SortedSet<E> tailSet(E fromElement)
		{
			return tailSet(fromElement, true);
		}

		@Override
		public NavigableSet<E> tailSet(E fromElement, boolean inclusive)
		{
			return new KeySet<E>(m.tailMap(fromElement, inclusive), valueForAdd);
		}

		@Override
		public NavigableSet<E> descendingSet()
		{
			return new KeySet<E>(m.descendingMap(), valueForAdd);
		}
	}

	static final class Values<E> extends AbstractCollection<E> implements Cloneable, Serializable, Set<E>
	{
		private static final long serialVersionUID = -7361721666799471987L;

		private final NavigableMap<?, E> m;

		Values(NavigableMap<?, E> map)
		{
			m = map;
		}

		@Override
		public Iterator<E> iterator()
		{
			if (m instanceof TransactionalSkipListMap)
				return ((TransactionalSkipListMap<?, E>) m).valueIterator();
			else
				return ((SubMap<?, E>) m).valueIterator();
		}

		@Override
		public int size()
		{
			return m.size();
		}

		@Override
		public boolean isEmpty()
		{
			return m.isEmpty();
		}

		@Override
		public boolean contains(Object o)
		{
			return m.containsValue(o);
		}

		@Override
		public void clear()
		{
			m.clear();
		}

		@Override
		public Object[] toArray()
		{
			return toList(this).toArray();
		}

		@Override
		public <T> T[] toArray(T[] a)
		{
			return toList(this).toArray(a);
		}
	}

	static final class EntrySet<K1, V1> extends AbstractSet<Map.Entry<K1, V1>> implements
			Cloneable, Serializable
	{
		private static final long serialVersionUID = -4630941737614279179L;

		private final NavigableMap<K1, V1> m;

		EntrySet(NavigableMap<K1, V1> map)
		{
			m = map;
		}

		@Override
		public Iterator<Map.Entry<K1, V1>> iterator()
		{
			if (m instanceof TransactionalSkipListMap)
				return ((TransactionalSkipListMap<K1, V1>) m).entryIterator();
			else
				return ((SubMap<K1, V1>) m).entryIterator();
		}

		@Override
		public int size()
		{
			return m.size();
		}

		@Override
		public boolean isEmpty()
		{
			return m.isEmpty();
		}

		@Override
		public boolean contains(Object o)
		{
			if (!(o instanceof Map.Entry))
				return false;
			Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
			V1 v = m.get(e.getKey());
			return v != null && v.equals(e.getValue());
		}

		@Override
		public boolean remove(Object o)
		{
			if (!(o instanceof Map.Entry))
				return false;
			Map.Entry<?, ?> e = (Map.Entry<?, ?>) o;
			V1 value = m.get(e.getKey());
			if (value != null && value.equals(e.getValue()))
			{
				m.remove(e.getKey());
				return true;
			}
			return false;
		}

		@Override
		public boolean removeAll(Collection<?> c)
		{
			boolean modified = false;
			for (Iterator<?> i = c.iterator(); i.hasNext();)
				modified |= remove(i.next());
			return modified;
		}

		@Override
		public boolean retainAll(Collection<?> c)
		{
			boolean modified = false;
			if (!(c instanceof Set<?>))
				c = new HashSet<Object>(c);
			for (Iterator<?> i = iterator(); i.hasNext();)
			{
				Object o = i.next();
				if (!c.contains(o))
				{
					i.remove();
					modified = true;
				}
			}
			return modified;
		}

		@Override
		public void clear()
		{
			m.clear();
		}

		@Override
		public boolean equals(Object o)
		{
			if (o == this)
				return true;
			if (!(o instanceof Set))
				return false;
			Collection<?> c = (Collection<?>) o;
			try
			{
				return containsAll(c) && c.containsAll(this);
			}
			catch (ClassCastException unused)
			{
				return false;
			}
			catch (NullPointerException unused)
			{
				return false;
			}
		}

		@Override
		public Object[] toArray()
		{
			return toList(this).toArray();
		}

		@Override
		public <T> T[] toArray(T[] a)
		{
			return toList(this).toArray(a);
		}
	}

	/* ---------------- SubMap ---------------- */

	static final class SubMap<K, V> implements NavigableMap<K, V>, Cloneable, Serializable
	{
		private static final long serialVersionUID = 8278928392955856660L;

		private final TransactionalSkipListMap<K, V> m;

		private final K lo;

		private final K hi;

		private final boolean loInclusive;

		private final boolean hiInclusive;

		private final boolean isDescending;

		private final Comparable<? super K> loComp;

		private final Comparable<? super K> hiComp;

		private transient KeySet<K> keySetView;

		private transient Set<Map.Entry<K, V>> entrySetView;

		private transient Collection<V> valuesView;

		SubMap(TransactionalSkipListMap<K, V> map, K fromKey, boolean fromInclusive, K toKey,
				boolean toInclusive, boolean isDescending)
		{
			if (fromKey != null && toKey != null && map.compare(fromKey, toKey) > 0)
				throw new IllegalArgumentException("inconsistent range");
			this.m = map;
			this.lo = fromKey;
			this.hi = toKey;
			this.loInclusive = fromInclusive;
			this.hiInclusive = toInclusive;
			this.isDescending = isDescending;
			this.loComp = fromKey == null ? null : map.comparable(fromKey);
			this.hiComp = toKey == null ? null : map.comparable(toKey);
		}

		/* ---------------- Object overrides ---------------- */

		@Override
		public boolean equals(Object o)
		{
			if (o == this)
				return true;
			if (!(o instanceof Map))
				return false;
			Map<?, ?> m = (Map<?, ?>) o;
			try
			{
				for (Map.Entry<K, V> e : this.entrySet())
				{
					if (!e.getValue().equals(m.get(e.getKey())))
						return false;
				}
				for (Map.Entry<?, ?> e : m.entrySet())
				{
					Object k = e.getKey();
					Object v = e.getValue();
					if (k == null || v == null || !v.equals(get(k)))
						return false;
				}
				return true;
			}
			catch (ClassCastException unused)
			{
				return false;
			}
			catch (NullPointerException unused)
			{
				return false;
			}
		}

		@Override
		public int hashCode()
		{
			int h = 0;
			Iterator<Entry<K, V>> i = entrySet().iterator();
			while (i.hasNext())
				h += i.next().hashCode();
			return h;
		}

		@Override
		public String toString()
		{
			Iterator<Entry<K, V>> i = entrySet().iterator();
			if (!i.hasNext())
				return "{}";

			StringBuilder sb = new StringBuilder();
			sb.append('{');
			for (;;)
			{
				Entry<K, V> e = i.next();
				K key = e.getKey();
				V value = e.getValue();
				sb.append(key == this ? "(this Map)" : key);
				sb.append('=');
				sb.append(value == this ? "(this Map)" : value);
				if (!i.hasNext())
					return sb.append('}').toString();
				sb.append(',').append(' ');
			}
		}

		/* ---------------- utilities ---------------- */

		@SuppressWarnings("unchecked")
		private K kcast(Object o)
		{
			return (K) o;
		}

		private boolean tooLow(K key)
		{
			if (lo != null)
			{
				int c = loComp.compareTo(key);
				if (c > 0 || (c == 0 && !loInclusive))
					return true;
			}
			return false;
		}

		private boolean tooHigh(K key)
		{
			if (hi != null)
			{
				int c = hiComp.compareTo(key);
				if (c < 0 || (c == 0 && !hiInclusive))
					return true;
			}
			return false;
		}

		private boolean inBounds(K key)
		{
			return !tooLow(key) && !tooHigh(key);
		}

		private void checkKeyBounds(K key) throws IllegalArgumentException
		{
			if (key == null)
				throw new NullPointerException();
			if (!inBounds(key))
				throw new IllegalArgumentException("key out of range");
		}

		/**
		 * Returns true if node key is less than upper bound of range
		 */
		private boolean isBeforeEnd(TransactionalSkipListMap.Node<K, V> n)
		{
			if (n == null)
				return false;
			return !tooHigh(n.key);
		}

		/**
		 * Returns lowest node. This node might not be in range, so most usages
		 * need to check bounds
		 */
		private Node<K, V> loNode()
		{
			if (lo == null)
				return m.findFirstNode();

			else if (loInclusive)
				return m.ceilingNode(loComp);
			else
				return m.higherNode(loComp);
		}

		/**
		 * Returns highest node. This node might not be in range, so most usages
		 * need to check bounds
		 */
		private Node<K, V> hiNode()
		{
			if (hi == null)
				return m.findLastNode();
			else if (hiInclusive)
				return m.floorNode(hiComp);
			else
				return m.lowerNode(hiComp);
		}

		private Node<K, V> lowestNode()
		{
			Node<K, V> node = loNode();
			if (node == null || tooHigh(node.key))
				return null;
			return node;
		}

		private Node<K, V> highestNode()
		{
			Node<K, V> node = hiNode();
			if (node == null || tooLow(node.key))
				return null;
			return node;
		}

		private K lowestKey()
		{
			Node<K, V> n = lowestNode();
			if (n != null)
				return n.key;
			else
				throw new NoSuchElementException();
		}

		private K highestKey()
		{
			Node<K, V> n = highestNode();
			if (n != null)
				return n.key;
			else
				throw new NoSuchElementException();
		}

		private Node<K, V> removeLowestNode()
		{
			if (lo == null && hi == null)
				return m.removeFirstNode();

			Node<K, V> node = lowestNode();
			if (node != null)
				m.removeNode(node);
			return node;
		}

		private Node<K, V> removeHighestNode()
		{
			if (lo == null && hi == null)
				return m.removeLastNode();

			Node<K, V> node = highestNode();
			if (node != null)
				m.removeNode(node);
			return node;
		}

		private Node<K, V> lowerNode(K kkey, boolean inclusive)
		{
			if (tooLow(kkey))
				return null;

			Comparable<? super K> key = m.comparable(kkey);
			Node<K, V> node = inclusive ? m.floorNode(key) : m.lowerNode(key);

			if (node != null && tooHigh(node.key))
				node = hiNode();
			if (node == null)
				return null;
			if (tooLow(node.key))
				return null;
			return node;
		}

		private Node<K, V> higherNode(K kkey, boolean inclusive)
		{
			if (tooHigh(kkey))
				return null;

			Comparable<? super K> key = m.comparable(kkey);
			Node<K, V> node = inclusive ? m.ceilingNode(key) : m.higherNode(key);

			if (node != null && tooLow(node.key))
				node = loNode();
			if (node == null)
				return null;
			if (tooHigh(node.key))
				return null;
			return node;
		}

		private SubMap<K, V> newSubMap(K fromKey, boolean fromInclusive, K toKey,
				boolean toInclusive)
		{
			if (isDescending)
			{
				K tk = fromKey;
				fromKey = toKey;
				toKey = tk;
				boolean ti = fromInclusive;
				fromInclusive = toInclusive;
				toInclusive = ti;
			}

			if (lo != null)
			{
				if (fromKey == null)
				{
					fromKey = lo;
					fromInclusive = loInclusive;
				}
				else
				{
					int c = loComp.compareTo(fromKey);
					if (c > 0 || (c == 0 && !loInclusive && fromInclusive))
						throw new IllegalArgumentException("key out of range");
				}
			}

			if (hi != null)
			{
				if (toKey == null)
				{
					toKey = hi;
					toInclusive = hiInclusive;
				}
				else
				{
					int c = hiComp.compareTo(toKey);
					if (c < 0 || (c == 0 && !hiInclusive && toInclusive))
						throw new IllegalArgumentException("key out of range");
				}
			}

			return new SubMap<K, V>(m, fromKey, fromInclusive, toKey, toInclusive, isDescending);
		}

		/* ---------------- Map API methods ---------------- */

		@Override
		public boolean containsKey(Object key)
		{
			if (key == null)
				throw new NullPointerException();

			K k = kcast(key);
			return inBounds(k) && m.containsKey(k);
		}

		@Override
		public V get(Object key)
		{
			if (key == null)
				throw new NullPointerException();

			K k = kcast(key);
			return ((!inBounds(k)) ? null : m.get(k));
		}

		@Override
		public V put(K key, V value)
		{
			checkKeyBounds(key);
			return m.put(key, value);
		}

		@Override
		public void putAll(Map<? extends K, ? extends V> m)
		{
			for (Map.Entry<? extends K, ? extends V> e : m.entrySet())
				put(e.getKey(), e.getValue());
		}

		@Override
		public V remove(Object key)
		{
			K k = kcast(key);
			return (!inBounds(k)) ? null : m.remove(k);
		}

		@Override
		public boolean containsValue(Object value)
		{
			if (value == null)
				throw new NullPointerException();

			for (Node<K, V> n = loNode(); isBeforeEnd(n); n = n.next)
			{
				V v = n.value;
				if (v != null && value.equals(v))
					return true;
			}
			return false;
		}

		@Override
		public int size()
		{
			long count = 0;
			for (Node<K, V> n = loNode(); isBeforeEnd(n); n = n.next)
				++count;
			return count >= Integer.MAX_VALUE ? Integer.MAX_VALUE : (int) count;
		}

		@Override
		public boolean isEmpty()
		{
			return lowestNode() == null;
		}

		@Override
		public void clear()
		{
			for (Node<K, V> n = loNode(); isBeforeEnd(n); n = n.next)
			{
				if (n.value != null)
					m.removeNode(n);
			}
		}

		/* ---------------- SortedMap API methods ---------------- */

		@Override
		public Comparator<? super K> comparator()
		{
			Comparator<? super K> cmp = m.comparator();
			if (isDescending)
				return Collections.reverseOrder(cmp);
			else
				return cmp;
		}

		@Override
		public K firstKey()
		{
			return isDescending ? highestKey() : lowestKey();
		}

		@Override
		public K lastKey()
		{
			return isDescending ? lowestKey() : highestKey();
		}

		/* ---------------- NavigableMap API methods ---------------- */

		@Override
		public K lowerKey(K key)
		{
			Node<K, V> node = isDescending ? higherNode(key, false) : lowerNode(key, false);
			return node == null ? null : node.key;
		}

		@Override
		public Map.Entry<K, V> lowerEntry(K key)
		{
			return isDescending ? higherNode(key, false) : lowerNode(key, false);
		}

		@Override
		public K higherKey(K key)
		{
			Node<K, V> node = isDescending ? lowerNode(key, false) : higherNode(key, false);
			return node == null ? null : node.key;
		}

		@Override
		public Map.Entry<K, V> higherEntry(K key)
		{
			return isDescending ? lowerNode(key, false) : higherNode(key, false);
		}

		@Override
		public K ceilingKey(K key)
		{
			Node<K, V> node = isDescending ? lowerNode(key, true) : higherNode(key, true);
			return node == null ? null : node.key;
		}

		@Override
		public Map.Entry<K, V> ceilingEntry(K key)
		{
			return isDescending ? lowerNode(key, true) : higherNode(key, true);
		}

		@Override
		public K floorKey(K key)
		{
			Node<K, V> node = isDescending ? higherNode(key, true) : lowerNode(key, true);
			return node == null ? null : node.key;
		}

		@Override
		public Map.Entry<K, V> floorEntry(K key)
		{
			return isDescending ? higherNode(key, true) : lowerNode(key, true);
		}

		@Override
		public Map.Entry<K, V> firstEntry()
		{
			return isDescending ? highestNode() : lowestNode();
		}

		@Override
		public Map.Entry<K, V> lastEntry()
		{
			return isDescending ? lowestNode() : highestNode();
		}

		@Override
		public Map.Entry<K, V> pollFirstEntry()
		{
			return isDescending ? removeHighestNode() : removeLowestNode();
		}

		@Override
		public Map.Entry<K, V> pollLastEntry()
		{
			return isDescending ? removeLowestNode() : removeHighestNode();
		}

		/* ---------------- view methods ---------------- */

		@Override
		public Set<K> keySet()
		{
			KeySet<K> ks = keySetView;
			return (ks != null) ? ks : (keySetView = new KeySet<K>(this));
		}

		@Override
		public Collection<V> values()
		{
			Collection<V> vs = valuesView;
			return (vs != null) ? vs : (valuesView = new Values<V>(this));
		}

		@Override
		public Set<Map.Entry<K, V>> entrySet()
		{
			Set<Map.Entry<K, V>> es = entrySetView;
			return (es != null) ? es : (entrySetView = new EntrySet<K, V>(this));
		}

		@Override
		public SortedMap<K, V> headMap(K toKey)
		{
			return headMap(toKey, false);
		}

		@Override
		public NavigableMap<K, V> headMap(K toKey, boolean inclusive)
		{
			if (toKey == null)
				throw new NullPointerException();

			return newSubMap(null, false, toKey, inclusive);
		}

		@Override
		public SortedMap<K, V> subMap(K fromKey, K toKey)
		{
			return subMap(fromKey, true, toKey, false);
		}

		@Override
		public NavigableMap<K, V> subMap(K fromKey, boolean fromInclusive, K toKey,
				boolean toInclusive)
		{
			if (fromKey == null || toKey == null)
				throw new NullPointerException();

			return newSubMap(fromKey, fromInclusive, toKey, toInclusive);
		}

		@Override
		public SortedMap<K, V> tailMap(K fromKey)
		{
			return tailMap(fromKey, true);
		}

		@Override
		public NavigableMap<K, V> tailMap(K fromKey, boolean inclusive)
		{
			if (fromKey == null)
				throw new NullPointerException();

			return newSubMap(fromKey, inclusive, null, false);
		}

		@Override
		public NavigableSet<K> navigableKeySet()
		{
			KeySet<K> ks = keySetView;
			return (ks != null) ? ks : (keySetView = new KeySet<K>(this));
		}

		@Override
		public NavigableSet<K> descendingKeySet()
		{
			return descendingMap().navigableKeySet();
		}

		@Override
		public NavigableMap<K, V> descendingMap()
		{
			return new SubMap<K, V>(m, lo, loInclusive, hi, hiInclusive, !isDescending);
		}

		/* ---------------- iterators ---------------- */

		/**
		 * Variant of main Iter class to traverse through submaps.
		 */
		abstract class SubMapIter<T> implements Iterator<T>
		{
			Node<K, V> lastReturned;
			Node<K, V> next;

			SubMapIter()
			{
				next = isDescending ? highestNode() : lowestNode();
			}

			@Override
			public final boolean hasNext()
			{
				return next != null;
			}

			final void advance()
			{
				if (next == null)
					throw new NoSuchElementException();

				lastReturned = next;

				if (isDescending)
					descend();
				else
					ascend();
			}

			private void ascend()
			{
				next = next.next;
				if (next != null && tooHigh(next.key))
					next = null;
			}

			private void descend()
			{
				next = m.lowerNode(m.comparable(lastReturned.key));
				if (next != null && tooLow(next.key))
					next = null;
			}

			@Override
			public void remove()
			{
				Node<K, V> l = lastReturned;
				if (l == null)
					throw new IllegalStateException();
				m.remove(l.key);
				lastReturned = null;
			}
		}

		final class SubMapKeyIterator extends SubMapIter<K>
		{
			@Override
			public K next()
			{
				advance();
				return lastReturned.key;
			}
		}

		final class SubMapValueIterator extends SubMapIter<V>
		{
			@Override
			public V next()
			{
				advance();
				return lastReturned.value;
			}
		}

		final class SubMapEntryIterator extends SubMapIter<Map.Entry<K, V>>
		{
			@Override
			public Map.Entry<K, V> next()
			{
				advance();
				return lastReturned;
			}
		}

		final Iterator<K> keyIterator()
		{
			return new SubMapKeyIterator();
		}

		final Iterator<V> valueIterator()
		{
			return new SubMapValueIterator();
		}

		final Iterator<Map.Entry<K, V>> entryIterator()
		{
			return new SubMapEntryIterator();
		}
	}
}
