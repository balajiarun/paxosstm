/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.utils;

/**
 * Generic class which objects can be used to store some value.
 * 
 * @author Tadeusz Kobus
 * @author Maciej Kokocinski
 * 
 * @param <T>
 *            type of the value to be stored within the Holder
 */
public class Holder<T>
{
	public T value;

	public Holder()
	{
	}
	
	public Holder(T value)
	{
		this.value = value;
	}
	
	/**
	 * Stores given value inside the Holder.
	 * 
	 * @param v
	 *            value to be stored
	 */
	public void set(T v)
	{
		value = v;
	}

	/**
	 * Returns the value stored within the Holder.
	 * 
	 * @return the value stored within the Holder
	 */
	public T get()
	{
		return value;
	}
}
