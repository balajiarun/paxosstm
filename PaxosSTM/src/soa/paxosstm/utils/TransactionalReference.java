package soa.paxosstm.utils;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalReference<T>
{
	private T value;
	
	public TransactionalReference()
	{
		this.value = null;
	}
	
	public TransactionalReference(T value)
	{
		this.value = value;
	}
	
	public void set(T value)
	{
		this.value = value;
	}
	
	public T get()
	{
		return value;
	}
	   
	public void clear()
	{
		this.value = null;
	}
}
