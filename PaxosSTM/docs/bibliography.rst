.. _paxosstm-bibliography:

============
Bibliography
============

.. [ST95] Nir Shavit and Dan Touitou. Software transactional memory. In
   Proceedings of PODCS '95: the 14th ACM SIGACT-SIGOPS Symposium on Principles
   of Distributed Computing, August 1995.

.. [BHG87] Philip A. Bernstein, Vassco Hadzilacos, and Nathan Goodman.
   Concurrency control and recovery in database systems. Addison-Wesley Longman
   Publishing Co., Inc., Boston, MA, USA, 1987.

.. [M06] Sergio Mena de la Cruz. Protocol composition frameworks and modular
   group communication. PhD thesis, EPFL, EPFL, 2006.

.. [KKW09] Tadeusz Kobus & Maciej Kokocinski & Pawel T. Wojciechowski (2009):
   Distributed Software Transactional Memory in Crash-recovery Failure Model.
   Technical Report TR-ITSOA-OB2-1-PR-09-7, Instytut Informatyki, Politechnika
   Poznanska.

.. [KKW10] Tadeusz Kobus & Maciej Kokocinski & Pawel T. Wojciechowski (2010):
   Paxos STM. Technical Report TR-ITSOA-OB2-1-PR-10-4, Instytut Informatyki,
   Politechnika Poznanska.

.. [JPaxosWeb] http://jpaxos.org

.. [C09] M. Couceiro. Cache coherence in distributed and replicated
   transactional memory systems, 2009.