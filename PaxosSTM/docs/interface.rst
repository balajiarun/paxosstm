.. _paxosstm-interface:

=====================
Programming Interface
=====================

There are four basic aspects of DSTM system's API:

- `Transactions`_ - how to perform a transaction and how to change the
  control flow inside a transaction,
- `Shared Objects`_ - what can act as an object shared between the nodes
  and how it has to be defined,
- `Incorporating Paxos STM framework into an application`_ - how to
  start the framework, utilize the recovery capabilities and make use
  of other features of Paxos STM,
- `Paxos STM's behavior on error occurrence`_ - how does Paxos STM behave 
  when the transactional code misbehaves, what happens when a/some 
  replicas crash.

Transactions
------------

In Paxos STM framework transactions are defined as objects of classes
derived from the abstract ``Transaction`` class. The ``Transaction``
class consists of the abstract ``atomic()`` method which has to be
implemented by the programmer and is supposed the transaction's
operation set.

The code inside the ``atomic()`` method can call several methods to
alter the transaction's flow:

- ``commit()`` - ends the transaction earlier than the end of the
  ``atomic()`` method,
- ``retry()``  - restarts the transaction,
- ``rollback()`` - aborts the transaction,

.. highlight:: java

::

  class CustomTransaction extends Transaction {
    private MySharedObject sharedObject;

    public CustomTransaction(MySharedObject sharedObject) {
      this.sharedObject = sharedObject;
    }

    @Override
    protected void atomic() {
      sharedObject.foo();
      // commit();
      // retry();
      // rollback();
    }
  }

The transaction is started when an instance of the class is created::

  new CustomTransaction(sharedArray);

The ``Transaction`` class contains also a parametrized constructor
which allows to delay the execution of a newly created transaction. In
such a case the transaction needs to be started manually by the
programmer by calling the ``execute()`` method on the instance of the
class.

It is most convenient to use Java's anonymous classes to define
transactions as shown on the example below::

  final MySharedObject sharedObject = new MySharedObject(); 

  new Transaction() {
    protected void atomic() {
      sharedObject.foo();
      // commit();
      // retry();
      // rollback();
    }
  };

Notice the use of the ``final`` keyword in the declaration of shared
objects. This is required by the Java semantics in order to pass
variables to the anonymous class' methods.

Shared Objects
--------------

There are three kinds of objects that can be shared between the nodes:

.. rubric:: instances of classes marked with the ``@TransactionObject`` annotation
  
Marking a class definition with the ``@TransactionObject`` annotation advices
Paxos STM framework to alter the class definition when loading it to JVM. This
allow automatic tracking of changes performed to the objects in transactions.

Instances of classes marked with the ``@TransactionObject`` annotation are
created normally with the ``new`` operator::

  @TransactionObject
  class MySharedObject {
    public int value;

    public void foo() {
    ...
    }
    ...
  }

  MySharedObject sharedObject = new MySharedObject(); 

Objects of classes marked with the ``@TransactionObject`` annotation can be
accessed inside the transaction directly, that is as ordinary Java objects in
native Java code::

  final MySharedObject sharedObject = new MySharedObject(); 

  new Transaction() {
    protected void atomic() {
      ...
      sharedObject.value = 5;
      ...
      sharedObject.foo();
      ...
    }
  };

.. rubric:: instances of ``ObjectWrapper`` class

The ``@TransactionObject`` annotation can be added to the definition of the
class only when the programmer has the control over it. It cannot be done with
system classes or classes from third-party libraries. It is due the way classes
are loaded to JVM in Paxos STM and the Java license which forbids performing
modification to the system classes (for details see [KKW10]_).  Therefore, in
order to allow using objects of system classes or classes from external
libraries as objects shared between the nodes, the solution based on
``ObjectWrappers`` is provided. Objects of any class can become shared objects
as long as they are *wrapped* inside the ``ObjectWrapper``::

  Vector<String> vector = new Vector<String>();
  vector.add("foo");
  vector.add("bar");
  ObjectWrapper<Vector<String>> sharedVector = new
    ObjectWrapper<Vector<String>>(vector);

Objects wrapped inside the ``ObjectWrappers`` has to be accessed by
calling the ``get()`` method on the ``ObjectWrapper`` object::

  ...
  final ObjectWrapper<Vector<String>> sharedVector = new
    ObjectWrapper<Vector<String>>(vector);

  new Transaction() {
    protected void atomic() {
      sharedVector.get().add("foo and bar");
      System.out.println(sharedVector.get().size());
      System.out.println(sharedVector.get().get(0));
    }
  };

.. rubric:: instances of ``ArrayWrapper`` class

Arrays cannot be treated as ordinary objects therefore they have to be
handled differently. Similarly to objects of system classes or classes
from external libraries, arrays have to be stored inside wrappers in
order to use them inside transactions. The array-specific wrapper is
called ``ArrayWrapper``::

  int[] array = new int[] {2, 3, 5};
  ArrayWrapper<Integer> sharedArray = new 
    ArrayWrapper<Integer>(array);

Array elements have to be accessed by calling the ``T get(int index)`` and
``void set(int index, T value)`` methods. The array stored inside the
``ArrayWrapper`` can be temporarily released by calling the ``release()`` method
so elements of the array can be modified directly. The array can be again locked
inside the ``ArrayWrapper`` object by calling the ``hold()`` method::

  int[] array = new int[] {2, 3, 5};
  final ArrayWrapper<Integer> sharedArray = new 
    ArrayWrapper<Integer>(array);

  new Transaction() {
    protected void atomic() {
      int firstValue = sharedArray.get(0);
      sharedArray.set(1, firstValue);
      int []a = sharedArray.release();
      System.out.println("[" + a[0] + ", " + a[1] + ", " + a[2] + "]");
      a[2] = 7;
      sharedArray.hold();
      sharedArray.set(2, 77);
    }
  };

Incorporating Paxos STM framework into an application
-----------------------------------------------------

Using Paxos STM in an application is not only related to creating shared objects
and performing transactions. Firstly, the Paxos STM framework has to be overtly
started by the application. Secondly, the application has to implement a set of
interfaces in order to profit from the Paxos STM recovery capabilities.
Thirdly, the programmer can use the so called *Shared Object Registry (SOR)* to
associate labels (of the type of ``java.util.String``) with shared object so
they can be easily accessed.

.. rubric:: starting the Paxos STM framework

Starting Paxos STM requires invoking the ``start()`` method on the instance of
the ``Paxos STM`` class which is implemented using the singleton pattern::

  PaxosSTM.getInstance().start();

.. rubric:: providing recovery capabilities for the whole application

In order to provide recovery capabilities for the whole application the
programmer is required to do the following:

- select crucial data that constitute the application's state and provide
  methods for storing and retrieving them from the stable storage,
- synchronize the process of storing/retrieving the data from the stable storage
  with analogue processes in the Paxos STM framework.

To illustrate how it should be performed let us consider the following example.
The whole program is confined in ``ExampleClass``. The processing of the
application is reduced to incrementing a value in an infinite loop; to shorten
the example it is not even performed transactionally. After each incrementation
the program saves its state to the stable storage. ::

  public class ExampleClass implements CommitListener, RecoveryListener
  {
    private Storage storage;
    private Object recoveryLock = new Object();
    private boolean ready = false;

    private int counter;
      
    public ExampleClass()
    {
      PaxosSTM.getInstance().addCommitListener(this);
      PaxosSTM.getInstance().addRecoveryListener(this);
      this.storage = PaxosSTM.getInstance().getStorage();
    }

    @Override
    public void onCommit(Object commitData)
    {
      int commitNumber = (Integer) commitData;
      int logVersion = commitNumber % 2;

      try
      {
        storage.log("Counter_" + logVersion, counter);
      }
      catch (StorageException e)
      {
        e.printStackTrace();
      }
    }

    @Override
    public void recoverFromCommit(Object commitData)
    {
      int commitNumber = (Integer) commitData;
      int logVersion = commitNumber % 2;

      try
      {
        counter = (Integer) storage.retrieve("Counter_" + logVersion);
      }
      catch (StorageException e)
      {
        counter = commitNumber;
      }
    }

    @Override
    public void recoveryFinished()
    {
      synchronized (recoveryLock)
      {
        ready = true;
        recoveryLock.notifyAll();
      }
    }
      
    public void start()
    {
      try
      {
        PaxosSTM.getInstance().start();
      }
      catch (StorageException e)
      {
        e.printStackTrace();
        System.exit(1);
      }

      synchronized (recoveryLock)
      {
        while (!ready)
        {
          try
          {
            recoveryLock.wait();
          }
          catch (InterruptedException e)
          {
            e.printStackTrace();
          }
        }
      }

      while (true)
      {
        // do some processing
        counter++;
      	    
        PaxosSTM.getInstance().commit(counter);
      }
    }

    public static void main(String[] args)
    {
      ExampleClass instance = new ExampleClass();
      instance.start();
    }
  }

Paxos STM framework provides a set of interfaces which have to be implemented so
the process of creating checkpoints/recovering could be coordinated throughout
the whole application:

- ``CommitListener`` - defines the method ``onCommit()`` which
  consists of the code necessary to save to the stable storage the
  application's state
- ``RecoveryListener`` - defines two methods:
  ``recoverFromCommit()`` which consists of code necessary to
  restore from stable storage application's state and
  ``recoveryFinished()`` which signalizes that the process of
  recovering is finished and the application can resume its duties.

The programmer can use the Storage interface provided by Paxos STM in order to
save necessary data::

  Storage storage = PaxosSTM.getInstance().getStorage();
  
  MyClass obj = new MyClass();
  storage.log("label", obj);
  MyClass obj2 = (MyClass) storage.retrieve("label");

  // obj.equals(obj2) == true

The application can trigger the process of creating a checkpoint by
calling the ``commit()`` method::

  Object commitData; // application snapshot or counter variable
  ...
  PaxosSTM.getInstance().commit(commitData);

The argument of the ``commit()`` method should be the number of checkpoint to be
created. This guarantees that the consecutive commits will be numbered
consistently also in case of a crash and recovery. The interface providing
checkpoint capabilities is designed in a way so the checkpoints can be also
created by passing a snapshot of the application to the Paxos STM, thus the
``Object`` as the type of the ``commitData``. In order to guarantee consistency
at all times at least the last two checkpoints have to be preserved on the
stable storage. 

Notice that the ``ExampleClass`` in its constructor adds itself as Paxos STM
``Commit-`` and ``RecoveryListener`` thus allowing the Paxos STM to coordinate
the process of creating checkpoints and recovering. The simple synchronization
present in the example above is necessary so the application do not compute in
the recovery phase.

.. rubric:: adding/removing a shared object to/from Shared Object Registry
  
Operations on Shared Object Registry (SOR) are performed by appropriate methods
on the instance of the ``PaxosSTM`` class. In the example below a process on one
node (``node1``) adds an object to the registry by the label of ``MyObjectA``.
Then any other process can retrieve the reference to this object by calling the
registry and passing the appropriate label (``MyObjectA``) as the argument::

  // on node1
  MySharedClass sharedObjectA = new MySharedClass(); 
  PaxosSTM.getInstance().addToRegistry("MyObjectA", sharedObjectA);

  // any process
  MySharedClass sharedObjectA = (MySharedClass)
    PaxosSTM.getInstance().getFromRegistry("MyObjectA");

A reference to an object can be removed from the registry::

  // any process
  MySharedClass sharedObjectA = 
    PaxosSTM.getInstance().removeFromRegistry("MyObjectA");

Paxos STM's behavior on error occurrence
----------------------------------------

.. rubric:: handling exception in transactional code

The programmer should secure the transactional code so there are no unhandled
exceptions. However, if by any chance an unhandled exception is thrown inside
the ``atomic()`` method it is caught by the Paxos STM and the whole transaction
is rolled back.

.. rubric:: crash of a/some replicas

JPaxos, on top of which Paxos STM is built, to be able to operate correctly
requires at least one more of the half of all the nodes to be up and running.
So if there are three replicas all together, if one crashes, Paxos STM can still
function normally. On the other hand if two crash simultaneously, the other
replica withholds performing commit attempts on any currently executed
transaction. Operating on Shared Object Registry is also suspended. Paxos STM
resumes all its duties once at least one of the crashed replicas recovers.

Javadoc
-------

The API documentation is available in :api:`HTML format
<resp/paxosstm>` (:api:`tarball
<resp/paxosstm/PaxosSTM-javadoc-0.1.tar.gz>` and in :api:`PDF format
<resp/paxosstm/PaxosSTM-javadoc-0.1.pdf>`).
