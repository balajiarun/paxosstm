.. _paxosstm-execution:

==========================================
Running an application employing Paxos STM
==========================================

.. highlight:: console

Let us assume there is an application samplepackage.App that employs Paxos STM.
The invocation of the program should look as below::

  $ java -cp "JPaxos.jar:PaxosSTM.jar:javassist.jar:objenesis-1.2.jar:
    <path to the App jar archive or directory with compiled classes>"
      soa.paxosstm.dstm.Main
        samplepackage.App <replica number> [samplepackage.App arguments]

In order to disable showing log information on the console the following system
property should be defined (logging_none.properties file is in the project's
main directory)::

  -Djava.util.logging.config.file=logging_none.properties

In the ``utils/unix`` and ``utils/windows`` directories there is a bunch of
useful scripts (cygwin scripts for the windows platform), e.g. one that helps to
start an application employing Paxos STM::

  $ ./run.sh samplepackage.App <replica number> [samplepackage.App arguments]

Other scripts are provided only for testing purposes.
