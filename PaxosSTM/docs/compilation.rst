.. _paxosstm-compilation:

=============================
Compilation and configuration
=============================

Configuration
-------------

Paxos STM relays on JPaxos library; configuration of Paxos STM is largely
determined by the configuration of JPaxos. Therefore the first thing that has be
done to run an application employing Paxos STM is to set up is JPaxos parameters
(``paxos.properties`` file in the projects main directory). For details of
JPaxos configuration please check JPaxos' documentation. However, the most
important parameters are:

**network configuration**

  IP addresses and ports on which JPaxos will run on each replica, e.g.:

  ::

    process.0 = 192.168.1.100:2021:3001
    process.1 = 192.168.1.101:2022:3002
    process.2 = 192.168.1.102:2023:3003
  
  This entry defines three replicas identified by numbers 0, 1 and 2. These
  numbers are used during execution of an application utilizing Paxos STM (see
  `Running an application employing Paxos STM <execution.html>`_ for details). Each instance of
  JPaxos requires two available ports.

**failure model configuration**

  There are a number of different failure models Paxos STM can work with. Some
  are not yet available due to the undergoing development of JPaxos library, as
  noted below):

  - **crash-stop** - the process which crashes ceases communication with
    other processes and never recovers; calling the commit primitive
    does nothing; very fast but does not provide any guarantees
    concerning fault tolerance
  - **full stable storage** - on each node each message passed by JPaxos
    is logged to the stable storage so upon crash and recovery all the
    missed messages will be delivered to the recovered process; the
    performance is limited by the performance of the stable storage,
    however there is no upper limit to the number of processes which can
    simultaneously crash
  - **storage-in-ram for homogeneous applications running on all nodes**
    [not yet fully implemented] - this setting may be used for
    transactional replication of a service. Since the copies of the same
    application run on each node and Paxos STM is used to ensure
    consistent state across all the replicas upon crash the process can
    retrieve and apply the state of one of the other working
    processes. Since processes use snapshots of other processes in order
    to recover there is no need for creating ordinary
    checkpoints. Therefore calling the commit primitive does
    nothing. When all the processes function correctly the performance
    of Paxos STM in such settings is approximately the same as in case
    of crash-stop failure model. However the number of processes that
    can crash simultaneously have to be lower than the half of the
    number of the processes.
  - **storage-in-ram for heterogeneous applications** [to be
    implemented] - processes save the history of exchanged messages in
    the volatile memory and from time to time they flash the logs to the
    stable storage. In the moments when all the processes are
    operational the logs can be shortened. The process may recover from
    the checkpoint made earlier or from the state of other process
    running the same application if it is operational. When all the
    processes are operational the performance of Paxos STM in such a
    setting should be similar to the performance in crash-stop
    model. The number of processes that can crash simultaneously depends
    on the model of distributed computing implemented in the
    applications.

  Choosing suitable failure model requires setting the ``FailureModel`` property
  in  JPaxos' configuration file::

    FailureModel = CrashStop

  for crash-stop failure model and::

    FailureModel = FullStableStorage

  for full stable storage failure model.

Building the library
--------------------

The compilation of Paxos STM requires all of the listed in
`Requirements <requirements.html>`_.  libraries. Appropriate jar archives are placed in
the lib directory.

The project can be build using `Apache Ant`_. The follwing ant targets
are available:

- [default] - compiles sources, generates documentation (see below)
  and creates jar archive,
- ``build`` - compiles source to the ``bin`` directory,
- ``clean`` - removes ``bin``, ``dist``, ``javadoc`` directories,
- ``docs`` - generates the documentation (sphinx in html format,
  javadocs); documenation to the project is placed in the ``docs``
  directory whereas the *javadoc* documentation is located in the
  ``javadoc`` directory,
- ``jar`` - creates jar archive and places it in the ``dist``
  directory,
- ``zip`` - creates the zip archive of the project and places it in
  the project's main directory,
- ``zipnosource`` - creates the zip archive of the project with jar
  archive but no sources and places it in the project's main
  directory.

In order to run a target execute the ``ant <target>`` command in the project's
base directory. Calling ``ant`` without any arguments invokes the default
target.

.. _Apache Ant: http://ant.apache.org/
