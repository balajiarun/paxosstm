.. _paxosstm-requirements:

============
Requirements
============

Paxos STM requires the following components to be installed on the
target system:

- execution platform: `Java Runtime Environment 1.6`_
- compilation platform: `Java Software Development Kit 1.6`_
- additional libraries:

  * `JPaxos`_ (`GPL v3`_)
  * `Javassist`_ (`MPL`_, `LGPL`_)
  * `Objenesis`_ (`Apache Licence v2.0`_) 

.. _Java Runtime Environment 1.6: http://www.java.com
.. _Java Software Development Kit 1.6: http://www.java.com
.. _Javassist: http://www.csg.is.titech.ac.jp/~chiba/javassist/
.. _Objenesis: http://objenesis.googlecode.com/svn/docs/index.html

.. _Apache Licence v2.0: http://www.apache.org/licenses/LICENSE-2.0
.. _GPL v3: http://www.gnu.org/licenses/gpl.html
.. _LGPL: http://www.gnu.org/licenses/lgpl.html
.. _MPL: http://www.mozilla.org/MPL/