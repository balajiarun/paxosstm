====================
Paxos STM User guide
====================

Contents:

.. toctree::
   :maxdepth: 2

   Overview <overview>
   Requirements <requirements>
   Download <download>
   Compilation and configuration <compilation>
   Programming Interface <interface>
   Running an application employing Paxos STM <execution>
   Bibliography <bibliography>

