.. _paxosstm-overview:

========
Overview
========

What is Paxos STM?
------------------

Paxos STM is a fast Java implementation of Distributed Software
Transactional Memory (DSTM) system. It aims at providing a convenient
way of developing and managing highly available and efficient services
by employing semantics based on transactional processing [ST95]_
[BHG87]_. Paxos STM fully supports the crash-recovery failure model
and relies on non-blocking transaction certification algorithms built
on the top of the group communication system layer providing such
primitives as distributed consensus and atomic broadcast. The
granularity of access in the transactions is defined as a single Java
object. One of the key features of Paxos STM system is a user
friendly, error-resistant API which shifts to the framework the burden
of tracking the changes that the transactions perform on objects that
are shared between nodes.

Paxos STM architecture
----------------------

Paxos STM is built in modular fashion with each building block providing
recovery capabilities: 

.. image:: img/architecture-small2.png
   :scale: 50%

At the bottom of the stack of
protocols used by Paxos STM is the JPaxos [JPaxosWeb]_ library which is
responsible for solving distributed consensus. It provides
fault-tolerance and is able to work in a network where messages can be
lost. On top of JPaxos there is the Atomic Broadcast [M06]_ module
which guarantees that all the nodes (also referred to as replicas)
will eventually receive the same set of messages in the same order.
This delivery property is necessary for the employed in the Paxos STM
optimistic transaction certification algorithms [C09]_. In the
optimitic approach every transaction is executed in a way that is
invisible to all the other concurrently executed transaction. When an
attempt to commit the transaction is undertaken all the necessary
information about the transaction (e.g. the sets containing objects
read and written in the transaction, logical time representing the
moment of the start of the transaction) is broadcast using atomic
broadcast mechanism to all the replicas. Therefore each node can
independently and deterministically perform the transaction
certification so all the replicas eventually decide whether the
transaction can be committed or it has to be rolled back and
restarted. Employing this certification scheme results in no
additional synchronization between the nodes during the execution of
the transactions. In order to enhance the level of fault tolerance and
to reduce the latency in accessing the shared objects, all the shared
objects are replicated on each node.

A complete description of Paxos STM appeared in the technical reports
[KKW09]_ [KKW10]_.

