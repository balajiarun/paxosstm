.. _paxosstm-download:

==================
Paxos STM Download
==================

In order to get a Paxos STM distribution, download the recent stable
release: :download:`PaxosSTM 0.1 <resp/paxosstm/PaxosSTM-0.1.zip>` (`Release Notes <../release/release-0.1.html>`_). 
