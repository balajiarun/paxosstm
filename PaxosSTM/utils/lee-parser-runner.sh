#!/bin/bash

let "nodes=$1+1"

#echo $nodes
let "first=1" 

#    let "node=0"    
#    while [  $node -lt "$nodes" ]; do
#             echo The counter is $node
#             let node=node+1 
#         done
for thread in `seq 1 10`; do    
    echo "file        rounds       av time       av gc     av attempts"
    let "node=0"
    while [  $node -lt "$nodes" ]; do
	name=lee_$node\_$thread
        let node=node+1 
	awk -f ../utils/lee-parser.awk $name
    done
done

for thread in `seq 6 16`; do    
    echo "file        rounds       av time       av gc     av attempts"
    let "node=0"
    let "t=2*thread"
    while [  $node -lt "$nodes" ]; do
	name=lee_$node\_$t
        let node=node+1 
	awk -f ../utils/lee-parser.awk $name
    done
done

