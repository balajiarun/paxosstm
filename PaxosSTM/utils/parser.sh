#!/bin/bash

awk -f ../utils/paxosstm-parser.awk hash_0|head -1

for i in `seq 0 $@`; do
    awk -f ../utils/paxosstm-parser.awk hash_$i|tail -1
done;
