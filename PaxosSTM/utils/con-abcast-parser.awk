function printhead()
{
    print "rounds\tommit\tthreads\tinstperthread\tinst_avg"
}

function printresults(threads, instperthread, total, ommit, inst)
{
    print total "\t" ommit "\t" threads "\t" instperthread "\t\t" inst/(total-ommit)
}

BEGIN{
    printhead()
}
{
    ommit=10

    if ($0 ~ /round/) {
	if (triggered == 0)
	{
	    triggered = 1
	}
	else
	{
	    printresults(threads, instperthread, i, ommit, instsum)
	}
	triggered=1;
	i = 0;
	instsum=0
	threads=0
	instperthread=0
    }
    if (triggered) {
	if ($0 !~ /round/)
	{
	    if (i == 0)
	    {
		threads=$2
		instperthread=$3
	    }
	    if (i >= ommit)
	    {
		instsum=instsum+$5
	    }
	    i=i+1	    
	}
    }
}
END {
	printresults(threads, instperthread, i, ommit, instsum)
}