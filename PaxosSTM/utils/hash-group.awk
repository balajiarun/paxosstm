function reset()
{
    i = 0;
    size = 0;
    range = 0;
    threads = 0; 
    rotran = 0;
    rwtran = 0;
    total = 0;
    ommit = 0;
    inst_avg = 0;
    abort_rate = 0;
}

function printOut()
{
    print i "\t" size "\t" range "\t" threads "\t" rotran "\t" rwtran "\t" total "\t" ommit "\t" inst_avg/i "\t" abort_rate/i;
}

BEGIN {
    reset();
    started = 0;
}
{
    if (NR == 1)
	print "nodes \t" $0
  if (/size/)
    {
	if (started == 0)
	    started = 1;
	else
	{ 
	    printOut();
	    reset();
	}
    }
  else
  {
      i++;
      size = $1;
      range = $2;
      threads = $3; 
      rotran = $4;
      rwtran = $5;
      total = $6;
      ommit = $7;
      inst_avg += $8;
      abort_rate += $9;
      
  }
}
END {
    printOut();
#  print "rounds \t av time \t av gc \t av addi"
 #   print rounds
#    print FILENAME "\t\t" rounds "\t" time/(rounds-1) "\t\t" gc/(rounds-1) "\t\t" additional/(rounds-1);
}