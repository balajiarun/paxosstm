#!/bin/bash

# id, threads
function runLee
{
    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 3 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_$1_$2 |tee ../../results/lee_$1_$2
#    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 1 ../PaxosSTMTest/sparselong.txt 2> ../../results/err_$1_$2 |tee ../../results/lee_$1_$2
}  
            
# id, threads
function runHash
{
    ./run.sh soa.paxosstm.example.Main $1 44 $2 10000 10000 0 0 1 50 | tee ../../results/hash_$1_$2 
#    ./run.sh soa.paxosstm.example.Main $1 44 $2 5000 5000 50 50 1 50 | tee ../../results/hash_$1_$2 
}

function runJTest
{
    ./run.sh soa.paxosstm.example.Main $1 8 $2 150000 100  | tee ../../results/hash_$1_$2
}

function runHash2
{
    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 0 0 2 10 100 10 10000
}


# id threads rounds accounts percentOfRW mills                                                                                                                      
function runBank
{
    ./run.sh soa.paxosstm.benchmark.bank.Main $1 $2 10 10000 $3 10000 | tee ../../results/bank_$3_$1_$2
}

for i in `seq 1 2`; do
    runBank $1 0 0
done

#for i in `seq 1 12`; do
#    runHash2 $1 0
#done

#for i in `seq 1 10`; do
#    let "t=$i"
#    runHash $1 $t
#done

#for i in `seq 6 16`; do
#    let "t=2*$i"
#    runHash $1 $t
#done

#for i in `seq 1 10`; do
#    let "t=$i"
#    runLee $1 $t
#done

#for i in `seq 6 16`; do
#    let "t=2*$i"
#    runLee $1 $t
#done

#for i in `seq 1 10`; do
#    let "t=$i"
#    runJTest $1 $t
#done

#for i in `seq 6 16`; do
#    let "t=2*$i"
#    runJTest $1 $t
#done


#./run.sh soa.paxosstm.benchmark.leetm.Main -1 20 1 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_0_20 |tee ../../results/lee_0_20
#./run.sh soa.paxosstm.benchmark.leetm.Main -1 24 1 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_0_24 |tee ../../results/lee_0_24
#./run.sh soa.paxosstm.benchmark.leetm.Main -1 30 1 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_0_30 |tee ../../results/lee_0_30
#./run.sh soa.paxosstm.benchmark.leetm.Main -1 32 1 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_0_32 |tee ../../results/lee_0_32
