#!/bin/bash

function runCon
{
    ./consensusTest.sh $1 $2 500 800 20 |tee ../../results/hash_$1_$2
}


for i in `seq 1 10`; do
    let "t=$i"
#    runCon $1 $t
done

for i in `seq 6 16`; do
    let "t=2*$i"
    runCon $1 $t
done

