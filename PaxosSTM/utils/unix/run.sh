#!/bin/bash
cd ../..
#java -Djava.util.logging.config.file=logging_none.properties -Xmx512m -cp "../JPaxos/bin/:../PaxosSTM/bin/:../PaxosSTMTest/bin:lib/javassist.jar:lib/objenesis-1.2.jar" soa.paxosstm.dstm.Main $@
#java -Djava.util.logging.config.file=logging_none.properties -Xmx1024m -Xms1024m -Xmn512m -XX:+UseParallelOldGC -XX:ParallelGCThreads=4 -cp "../PaxosSTM/lib/JPaxos.jar:../PaxosSTM/bin/:../PaxosSTMTest/bin:lib/javassist.jar:lib/objenesis-1.2.jar" soa.paxosstm.dstm.Main $@

java -Djava.util.logging.config.file=logging_none.properties -Xmx512m -Xms512m -Xmn256m -cp "../JPaxos/bin:../PaxosSTM/bin/:../PaxosSTMTest/bin:lib/javassist.jar:lib/objenesis-1.2.jar" soa.paxosstm.dstm.Main $@
#java -Djava.util.logging.config.file=logging_none.properties -Xmx256m -Xms256m -Xmn128m -cp "../JPaxos/bin:../PaxosSTM/bin/:../PaxosSTMTest/bin:lib/javassist.jar:lib/objenesis-1.2.jar" soa.paxosstm.dstm.Main $@
#java -Djava.util.logging.config.file=logging.properties -Xmx512m -Xms512m -Xmn256m -cp "../JPaxos/bin:../PaxosSTM/bin/:../PaxosSTMTest/bin:lib/javassist.jar:lib/objenesis-1.2.jar" soa.paxosstm.dstm.Main $@

