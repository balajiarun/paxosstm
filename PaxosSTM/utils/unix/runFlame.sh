#!/bin/bash
cd ../..

java -Djava.util.logging.config.file=logging_none.properties -Xmx512m -Xms512m -Xmn256m -cp "../JPaxos/bin:../PaxosSTM/bin/:../PaxosSTMTest/bin:lib/javassist.jar:lib/objenesis-1.2.jar:lib/StackMonitor-1.0.jar:lib/args4j-2.0.10.jar" com.zoltran.stackmonitor.Monitor -ss -si 100 -w 2000 -f /tmp/perfChart.html -main soa.paxosstm.dstm.Main $@