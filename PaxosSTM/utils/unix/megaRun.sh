#!/bin/bash
results=../../results
sleepTime=60

runStep()
{
    echo "==================="
    echo "sleeping $sleepTime"
    sleep $sleepTime
    if [ $1 -eq "0" ]; then
	echo "rm -rf $results"
	rm -rf $results
	echo "mkdir $results"
	mkdir $results
	echo "cp ../../paxos.properties-$2 ../../paxos.properties"
	cp "../../paxos.properties-$2" "../../paxos.properties"
    fi
    echo sleeping $sleepTime
    sleep $sleepTime
    if [ $1 -lt $2 ]; then
	./benchmarks.sh $1
	echo "sleeping $sleepTime"
	sleep $sleepTime
    fi
    if [ $1 -eq "0" ]; then
	echo "cp -r $results ../../results-total-$2"
	cp -r $results "../../results-total-$2"
    fi
}

runStep $1 6
runStep $1 5
runStep $1 4
runStep $1 3
runStep $1 2
