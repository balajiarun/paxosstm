#!/bin/bash

# id, threads
function runLee
{
#echo "    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 3 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_$1_$2 |tee ../../results/lee_$1_$2"
#    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 3 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_$1_$2 |tee ../../results/lee_$1_$2
#    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 1 ../PaxosSTMTest/sparselong.txt 2> ../../results/err_$1_$2 |tee ../../results/lee_$1_$2
#    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 1 ../PaxosSTMTest/mainboard-short.txt |tee ../../results/lee_$1_$2
#    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 1 ../PaxosSTMTest/mainboard-short.txt |tee ../../results/lee_$1_$2
#    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 1 ../PaxosSTMTest/mainboard-short4.txt 2>&1 |tee ../../results/lee_$1_$2
#    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 1 ../PaxosSTMTest/mainboard.txt 2>&1 |tee ../../results/lee_$1_$2
#    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 1 ../PaxosSTMTest/mainboard-little.txt 2>&1 |tee ../../results/lee_$1_$2
#    ./run.sh soa.paxosstm.benchmark.leetm.Main $1 $2 1 ../PaxosSTMTest/sparseshort.txt |tee ../../results/lee_$1_$2

    ./run.sh soa.paxosstm.benchmark.generic.Main $1 soa.paxosstm.benchmark.gleetm.LeeBenchmarkFactory $3 $2 ../PaxosSTMTest/mainboard.txt 2>&1 | tee ../../results/lee_$1_$2
#    ./run.sh soa.paxosstm.benchmark.generic.Main $1 soa.paxosstm.benchmark.gleetm.LeeBenchmarkFactory $3 $2 ../PaxosSTMTest/memboard.txt 2>&1 | tee ../../results/lee_$1_$2
}  

function runJTest
{
    ./run.sh soa.paxosstm.example.Main $1 8 $2 150000 100  | tee ../../results/hash_$1_$2
}
            
# id, threads
function runHash2
{
#    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 90 10 2 10 100 10 10000 1000| tee ../../results/hash_10_$1_$2 
#    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 90 10 2 20 100 10 10000 | tee ../../results/hash-D--_10_$1_$2 
    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 50 50 2 4 100 10 10000 | tee ../../results/hash-D--_50_$1_$2 
#    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 10 90 2 20 100 10 10000 | tee ../../results/hash-D--_90_$1_$2 
#    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 0 100 2 10 100 10 10000 | tee ../../results/hash_100_$1_$2 
}

# id, threads
function runHash2Pro
{
#    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 90 10 2 10 100 10 10000 1000| tee ../../results/hash_10_$1_$2 
    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 90 10 2 20 100 10 20000 1000 | tee ../../results/hash-P--_10_$1_$2 
    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 50 50 2 20 100 10 20000 1000 | tee ../../results/hash-P--_50_$1_$2 
    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 10 90 2 20 100 10 20000 1000 | tee ../../results/hash-P--_90_$1_$2 
#    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 0 100 2 10 100 10 10000 | tee ../../results/hash_100_$1_$2 
}

# id, threads
function runHash2Cont
{
#    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 90 10 2 10 100 10 10000 1000| tee ../../results/hash_10_$1_$2 
    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 90 10 10 20 100 50 20000 | tee ../../results/hash-C--_10_$1_$2 
    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 50 50 10 20 100 50 20000 | tee ../../results/hash-C--_50_$1_$2 
    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 10 90 10 20 100 50 20000 | tee ../../results/hash-C--_90_$1_$2 
#    ./run.sh soa.paxosstm.benchmark.hashmap.Main $1 $2 10000 10000 0 100 2 10 100 10 10000 | tee ../../results/hash_100_$1_$2 
}




# id threads rounds accounts percentOfRW mills
function runBank
{
#    ./run.sh soa.paxosstm.benchmark.bank.Main $1 $2 30 10000 $3 10000 2> ../../results/err | tee ../../results/bank_$3_$1_$2 
    ./run.sh soa.paxosstm.benchmark.bank.Main $1 $2 30 10000 $3 10000 | tee ../../results/bank_$3_$1_$2 
}

#
function runSB7
{
#    ./runSB7.sh $1 -g stm -s stmbench7.impl.paxosstm.PaxosSTMInitializer -t $2 -l $3 -w $4| tee ../../results/sb_$2_$1--$4
#    sleep 10
    ./runSB7.sh $1 -g stm -s stmbench7.impl.paxosstm.PaxosSTMInitializer -t $2 -l $3 -w $4 --no-traversals | tee ../../results/sb_$2_$1-t-$4
    sleep 10
#    ./runSB7.sh $1 -g stm -s stmbench7.impl.paxosstm.PaxosSTMInitializer -t $2 -l $3 -w $4 --no-sms | tee ../../results/sb_$2_$1-s-$4
#    sleep 10
#    ./runSB7.sh $1 -g stm -s stmbench7.impl.paxosstm.PaxosSTMInitializer -t $2 -l $3 -w $4 --no-traversals --no-sms | tee ../../results/sb_$2_$1-st-$4
#    sleep 10
}

#runSB7 $1 4 120 r
#runLee $1 2 6

#runHash2Pro $1 20
runHash2 $1 20
#runHash2Cont $1 20

#runBank $1 20 50
#runBank $1 80 90
#runBank $1 80 50
#runBank $1 80 10
#runBank $1 80 0
#runBank $1 50 0


#runBank $1 30 0
#runBank $1 40 0
#runBank $1 20 100
#runBank $1 30 100
#runBank $1 40 100
#runBank $1 20 90
#runBank $1 30 90
#runBank $1 40 90
#runBank $1 20 50
#runBank $1 30 50
#runBank $1 40 50
#runBank $1 20 10
#runBank $1 30 10
#runBank $1 40 10
#runBank $1 128 100

#runBank $1 20 100
#runBank $1 24 100
#runBank $1 28 100
#runBank $1 32 100

#runBank $1 20 90
#runBank $1 24 90
#runBank $1 28 90
#runBank $1 32 90

#runBank $1 20 50
#runBank $1 24 50
#runBank $1 28 50
#runBank $1 32 50

#runBank $1 20 10
#runBank $1 24 10
#runBank $1 28 10
#runBank $1 32 10

# runBank $1 1 100
# runBank $1 2 100
# runBank $1 4 100
# runBank $1 8 100
# runBank $1 16 100

#  runBank $1 1 90
#  runBank $1 2 90
#  runBank $1 4 90
#  runBank $1 8 90
#  runBank $1 16 90

#  runBank $1 1 50
#  runBank $1 2 50
#  runBank $1 4 50
#  runBank $1 8 50
#  runBank $1 16 50

#  runBank $1 1 10
#  runBank $1 2 10
#  runBank $1 4 10
#  runBank $1 8 10
#  runBank $1 16 10

for i in `seq 1 10`; do
    let "t=$i"
#    runHash $1 $t
done

for i in `seq 6 16`; do
#for i in `seq 16 16`; do
    let "t=2*$i"
#    runHash $1 $t
done

#runLee $1 4

#runHash $1 4
#runHash $1 8
#runHash $1 16
#runHash $1 24
#runHash $1 32

for i in `seq 1 10`; do
    let "t=$i"
#    runLee $1 $t
done

for i in `seq 6 16`; do
    let "t=2*$i"
#    runLee $1 $t
done

for i in `seq 1 10`; do
    let "t=$i"
#    runJTest $1 $t
done

for i in `seq 6 16`; do
    let "t=2*$i"
#    runJTest $1 $t
done


#./run.sh soa.paxosstm.benchmark.leetm.Main -1 20 1 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_0_20 |tee ../../results/lee_0_20
#./run.sh soa.paxosstm.benchmark.leetm.Main -1 24 1 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_0_24 |tee ../../results/lee_0_24
#./run.sh soa.paxosstm.benchmark.leetm.Main -1 30 1 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_0_30 |tee ../../results/lee_0_30
#./run.sh soa.paxosstm.benchmark.leetm.Main -1 32 1 ../PaxosSTMTest/mainboard.txt 2> ../../results/err_0_32 |tee ../../results/lee_0_32
