#!/bin/bash

# ./runSB7.sh -1 -g stm -s stmbench7.impl.paxosstm.PaxosSTMInitializer -t 4 -l 30
# uwaga: -XX:-UseSplitVerifier zeby SB7 dzialal w javie 7

cd ../..
java -Djava.util.logging.config.file=logging_none.properties -XX:-UseSplitVerifier -Xmx800m -cp "../JPaxos/bin:../PaxosSTM/bin/:../PaxosSTMsb7/build:../PaxosSTMsb7/lib/deuceAgent.jar:lib/javassist.jar:lib/objenesis-1.2.jar" soa.paxosstm.dstm.Main stmbench7.Benchmark $@
#java -agentpath:"/scratch/linuxadd/programs/yourkit/yjp-11.0.1/bin/linux-x86-32/libyjpagent.so" -Djava.util.logging.config.file=logging_none.properties -XX:-UseSplitVerifier -Xmx512m -cp "../JPaxos/bin:../PaxosSTM/bin/:../PaxosSTMsb7/build:../PaxosSTMsb7/lib/deuceAgent.jar:lib/javassist.jar:lib/objenesis-1.2.jar:/scratch/linuxadd/programs/yourkit/yjp-11.0.1/lib/yjp-controller-api-redist.jar" soa.paxosstm.dstm.Main stmbench7.Benchmark $@
