#!/bin/bash

echo "nodes   file        rounds       av time       av gc     av attempts"

for thread in `seq 1 10`; do

    for i in $( ls lee_-1_$thread ) ; do
	awk -f ../utils/lee-parser-localconsensus.awk $i
	#awk -f ../utils/lee-parser.awk $i
    done
done

for thread in `seq 6 16`; do
    let "t=2*thread"
    for i in $( ls lee_-1_$t ) ; do
	awk -f ../utils/lee-parser-localconsensus.awk $i
	#awk -f ../utils/lee-parser.awk $i
    done
done
