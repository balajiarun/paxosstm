#!/bin/bash
# ../utils/sb7.sh \-r
t=""
if [ $# -eq 1 ]; then
    t=$1
fi

for i in $( ls sb*_0*$t |grep -v err ); do
    awk -f ../utils/sb7.awk $i
done

#grep "Total throughput" *|grep _0 |awk -F":" '{ print $1 $3 }'|awk '{print $1"\t"$2"\t"$4}'|awk -F"(" '{print $1"\t"$2}'
