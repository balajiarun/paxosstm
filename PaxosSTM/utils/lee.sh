#!/bin/bash

let t=0
if [ $# -eq 1 ]; then
    t=$1
fi

for i in $( ls lee_*_0 |grep -v "~"|grep -v err ); do
    awk -f ../utils/lee.awk $i
done