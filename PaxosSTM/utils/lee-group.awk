function reset()
{
    i = 0;
    file = 0;
    rounds = 0;
    av_time = 0;
    av_gc = 0;
    av_addi = 0;
}

function printOut()
{
    print i "\t" file "\t" rounds "\t" av_time/i "\t" av_gc/i "\t" av_addi/i;
}

BEGIN {
    reset();
    started = 0;
}
{
    if (NR == 1)
	print "nodes \t" $0
   
    {
	if (/file/)
	{
	    if (started == 0)
		started = 1;
	    else
	    { 
		printOut();
		reset();
	    }
	}
	else
	{
	    i++;
	    file = $1
	    rounds = $2;
	    av_time += $3; 
	    av_gc += $4;
	    av_addi += $5;
  }
    }
}
END {
    printOut();
#  print "rounds \t av time \t av gc \t av addi"
 #   print rounds
#    print FILENAME "\t\t" rounds "\t" time/(rounds-1) "\t\t" gc/(rounds-1) "\t\t" additional/(rounds-1);
}