#!/bin/bash

# args: max number of replicas (for 4 nodes - 3)
# args: [ number of threads ] 

if [ $# -eq 2 ]; then
    let flag=1
    let threads=$2
else
    let flag=0;
    let threads=''
fi

if [ $flag -eq 1 ]; then
    awk -f ../utils/hash-parser.awk hash_0_$threads|head -1

    for i in `seq 0 $1`; do
#	echo hash_$i\_$threads
	awk -f ../utils/hash-parser.awk hash_$i\_$threads|tail -1
    done;
else

    awk -f ../utils/hash-parser.awk hash_0|head -1

    for i in `seq 0 $1`; do
	awk -f ../utils/hash-parser.awk hash_$i|tail -1
    done;

fi
