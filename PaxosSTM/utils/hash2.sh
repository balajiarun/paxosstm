#!/bin/bash

let t=0
if [ $# -eq 1 ]; then
    t=$1
fi

for i in $( ls hash-*--_*_$t\_* |grep -v err|grep -v "~"); do
    awk -f ../utils/hash2.awk $i
done