#!/bin/bash

t=""
if [ $# -eq 1 ]; then
    t=$1
fi

for i in $( ls vc*_*_0*$t |grep -v "~" |grep -v err); do
    awk -f ../utils/vacation.awk $i
done
