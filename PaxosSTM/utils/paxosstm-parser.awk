function printhead()
{
    print "size\trange\tthreads\trotran\trwtran\ttotal\tommit\tinst_avg\tabort_rate"
}

function printresults(size, range, threads, rotran, rwtran, total, ommit, inst, ga, la)
{
    ppp=threads*(rotran+rwtran)
#    print size "\t" range "\t" threads "\t" rotran "\t" rwtran "\t" total "\t" ommit "\t" inst/(total-ommit) "\t\t" (ga+la)/(threads*(rotran+twtran))/(total-ommit)
    print size "\t" range "\t" threads "\t" rotran "\t" rwtran "\t" total "\t" ommit "\t" inst/(total-ommit) "\t\t" (ga+la)/ppp/(total-ommit)
}

BEGIN{
    printhead()
}
{
    ommit=10

    if ($0 ~ /round/) {
	triggered=1;
	i = 0;
	trsum=0
	ga=0
	la=0
	threads=0
	rotran=0
	rwtran=0
	size=0
	range=0
    }
    if (triggered) {
	if ($0 ~ /DSTM/) {
	    triggered=0;
	    printresults(size, range, threads, rotran, rwtran, i, ommit, trsum, ga, la)
	}
	else
	{
	    if ($0 !~ /round/ && $0 !~ /DSTM/)
	    {
		if (i == 0)
		{
		    size=$2
		    range=$3
		    threads=$4
		    rotran=$5
		    rwtran=$6
		}
		if (i >= ommit)
		{
		    trsum=trsum+$8
		    ga=ga+$9
		    la=la+$13
		}
		i=i+1
	    }
	}
    }
}
END {
    printresults(size, range, threads, rotran, rwtran, i, ommit, trsum, ga, la)
#    printresults(i, ommit, trsum, ga, la)
}