#!/bin/bash

# arg: max nr of replica (for 4 replicas - 3)

for i in `seq 1 10`; do
    ../utils/hash-parser.sh $1 $i
done

for i in `seq 6 16`; do
    let "t=2*i"
    ../utils/hash-parser.sh $1 $t
done


