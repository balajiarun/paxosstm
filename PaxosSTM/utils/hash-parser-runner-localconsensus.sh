#!/bin/bash

../utils/hash-parser-localconsensus.sh 1|head -1

for i in `seq 1 10`; do
    ../utils/hash-parser-localconsensus.sh $i|tail -1
done

for i in `seq 6 16`; do
    let "t=2*i"
    ../utils/hash-parser-localconsensus.sh $t|tail -1
done


