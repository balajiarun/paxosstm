function printresults(th1, th2, ar)
{
    split(th2, tab, "(")
    print FILENAME "\t" th1 "\t\t" tab[2] "\t\t" ar
}

BEGIN{
    tp1=0
    tp2=0
    ar=0
}
{
    if ($0 ~ /Total throughput/)
    {
        tp1=$3
        tp2=$5
    }
    else if ($0 ~ /^abort rate:/)
    {
#       print ">"$3"<    >"$2"<    >"$4"<   >>>>>>>>"$0 
        ar=$3
    }
}
END {
    printresults(tp1, tp2, ar)
}