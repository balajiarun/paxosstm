function printresults(th, trsum, arsum, i)
{
    FS = "_"
    percent = FILENAME
    #sub(/hash_/,"",percent)
    split(percent, tab, "_")
    print FILENAME " \t\t" th "\t" timesum/(i-ommit) "\t\t" arsum/(i-ommit)
}

BEGIN{

}
{
    ommit=1
    if ($0 ~ /spie/)
	print bdfdf
    else
    {

    if ($0 ~ /rNr/) {
	triggered=1;
	i = 0;
	th = 0;
	timesum = 0;
	arsum = 0;
    }
    if (triggered) {
	if ($0 ~ /DSTM/) {
#	    triggered=0;
#	    printresults(th, timesum, arsum, i)
	}
	else
	{
	    if ($0 !~ /rNr/ && $0 !~ /DSTM/)
	    {
		if (i == 0)
		{
		    th=$2
		}
		if (i >= ommit)
		{
		    timesum=timesum+$3
		    arsum=arsum+$13
		}
		i=i+1
	    }
	}
    }
    }
}
END {
    printresults(th, trsum, arsum, i)
}