BEGIN {
  rounds = 0;
  time = 0;
  gc = 0;
  attempts = 0;
  joints;
}
{
  i = NR - 1;
  if (i >= 3)
    {
      #if (i % (3012 + 5) == 0)
	if (NF == 7)
	{
#	  print $0;
	  joints = $2;
	  rounds++;
	  if (rounds > 1)
	    {
	      time += $4;
	      gc += $6;
	    }
	}
      if (NF == 2)
	{
	#  print $0;
	  if (rounds > 1)
	    {
	    attempts += $2;
}
	}
    }
}
END {
#  print "rounds \t av time \t av gc \t av addi"
 #   print rounds
    print FILENAME "\t\t" rounds "\t" time/(rounds-1) "\t\t" gc/(rounds-1) "\t\t" attempts/(rounds-1);
}