#!/bin/bash
results=../../results

let consensusrun=1
let abcastrun=1
let paxosstmrun=1
let javarun=1

if [ "$1" == 0 ]; then
    consensusrun=0
else
    consensusrun=1
fi
if [ "$2" == 0 ]; then
    abcastrun=0
else
    abcastrun=1
fi
if [ "$3" == 0 ]; then
   paxosstmrun=0
else
    paxosstmrun=1
fi
if [ "$4" == 0 ]; then
   javarun=0
else
   javarun=1
fi

echo $results

if [ ! -d $results ]; then
    mkdir $results
fi

let r=50
let counter=0

if [ "$consensusrun" == 1 ]; then
    echo running consensus
    ./consensusTest.sh -1 20 10000 1 $r > $results/consensus.txt
    ./consensusTest.sh -1 20 10000 10 $r >> $results/consensus.txt
    ./consensusTest.sh -1 20 10000 100 $r >> $results/consensus.txt
    ./consensusTest.sh -1 20 10000 1000 $r >> $results/consensus.txt
    echo consensus done
fi

if [ "$abcastrun" == 1 ]; then
    echo running abcast
    ./abcastTest.sh -1 2 20 10000 1 $r > $results/abcast.txt
    ./abcastTest.sh -1 2 20 10000 10 $r >> $results/abcast.txt
    ./abcastTest.sh -1 2 20 10000 100 $r >> $results/abcast.txt
    ./abcastTest.sh -1 2 20 10000 1000 $r >> $results/abcast.txt
    echo abcast done
fi

if [ "$paxosstmrun" == 1 ]; then
    echo running paxosstm
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 90 10 1 $r > $results/5p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 50 50 1 $r >> $results/5p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 10 90 1 $r >> $results/5p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 90 10 2 $r > $results/10p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 50 50 2 $r >> $results/10p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 10 90 2 $r >> $results/10p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 90 10 3 $r > $results/15p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 50 50 3 $r >> $results/15p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 10 90 3 $r >> $results/15p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 90 10 4 $r > $results/20p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 50 50 4 $r >> $results/20p.txt
    echo bing $counter
    let counter=$counter+1
    ./run.sh soa.paxosstm.example.Main -1 4 20 10000 10000 10 90 4 $r >> $results/20p.txt
fi

if [ "$javarun" == 1 ]; then
    echo javarun
    ./run.sh soa.paxosstm.example.Main -1 5 > $results/java5.txt
    ./run.sh soa.paxosstm.example.Main -1 6 > $results/java6.txt
    ./run.sh soa.paxosstm.example.Main -1 7 > $results/java7.txt
    echo javadone
fi