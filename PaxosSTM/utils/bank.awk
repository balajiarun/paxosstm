function printresults(th, trsum, arsum, i, latsum)
{
    FS = "_"
    percent = FILENAME
    #sub(/hash_/,"",percent)
    split(percent, tab, "_")
    print FILENAME "\t\t" th "\t" tab[2] "\t" trsum/(i-ommit) "\t\t" arsum/(i-ommit) "\t\t" latsum/(i-ommit)
}

BEGIN{

}
{
    ommit=3
    if ($0 ~ /spie/)
	print bdfdf
    else
    {

    if ($0 ~ /round/) {
	triggered=1;
	i = 0;
	th = 0;
	trsum = 0;
	arsum = 0;
	latsum = 0;
    }
    if (triggered) {
	if ($0 ~ /DSTM/) {
#	    triggered=0;
#	    printresults(th, trsum, arsum, i, latsum)
	}
	else
	{
	    if ($0 !~ /round/ && $0 !~ /DSTM/)
	    {
		if (i == 0)
		{
		    th=$4;
		}
		if (i >= ommit)
		{
		    trsum=trsum+$11
		    arsum=arsum+$12
#		    latsum=latsum+$15
		}
		i=i+1
	    }
	}
    }
    }
}
END {
    printresults(th, trsum, arsum, i, latsum)
}