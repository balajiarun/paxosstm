%
%  ITSOA Javadoc Front Page LaTeX class definition
%
%  $Id$
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{javadocfront}[2009/03/23 v1.0 ITSOA Javadoc Front Page class]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass{article}


\RequirePackage{geometry}
\RequirePackage[pagestyles,clearempty,psfloats,newlinetospace]{titlesec}
\RequirePackage[theorems]{dcslib}[2009/01/20]
\RequirePackage[scaled=0.82]{beramono}
\RequirePackage{lastpage}
\RequirePackage[dotinlabels]{titletoc}
\RequirePackage[nottoc,notlot,notlof,numbib]{tocbibind}
\RequirePackage{booktabs}
\RequirePackage{translator}
\RequirePackage{svn-multi}
\RequirePackage{flafter}


\usedictionary{itsoa}
\usedictionary{translator-basic-dictionary}


% default page layout
\newlength{\bindinglen}
\setlength{\bindinglen}{1cm}
\geometry{a4paper,tmargin=2.9cm,bmargin=2.5cm,lmargin=4cm,rmargin=2cm,
          headheight=2cm,headsep=0.9cm,footskip=1.2cm,
          marginparwidth=1.7cm,marginparsep=0.3cm,
          bindingoffset=\bindinglen}

% twoside
\DeclareOption{twoside}{
  \geometry{a4paper,twoside=true,lmargin=4cm,rmargin=2cm,
            asymmetric,
            bindingoffset=\bindinglen}
}


% fonts
\newcommand{\secfont}{\sffamily}
\def\normalsize{\@setfontsize{\normalsize}{10.27pt}{12pt}}

%listings
\lstset{style=linesbg}

% matters
\newif\if@mainmatter \@mainmattertrue
\newcommand\mainmatter{%
  \cleardoublepage
  \@mainmattertrue
  \pagenumbering{arabic}}
\newcommand\backmatter{%
  \if@openright
    \cleardoublepage
  \else
    \clearpage
  \fi
  \@mainmatterfalse}


% default colors
\lstset{language=c}
\lstset{keywordstyle=\bfseries\color{dkgreen},
        commentstyle=\itshape\color{dkblue},
        stringstyle=\color{dkred},
        directivestyle=\color{directive}}
\lstset{language=}


% lists spacing
\def\enumerate{%
  \ifnum \@enumdepth >\thr@@\@toodeep\else
    \advance\@enumdepth\@ne
    \edef\@enumctr{enum\romannumeral\the\@enumdepth}%
      \expandafter
      \list
        \csname label\@enumctr\endcsname
        {\usecounter\@enumctr\def\makelabel##1{\hss\llap{##1}}}%
        \if@firmlists\firmlist\fi%
  \fi}

\def\itemize{%
  \ifnum \@itemdepth >\thr@@\@toodeep\else
    \advance\@itemdepth\@ne
    \edef\@itemitem{labelitem\romannumeral\the\@itemdepth}%
    \expandafter
    \list
      \csname\@itemitem\endcsname
      {\def\makelabel##1{\hss\llap{##1}}}%
      \if@firmlists\firmlist\fi%
  \fi}

\renewenvironment{description}
  {\list{}{\labelwidth\z@ \itemindent-\leftmargin
           \let\makelabel\descriptionlabel}%
           \if@firmlists\firmlist\fi}
  {\endlist}


% additional commands
\newcommand{\@obtitle}{}
\newcommand{\obtitle}[1]{\renewcommand{\@obtitle}{#1}}
\newcommand{\@ob}{}
\newcommand{\ob}[1]{\renewcommand{\@ob}{#1}}
\newcommand{\@subtitle}{\translate{Report on works performed within project} \@ob}
\newcommand{\subtitle}[1]{\renewcommand{\@subtitle}{#1}}
\newcommand{\@infolinei}{\translate{Period}:}
\newcommand{\infolinei}[1]{\renewcommand{\@infolinei}{#1}}
\newcommand{\@infovaluei}{}
\newcommand{\infovaluei}[1]{\renewcommand{\@infovaluei}{#1}}
\newcommand{\@infolineii}{\translate{Coordinator of the project}:}
\newcommand{\infolineii}[1]{\renewcommand{\@infolineii}{#1}}
\newcommand{\@infolineiii}{\translate{Authors}:}
\newcommand{\infolineiii}[1]{\renewcommand{\@infolineiii}{#1}}
\newcommand{\@reportnumber}{}
\newcommand{\reportnumber}[1]{\renewcommand{\@reportnumber}{#1}}
\newcommand{\@infovalueii}{}
\newcommand{\infovalueii}[1]{\renewcommand{\@infovalueii}{#1}}
\newcommand{\@keywords}{}
\newcommand{\keywords}[1]{\renewcommand{\@keywords}{#1}}
\newcommand{\@version}{}
\newcommand{\version}[1]{\renewcommand{\@version}{#1}}
\newcommand{\@accesslevel}{\translate{Internal}}
\newcommand{\accesslevel}[1]{\renewcommand{\@accesslevel}{#1}}

% add history of changes from file history.tex
\newcommand{\@historyOfChangesSec}{\translate{History of changes}}
\newcommand{\historyOfChanges}{
  \section*{\@historyOfChangesSec}
  {\sffamily\fontsize{8.1}{11}\selectfont\begin{longtable}{p{1cm} p{3cm} p{1.5cm} p{6.8cm}}
  \textbf{\translate{Revision}} &
  \textbf{\translate{Author}}  &
  \textbf{\translate{Date}} &
  \textbf{\translate{Description}}\\
  \hline \endhead
  \input{svn-history}
  \end{longtable}}
  \clearpage
}

\newif\if@includeob
\@includeobfalse
\DeclareOption{ob}{
  \@includeobtrue
}

\newif\if@includeauthors
\@includeauthorsfalse
\DeclareOption{authors}{
  \@includeauthorstrue
}

\newif\if@includesvndate
\@includesvndatefalse
\DeclareOption{svndate}{
  \@includesvndatetrue
  \input{svn-users.tex}
}

\newif\if@includehistory
\@includehistoryfalse
\DeclareOption{history}{
  \@includehistorytrue
}


% heading indent
\setlength{\headindent}{2cm}
\widenhead[\headindent][0pt]{\headindent}{0pt}

\providecommand{\dataRaportu}{brak daty}
\renewcommand{\date}[1]{\gdef\@date{#1}\renewcommand{\dataRaportu}{#1}}

\newcommand{\HeadFirstLine}{Nowe technologie informacyjne dla elektronicznej gospodarki i spo\l{}ecze\'nstwa informacyjnego oparte na paradygmacie SOA}
\newcommand{\HeadSecondLine}{\@subtitle\ %
  \translate{for}\ \@infovaluei \hfill%
  \ifthenelse{\equal{\@version}{}}{}{\@version}}

% page styles
\newpagestyle{main}[\normalsize]{
  \headrule
  \footrule
  \sethead{\parbox[b]{\linewidth}
    {\fontsize{8.1}{12}\selectfont\resizebox{\linewidth}{\height}{\HeadFirstLine}\\\HeadSecondLine\vspace{0.8ex}}}{}{}
  \renewcommand{\makefootrule}{\rule[1.2\baselineskip]{\linewidth}{0.4pt}}
  \setfoot[\thepage/\pageref{LastPage}][\@date][Sie\'c Naukowa IT-SOA]%
          {Sie\'c Naukowa IT-SOA}{\@date}{\thepage/\pageref{LastPage}}
}
\pagestyle{main}


% titlesec
\titleformat{\section}{\fontsize{18}{20}\sffamily\bfseries\filright}
            {\parbox[b]{1cm}{\thesection.}}{0cm}{}
\titlespacing*{\section}{-\headindent}{3.5ex plus 1ex minus .2ex}{2.3ex plus .2ex}

\titleformat{\subsection}{\fontsize{13}{15}\sffamily\bfseries\filright}
            {\hspace{-5mm}\parbox[b]{13mm}{\filleft\thesubsection.}}{2mm}{}
\titlespacing*{\subsection}{-\headindent}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

\titleformat{\subsubsection}{\fontsize{12}{13}\sffamily\bfseries\filright}
            {\parbox[b]{1.8cm}{\filleft\thesubsubsection.}}{2mm}{}
\titlespacing*{\subsubsection}{-\headindent}{3.25ex plus 1ex minus .2ex}{1.5ex plus .2ex}

\titleformat{\paragraph}{\fontsize{11}{12}\sffamily\bfseries\filright}{}{0em}{}
\titlespacing*{\paragraph}{0pt}{1ex plus 0.5ex minus .2ex}{0.2ex plus .1ex}


% titletoc
\newlength{\tocsep}
\setlength{\tocsep}{0.6ex}
\titlecontents{section}[1cm]{\vspace{\tocsep}}%
  {\bfseries\contentslabel{1cm}}{\bfseries\hspace{-1cm}}%
  {~\titlerule*[0.2em]{.}\contentspage}[\vspace{\tocsep}]
\titlecontents{subsection}[1cm]{\vspace{\tocsep}}%
  {\contentslabel{1cm}}{\hspace*{0em}}%
  {~\titlerule*[0.2em]{.}\contentspage}[\vspace{\tocsep}]

% numbering and TOC depth
\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{2}

% paragraph separation
\setlength{\parskip}{\medskipamount}
\setlength{\parindent}{0pt}

% full report
\DeclareOption{big}{
  \infolineii{\translate{Coordinator of the research area}:}
  \subtitle{\translate{Report on works performed within research area} \@ob}
}

% abstract environment
\renewenvironment{abstract}{
  \section*{\abstractname}
  \addcontentsline{toc}{section}{\abstractname}
}{\clearpage}

\providecommand{\tabularnewline}{\\}


% float placement
\setcounter{totalnumber}{3}
\setcounter{bottomnumber}{1}
\setcounter{topnumber}{1}
\renewcommand{\textfraction}{0.1}
\renewcommand{\topfraction}{0.8}
\renewcommand{\bottomfraction}{0.7}
\renewcommand{\floatpagefraction}{0.7}


% default date
\date{\ifthenelse{\equal{\svndate}{}}{\today}{\svntoday}}


% title page - here goes the hell ;-)
\newlength{\tmplen}
\renewcommand{\and}{\\}
\renewcommand{\maketitle}{%
  \setlength{\tmplen}{\parskip}
  \setlength{\parskip}{0pt}
  \begin{titlepage}
  \sffamily
  \setlength{\parindent}{0pt}
  \thispagestyle{empty}
  \mbox{}\vspace{-1.8cm}\par
  \rule[-4cm]{0pt}{4cm}\parbox[t]{\linewidth}{%
  \includegraphics[bb=81bp 78bp 343bp 164bp,clip,height=1.2cm]{innowacyjna-gospodarka}%
  \hfill\includegraphics[height=1.2cm]{agh-logo}%
  \hfill\includegraphics[height=1.2cm]{UE-EFRR}\par
  {\sodef\progop{}{.1em}{0.7em plus1em}{2em plus.1em minus.1em}
  \sodef\progopb{}{.040em}{0.25em plus1em}{2em plus.1em minus.1em}
  \fontsize{12.5}{14}\selectfont
  \parbox{\linewidth}{\vspace{3ex}\resizebox{\linewidth}{\height}{\progop{Program Operacyjny Innowacyjna Gospodarka: Dzia\l{}anie 1.3.1}}}\par
  \vspace{0.3cm}
  \parbox[t]{2.3cm}{Projekt:}\parbox[t]{11.7cm}{\progopb{Nowe technologie informacyjne dla elektronicznej gospodarki i spo\l{}ecze\'nstwa informacyjnego oparte na paradygmacie SOA}}}\par
  \rule[-0.3cm]{\linewidth}{0.4pt}}\par
  \if@includeob
    \vspace{-0.5cm}
    \rule{0pt}{0.2cm}\parbox[b]{\linewidth}{\fontsize{13.2}{15}\selectfont\bfseries\raggedright{Raport cz\k{e}\'sciowy z prac wykonywanych w ramach zadania \@ob}}\par
    \rule{0pt}{0.8cm}\parbox[b]{\linewidth}{\fontsize{13.2}{15}\selectfont\raggedright{\@obtitle}}\par
    \vspace{1.85cm}
    \rule{0pt}{2cm}\parbox[b]{\linewidth}{\huge\raggedright\@subtitle}\par
    \vspace{1.5cm}
    \rule[-2cm]{0pt}{2cm}\parbox[t]{\linewidth}{\huge\raggedright\@title}\par
    \vspace{2.5cm}
  \else
    \vspace{2.85cm}
    \rule{0pt}{2cm}\parbox[b]{\linewidth}{\fontsize{14.2}{16}\selectfont\bfseries\raggedright\@subtitle}\par
    \vspace{0.6cm}
    \rule[-2cm]{0pt}{2cm}\parbox[t]{\linewidth}{\huge\raggedright\@title}\par
    \vspace{3.3cm}
  \fi
  \rule[-4cm]{0pt}{4cm}\parbox[t]{\linewidth}{\large\@infolinei\par
    \vspace{0.2cm}{\bfseries\@infovaluei}\par
    \if@includeauthors
      \vspace{1cm}\translate{Authors}:\par
      \vspace{0.2cm}{\bfseries\@author}
    \else
      \vspace{1cm}\@infolineii\par
      \vspace{0.2cm}{\bfseries\@infovalueii}
    \fi%
  }\par
  \vspace{2.35cm}
  \parbox[b]{3cm}{\includegraphics[width=3cm]{SOA-logo}}%
  \hspace{0.2cm}\raisebox{0.25cm}{\large Sie\'c Naukowa Technologii Informacyjnych SOA}
  \end{titlepage}
%  \clearpage
%  \tableofcontents{}
%  \clearpage
  % info o dokumencie
%  \section*{\translate{Document properties}}
%  \addcontentsline{toc}{section}{\translate{Document properties}}
%  \medskip
%  {\sffamily\fontsize{9}{11}\selectfont\begin{tabular}{>{\raggedright}p{2.8cm}>{\raggedright}p{10.3cm}}
%  \addlinespace[5pt]
%  \translate{Report number} & \@reportnumber \tabularnewline\addlinespace[3pt]
%  \midrule
%  \addlinespace[5pt]
%  \translate{Coordinator} & \@infovalueii \tabularnewline\addlinespace[3pt]
%  \midrule
%  \addlinespace[5pt]
%  \translate{Authors} & \@author \tabularnewline\addlinespace[3pt]
%  \midrule
%  \addlinespace[5pt]
%  \if@includesvndate
%    \ifthenelse{\equal{\svndate}{}}{}{%
%      \translate{Last modified} & \svnyear/\svnmonth/\svnday~\svnhour:\svnminute
%      \ifthenelse{\equal{\svnauthor}{}}{}{~\translate{by} \svnFullAuthor{\svnauthor}}
%      \tabularnewline\addlinespace[3pt]
%      \midrule
%      \addlinespace[5pt]}%
%  \fi
%  \translate{Access level} & \@accesslevel \tabularnewline\addlinespace[3pt]
%  \midrule
%  \addlinespace[5pt]
%  \translate{Keywords} & \@keywords \tabularnewline\addlinespace[3pt]
%  \end{tabular}}
%  \clearpage
%  \setlength{\parskip}{\tmplen}
%  \if@includehistory\historyOfChanges\fi
}

% booktabs
\setlength{\heavyrulewidth}{1pt}
\setlength{\lightrulewidth}{0.4pt}
\setlength{\aboverulesep}{0.3ex}
\setlength{\belowrulesep}{0.3ex}


% Options

% B&W
\DeclareOption{bw}{
  \lstset{language=c}
  \lstset{keywordstyle=\bfseries,commentstyle=\itshape,
          stringstyle=,directivestyle=}
  \lstset{language=}
  \definecolor{directive}{rgb}{0, 0, 0}
  \definecolor{linkcolor}{rgb}{0, 0, 0}
  \definecolor{pathcolor}{rgb}{0, 0, 0}
  \definecolor{namecolor}{rgb}{0, 0, 0}
  \definecolor{cmdcolor}{rgb}{0, 0, 0}
  \definecolor{mancolor}{rgb}{0, 0, 0}
  \definecolor{codecolor}{rgb}{0, 0, 0}
}


\ProcessOptions\relax
