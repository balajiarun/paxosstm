package soa.paxosstm.example;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class BenchmarkStats
{
	public int totalLocalConflicts;
	public int totalROTransactions;
	public int totalRWTransactions;
	public long totalTime;
	public int totalNumberOfThreads;

	public BenchmarkStats()
	{
	}
}
