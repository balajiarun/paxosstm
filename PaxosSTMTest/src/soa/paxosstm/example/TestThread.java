/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.example;

import java.util.Random;

import soa.paxosstm.dstm.Transaction;

public class TestThread extends Thread
{
	final A[] vars;
	final int repeats;
	final double writesPercentage;
	final int start;
	final int maxDuration;

	public TestThread(A[] vars, int repeats, double writesPercentage, int start, int maxDuration)
	{
		this.vars = vars;
		this.repeats = repeats;
		this.writesPercentage = writesPercentage;
		this.start = start;
		this.maxDuration = maxDuration;
	}

	@Override
	public void run()
	{
		try
		{
			sleep(start);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		Random random = new Random();
		for (int i = 0; i < repeats; i++)
		{
			final int duration = random.nextInt(maxDuration + 1);
			boolean write = random.nextDouble() < writesPercentage;

			final A a = vars[random.nextInt(vars.length)];
			final A b = vars[random.nextInt(vars.length)];

			if (write)
			{
				new Transaction()
				{
					protected void atomic()
					{
						int d = a.x;
						int d2 = b.x;
						try
						{
							if (duration > 0)
								sleep(duration);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						if (d >= d2)
						{
							b.x = d2 + 1;
						}
						else
						{
							a.x = d + 1;
						}
					}
				};
			}
			else
			{
				new Transaction()
				{
					protected void atomic()
					{
						int d = a.x;
						try
						{
							if (duration > 0)
								sleep(duration);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						int d2 = b.x;
						int diff = d2 - d;
						if (diff == Integer.MAX_VALUE)
						{
							System.out.println("WOW");
						}
					}
				};
			}
		}
	}
}
