package soa.paxosstm.example;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

public class LogParser
{
	public static void main(String[] args)
	{
		// input parameters
		long timeWindow = 1000; //ms
		int resolution = 1000;
		long startOffset = 0;
		long duration = Long.MAX_VALUE / 2;

		try
		{
			timeWindow = Long.parseLong(args[0]);
			resolution = Integer.parseInt(args[1]);
			startOffset = Long.parseLong(args[2]) * 1000;
			duration = Long.parseLong(args[3]) * 1000;
		}
		catch (Throwable e)
		{
		}

		long endTime = startOffset + duration;

		// read entries from input
		TreeSet<LogEntry> entries = new TreeSet<LogEntry>();

		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine())
		{
			LogEntry entry = JPaxosLogEntry.fromString(sc.nextLine());
			if (entry != null)
				entries.add(entry);
		}

		// compute clients' last requests
		Map<Long, Long> lastRequests = new HashMap<Long, Long>();
		for (LogEntry entry : entries)
		{
			Long lastValue = lastRequests.put(entry.threadId, entry.timestamp);
			assert (lastValue == null || lastValue <= entry.timestamp);
		}

		// compute results
		class ResultRecord
		{
			public long timestamp;
			public int requests;
			public int clients;

			public ResultRecord(long timestamp, int requests, int clients)
			{
				this.timestamp = timestamp;
				this.requests = requests;
				this.clients = clients;
			}
		}
		List<ResultRecord> results = new Vector<ResultRecord>();
		
		Iterator<LogEntry> left = entries.iterator();
		Iterator<LogEntry> right = entries.iterator();

		LogEntry leftEntry = left.next();
		int count = 0;
		Set<Long> activeClients = new HashSet<Long>();
		long firstTimestamp = leftEntry.timestamp;

		while (right.hasNext())
		{
			LogEntry rightEntry = right.next();
			if (rightEntry.timestamp - firstTimestamp > endTime)
				break;

			if (rightEntry.end)
			{
				count++;
				activeClients.add(rightEntry.threadId);
				if (lastRequests.get(rightEntry.threadId) <= rightEntry.timestamp)
					activeClients.remove(rightEntry.threadId);
			}
			while (rightEntry.timestamp - leftEntry.timestamp > timeWindow)
			{
				if (leftEntry.end)
					count--;
				leftEntry = left.next();
			}

			if (rightEntry.end && (rightEntry.timestamp - firstTimestamp >= startOffset))
				results.add(new ResultRecord(rightEntry.timestamp, count, activeClients.size()));
		}

		// adjust and output results
		NumberFormat nf = NumberFormat.getInstance(new Locale("pl"));
		System.out.println("\"time\"\t\"req/s\"\t\"clients\"");
		int total = results.size();
		double ratio = total / (resolution - 1);
		for (int i = 0; i < resolution; i++)
		{
			int idx = Math.min((int) (i * ratio), total - 1);
			ResultRecord record = results.get(idx);
			double time = (record.timestamp - firstTimestamp) / 1000.0f;
			double reqPerSec = (1000.0f * record.requests) / timeWindow;
			System.out.println(nf.format(time) + "\t" + nf.format(reqPerSec) + "\t"
					+ record.clients);
		}
	}
}
