package soa.paxosstm.example;

public class JPaxosLogEntry extends LogEntry
{
	public JPaxosLogEntry(long seqNo, long threadId, long sent, long duration)
	{
		super(seqNo, threadId, (sent + duration) / 1000, true, null);
	}

	public static JPaxosLogEntry fromString(String src)
	{
		// awk '{ print FILENAME "\t" $0 }' client-*
		try
		{
			String[] elements = src.split("\t");
			String filename = elements[0];
			int idx1 = filename.indexOf('-');
			int idx2 = filename.indexOf('.');
			long id = Long.parseLong(filename.substring(idx1 + 1, idx2));
			long seqNo = Long.parseLong(elements[1]);
			long sent = Long.parseLong(elements[2]);
			long duration = Long.parseLong(elements[3]);
			return new JPaxosLogEntry(seqNo, id, sent, duration);
		}
		catch (Throwable e)
		{
			return null;
		}
	}
}
