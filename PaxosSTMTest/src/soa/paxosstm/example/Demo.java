/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.example;

import java.util.Scanner;

import soa.paxosstm.common.StorageException;
import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;

public class Demo
{
	// public static void printAll(Object o)
	// {
	// try
	// {
	// Field[] fields = o.getClass().getDeclaredFields();
	// for (Field field : fields)
	// {
	// if (Modifier.isStatic(field.getModifiers()))
	// continue;
	// // if (field.getName().startsWith(ClassAlterer.NAMES_PREFIX))
	// // continue;
	// field.setAccessible(true);
	// String fieldValueString;
	// Object fieldValue = field.get(o);
	// if (fieldValue instanceof Observable)
	// fieldValueString = ((Observable) fieldValue).__tc_getId().toString();
	// else if (fieldValue != null)
	// fieldValueString = fieldValue.toString();
	// else
	// fieldValueString = "null";
	// System.err.println("* " + field.getName() + ": " + fieldValueString +
	// "\n");
	// }
	// }
	// catch (IllegalArgumentException e)
	// {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// catch (IllegalAccessException e)
	// {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// }

	public static void main(String[] args) throws StorageException
	{
		PaxosSTM.getInstance().start();

		Bank myBank = (Bank) PaxosSTM.getInstance().getFromSharedObjectRegistry("Bank");
		if (myBank == null)
		{
			myBank = new Bank();
			new Transaction()
			{
				public void atomic()
				{
				}
			};
			PaxosSTM.getInstance().addIfAbsentToSharedObjectRegistry("Bank", myBank);
			myBank = (Bank) PaxosSTM.getInstance().getFromSharedObjectRegistry("Bank");
		}
		final Bank bank = myBank;

		// new Transaction()
		// {
		// public void atomic()
		// {
		// System.err.println("bank:" + ((Observable) bank).__tc_getId());
		// System.err.println("hashtable:" + ((Observable)
		// bank.accounts).__tc_getId());
		// }
		// };
		// printAll(bank);

		while (true)
		{
			System.out
					.println("PaxosSTM Bank Demo\n"
							+ "  n                              - create a new account\n"
							+ "  r <number>                     - remove the account\n"
							+ "  b <number>                     - check the account's balance\n"
							+ "  d <number> <amount>            - deposit specified amount in the account\n"
							+ "  w <number> <amount>            - withdraw specified amount from the account\n"
							+ "  t <number> <number2> <amount>  - transfer specified amount from one account to another\n"
							+ "  q                              - quit\n" + "    :");
			try
			{
				Scanner sc = new Scanner(System.in);
				final String command = sc.nextLine();
				if (command.length() != 0)
				{
					switch (command.charAt(0))
					{
					case 'n':
						{
							new Transaction()
							{
								public void atomic()
								{
									System.out.println("The newly created account's number: "
											+ bank.createNewAccount() + ".");
								}
							};
							break;
						}
					case 'r':
						{
							new Transaction()
							{
								public void atomic()
								{
									try
									{
										bank.removeAccount(command.substring(2));
										System.out.println("Done.");
									}
									catch (NotFoundException e)
									{
										System.out.println(e.getMessage());
									}
								}
							};
							break;
						}
					case 'b':
						{
							new Transaction()
							{
								public void atomic()
								{
									try
									{
										double res = bank.checkBalance(command.substring(2));
										System.out
												.format("The account's balance is %10.2f.%n", res);
									}
									catch (NotFoundException e)
									{
										System.out.println(e.getMessage());
									}
								}
							};
							break;
						}
					case 'd':
						{
							final String[] params = command.split(" ");
							new Transaction()
							{
								public void atomic()
								{
									try
									{
										bank.deposit(params[1], Double.parseDouble(params[2]));
										System.out.println("Done.");
									}
									catch (NotFoundException e)
									{
										System.out.println(e.getMessage());
									}
								}
							};
							break;
						}
					case 'w':
						{
							final String[] params = command.split(" ");
							new Transaction()
							{
								public void atomic()
								{
									try
									{
										boolean res = bank.withdraw(params[1], Double
												.parseDouble(params[2]));
										if (res)
											System.out.println("Done.");
										else
											System.out.println("Insufficient funds!");
									}
									catch (NotFoundException e)
									{
										System.out.println(e.getMessage());
									}
								}
							};
							break;
						}
					case 't':
						{
							final String[] params = command.split(" ");
							new Transaction()
							{
								public void atomic()
								{
									try
									{
										boolean res = bank.transfer(params[1], params[2], Double
												.parseDouble(params[3]));
										if (res)
											System.out.println("Done.");
										else
											System.out.println("Insufficient funds!");
									}
									catch (NotFoundException e)
									{
										System.out.println(e.getMessage());
									}
								}
							};
							break;
						}
					case 'q':
						System.exit(0);
					default:
						throw new Exception();
					}
					System.out.println();
				}
			}
			catch (Exception e)
			{
				System.out.println("Invalid command");
				e.printStackTrace();
			}
		}
	}
}
