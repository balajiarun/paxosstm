/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.example;

import java.util.Scanner;

import soa.paxosstm.common.StorageException;
import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.example.DataBean.DataBeanType;

public class MainDemo
{
	public static void main(String[] args) throws StorageException, InterruptedException
	{
		ServiceDemo service = new ServiceDemo();
		service.start();

		while (true)
		{
			System.out
					.println("Hello! This is PaxosSTM!\n"
							+ "  c <number>                        - commit\n"
							+ "  G <name> [<type>]                 - get object data [type: String - 0, Integer - 1, Object ref - 2]\n"
							+ "  g <name> [timeout]                - get object data\n"
							+ "  n <name> [timeout]                - create new object\n"
							+ "  p <name> <type> <value> [timeout] - put object data [type: String - 0, Integer - 1, Object ref - 2]\n"
							+ "  d <name> [timeout]                - delete object\n"
							+ " :r <registryName> <name>           - register given object in the registry\n"
							+ " :u <registryName>                  - unregister object from the registry\n"
							+ " :f <registryName>                  - retrieve object from the registry\n"
							+ " #b <name> <n>                      - enter barrier\n"
							+ "  q                                 - quit\n" + "    :");
			try
			{
				Scanner sc = new Scanner(System.in);
				String command = sc.nextLine();
				if (command.length() != 0)
				{
					switch (command.charAt(0))
					{
					case 'y':
						service.create("ala", 0);
						Thread.sleep(250);
						service.put("ala", DataBeanType.String, "Ala", 0);
						Thread.sleep(250);
						service.create("kot", 0);
						Thread.sleep(250);
						service.put("kot", DataBeanType.String, "Kot", 0);
						Thread.sleep(250);
						service.put("ala", DataBeanType.Bean, "kot", 0);
						Thread.sleep(250);
						//service.scheduleCheckpoint();
						break;
					case 'c':
						service.scheduleCheckpoint();
						break;
					case 'G':
						{
							String[] array = command.split(" ");
							String name = array[1];
							String typeString = array.length > 2 ? array[2] : "";
							service.get(name, DataBeanType.convert(typeString), 0);
							break;
						}
					case 'g':
						{
							String[] array = command.split(" ");
							String name = array[1];
							int timeout = (int) (array.length > 2 ? Integer.parseInt(array[2]) : 0) * 1000;
							service.get(name, DataBeanType.All, timeout);
							break;
						}
					case 'n':
						{
							String[] array = command.split(" ");
							String name = array[1];
							int timeout = (int) (array.length > 2 ? Integer.parseInt(array[2]) : 0) * 1000;
							service.create(name, timeout);
							break;
						}
					case 'p':
						{
							String[] array = command.split(" ");
							String name = array[1];
							String typeString = array[2];
							String valueString = array[3];
							int timeout = (int) (array.length > 4 ? Integer.parseInt(array[4]) : 0) * 1000;
							service.put(name, DataBeanType.convert(typeString), valueString,
									timeout);
							break;
						}
					case 'd':
						{
							String[] array = command.split(" ");
							String name = array[1];
							int timeout = (int) (array.length > 2 ? Integer.parseInt(array[2]) : 0) * 1000;
							service.delete(name, timeout);
							break;
						}
					case 'r':
						{
							String[] array = command.split(" ");
							String registryName = array[1];
							String name = array[2];
							service.register(registryName, name);
							break;
						}
					case 'u':
						{
							service.unregister(command.substring(2));
							break;
						}
					case 'f':
						{
							service.getFromRegister(command.substring(2));
							break;
						}
					case 'b':
						{
							String[] array = command.split(" ");
							String name = array[1];
							int n = Integer.parseInt(array[2]);
							PaxosSTM.getInstance().enterBarrier(name, n);
							break;
						}
					case 'q':
						System.exit(0);
					default:
						throw new Exception();
					}
				}
			}
			catch (Exception e)
			{
				System.out.println("Invalid command");
				e.printStackTrace();
			}
		}

	}
}
