/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.example;

import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.Checkpointable;
import soa.paxosstm.common.RecoveryListener;
import soa.paxosstm.common.Storage;
import soa.paxosstm.common.StorageException;
import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.internal.exceptions.TransactionCorruptedException;
import soa.paxosstm.example.DataBean.DataBeanType;
import soa.paxosstm.utils.Holder;
import soa.paxosstm.utils.TransactionalHashTable;

public class ServiceDemo implements Checkpointable, CheckpointListener, RecoveryListener
{
	private TransactionalHashTable<String, DataBean> map;
	// private ObjectWrapper<Map<String, DataBean>> map = new
	// ObjectWrapper<Map<String, DataBean>>(
	// new HashMap<String, DataBean>());
	private Object lock = new Object();
	private boolean serviceReady = false;

	public static final String MAP = "map";

	public ServiceDemo()
	{
	}

	private void waitForServiceReady()
	{
		synchronized (lock)
		{
			while (!serviceReady)
			{
				try
				{
					lock.wait();
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public void start() throws StorageException
	{
		PaxosSTM.getInstance().addCheckpointListener(this);
		PaxosSTM.getInstance().addRecoveryListener(this);
		PaxosSTM.getInstance().start();
		// omijanie recovery przy Crash Stop
		new Transaction()
		{
			protected void atomic()
			{
				map = new TransactionalHashTable<String, DataBean>();
			}
		};
		PaxosSTM.getInstance().addIfAbsentToSharedObjectRegistry(MAP, map);
		map = (TransactionalHashTable<String, DataBean>) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry(MAP);
		serviceReady = true;
	}

	public void create(final String name, final long timeout)
	{
		waitForServiceReady();

		new Thread()
		{
			public void run()
			{
				try
				{
					new Transaction()
					{
						protected void atomic()
						{
							map.put(name, new DataBean());
							try
							{
								if (timeout > 0)
									sleep(timeout);
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						}
					};
				}
				catch (TransactionCorruptedException e)
				{
					System.out.println("Transaction Corrupted:");
					e.printStackTrace();
				}
			}
		}.start();

	}

	public void put(final String name, final DataBeanType type, final String valueString,
			final long timeout)
	{
		waitForServiceReady();

		new Thread()
		{
			public void run()
			{
				try
				{
					new Transaction()
					{
						protected void atomic()
						{
							final DataBean bean = map.get(name);

							switch (type)
							{
							case String:
								bean.string = valueString;
								break;
							case Integer:
								bean.integer = Integer.parseInt(valueString);
								break;
							case Bean:
								bean.bean = map.get(valueString);
								break;
							default:
								break;
							}

							try
							{
								if (timeout > 0)
									sleep(timeout);
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						}
					};
				}
				catch (TransactionCorruptedException e)
				{
					System.out.println("Transaction Corrupted:");
					e.printStackTrace();
				}
			}
		}.start();

	}

	public void get(final String name, final DataBeanType type, final long timeout)
	{
		waitForServiceReady();

		new Thread()
		{
			public void run()
			{
				try
				{
					new Transaction()
					{
						protected void atomic()
						{
							// System.err.println("before");
							final DataBean bean = map.get(name);
							// System.err.println("after");

							switch (type)
							{
							case All:
								System.out.println(bean);
								break;
							case String:
								System.out.println(bean.string);
								break;
							case Integer:
								System.out.println(bean.integer);
								break;
							case Bean:
								System.out.println(bean.bean);
								break;
							}

							try
							{
								if (timeout > 0)
									sleep(timeout);
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						}
					};
				}
				catch (TransactionCorruptedException e)
				{
					System.out.println("Transaction Corrupted:");
					e.printStackTrace();
				}
			}
		}.start();

	}

	public void delete(final String name, final long timeout)
	{
		waitForServiceReady();

		new Thread()
		{
			public void run()
			{
				try
				{
					new Transaction()
					{
						protected void atomic()
						{
							// System.err.println("before");
							map.remove(name);
							// System.err.println("after");

							try
							{
								if (timeout > 0)
									sleep(timeout);
							}
							catch (InterruptedException e)
							{
								e.printStackTrace();
							}
						}
					};
				}
				catch (TransactionCorruptedException e)
				{
					System.out.println("Transaction Corrupted:");
					e.printStackTrace();
				}
			}
		}.start();

	}

	@Override
	public void scheduleCheckpoint()
	{
		PaxosSTM.getInstance().scheduleCheckpoint();
	}

	@Override
	public void onCheckpoint(int seqNumber, Storage storage)
	{
		System.out.println("SERVICE - onCommit");

		// int commitNumber = (Integer) commitData;
		// int logVersion = commitNumber % 2;
		// int logVersion = 0;
		//
		// try
		// {
		// storage.log(MAP + "_" + logVersion, map);
		// }
		// catch (StorageException e)
		// {
		// e.printStackTrace();
		// }
	}

	public void register(final String registryName, final String name)
	{
		final Holder<DataBean> holder = new Holder<DataBean>();
		new Transaction()
		{
			protected void atomic()
			{
				holder.set(map.get(name));
			}
		};

		PaxosSTM.getInstance().addToSharedObjectRegistry(registryName, holder.get());
	}

	public void unregister(final String registryName)
	{
		PaxosSTM.getInstance().removeFromSharedObjectRegistry(registryName);
	}

	public void getFromRegister(final String registerName)
	{

		final DataBean bean = (DataBean) PaxosSTM.getInstance().getFromSharedObjectRegistry(
				registerName);
		new Transaction()
		{
			protected void atomic()
			{
				System.out.println(bean);
			}
		};
	}

	// @SuppressWarnings("unchecked")
	@Override
	public void recoverFromCheckpoint(Storage storage)
	{
		// try
		// {
		// // int commitNumber = (Integer) commitData;
		// // int logVersion = commitNumber % 2;
		// int logVersion = 0;
		//
		// map = (ObjectWrapper<Map<String, DataBean>>) storage.retrieve(MAP +
		// "_" + logVersion);
		// }
		// catch (StorageException ex)
		// {
		// System.out.println("Cannot recover the service layer, exiting...");
		// ex.printStackTrace();
		// System.exit(1);
		// }

		System.out.println("SERVICE - recoverFromCommit");
	}

	@Override
	public void recoveryFinished()
	{
		synchronized (lock)
		{
			serviceReady = true;
			lock.notifyAll();
		}

		System.out.println("SERVICE - recoveryFinished");
	}

	@Override
	public boolean addCheckpointListener(CheckpointListener listener)
	{
		return false;
	}

	@Override
	public boolean addRecoveryListener(RecoveryListener listener)
	{
		return false;
	}

	@Override
	public boolean removeCheckpointListener(CheckpointListener listener)
	{
		return false;
	}

	@Override
	public boolean removeRecoveryListener(RecoveryListener listener)
	{
		return false;
	}

	@Override
	public void onCheckpointFinished(int seqNumber)
	{
	}

}
