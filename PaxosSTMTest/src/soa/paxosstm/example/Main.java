/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.example;

import java.io.Serializable;
import java.util.Random;
import java.util.TreeSet;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;

import org.objenesis.Objenesis;
import org.objenesis.ObjenesisStd;
import org.objenesis.instantiator.ObjectInstantiator;

import soa.paxosstm.benchmark.bank.WorkerThread;
import soa.paxosstm.common.StorageException;
import soa.paxosstm.dstm.ObjectWrapper;
import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.TransactionObject;
import soa.paxosstm.dstm.internal.exceptions.TransactionCorruptedException;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.instrumentation.Observable;
import soa.paxosstm.dstm.internal.local.LocalTransactionManager;
import soa.paxosstm.test.TestCounter;
import soa.paxosstm.utils.Holder;
import soa.paxosstm.utils.TransactionalHashTable;
import soa.paxosstm.utils.TransactionalHashTableIntInt;
import soa.paxosstm.utils.TransactionalMultiQueue;

public class Main
{
	public static void main(String[] args) throws StorageException, InterruptedException
	{
		// Scanner s = new Scanner(System.in);
		// String command = s.nextLine();

		int testType = 1;
		if (args.length >= 1)
		{
			try
			{
				testType = Integer.parseInt(args[0]);
			}
			catch (NumberFormatException e)
			{
				System.err.println("usage: [test_type [parameters]]");
				System.err.println("test_type:");
				System.err.println("* 1 -> manual testing (default)");
				System.err.println("* 2 -> simple stress test, "
						+ "arguments are threads, repeats, rounds");
				System.err.println("* 3 -> hashtable stress test, "
						+ "arguments are threads, capacity, range, reads, writes, rounds");
				System.err
						.println("* 4 -> hashtable stress test2, "
								+ "arguments are threads, capacity, range, read trans, write trans, writes per trans, rounds");
				System.err
						.println("* 9 -> reorder list test (remember to increase flush_time and print numbers of transactions being flushed)");
				System.exit(1);
			}
		}

		switch (testType)
		{
		case 1:
			{
				MainDemo.main(args);
				System.exit(0);
			}
		case 2:
			{
				int threads = 20;
				int repeats = 100;
				int rounds = 30;
				try
				{
					threads = Integer.parseInt(args[1]);
					repeats = Integer.parseInt(args[2]);
					rounds = Integer.parseInt(args[3]);
				}
				catch (Exception e)
				{
				}

				simpleStressTest(threads, repeats, rounds);
				System.exit(0);
			}
		case 3:
			{
				int threads = 20;
				int capacity = 131072;
				int range = 1000;
				int reads = 100;
				int writes = 20;
				int rounds = 30;
				try
				{
					threads = Integer.parseInt(args[1]);
					capacity = Integer.parseInt(args[2]);
					range = Integer.parseInt(args[3]);
					reads = Integer.parseInt(args[4]);
					writes = Integer.parseInt(args[5]);
					rounds = Integer.parseInt(args[6]);
				}
				catch (Exception e)
				{
				}

				hashtableStressTest(threads, capacity, range, reads, writes, rounds);
				System.exit(0);
			}
		case 4:
			{
				int threads = 20;
				int capacity = 10000;
				int range = 10000;
				int reads = 50;
				int writes = 50;
				int writesPerTrans = 1;
				int rounds = 30;
				int readsPerTran = 200;
				int operationsPerWriteTran = 20;
				try
				{
					threads = Integer.parseInt(args[1]);
					capacity = Integer.parseInt(args[2]);
					range = Integer.parseInt(args[3]);
					reads = Integer.parseInt(args[4]);
					writes = Integer.parseInt(args[5]);
					writesPerTrans = Integer.parseInt(args[6]);
					rounds = Integer.parseInt(args[7]);
					readsPerTran = Integer.parseInt(args[8]);
					operationsPerWriteTran = Integer.parseInt(args[9]);
				}
				catch (Exception e)
				{
				}

				hashtableStressTest2(threads, capacity, range, reads, writes, readsPerTran,
						operationsPerWriteTran, writesPerTrans, rounds);
				for (int round = rounds;; round++)
				{
					Thread.sleep(1000);
					PaxosSTM.getInstance().scheduleCheckpoint();
				}
				// System.exit(0);
			}
		case 44:
			{
				int threads = 20;
				int capacity = 10000;
				int range = 10000;
				int reads = 50;
				int writes = 50;
				int writesPerTrans = 1;
				int rounds = 30;
				int readsPerTran = 200;
				int operationsPerWriteTran = 20;
				try
				{
					threads = Integer.parseInt(args[1]);
					capacity = Integer.parseInt(args[2]);
					range = Integer.parseInt(args[3]);
					reads = Integer.parseInt(args[4]);
					writes = Integer.parseInt(args[5]);
					writesPerTrans = Integer.parseInt(args[6]);
					rounds = Integer.parseInt(args[7]);
					readsPerTran = Integer.parseInt(args[8]);
					operationsPerWriteTran = Integer.parseInt(args[9]);
				}
				catch (Exception e)
				{
				}

				hashtableStressTest2Distributed(threads, capacity, range, reads, writes,
						readsPerTran, operationsPerWriteTran, writesPerTrans, rounds);
				System.exit(0);
				for (int round = rounds;; round++)
				{
					Thread.sleep(1000);
					PaxosSTM.getInstance().scheduleCheckpoint();
				}
			}
		case 444:
			{
				int threads = 20;
				int capacity = 10000;
				int range = 10000;
				int reads = 50;
				int writes = 50;
				int writesPerTrans = 2;
				int rounds = 30;
				int readsPerTran = 100;
				int operationsPerWriteTran = 10;
				int mills = 10000;
				try
				{
					threads = Integer.parseInt(args[1]);
					capacity = Integer.parseInt(args[2]);
					range = Integer.parseInt(args[3]);
					reads = Integer.parseInt(args[4]);
					writes = Integer.parseInt(args[5]);
					writesPerTrans = Integer.parseInt(args[6]);
					rounds = Integer.parseInt(args[7]);
					readsPerTran = Integer.parseInt(args[8]);
					operationsPerWriteTran = Integer.parseInt(args[9]);
					mills = Integer.parseInt(args[10]);
				}
				catch (Exception e)
				{
				}

				int percent = 100 * writes / (reads + writes);

				hashtableStressTest2DistributedWithTime(threads, capacity, range, percent,
						readsPerTran, operationsPerWriteTran, writesPerTrans, rounds, mills);
				System.exit(0);
				for (int round = rounds;; round++)
				{
					Thread.sleep(1000);
					PaxosSTM.getInstance().scheduleCheckpoint();
				}
			}
		case 4444:
		{
			int threads = 20;
			int capacity = 10000;
			int range = 10000;
			int reads = 50;
			int writes = 50;
			int writesPerTrans = 2;
			int rounds = 30;
			int readsPerTran = 100;
			int operationsPerWriteTran = 10;
			int mills = 10000;
			try
			{
				threads = Integer.parseInt(args[1]);
				capacity = Integer.parseInt(args[2]);
				range = Integer.parseInt(args[3]);
				reads = Integer.parseInt(args[4]);
				writes = Integer.parseInt(args[5]);
				writesPerTrans = Integer.parseInt(args[6]);
				rounds = Integer.parseInt(args[7]);
				readsPerTran = Integer.parseInt(args[8]);
				operationsPerWriteTran = Integer.parseInt(args[9]);
				mills = Integer.parseInt(args[10]);
			}
			catch (Exception e)
			{
			}

			int percent = 100 * writes / (reads + writes);

			hashtableStressTest2DistributedWithTimeAndCrash(threads, capacity, range, percent,
					readsPerTran, operationsPerWriteTran, writesPerTrans, rounds, mills);
			System.exit(0);
			for (int round = rounds;; round++)
			{
				Thread.sleep(1000);
				PaxosSTM.getInstance().scheduleCheckpoint();
			}
		}
		case 5:
			{
				int threads = 10;
				int operations = 100000000;
				int rounds = 30;
				try
				{
					threads = Integer.parseInt(args[1]);
					operations = Integer.parseInt(args[2]);
					rounds = Integer.parseInt(args[3]);
				}
				catch (Exception e)
				{
				}

				jvmStressTest1(threads, operations, rounds);
				System.exit(0);
			}
		case 6:
			{
				int threads = 10;
				int size = 10000;
				int operations = 1000000;
				int rounds = 30;
				try
				{
					threads = Integer.parseInt(args[1]);
					size = Integer.parseInt(args[2]);
					operations = Integer.parseInt(args[3]);
					rounds = Integer.parseInt(args[4]);
				}
				catch (Exception e)
				{
				}

				jvmStressTest2(threads, size, operations, rounds);
				System.exit(0);
			}
		case 7:
			{
				int threads = 10;
				int size = 100000;
				int operations = 2000000;
				int rounds = 30;
				try
				{
					threads = Integer.parseInt(args[1]);
					size = Integer.parseInt(args[2]);
					operations = Integer.parseInt(args[3]);
					rounds = Integer.parseInt(args[4]);
				}
				catch (Exception e)
				{
				}

				jvmStressTest3(threads, size, operations, rounds);
				System.exit(0);
			}
		case 8:
			{
				int threads = 20;
				int operations = 50000;
				int rounds = 30;
				try
				{
					threads = Integer.parseInt(args[1]);
					operations = Integer.parseInt(args[2]);
					rounds = Integer.parseInt(args[3]);
				}
				catch (Exception e)
				{
				}

				jvmStressTest4(threads, operations, rounds);
				System.exit(0);
			}
		case 9:
			{
				reorderListTest();
				System.exit(0);
			}
		case 10:
			{
				try
				{
					nestedSequentialCommitRollbackTest();
				}
				catch (TransactionCorruptedException e)
				{
					System.err.println("TRANSACTION CORRUPTED EXCEPTION");
					e.printStackTrace();
					System.err.println("TRANSACTION CORRUPTED EXCEPTION - CAUSE");
					if (e.getCause() != null)
						e.getCause().printStackTrace();
				}
				System.exit(0);
			}
		case 11:
			{
				threadedTransactionsTest();
				System.exit(0);
			}
		case 12:
			{
				transactionalQueueTest();
				System.exit(0);
			}
		default:
			{
				System.err.println("test type needs to be in {1,2,3,4,5,6,7,8,9}.");
				System.exit(1);
			}
		}
	}

	private static void transactionalQueueTest() throws StorageException
	{
		PaxosSTM.getInstance().start();

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				TransactionalMultiQueue<Integer> queue = new TransactionalMultiQueue<Integer>(10);
				Object o = PaxosSTM.getInstance().addIfAbsentToSharedObjectRegistry("queue", queue);
				if (o != null)
					rollback();
			}
		};

		try
		{
			Thread.sleep(10000);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		PaxosSTM.getInstance().enterBarrier("barrier", 3);
		System.out.println("first barrier passed");

		for (int i = 0; i < 100; i++)
		{
			final Holder<Integer> counter = new Holder<Integer>();
			counter.set(i);

			new Transaction()
			{
				@SuppressWarnings("unchecked")
				@Override
				protected void atomic()
				{
					System.err.print(".");
					TransactionalMultiQueue<Integer> queue = (TransactionalMultiQueue<Integer>) PaxosSTM
							.getInstance().getFromSharedObjectRegistry("queue");

					int myOffer = counter.get() * 100 + PaxosSTM.getInstance().getId();
					queue.offer(myOffer);

					System.out.println(PaxosSTM.getInstance().getId() + " offers " + myOffer);
				}
			};
		}

		for (int i = 0; i < 100; i++)
		{
			final Holder<Integer> counter = new Holder<Integer>();
			counter.set(i);

			new Transaction()
			{
				@Override
				protected void atomic()
				{
					TransactionalMultiQueue<Integer> queue = (TransactionalMultiQueue<Integer>) PaxosSTM
							.getInstance().getFromSharedObjectRegistry("queue");

					Integer value = queue.poll();

					System.out.println(PaxosSTM.getInstance().getId() + " polls " + value);
				}
			};
		}

		PaxosSTM.getInstance().enterBarrier("barrier", 3);
	}

	private static void jvmStressTest1(final int numberOfThreads, final int operations,
			final int numberOfRounds) throws StorageException, InterruptedException
	{
		long totalTime = 0;
		long totalRes = 0;

		class TestThread extends Thread
		{
			public long start;
			public long stop;
			public int res;

			public TestThread()
			{
			}

			public void run()
			{
				start = System.currentTimeMillis();

				Random rand = new Random(start + hashCode());

				int x = rand.nextInt(1000);
				res = rand.nextInt(65536);
				for (int i = 0; i < operations; i++)
				{
					res *= x;
					res += x;
					res &= 0xFFFF;
				}

				stop = System.currentTimeMillis();
			}

		}

		System.out
				.println("round   threads   operations per thread   avg time   operations per sec");
		for (int round = 1; round <= numberOfRounds; round++)
		{
			totalTime = 0;

			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new TestThread();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				totalRes += threads[i].res;
				threads[i] = null;
			}
			threads = null;
			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d     %8d   %10d           %8.3f   %14.2f   %7d\n";
			System.out.format(format, round, numberOfThreads, operations, avgTime,
					(numberOfThreads * operations) / avgTime, totalRes);
		}
	}

	private static void jvmStressTest2(final int numberOfThreads, final int size,
			final int operations, final int numberOfRounds) throws StorageException,
			InterruptedException
	{
		long totalTime = 0;
		long totalRes = 0;

		class TestThread extends Thread
		{
			public long start;
			public long stop;
			public TreeSet<Integer> set = new TreeSet<Integer>();

			public TestThread()
			{
			}

			public void run()
			{
				start = System.currentTimeMillis();

				Random rand = new Random(start + hashCode());

				for (int i = 0; i < operations; i++)
				{
					int x = rand.nextInt();
					set.add(x);
					if (set.size() > size)
						set.clear();
				}

				stop = System.currentTimeMillis();
			}

		}

		System.out
				.println("round   threads   operations per thread   avg time   operations per sec");
		for (int round = 1; round <= numberOfRounds; round++)
		{
			totalTime = 0;

			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new TestThread();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				totalRes += threads[i].set.first();
				threads[i] = null;
			}
			threads = null;
			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d     %8d   %10d           %8.3f   %14.2f   %7d\n";
			System.out.format(format, round, numberOfThreads, operations, avgTime,
					(numberOfThreads * operations) / avgTime, totalRes % 1000);
		}
	}

	private static void jvmStressTest3(final int numberOfThreads, final int size,
			final int operations, final int numberOfRounds) throws StorageException,
			InterruptedException
	{
		class TestClass
		{
			public int x;
		}

		Objenesis objenesis = new ObjenesisStd();
		final ObjectInstantiator instantiator = objenesis.getInstantiatorOf(TestClass.class);

		long totalTime = 0;
		long totalRes = 0;

		class TestThread extends Thread
		{
			public long start;
			public long stop;
			public TestClass[] array = new TestClass[size];

			public TestThread()
			{
			}

			public void run()
			{
				start = System.currentTimeMillis();

				Random rand = new Random(start + hashCode());

				for (int i = 0; i < operations; i++)
				{
					int x = rand.nextInt();
					int oldx;
					if (array[i % size] != null)
						oldx = array[i % size].x;
					else
						oldx = 0;
					array[i % size] = (TestClass) instantiator.newInstance();
					array[i % size].x = x + oldx;
				}

				stop = System.currentTimeMillis();
			}

		}

		System.out
				.println("round   threads   operations per thread   avg time   operations per sec");
		for (int round = 1; round <= numberOfRounds; round++)
		{
			totalTime = 0;

			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new TestThread();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				totalRes += threads[i].array[size - 1].x;
				threads[i] = null;
			}
			threads = null;
			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d     %8d   %10d           %8.3f   %14.2f   %7d\n";
			System.out.format(format, round, numberOfThreads, operations, avgTime,
					(numberOfThreads * operations) / avgTime, totalRes % 1000);
		}
	}

	@SuppressWarnings("unchecked")
	private static void jvmStressTest4(final int numberOfThreads, final int operations,
			final int numberOfRounds) throws StorageException, InterruptedException
	{
		long totalTime = 0;

		class TestThread extends Thread
		{
			public long start;
			public long stop;

			private LinkedBlockingQueue<Integer> in;
			private LinkedBlockingQueue<Integer> out;

			public TestThread(LinkedBlockingQueue<Integer> in, LinkedBlockingQueue<Integer> out)
			{
				this.in = in;
				this.out = out;
			}

			public void run()
			{
				start = System.currentTimeMillis();

				// Random rand = new Random(start + hashCode());
				// int x = rand.nextInt(1000);

				for (int i = 0; i < operations; i++)
				{
					synchronized (out)
					{
						out.add(i);
						out.notifyAll();
					}
					synchronized (in)
					{
						try
						{
							while (in.peek() == null)
								in.wait();
							Integer res = in.poll();
							if (res == null)
							{
								System.out.println("should NOT happen!");
								System.exit(1);
							}
							// in.take();
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
					}
				}

				stop = System.currentTimeMillis();
			}

		}

		System.out
				.println("round   threads   operations per thread   avg time   operations per sec");
		for (int round = 1; round <= numberOfRounds; round++)
		{
			totalTime = 0;

			LinkedBlockingQueue<Integer>[] queues = new LinkedBlockingQueue[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				queues[i] = new LinkedBlockingQueue<Integer>();
			}

			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new TestThread(queues[(i % 2 == 0) ? i + 1 : i - 1], queues[i]);
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				threads[i] = null;
			}
			threads = null;
			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d     %8d   %10d           %8.3f   %14.2f\n";
			System.out.format(format, round, numberOfThreads, operations, avgTime,
					(numberOfThreads * operations) / avgTime);
		}
	}

	@SuppressWarnings("unused")
	private static void objenesisTest()
	{
		class TestClass
		{
			public int x;

			public TestClass(int x)
			{
				this.x = x;
			}
		}

		Objenesis objenesis = new ObjenesisStd();
		ObjectInstantiator instantiator = objenesis.getInstantiatorOf(TestClass.class);

		long start, stop;
		final int repeats = 1000000;
		final int rounds = 30;

		for (int round = 1; round <= rounds; round++)
		{
			System.out.println("Round " + round + ": ");

			TestClass[] table = new TestClass[repeats];

			start = System.currentTimeMillis();
			for (int i = 0; i < repeats; i++)
			{
				table[i] = new TestClass(i);
			}
			stop = System.currentTimeMillis();

			System.out.println("Creating " + repeats + " objects using constructor took "
					+ (stop - start) / 1000.0 + " seconds");

			int sum = 0;
			for (int i = 0; i < repeats; i++)
			{
				sum += table[i].x;
			}
			if (sum == 1000)
			{
				System.exit(1);
			}

			start = System.currentTimeMillis();
			for (int i = 0; i < repeats; i++)
			{
				table[i] = (TestClass) instantiator.newInstance();
				table[i].x = i;
			}
			stop = System.currentTimeMillis();

			System.out.println("Creating " + repeats + " objects using instantiator took "
					+ (stop - start) / 1000.0 + " seconds");

			sum = 0;
			for (int i = 0; i < repeats; i++)
			{
				sum += table[i].x;
			}
			if (sum == 1000)
			{
				System.exit(1);
			}
		}
	}

	private static void hashtableStressTest(final int numberOfThreads, final int capacity,
			final int range, final int readsPerThread, final int writesPerThread,
			final int numberOfRounds) throws StorageException, InterruptedException
	{
		PaxosSTM.getInstance().start();

		long totalTime = 0;

		final TransactionalHashTable<Integer, Integer> hashtable = new TransactionalHashTable<Integer, Integer>(
				capacity);
		new Transaction()
		{
			public void atomic()
			{
			}
		};

		class TestThread extends Thread
		{
			public long start;
			public long stop;

			public TestThread()
			{
			}

			public void run()
			{
				start = System.currentTimeMillis();

				Random rand = new Random(start + hashCode());

				int readsDone = 0;
				int writesDone = 0;
				final int total = readsPerThread + writesPerThread;

				for (int i = 0; i < total; i++)
				{
					final int key = rand.nextInt(range);
					if (rand.nextInt(total - i) < readsPerThread - readsDone)
					{
						new Transaction()
						{
							protected void atomic()
							{
								hashtable.get(key);
							}
						};
						readsDone++;
					}
					else
					{
						new Transaction()
						{
							protected void atomic()
							{
								hashtable.put(key, key * 2);
							}
						};
						writesDone++;
					}
				}

				stop = System.currentTimeMillis();
			}

		}

		System.out
				.println("round   capacity   range   threads   reads per thread   writes per thread   avg time   transactions per second");
		for (int round = 1; round <= numberOfRounds; round++)
		{
			totalTime = 0;
			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new TestThread();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				threads[i] = null;
			}
			threads = null;
			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d     %8d   %5d   %3d       %5d              %5d               %8.3f   %7.2f  %4d  %4d\n";
			System.out.format(format, round, capacity, range, numberOfThreads, readsPerThread,
					writesPerThread, avgTime,
					(numberOfThreads * (readsPerThread + writesPerThread)) / avgTime, TestCounter
							.getInstance().getGlobalConflicts(), TestCounter.getInstance()
							.getLocalConflicts());
		}
	}

	private static void hashtableStressTest2(final int numberOfThreads, final int capacity,
			final int range, final int readTrans, final int writeTrans, final int readsPerTran,
			final int operationsPerWriteTran, final int writesPerTran, final int numberOfRounds)
			throws StorageException, InterruptedException
	{
		PaxosSTM.getInstance().start();
		PaxosSTM.getInstance().enterBarrier("init", PaxosSTM.getInstance().getNumberOfNodes());

		long totalTime = 0;

		class TestThread extends Thread
		{
			public long start;
			public long stop;

			private TransactionalHashTableIntInt hashtable;

			public TestThread(TransactionalHashTableIntInt hashtable)
			{
				this.hashtable = hashtable;
			}

			public void run()
			{
				PaxosSTM.getInstance().enterBarrier("round",
						PaxosSTM.getInstance().getNumberOfNodes() * numberOfThreads);
				start = System.currentTimeMillis();

				Random rand = new Random(start + hashCode());

				int readsDone = 0;
				int writesDone = 0;
				final int total = readTrans + writeTrans;

				for (int i = 0; i < total; i++)
				{
					if (rand.nextInt(total - i) < readTrans - readsDone)
					{
						final int[] keys = new int[readsPerTran];
						for (int j = 0; j < readsPerTran; j++)
							keys[j] = rand.nextInt(2 * range + 1) - range;
						new Transaction()
						{
							protected void atomic()
							{
								for (int key : keys)
									hashtable.get(key);
							}
						};
						readsDone++;
					}
					else
					{
						final int[] keys = new int[operationsPerWriteTran];
						for (int j = 0; j < operationsPerWriteTran; j++)
							keys[j] = rand.nextInt(2 * range + 1) - range;
						new Transaction()
						{
							protected void atomic()
							{
								Integer[] result = new Integer[writesPerTran];
								for (int j = 0; j < writesPerTran; j++)
									result[j] = hashtable.get(keys[j]);
								for (int j = 2 * writesPerTran; j < operationsPerWriteTran; j++)
									hashtable.get(keys[j]);

								for (int j = 0; j < writesPerTran; j++)
								{
									if (result[j] == null)
										hashtable.put(keys[j], keys[j]);
									else
										hashtable.remove(keys[j]);
								}
							}
						};
						writesDone++;
					}
				}

				stop = System.currentTimeMillis();
			}

		}

		System.out
				.println("round   capacity   range   threads   read-trans per thread   write-trans per thread   avg time   tps          gc    pc    dc    rc    lc");
		for (int round = 1; round <= numberOfRounds; round++)
		{
			totalTime = 0;
			final TransactionalHashTableIntInt hashtable = new TransactionalHashTableIntInt(
					capacity);
			final Random rand = new Random();
			new Transaction()
			{
				public void atomic()
				{
					for (int i = 0; i < range; i++)
					{
						int key;
						Integer res;
						do
						{
							key = rand.nextInt(2 * range + 1) - range;
							res = hashtable.get(key);
						} while (res != null);
						hashtable.put(key, key);
					}
				}
			};

			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new TestThread(hashtable);
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				threads[i] = null;
			}
			threads = null;

			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d     %8d   %5d   %3d         %5d                   %5d                  %8.3f   %7.2f    %4d  %4d  %4d  %4d  %4d\n";
			System.out.format(format, round, capacity, range, numberOfThreads, readTrans,
					writeTrans, avgTime, (numberOfThreads * (readTrans + writeTrans)) / avgTime,
					TestCounter.getInstance().getGlobalConflicts(), TestCounter.getInstance()
							.getGlobalPreviousConflicts(), TestCounter.getInstance()
							.getGlobalDeleteConflicts(), TestCounter.getInstance()
							.getGlobalReorderConflicts(), TestCounter.getInstance()
							.getLocalConflicts());

			TestCounter.getInstance().reset();
			new Transaction()
			{
				@Override
				protected void atomic()
				{
					hashtable.purge();
				}
			};
			// System.err.println("XXX: " +
			// GlobalTransactionManager.getInstance().previousTransactions.size());

			PaxosSTM.getInstance().scheduleCheckpoint();

		}
	}

	private static void hashtableStressTest2Distributed(final int numberOfThreads,
			final int capacity, final int range, final int readTrans, final int writeTrans,
			final int readsPerTran, final int operationsPerWriteTran, final int writesPerTran,
			final int numberOfRounds) throws StorageException, InterruptedException
	{
		PaxosSTM.getInstance().start();
		PaxosSTM.getInstance().enterBarrier("init", PaxosSTM.getInstance().getNumberOfNodes());

		long totalTime = 0;

		class TestThread extends Thread
		{
			public long start;
			public long stop;

			private TransactionalHashTableIntInt hashtable;

			public TestThread(TransactionalHashTableIntInt hashtable)
			{
				this.hashtable = hashtable;
			}

			public void run()
			{
				PaxosSTM.getInstance().enterBarrier("round",
						PaxosSTM.getInstance().getNumberOfNodes() * numberOfThreads);
				start = System.currentTimeMillis();

				Random rand = new Random(start + hashCode());

				int readsDone = 0;
				int writesDone = 0;
				final int total = readTrans + writeTrans;

				for (int i = 0; i < total; i++)
				{
					if (rand.nextInt(total - i) < readTrans - readsDone)
					{
						final int[] keys = new int[readsPerTran];
						for (int j = 0; j < readsPerTran; j++)
							keys[j] = rand.nextInt(2 * range + 1) - range;
						new Transaction()
						{
							protected void atomic()
							{
								for (int key : keys)
									hashtable.get(key);
							}
						};
						readsDone++;
					}
					else
					{
						final int[] keys = new int[operationsPerWriteTran];
						for (int j = 0; j < operationsPerWriteTran; j++)
							keys[j] = rand.nextInt(2 * range + 1) - range;
						new Transaction()
						{
							protected void atomic()
							{
								Integer[] result = new Integer[writesPerTran];
								for (int j = 0; j < writesPerTran; j++)
									result[j] = hashtable.get(keys[j]);
								for (int j = 2 * writesPerTran; j < operationsPerWriteTran; j++)
									hashtable.get(keys[j]);

								for (int j = 0; j < writesPerTran; j++)
								{
									if (result[j] == null)
										hashtable.put(keys[j], keys[j]);
									else
										hashtable.remove(keys[j]);
								}
							}
						};
						writesDone++;
					}
				}

				stop = System.currentTimeMillis();
			}

		}

		// System.out
		// .println("round   capacity   range   threads   read-trans per thread   write-trans per thread   avg time   tps         Gtps      Glc     gc    pc    dc    rc    lc");
		System.out
				.println("round capacity  range   ths  RO/th  RW/th  avgTime    tps    GRO-trans  GRW-trans     Gtps   GabortRate  Glc     gc    pc    dc    rc    lc");

		// String format =
		// "%3d     %8d   %5d   %3d      %5d       %5d        %8.3f   %7.2f    %6d   %6d     %8.2f    %5d  %5d  %4d  %4d  %4d  %4d\n";

		for (int round = 1; round <= numberOfRounds; round++)
		{
			if (PaxosSTM.getInstance().getId() == 0)
			{
				new Transaction()
				{
					public void atomic()
					{
						final Random rand = new Random();
						TransactionalHashTableIntInt hashtable = new TransactionalHashTableIntInt(
								capacity);
						for (int i = 0; i < range; i++)
						{
							int key;
							Integer res;
							do
							{
								key = rand.nextInt(2 * range + 1) - range;
								res = hashtable.get(key);
							} while (res != null);
							hashtable.put(key, key);
						}
						PaxosSTM.getInstance().addToSharedObjectRegistry("hashtable", hashtable);
						final BenchmarkStats stats = new BenchmarkStats();
						PaxosSTM.getInstance().addToSharedObjectRegistry("stats", stats);
					}
				};
			}
			PaxosSTM.getInstance().enterBarrier("roundInit",
					PaxosSTM.getInstance().getNumberOfNodes());

			totalTime = 0;
			final TransactionalHashTableIntInt hashtable = (TransactionalHashTableIntInt) PaxosSTM
					.getInstance().getFromSharedObjectRegistry("hashtable");
			final BenchmarkStats stats = (BenchmarkStats) PaxosSTM.getInstance()
					.getFromSharedObjectRegistry("stats");

			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new TestThread(hashtable);
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				threads[i] = null;
			}
			threads = null;

			final Holder<Long> totalTimeHolder = new Holder<Long>(totalTime);

			new Transaction()
			{
				@Override
				protected void atomic()
				{
					stats.totalLocalConflicts += TestCounter.getInstance().getLocalConflicts();
					stats.totalROTransactions += readTrans * numberOfThreads;
					stats.totalRWTransactions += writeTrans * numberOfThreads;
					stats.totalTime += totalTimeHolder.value;
					stats.totalNumberOfThreads += numberOfThreads;
				}
			};

			PaxosSTM.getInstance().enterBarrier("stop", PaxosSTM.getInstance().getNumberOfNodes());

			final Holder<Integer> totalROTransactions = new Holder<Integer>(0);
			final Holder<Integer> totalRWTransactions = new Holder<Integer>(0);
			final Holder<Integer> totalLocalConflicts = new Holder<Integer>(0);
			final Holder<Long> totalTimeHolder2 = new Holder<Long>(0l);
			final Holder<Integer> totalNumberOfThreads = new Holder<Integer>(0);

			new Transaction()
			{
				@Override
				protected void atomic()
				{
					totalROTransactions.value = stats.totalROTransactions;
					totalRWTransactions.value = stats.totalRWTransactions;
					totalLocalConflicts.value = stats.totalLocalConflicts;
					totalTimeHolder2.value = stats.totalTime;
					totalNumberOfThreads.value = stats.totalNumberOfThreads;
				}
			};
			// System.out
			// .println("round   capacity   range   threads   RO-trans/th   RW-trans/th    avg time   tps    GRO-trans   RW-trans     Gtps   abortRate  Glc     gc    pc    dc    rc    lc");
			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d   %6d   %6d  %3d %5d  %5d  %8.3f   %7.2f   %6d     %6d    %8.2f   %7.3f  %5d  %5d  %4d  %4d  %4d  %4d\n";
			System.out
					.format(
							format,
							round,
							capacity,
							range,
							numberOfThreads,
							readTrans,
							writeTrans,
							avgTime,
							(numberOfThreads * (readTrans + writeTrans)) / avgTime,
							totalROTransactions.value,
							totalRWTransactions.value,
							1f * (totalROTransactions.value + totalRWTransactions.value)
									/ (totalTimeHolder2.value) * totalNumberOfThreads.value * 1000,
							1f
									* (TestCounter.getInstance().getGlobalConflicts() + totalLocalConflicts.value)
									/ (TestCounter.getInstance().getGlobalConflicts()
											+ totalLocalConflicts.value + totalROTransactions.value + totalRWTransactions.value),
							totalLocalConflicts.value, TestCounter.getInstance()
									.getGlobalConflicts(), TestCounter.getInstance()
									.getGlobalPreviousConflicts(), TestCounter.getInstance()
									.getGlobalDeleteConflicts(), TestCounter.getInstance()
									.getGlobalReorderConflicts(), TestCounter.getInstance()
									.getLocalConflicts());

			PaxosSTM.getInstance().enterBarrier("roundCleanup",
					PaxosSTM.getInstance().getNumberOfNodes());
			TestCounter.getInstance().reset();

			if (PaxosSTM.getInstance().getId() == 0)
			{
				new Transaction()
				{
					@Override
					protected void atomic()
					{
						hashtable.purge();
						PaxosSTM.getInstance().removeFromSharedObjectRegistry("hashtable");
						BenchmarkStats stats = (BenchmarkStats) PaxosSTM.getInstance()
								.removeFromSharedObjectRegistry("stats");
						((Observable) stats).__tc_delete();
					}
				};
			}

			// System.err.println("XXX: " +
			// GlobalTransactionManager.getInstance().previousTransactions.size());

			PaxosSTM.getInstance().scheduleCheckpoint();
		}

		PaxosSTM.getInstance().enterBarrier("exit", PaxosSTM.getInstance().getNumberOfNodes());
	}

	public static volatile boolean stopFlag = false;

	private static void hashtableStressTest2DistributedWithTime(final int numberOfThreads,
			final int capacity, final int range, final int percent, final int readsPerTran,
			final int operationsPerWriteTran, final int writesPerTran, final int numberOfRounds,
			int mills) throws StorageException, InterruptedException
	{
		PaxosSTM.getInstance().start();
		PaxosSTM.getInstance().enterBarrier("init", PaxosSTM.getInstance().getNumberOfNodes());

		long totalTime = 0;
		int totalReads = 0;
		int totalWrites = 0;

		class TestThread extends Thread
		{
			public long start;
			public long stop;
			public int readsDone = 0;
			public int writesDone = 0;

			private TransactionalHashTableIntInt hashtable;

			public TestThread(TransactionalHashTableIntInt hashtable)
			{
				this.hashtable = hashtable;
			}

			public void run()
			{
				// PaxosSTM.getInstance().enterBarrier(
				// "round",
				// PaxosSTM.getInstance().getNumberOfNodes()
				// * numberOfThreads);
				start = System.currentTimeMillis();

				Random rand = new Random(start + hashCode());

				while (true)
				{
					if (stopFlag)
						break;

					final boolean localStop = stopFlag;

					if (Math.abs(rand.nextInt()) % 100 >= percent)
					{
						final int[] keys = new int[readsPerTran];
						for (int j = 0; j < readsPerTran; j++)
							keys[j] = rand.nextInt(range);
						new Transaction()
						{
							protected void atomic()
							{
								if (localStop)
									return;

								for (int key : keys)
									hashtable.get(key);
							}
						};
						if (!localStop)
							readsDone++;
					}
					else
					{
						final int[] keys = new int[operationsPerWriteTran];
						for (int j = 0; j < operationsPerWriteTran; j++)
							keys[j] = rand.nextInt(range);
						new Transaction()
						{
							protected void atomic()
							{
								if (localStop)
									return;

								Integer[] result = new Integer[writesPerTran];
								for (int j = 0; j < writesPerTran; j++)
									result[j] = hashtable.get(keys[j]);
								for (int j = 2 * writesPerTran; j < operationsPerWriteTran; j++)
									hashtable.get(keys[j]);

								for (int j = 0; j < writesPerTran; j++)
								{
									if (result[j] == null)
										hashtable.put(keys[j], keys[j]);
									else
										hashtable.remove(keys[j]);
								}
							}
						};
						if (!localStop)
							writesDone++;
					}
				}

				stop = System.currentTimeMillis();
			}

		}

		// System.out
		// .println("round   capacity   range   threads   read-trans per thread   write-trans per thread   avg time   tps         Gtps      Glc     gc    pc    dc    rc    lc");
		System.out
				.println("round capacity  range   ths  RO/th  RW/th  avgTime    tps    GRO-trans  GRW-trans     Gtps   GabortRate  Glc     gc    pc    dc    rc    lc");

		// String format =
		// "%3d     %8d   %5d   %3d      %5d       %5d        %8.3f   %7.2f    %6d   %6d     %8.2f    %5d  %5d  %4d  %4d  %4d  %4d\n";

		for (int round = 1; round <= numberOfRounds; round++)
		{
			if (PaxosSTM.getInstance().getId() == 0)
			{
				new Transaction()
				{
					public void atomic()
					{
						final Random rand = new Random();
						TransactionalHashTableIntInt hashtable = new TransactionalHashTableIntInt(
								capacity);
						for (int i = 0; i < range / 2; i++)
						{
							int key;
							Integer res;
							do
							{
								key = rand.nextInt(range);
								res = hashtable.get(key);
							} while (res != null);
							hashtable.put(key, key);
						}
						PaxosSTM.getInstance().addToSharedObjectRegistry("hashtable", hashtable);
						final BenchmarkStats stats = new BenchmarkStats();
						PaxosSTM.getInstance().addToSharedObjectRegistry("stats", stats);
					}
				};
			}
			PaxosSTM.getInstance().enterBarrier("roundInit",
					PaxosSTM.getInstance().getNumberOfNodes());

			totalTime = 0;
			totalReads = 0;
			totalWrites = 0;
			stopFlag = false;

			final TransactionalHashTableIntInt hashtable = (TransactionalHashTableIntInt) PaxosSTM
					.getInstance().getFromSharedObjectRegistry("hashtable");
			final BenchmarkStats stats = (BenchmarkStats) PaxosSTM.getInstance()
					.getFromSharedObjectRegistry("stats");

			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new TestThread(hashtable);
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}

			try
			{
				Thread.sleep(mills);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			stopFlag = true;

			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				totalReads += threads[i].readsDone;
				totalWrites += threads[i].writesDone;
				threads[i] = null;
			}
			threads = null;

			final Holder<Long> totalTimeHolder = new Holder<Long>(totalTime);
			final Holder<Integer> totalReadsHolder = new Holder<Integer>(totalReads);
			final Holder<Integer> totalWritesHolder = new Holder<Integer>(totalWrites);

			new Transaction()
			{
				@Override
				protected void atomic()
				{
					stats.totalLocalConflicts += TestCounter.getInstance().getLocalConflicts();
					stats.totalROTransactions += totalReadsHolder.value;
					stats.totalRWTransactions += totalWritesHolder.value;
					stats.totalTime += totalTimeHolder.value;
					stats.totalNumberOfThreads += numberOfThreads;
				}
			};

			PaxosSTM.getInstance().enterBarrier("stop", PaxosSTM.getInstance().getNumberOfNodes());

			final Holder<Integer> totalROTransactions = new Holder<Integer>(0);
			final Holder<Integer> totalRWTransactions = new Holder<Integer>(0);
			final Holder<Integer> totalLocalConflicts = new Holder<Integer>(0);
			final Holder<Long> totalTimeHolder2 = new Holder<Long>(0l);
			final Holder<Integer> totalNumberOfThreads = new Holder<Integer>(0);

			new Transaction()
			{
				@Override
				protected void atomic()
				{
					totalROTransactions.value = stats.totalROTransactions;
					totalRWTransactions.value = stats.totalRWTransactions;
					totalLocalConflicts.value = stats.totalLocalConflicts;
					totalTimeHolder2.value = stats.totalTime;
					totalNumberOfThreads.value = stats.totalNumberOfThreads;
				}
			};
			// System.out
			// .println("round   capacity   range   threads   RO-trans/th   RW-trans/th    avg time   tps    GRO-trans   RW-trans     Gtps   abortRate  Glc     gc    pc    dc    rc    lc");
			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d   %6d   %6d  %3d %5d  %5d  %8.3f   %7.2f   %6d     %6d    %8.2f   %7.3f  %5d  %5d  %4d  %4d  %4d  %4d\n";
			System.out
					.format(
							format,
							round,
							capacity,
							range,
							numberOfThreads,
							totalReads,
							totalWrites,
							avgTime,
							(totalReads + totalWrites) / avgTime,
							totalROTransactions.value,
							totalRWTransactions.value,
							1f * (totalROTransactions.value + totalRWTransactions.value)
									/ (totalTimeHolder2.value) * totalNumberOfThreads.value * 1000,
							1f
									* (TestCounter.getInstance().getGlobalConflicts() + totalLocalConflicts.value)
									/ (TestCounter.getInstance().getGlobalConflicts()
											+ totalLocalConflicts.value + totalROTransactions.value + totalRWTransactions.value),
							totalLocalConflicts.value, TestCounter.getInstance()
									.getGlobalConflicts(), TestCounter.getInstance()
									.getGlobalPreviousConflicts(), TestCounter.getInstance()
									.getGlobalDeleteConflicts(), TestCounter.getInstance()
									.getGlobalReorderConflicts(), TestCounter.getInstance()
									.getLocalConflicts());

			PaxosSTM.getInstance().enterBarrier("roundCleanup",
					PaxosSTM.getInstance().getNumberOfNodes());
			TestCounter.getInstance().reset();

			if (PaxosSTM.getInstance().getId() == 0)
			{
				new Transaction()
				{
					@Override
					protected void atomic()
					{
						hashtable.purge();
						PaxosSTM.getInstance().removeFromSharedObjectRegistry("hashtable");
						BenchmarkStats stats = (BenchmarkStats) PaxosSTM.getInstance()
								.removeFromSharedObjectRegistry("stats");
						((Observable) stats).__tc_delete();
					}
				};
			}

			// System.err.println("XXX: " +
			// GlobalTransactionManager.getInstance().previousTransactions.size());

			PaxosSTM.getInstance().scheduleCheckpoint();
		}

		PaxosSTM.getInstance().enterBarrier("exit", PaxosSTM.getInstance().getNumberOfNodes());
	}


	private static void hashtableStressTest2DistributedWithTimeAndCrash(final int numberOfThreads,
			final int capacity, final int range, final int percent, final int readsPerTran,
			final int operationsPerWriteTran, final int writesPerTran, final int numberOfRounds,
			int mills) throws StorageException, InterruptedException
	{
		PaxosSTM.getInstance().start();
		PaxosSTM.getInstance().enterBarrier("init", PaxosSTM.getInstance().getNumberOfNodes());

		long totalTime = 0;
		int totalReads = 0;
		int totalWrites = 0;

		class TestThread extends Thread
		{
			public long start;
			public long stop;
			public int readsDone = 0;
			public int writesDone = 0;

			private TransactionalHashTableIntInt hashtable;

			public TestThread(TransactionalHashTableIntInt hashtable)
			{
				this.hashtable = hashtable;
			}

			public void run()
			{
				// PaxosSTM.getInstance().enterBarrier(
				// "round",
				// PaxosSTM.getInstance().getNumberOfNodes()
				// * numberOfThreads);
				start = System.currentTimeMillis();

				Random rand = new Random(start + hashCode());

				//int counter = 0;
				while (true)
				{
					//if (++counter % 100 == 0)
					//	PaxosSTM.getInstance().scheduleCheckpoint();
					if (stopFlag)
						break;

					final boolean localStop = stopFlag;

					if (Math.abs(rand.nextInt()) % 100 >= percent)
					{
						final int[] keys = new int[readsPerTran];
						for (int j = 0; j < readsPerTran; j++)
							keys[j] = rand.nextInt(range);
						new Transaction()
						{
							protected void atomic()
							{
								if (localStop)
									return;

								for (int key : keys)
									hashtable.get(key);
							}
						};
						if (!localStop)
							readsDone++;
					}
					else
					{
						final int[] keys = new int[operationsPerWriteTran];
						for (int j = 0; j < operationsPerWriteTran; j++)
							keys[j] = rand.nextInt(range);
						new Transaction()
						{
							protected void atomic()
							{
								if (localStop)
									return;

								Integer[] result = new Integer[writesPerTran];
								for (int j = 0; j < writesPerTran; j++)
									result[j] = hashtable.get(keys[j]);
								for (int j = 2 * writesPerTran; j < operationsPerWriteTran; j++)
									hashtable.get(keys[j]);

								for (int j = 0; j < writesPerTran; j++)
								{
									if (result[j] == null)
										hashtable.put(keys[j], keys[j]);
									else
										hashtable.remove(keys[j]);
								}
							}
						};
						if (!localStop)
							writesDone++;
					}
				}

				stop = System.currentTimeMillis();
			}

		}

		// System.out
		// .println("round   capacity   range   threads   read-trans per thread   write-trans per thread   avg time   tps         Gtps      Glc     gc    pc    dc    rc    lc");
		System.out
				.println("round capacity  range   ths  RO/th  RW/th  avgTime    tps    GRO-trans  GRW-trans     Gtps   GabortRate  Glc     gc    pc    dc    rc    lc");

		// String format =
		// "%3d     %8d   %5d   %3d      %5d       %5d        %8.3f   %7.2f    %6d   %6d     %8.2f    %5d  %5d  %4d  %4d  %4d  %4d\n";

		for (int round = 1; round <= numberOfRounds; round++)
		{
			if (PaxosSTM.getInstance().getId() == 0)
			{
				new Transaction()
				{
					public void atomic()
					{
						final Random rand = new Random();
						TransactionalHashTableIntInt hashtable = new TransactionalHashTableIntInt(
								capacity);
						for (int i = 0; i < range / 2; i++)
						{
							int key;
							Integer res;
							do
							{
								key = rand.nextInt(range);
								res = hashtable.get(key);
							} while (res != null);
							hashtable.put(key, key);
						}
						PaxosSTM.getInstance().addToSharedObjectRegistry("hashtable", hashtable);
						final BenchmarkStats stats = new BenchmarkStats();
						PaxosSTM.getInstance().addToSharedObjectRegistry("stats", stats);
					}
				};
			}
			PaxosSTM.getInstance().enterBarrier("roundInit",
					PaxosSTM.getInstance().getNumberOfNodes());

			totalTime = 0;
			totalReads = 0;
			totalWrites = 0;
			stopFlag = false;

			final TransactionalHashTableIntInt hashtable = (TransactionalHashTableIntInt) PaxosSTM
					.getInstance().getFromSharedObjectRegistry("hashtable");
			final BenchmarkStats stats = (BenchmarkStats) PaxosSTM.getInstance()
					.getFromSharedObjectRegistry("stats");

			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new TestThread(hashtable);
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}

			Random random = new Random();
			int skip = random.nextInt(mills);
			
			try
			{
				Thread.sleep(skip);	
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			
			PaxosSTM.getInstance().scheduleCheckpoint();
			
			try
			{
				Thread.sleep(mills - skip);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

			stopFlag = true;

			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				totalReads += threads[i].readsDone;
				totalWrites += threads[i].writesDone;
				threads[i] = null;
			}
			threads = null;

			final Holder<Long> totalTimeHolder = new Holder<Long>(totalTime);
			final Holder<Integer> totalReadsHolder = new Holder<Integer>(totalReads);
			final Holder<Integer> totalWritesHolder = new Holder<Integer>(totalWrites);

			new Transaction()
			{
				@Override
				protected void atomic()
				{
					stats.totalLocalConflicts += TestCounter.getInstance().getLocalConflicts();
					stats.totalROTransactions += totalReadsHolder.value;
					stats.totalRWTransactions += totalWritesHolder.value;
					stats.totalTime += totalTimeHolder.value;
					stats.totalNumberOfThreads += numberOfThreads;
				}
			};

			PaxosSTM.getInstance().enterBarrier("stop", PaxosSTM.getInstance().getNumberOfNodes());

			final Holder<Integer> totalROTransactions = new Holder<Integer>(0);
			final Holder<Integer> totalRWTransactions = new Holder<Integer>(0);
			final Holder<Integer> totalLocalConflicts = new Holder<Integer>(0);
			final Holder<Long> totalTimeHolder2 = new Holder<Long>(0l);
			final Holder<Integer> totalNumberOfThreads = new Holder<Integer>(0);

			new Transaction()
			{
				@Override
				protected void atomic()
				{
					totalROTransactions.value = stats.totalROTransactions;
					totalRWTransactions.value = stats.totalRWTransactions;
					totalLocalConflicts.value = stats.totalLocalConflicts;
					totalTimeHolder2.value = stats.totalTime;
					totalNumberOfThreads.value = stats.totalNumberOfThreads;
				}
			};
			// System.out
			// .println("round   capacity   range   threads   RO-trans/th   RW-trans/th    avg time   tps    GRO-trans   RW-trans     Gtps   abortRate  Glc     gc    pc    dc    rc    lc");
			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d   %6d   %6d  %3d %5d  %5d  %8.3f   %7.2f   %6d     %6d    %8.2f   %7.3f  %5d  %5d  %4d  %4d  %4d  %4d\n";
			System.out
					.format(
							format,
							round,
							capacity,
							range,
							numberOfThreads,
							totalReads,
							totalWrites,
							avgTime,
							(totalReads + totalWrites) / avgTime,
							totalROTransactions.value,
							totalRWTransactions.value,
							1f * (totalROTransactions.value + totalRWTransactions.value)
									/ (totalTimeHolder2.value) * totalNumberOfThreads.value * 1000,
							1f
									* (TestCounter.getInstance().getGlobalConflicts() + totalLocalConflicts.value)
									/ (TestCounter.getInstance().getGlobalConflicts()
											+ totalLocalConflicts.value + totalROTransactions.value + totalRWTransactions.value),
							totalLocalConflicts.value, TestCounter.getInstance()
									.getGlobalConflicts(), TestCounter.getInstance()
									.getGlobalPreviousConflicts(), TestCounter.getInstance()
									.getGlobalDeleteConflicts(), TestCounter.getInstance()
									.getGlobalReorderConflicts(), TestCounter.getInstance()
									.getLocalConflicts());

			PaxosSTM.getInstance().enterBarrier("roundCleanup",
					PaxosSTM.getInstance().getNumberOfNodes());
			TestCounter.getInstance().reset();

			if (PaxosSTM.getInstance().getId() == 0)
			{
				new Transaction()
				{
					@Override
					protected void atomic()
					{
						hashtable.purge();
						PaxosSTM.getInstance().removeFromSharedObjectRegistry("hashtable");
						BenchmarkStats stats = (BenchmarkStats) PaxosSTM.getInstance()
								.removeFromSharedObjectRegistry("stats");
						((Observable) stats).__tc_delete();
					}
				};
			}

			// System.err.println("XXX: " +
			// GlobalTransactionManager.getInstance().previousTransactions.size());

			PaxosSTM.getInstance().scheduleCheckpoint();
		}

		PaxosSTM.getInstance().enterBarrier("exit", PaxosSTM.getInstance().getNumberOfNodes());
	}


	
	private static void simpleStressTest(final int numberOfThreads, final int repeatsPerThread,
			final int numberOfRounds) throws StorageException, InterruptedException
	{
		PaxosSTM.getInstance().start();

		long totalTime = 0;

		@TransactionObject
		class TransactionalInteger
		{
			public int x;

			public TransactionalInteger(int x)
			{
				this.x = x;
			}
		}

		class IntSetterThread extends Thread
		{
			public long start;
			public long stop;

			private TransactionalInteger ti;

			public IntSetterThread()
			{
				new Transaction()
				{
					public void atomic()
					{
						ti = new TransactionalInteger(0);
					}
				};
			}

			public void run()
			{
				start = System.currentTimeMillis();

				for (int i = 0; i < repeatsPerThread; i++)
				{
					final Holder<Integer> res = new Holder<Integer>();
					res.set(0);

					new Transaction()
					{
						public void atomic()
						{
							res.set(ti.x++);
						}
					};

					// to jest dodane tylko po to, by kompilator nie wyrzucil
					// kodu w trakcie optymalizacji
					if (res.get() > repeatsPerThread)
					{
						System.exit(1);
					}
				}

				stop = System.currentTimeMillis();
			}
		}

		System.out
				.println("round     threads       transactions per thread     avg time   transactions per second   flushes per second");
		for (int round = 1; round <= numberOfRounds; round++)
		{
			totalTime = 0;
			TestCounter.getInstance().reset();

			IntSetterThread[] threads = new IntSetterThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i] = new IntSetterThread();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].join();
				totalTime += threads[i].stop - threads[i].start;
				threads[i] = null;
			}
			threads = null;

			double avgTime = (double) totalTime / (double) numberOfThreads / 1000;
			String format = "%3d       %3d                   %5d              %8.3f            %7.2f              %7.2f\n";
			System.out.format(format, round, numberOfThreads, repeatsPerThread, avgTime,
					(numberOfThreads * repeatsPerThread) / avgTime, TestCounter.getInstance()
							.getFlushes()
							/ avgTime);
		}
	}

	@SuppressWarnings("unused")
	private static void objectInitializationTest() throws StorageException
	{
		PaxosSTM.getInstance().start();

		final A a = new B();
		Vector<String> vs = new Vector<String>();
		vs.add("Ala");
		vs.add("ma");
		vs.add("corse");
		final ObjectWrapper<Vector<String>> ow = new ObjectWrapper<Vector<String>>(vs);

		new Transaction()
		{
			protected void atomic()
			{
				a.x = 10;
			}
		};
		new Transaction()
		{
			protected void atomic()
			{
				System.out.println(a.x);
				int n = a.intArray.length();
				for (int i = 0; i < n; i++)
				{
					Integer x = a.intArray.get(i);
					if (x == null)
					{
						x = -1;
						a.intArray.set(i, x);
					}
					System.out.println("* " + x);
				}
				for (String s : ow.get())
				{
					System.out.println(s);
				}
			}
		};
		new Transaction()
		{
			protected void atomic()
			{
				ow.get().add("koloru");
				ow.get().add("czerwonego");
			}
		};
		new Transaction()
		{
			protected void atomic()
			{
				for (String s : ow.get())
				{
					System.out.println(s);
				}
			}
		};
	}

	@SuppressWarnings("unused")
	private static void threadsTransactionTest() throws StorageException
	{
		PaxosSTM.getInstance().start();

		final int n = 9;
		final int m = 30;

		final A[] tab = new A[m];
		final Thread[] threads = new Thread[n];

		final Random random = new Random();

		new Transaction()
		{
			protected void atomic()
			{
				for (int i = 0; i < m; i++)
				{
					tab[i] = new B();
					tab[i].x = random.nextInt(100);
				}
			}
		};

		for (int i = 0; i < n; i++)
		{
			int start = random.nextInt(100);
			int maxDuration = random.nextInt(1);

			threads[i] = new TestThread(tab, 10, 0.3, start, maxDuration);
		}
		for (int i = 0; i < n; i++)
		{
			threads[i].start();
		}
		for (int i = 0; i < n; i++)
		{
			try
			{
				threads[i].join();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
		System.out.println("Finished testing!");
	}

	private static void threadedTransactionsTest() throws StorageException
	{
		PaxosSTM.getInstance().start();

		class ThreadStub extends Thread
		{
			final A a;
			long start;
			long duration;

			ThreadStub(A a, long start, long duration)
			{
				this.a = a;
				this.start = start;
				this.duration = duration;
			}
		}

		class WriteOnlyThread extends ThreadStub
		{
			WriteOnlyThread(A a, long start, long duration)
			{
				super(a, start, duration);
			}

			public void run()
			{

				try
				{
					sleep(start);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}

				new Transaction()
				{
					protected void atomic()
					{
						System.err.println("WriteOnlyThread atomic start");
						a.x = 100;
						try
						{
							sleep(duration);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						a.x = 150;
						System.err.println("WriteOnlyThread atomic end");
					}
				};
				System.err.println("WriteOnlyThread atomic committed");
			}
		}

		class ReadWriteThread extends ThreadStub
		{
			ReadWriteThread(A a, long start, long duration)
			{
				super(a, start, duration);
			}

			public void run()
			{
				try
				{
					sleep(start);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				new Transaction()
				{
					protected void atomic()
					{
						System.err.println("ReadWriteThread atomic start");
						System.err.println("ReadWriteThread (1): " + a.x);
						a.x++;
						try
						{
							sleep(duration);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						System.err.println("ReadWriteThread atomic end");
					}
				};
				System.err.println("ReadWriteThread atomic committed");
			}

		}

		class ReadOnlyThread extends ThreadStub
		{
			ReadOnlyThread(A a, long start, long duration)
			{
				super(a, start, duration);
			}

			public void run()
			{
				try
				{
					sleep(start);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				new Transaction()
				{
					protected void atomic()
					{
						System.err.println("ReadOnlyThread atomic start");
						System.err.println("ReadOnlyThread (1): " + a.x);
						try
						{
							sleep(duration);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						// System.out.println("ReadOnlyThread (2): " + a.x);
						System.err.println("ReadOnlyThread atomic end");
					}
				};
				System.err.println("ReadOnlyThread atomic committed");
			}

		}

		class DeleteOnlyThread extends ThreadStub
		{
			DeleteOnlyThread(A a, long start, long duration)
			{
				super(a, start, duration);
			}

			public void run()
			{
				try
				{
					sleep(start);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				new Transaction()
				{
					protected void atomic()
					{
						System.out.println("DeleteOnlyThread atomic start");
						// System.out.println("DeleteOnlyThread (1): " + a.x);
						((Observable) a).__tc_delete();
						try
						{
							sleep(duration);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						System.out.println("DeleteOnlyThread atomic end");
					}
				};
				System.out.println("DeleteOnlyThread atomic committed");
			}
		}

		final int n = 27;
		final int m = 3;

		final A[] tab = new A[m];
		final Thread[] threads = new Thread[n];

		new Transaction()
		{
			protected void atomic()
			{
				for (int i = 0; i < m; i++)
				{
					tab[i] = new B();
					tab[i].x = 0;
				}
			}
		};

		// Random random = new Random();
		// for (int i = 0; i < n; i++)
		// {
		// long start = random.nextInt(1);
		// long durex = random.nextInt(1);
		//
		// int var = random.nextInt(m);
		// int type = random.nextInt(10);
		// if (type < 5)
		// threads[i] = new ReadOnlyThread(tab[var], start, durex);
		// else
		// threads[i] = new ReadWriteThread(tab[var], start, durex);
		// }
		// for (int i = 0; i < n; i++)
		// {
		// threads[i].start();
		// }
		// for (int i = 0; i < n; i++)
		// {
		// try
		// {
		// threads[i].join();
		// }
		// catch (InterruptedException e)
		// {
		// e.printStackTrace();
		// }
		// }
		// System.out.println("Finished testing!");
		// System.exit(0);
		Thread first = new DeleteOnlyThread(tab[0], 0, 300);
		// Thread second = new ReadOnlyThread(tab[0], 100, 1400);
		// Thread second = new ReadWriteThread(tab[0], 100, 1400);
		Thread second = new WriteOnlyThread(tab[0], 100, 1400);

		first.start();
		second.start();

		try
		{
			first.join();
			second.join();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		// first.start();
		// //second.start();
		//
		// try
		// {
		// first.join();
		// // second.join();
		// }
		// catch (InterruptedException e)
		// {
		// e.printStackTrace();
		// }
	}

	public static class TransactionalIntegerNested implements Serializable
	{
		public int value;

		public TransactionalIntegerNested()
		{
		}

		public TransactionalIntegerNested(int value)
		{
			this.value = value;
		}

		public boolean equals(Object aThat)
		{
			if (aThat == null)
				return false;
			if (this == aThat)
				return true;
			if (!(aThat instanceof TransactionalIntegerNested))
				return false;
			TransactionalIntegerNested that = (TransactionalIntegerNested) aThat;
			return that.value == value;
		}

	}

	private static void nestedSequentialTransactionsTestInit(A[] toArray,
			ObjectWrapper<TransactionalIntegerNested>[] owArray)
	{
		final A[] tab = toArray;
		final ObjectWrapper<TransactionalIntegerNested>[] owtab = owArray;

		System.err.println("=== NESTED SEQ TRANSACTION TEST INIT ============");
		new Transaction()
		{
			protected void atomic()
			{
				A a = new B();
				a.x = 33;
				// a.foo();

				TransactionalIntegerNested ti = new TransactionalIntegerNested();

				ObjectWrapper<TransactionalIntegerNested> ow = new ObjectWrapper<TransactionalIntegerNested>(
						ti);
				ow.get().value = 66;

				tab[0] = a;
				owtab[0] = ow;

				System.out.println(a.x + " " + ow.get().value);

				System.err.println(LocalTransactionManager.debug());
			}
		};
	}

	private static void nestedSequentialTransactionsTestFinish(A[] toArray,
			ObjectWrapper<TransactionalIntegerNested>[] owArray)
	{
		final A[] tab = toArray;
		final ObjectWrapper<TransactionalIntegerNested>[] owtab = owArray;

		System.err.println("=== NESTED SEQ TRANSACTION TEST FINISH ==========");
		new Transaction()
		{
			protected void atomic()
			{
				A a = tab[0];
				ObjectWrapper<TransactionalIntegerNested> ow = owtab[0];
				System.out.println(a.x + " " + ow.get().value);
				System.err.println(LocalTransactionManager.debug());
				// a.foo();
			}
		};
	}

	@SuppressWarnings( { "unused", "unchecked" })
	private static void nestedSequentialCommitRollbackTest() throws StorageException
	{
		PaxosSTM.getInstance().start();

		final A[] tab = new A[1];
		final ObjectWrapper<TransactionalIntegerNested>[] owtab = new ObjectWrapper[1];

		nestedSequentialTransactionsTestInit(tab, owtab);

		System.out.println("===============================");

		// function arg arg rb1 r1 w1 d1 rb2 r2 w2 d2 rb3 r3 w3 d3
		// [ r [ r ] r ]
		// nestedSequentialTransactionsTestBody1(tab, owtab, false, true, false,
		// false, false, true, false, false, false, true, false, false);
		// [ r [ rw ] r ]
		// nestedSequentialTransactionsTestBody1(tab, owtab, false, true, false,
		// false, false, true, true, false, false, true, false, false);
		// [ r [ rw R ] r ]
		// nestedSequentialTransactionsTestBody1(tab, owtab, false, true, false,
		// false, true, true, true, false, false, true, false, false);
		// [ r [ rw R ] w ]
		// nestedSequentialTransactionsTestBody1(tab, owtab, false, true, false,
		// false, true, true, true, false, false, false, true, false);
		// [ r [ d ] r ]
		// nestedSequentialTransactionsTestBody1(tab, owtab, false, true, false,
		// false, false, false, false, true, false, true, false, false);
		// [ r [ d R ] r ]
		// nestedSequentialTransactionsTestBody1(tab, owtab, false, true, false,
		// false, true, false, false, true, false, true, false, false);
		// [ r [ d ] w ]
		nestedSequentialTransactionsTestBody1(tab, owtab, false, true, false, false, false, false,
				false, true, false, false, true, false);
		// [ r [ d ] d ]
		// nestedSequentialTransactionsTestBody1(tab, owtab, false, true, false,
		// false, false, false, false, true, false, false, false, true);
		// [ r [ d ] w R ]
		// nestedSequentialTransactionsTestBody1(tab, owtab, false, true, false,
		// false, false, false,
		// false, true, true, false, true, false);
		System.out.println("===============================");

		nestedSequentialTransactionsTestFinish(tab, owtab);
	}

	private static void nestedSequentialTransactionsTestBody1(A[] toArray,
			ObjectWrapper<TransactionalIntegerNested>[] owArray, final boolean firstRollback,
			final boolean firstRead, final boolean firstWrite, final boolean firstDelete,
			final boolean secondRollback, final boolean secondRead, final boolean secondWrite,
			final boolean secondDelete, final boolean thirdRollback, final boolean thirdRead,
			final boolean thirdWrite, final boolean thirdDelete)
	{
		final A[] tab = toArray;
		final ObjectWrapper<TransactionalIntegerNested>[] owtab = owArray;

		System.err.println("=== NESTED SEQ TRANSACTION TEST BODY ============");
		new Transaction()
		{
			protected void atomic()
			{
				final A a = tab[0];
				final ObjectWrapper<TransactionalIntegerNested> ow = owtab[0];

				if (firstRead)
					System.out.println(a.x + " " + ow.get().value);
				if (firstWrite)
				{
					a.x = 133;
					ow.get().value = 166;
				}
				if (firstDelete)
				{
					((Observable) a).__tc_delete();
					ow.__tc_delete();
				}
				System.err.println(LocalTransactionManager.debug());
				if (firstRollback)
					rollback();

				new Transaction()
				{
					protected void atomic()
					{
						if (secondRead)
							System.out.println(a.x + " " + ow.get().value);
						if (secondWrite)
						{
							a.x = 233;
							ow.get().value = 266;
						}
						if (secondDelete)
						{
							((Observable) a).__tc_delete();
							ow.__tc_delete();
						}
						System.err.println(LocalTransactionManager.debug());
						if (secondRollback)
							rollback();
					}
				};
				if (thirdRead)
					System.out.println(a.x + " " + ow.get().value);
				if (thirdWrite)
				{
					a.x = 333;
					ow.get().value = 366;
				}
				if (thirdDelete)
				{
					((Observable) a).__tc_delete();
					ow.__tc_delete();
				}
				System.err.println(LocalTransactionManager.debug());
				if (thirdRollback)
					rollback();
			}
		};
	}

	private static void reorderListTest() throws StorageException
	{
		PaxosSTM.getInstance().start();

		@TransactionObject
		class TransactionalInteger
		{
			public int value;

			public TransactionalInteger(int value)
			{
				this.value = value;
			}
		}

		final TransactionalInteger x = new TransactionalInteger(0);
		final TransactionalInteger y = new TransactionalInteger(1);
		final TransactionalInteger z = new TransactionalInteger(2);
		final TransactionalInteger t = new TransactionalInteger(3);
		final TransactionalInteger q = new TransactionalInteger(4);
		final TransactionalInteger r = new TransactionalInteger(5);

		TransactionalInteger[] variables = new TransactionalInteger[6];
		variables[0] = x;
		variables[1] = y;
		variables[2] = z;
		variables[3] = t;
		variables[4] = q;
		variables[5] = r;

		new Transaction()
		{
			protected void atomic()
			{
				System.out.println(x.value + " " + y.value + " " + z.value + " " + t.value + " "
						+ q.value + " " + r.value);
			}

		};

		class TestThread1 extends Thread
		{
			final TransactionalInteger x;
			final TransactionalInteger y;
			final TransactionalInteger z;
			final TransactionalInteger t;
			final TransactionalInteger q;
			final TransactionalInteger r;

			public TestThread1(TransactionalInteger[] variables)
			{
				x = variables[0];
				y = variables[1];
				z = variables[2];
				t = variables[3];
				q = variables[4];
				r = variables[5];
			}

			public void run()
			{
				System.out.println("Transaction 1 started");

				new Transaction()
				{
					protected void atomic()
					{
						try
						{
							sleep(2000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}

						// RS: x,q (0,4)
						// WS: y,t (1,3)

						int localX = x.value;
						int localQ = q.value;
						y.value = localX;
						t.value = localQ;

						// System.out.println("Transaction 1 about to commit");
					}
				};
				System.out.println("Transaction 1 finished");
			}
		}

		class TestThread2 extends Thread
		{
			final TransactionalInteger x;
			final TransactionalInteger y;
			final TransactionalInteger z;
			final TransactionalInteger t;
			final TransactionalInteger q;
			final TransactionalInteger r;

			public TestThread2(TransactionalInteger[] variables)
			{
				x = variables[0];
				y = variables[1];
				z = variables[2];
				t = variables[3];
				q = variables[4];
				r = variables[5];
			}

			public void run()
			{
				System.out.println("Transaction 2 started");

				new Transaction()
				{
					protected void atomic()
					{
						try
						{
							sleep(2000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}

						// RS: Y
						// WS: Z

						int localY = y.value;
						z.value = localY;

						try
						{
							sleep(1000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						// System.out.println("Transaction 2 about to commit");
					}
				};
				System.out.println("Transaction 2 finished");
			}
		}

		class TestThread3 extends Thread
		{
			final TransactionalInteger x;
			final TransactionalInteger y;
			final TransactionalInteger z;
			final TransactionalInteger t;
			final TransactionalInteger q;
			final TransactionalInteger r;

			public TestThread3(TransactionalInteger[] variables)
			{
				x = variables[0];
				y = variables[1];
				z = variables[2];
				t = variables[3];
				q = variables[4];
				r = variables[5];
			}

			public void run()
			{
				System.out.println("Transaction 3 started");

				new Transaction()
				{
					protected void atomic()
					{
						try
						{
							sleep(2000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}

						// RS: T
						// WS: Y

						int localT = t.value;
						y.value = localT;

						try
						{
							sleep(2000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						// System.out.println("Transaction 3 about to commit");
					}
				};
				System.out.println("Transaction 3 finished");
			}
		}

		class TestThread4 extends Thread
		{
			final TransactionalInteger x;
			final TransactionalInteger y;
			final TransactionalInteger z;
			final TransactionalInteger t;
			final TransactionalInteger q;
			final TransactionalInteger r;

			public TestThread4(TransactionalInteger[] variables)
			{
				x = variables[0];
				y = variables[1];
				z = variables[2];
				t = variables[3];
				q = variables[4];
				r = variables[5];
			}

			public void run()
			{
				System.out.println("Transaction 4 started");

				new Transaction()
				{
					protected void atomic()
					{
						try
						{
							sleep(2000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}

						// RS: X
						// WS: X, Y, T

						int localX = x.value;
						x.value = x.value + 1;
						y.value = x.value + 2;
						t.value = x.value + 3;

						try
						{
							sleep(3000);
						}
						catch (InterruptedException e)
						{
							e.printStackTrace();
						}
						// System.out.println("Transaction 4 about to commit");
					}
				};
				System.out.println("Transaction 4 finished");
			}
		}

		class TestThread5 extends Thread
		{
			final TransactionalInteger x;
			final TransactionalInteger y;
			final TransactionalInteger z;
			final TransactionalInteger t;
			final TransactionalInteger q;
			final TransactionalInteger r;

			public TestThread5(TransactionalInteger[] variables)
			{
				x = variables[0];
				y = variables[1];
				z = variables[2];
				t = variables[3];
				q = variables[4];
				r = variables[5];
			}

			public void run()
			{
				try
				{
					sleep(3000);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}

				new Transaction()
				{
					protected void atomic()
					{
						System.out.println("Transaction 5 started");
						// RS: R
						// WS: Q

						int localR = r.value;
						q.value = localR;
						System.out.println("Transaction 5 about to commit");
					}
				};
				System.out.println("Transaction 5 finished");
			}
		}

		Thread threads[] = new Thread[5];
		threads[0] = new TestThread1(variables);
		threads[1] = new TestThread2(variables);
		threads[2] = new TestThread3(variables);
		threads[3] = new TestThread4(variables);
		threads[4] = new TestThread5(variables);
		for (int i = 0; i < 4; i++)
		{
			threads[i].start();
		}
		for (int i = 0; i < 4; i++)
		{
			try
			{
				threads[i].join();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}

		new Transaction()
		{
			protected void atomic()
			{
				System.out.println(x.value + " " + y.value + " " + z.value + " " + t.value + " "
						+ q.value + " " + r.value);
			}

		};

		// StringBuilder builder = new StringBuilder();
		// for (int i = 0; i < variables.length(); i++)
		// builder.append(variables.get(i) + " ");
		// System.out.println(builder.toString());
	}
}
