/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.example;

import java.util.Random;

import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.TransactionObject;
import soa.paxosstm.utils.Holder;
import soa.paxosstm.utils.TransactionalHashTable;

@TransactionObject
public class Bank
{
	protected TransactionalHashTable<String, Integer> accounts;
	private static Random rand = new Random();

	public Bank()
	{
		accounts = new TransactionalHashTable<String, Integer>(1);
	}

	public String createNewAccount()
	{
		final Holder<String> accountNumber = new Holder<String>();

		new Transaction()
		{
			public void atomic()
			{
				double d = rand.nextDouble() * 0.9 + 0.1;
				int number = (int) (d * 1000000.0);
				accountNumber.set(Integer.toString(number));

				Integer balance = accounts.get(accountNumber.get());
				if (balance != null)
					this.retry();

				accounts.put(accountNumber.get(), 0);
			}
		};

		return accountNumber.get();
	}

	public void removeAccount(String accountNumber) throws NotFoundException
	{
		Integer res = accounts.remove(accountNumber);
		if (res == null)
			throw new NotFoundException("No such account. Acc#" + accountNumber);
	}

	public double checkBalance(String accountNumber) throws NotFoundException
	{
		Integer balance = accounts.get(accountNumber);
		if (balance == null)
			throw new NotFoundException("No such account. Acc#" + accountNumber);
		return balance / 100.0;
	}

	public void deposit(final String accountNumber, final double amount) throws NotFoundException
	{
		final Holder<Boolean> success = new Holder<Boolean>();
		success.set(true);

		new Transaction()
		{
			public void atomic()
			{
				Integer balance = accounts.get(accountNumber);
				if (balance == null)
				{
					success.set(false);
					commit();
				}
				Integer newBalance = (int) (balance + amount * 100.0);
				accounts.put(accountNumber, newBalance);
			}
		};

		if (!success.get())
			throw new NotFoundException("No such account. Acc#" + accountNumber);
	}

	public boolean withdraw(final String accountNumber, final double amount)
			throws NotFoundException
	{
		final Holder<Boolean> success = new Holder<Boolean>();
		final Holder<Boolean> fundsAvailable = new Holder<Boolean>();
		success.set(true);

		new Transaction()
		{
			public void atomic()
			{
				Integer balance = accounts.get(accountNumber);
				if (balance == null)
				{
					success.set(false);
					commit();
				}
				int roundedAmount = (int) (amount * 100.0);
				Integer newBalance = (int) (balance - roundedAmount);
				fundsAvailable.set(newBalance >= 0);
				if (fundsAvailable.get())
					accounts.put(accountNumber, newBalance);
			}
		};

		if (!success.get())
			throw new NotFoundException("No such account. Acc#" + accountNumber);
		return fundsAvailable.get();
	}

	public boolean transfer(final String accountNumber1, final String accountNumber2,
			final double amount) throws NotFoundException
	{
		final Holder<String> failedAccount = new Holder<String>();
		final Holder<Boolean> fundsAvailable = new Holder<Boolean>();

		new Transaction()
		{
			public void atomic()
			{
				try
				{
					fundsAvailable.set(withdraw(accountNumber1, amount));
					if (!fundsAvailable.get())
						commit();
				}
				catch (NotFoundException e)
				{
					failedAccount.set(accountNumber1);
					commit();
				}
				try
				{
					deposit(accountNumber2, amount);
				}
				catch (NotFoundException e)
				{
					failedAccount.set(accountNumber2);
					try
					{
						// compensation
						deposit(accountNumber1, amount);
					}
					catch (NotFoundException e1)
					{
					}
					commit();
				}
			}
		};

		if (failedAccount.get() != null)
			throw new NotFoundException("No such account. Acc#" + failedAccount.get());

		return fundsAvailable.get();
	}
}
