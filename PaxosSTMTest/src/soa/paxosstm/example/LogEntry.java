package soa.paxosstm.example;

public class LogEntry implements Comparable<LogEntry>
{
	public long seqNo;
	public long threadId;
	public long timestamp;
	public boolean end;
	public String message;

	public LogEntry(long seqNo, long threadId, long timestamp, boolean end, String message)
	{
		this.seqNo = seqNo;
		this.threadId = threadId;
		this.timestamp = timestamp;
		this.end = end;
		this.message = message;
	}

	public void print()
	{
		System.err.println("###\t" + threadId + "\t" + seqNo + "\t" + timestamp + "\t" + end + "\t"
				+ message);
	}

	public static LogEntry fromString(String src)
	{
		try
		{
			String[] elements = src.split("\t");
			long threadId = Long.parseLong(elements[1]);
			int seqNo = Integer.parseInt(elements[2]);
			long timestamp = Long.parseLong(elements[3]);
			boolean end = Boolean.parseBoolean(elements[4]);
			String message = elements[5];
			return new LogEntry(seqNo, threadId, timestamp, end, message);
		}
		catch (Throwable e)
		{
			return null;
		}
	}

	@Override
	public int compareTo(LogEntry other)
	{
		if (other.timestamp != timestamp)
			return (timestamp - other.timestamp > 0) ? 1 : -1;
		if (other.end != end)
			return end ? 1 : -1;
		if (other.threadId != threadId)
			return (threadId - other.threadId > 0) ? 1 : -1;
		return (seqNo - other.seqNo > 0) ? 1 : -1;
	}
}
