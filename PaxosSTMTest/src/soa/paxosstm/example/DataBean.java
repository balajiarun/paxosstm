/*  
 * Paxos STM - Distributed Software Transactional Memory framework for Java.
 * Copyright (C) 2009-2010  Tadeusz Kobus, Maciej Kokocinski. All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package soa.paxosstm.example;

import java.io.Serializable;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class DataBean implements Serializable
{
	private static final long serialVersionUID = -2813249798376273576L;

	public enum DataBeanType
	{
		String, Integer, Bean, All;

		public static DataBeanType convert(String typeString)
		{
			try
			{
				int type = java.lang.Integer.parseInt(typeString);
				switch (type)
				{
				case 0:
					return String;
				case 1:
					return Integer;
				case 2:
					return Bean;
				default:
					return All;
				}
			}
			catch (Exception e)
			{
				return All;
			}
		}
	}

	public String string;
	public int integer;
	public DataBean bean;

	DataBeanType t;

	@Override
	public String toString()
	{
		return "[" + (string != null ? string : "null") + ", " + integer + ", "
				+ (bean != null ? bean.toString() : "null") + "]";
	}

	// public void foo()
	// {
	// DataBean parent;
	// boolean parentIsProxy;
	// try
	// {
	// parent = (DataBean)
	// this.getClass().getDeclaredField("__tc_parent").get(this);
	// parentIsProxy = (Boolean)
	// this.getClass().getDeclaredField("__tc_isProxyCopy").get(
	// parent);
	// System.out.println(this + " " + parent + " " + parentIsProxy);
	// }
	// catch (IllegalArgumentException e)
	// {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// catch (SecurityException e)
	// {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// catch (IllegalAccessException e)
	// {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// catch (NoSuchFieldException e)
	// {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }
}
