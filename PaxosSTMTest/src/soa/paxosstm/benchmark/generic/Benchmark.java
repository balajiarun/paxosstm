package soa.paxosstm.benchmark.generic;

import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.Storage;
import soa.paxosstm.dstm.PaxosSTM;

public abstract class Benchmark
{
	Object commitLock = new Object();

	protected int processId;
	protected int numberOfProcesses;
	
	protected Benchmark(int processId, int numberOfProcesses, String[] args)
	{
		this.processId = processId;
		this.numberOfProcesses = numberOfProcesses;
	}
	
	public abstract void setupMaster();
	public abstract void setupSlave();
	
	public void cleanUpMaster()
	{
	}
	
	public void cleanUpSlave()
	{
	}
	
	public abstract void run();
	
	public void initCommitListener()
	{
		PaxosSTM.getInstance().addCheckpointListener(new CheckpointListener()
		{
			@Override
			public void onCheckpoint(int seqNumber, Storage storage)
			{
			}

			@Override
			public void onCheckpointFinished(int seqNumber)
			{
				synchronized (commitLock)
				{
					commitLock.notifyAll();
				}
			}
		});
	}

	public void makeSnapshot()
	{
		synchronized (commitLock)
		{
			PaxosSTM.getInstance().scheduleCheckpoint();
			try
			{
				commitLock.wait();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	protected boolean isMaster()
	{
		return processId == 0;
	}
}
