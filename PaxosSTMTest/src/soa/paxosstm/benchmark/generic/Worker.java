package soa.paxosstm.benchmark.generic;

public abstract class Worker extends Thread
{
	protected WorkerInBean inBean;
	protected WorkerOutBean outBean;
	
	public Worker(WorkerInBean inBean, WorkerOutBean outBean)
	{
		this.inBean = inBean;
		this.outBean = outBean;
	}
	
	public abstract void run();
}
