package soa.paxosstm.benchmark.generic;

import java.util.Arrays;

import soa.paxosstm.common.StorageException;
import soa.paxosstm.dstm.PaxosSTM;

public class Main
{
	public static void main(String[] args) throws StorageException,
			InstantiationException, IllegalAccessException,
			ClassNotFoundException
	{
		PaxosSTM.getInstance().start();

		int processId = PaxosSTM.getInstance().getId();
		int numberOfProcesses = PaxosSTM.getInstance().getNumberOfNodes();

		String benchmarkFactoryName = args[0];

		BenchmarkFactory factory = (BenchmarkFactory) Class.forName(
				benchmarkFactoryName).newInstance();

		Benchmark benchmark = factory.createBenchmark(processId,
				numberOfProcesses,
				args.length > 1 ? Arrays.copyOfRange(args, 1, args.length)
						: null);
		benchmark.initCommitListener();

		boolean isMaster = processId == 0 ? true : false;

		PaxosSTM.getInstance().enterBarrier("start", numberOfProcesses);

		if (isMaster)
			benchmark.setupMaster();

		PaxosSTM.getInstance().enterBarrier("setup", numberOfProcesses);

		if (!isMaster)
			benchmark.setupSlave();

		PaxosSTM.getInstance().enterBarrier("benchmark", numberOfProcesses);

		System.err.println("benchmark started");
		benchmark.run();
		System.err.println("benchmark finished");
		PaxosSTM.getInstance().enterBarrier("benchmarkFinish",
				numberOfProcesses);

		if (!isMaster)
			benchmark.cleanUpSlave();
		
		PaxosSTM.getInstance().enterBarrier("cleanUp", numberOfProcesses);

		if (isMaster)
			benchmark.cleanUpMaster();
		
		PaxosSTM.getInstance().enterBarrier("exit", numberOfProcesses);
		
		System.exit(0);
	}
}
