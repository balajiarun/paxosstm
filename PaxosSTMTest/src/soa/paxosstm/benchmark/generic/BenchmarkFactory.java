package soa.paxosstm.benchmark.generic;

import soa.paxosstm.benchmark.dummy.DummyBenchmark;
import soa.paxosstm.benchmark.dummy.DummyWorker;

public abstract class BenchmarkFactory
{
	public Benchmark createBenchmark(int processId, int numberOfProcesses, String[] args)
	{
		return new DummyBenchmark(processId, numberOfProcesses, args);
	}
	
	public Worker createWorker(WorkerInBean inBean, WorkerOutBean outBean)
	{
		return new DummyWorker(inBean, outBean);
	}
}
