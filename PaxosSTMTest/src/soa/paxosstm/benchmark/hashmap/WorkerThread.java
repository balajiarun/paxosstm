package soa.paxosstm.benchmark.hashmap;

import java.util.Random;
import java.util.concurrent.Semaphore;

import lsr.paxos.client.Client;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.TransactionStatistics;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.utils.TransactionalHashTableIntInt;

class WorkerThread extends Thread
{
	public static volatile boolean stop = false;
	public static Semaphore semaphore;

	public long startTime;
	public long stopTime;

	final int range;
	final int percent;
	final int readsPerTran;
	final int operationsPerWriteTran;
	final int writesPerTran;

	final int numberOfNodes;
	final int localNumberOfThreads;

	final int processId;
	final int threadId;

	final long reqExecutionTime;
	final long reqExecutionMillis;
	final int reqExecutionNanos;

	public int roCommits, rwCommits, rwCommitsNoWrite, aborts, localAborts,
			rollbacks, rollbacksReadOnly;
	public int totalCommitPackageSizes, totalAbortPackageSizes;
	public int totalCommitReadSetSizes, totalAbortReadSetSizes;
	public int totalCommitWriteSetSizes, totalAbortWriteSetSizes;
	public int totalCommitNewSetSizes, totalAbortNewSetSizes;
	public int totalCommitTypeSetSizes, totalAbortTypeSetSizes;

	public long totalCommitReadOnlyExecutionTimes,
			totalCommitReadWriteExecutionTimes,
			totalCommitReadWriteNoWriteExecutionTimes,
			totalAbortExecutionTimes, totalLocalAbortExecutionTimes,
			totalRollbackExecutionTimes, totalRollbackReadOnlyExecutionTimes;
	public long totalCommitCommittingPhaseLatency,
			totalAbortCommittingPhaseLatency;
	public long totalCommitAbcastDuration, totalCommitValidationTime,
			totalCommitUpdateTime, totalAbortAbcastDuration,
			totalAbortValidationTime;

	private TransactionalHashTableIntInt hashtable;

	public WorkerThread(TransactionalHashTableIntInt hashtable,
			BenchmarkParameters benchmarkParameters, int threadId)
	{
		this.hashtable = hashtable;
		this.range = benchmarkParameters.range;
		this.percent = benchmarkParameters.percent;
		this.readsPerTran = benchmarkParameters.readsPerTran;
		this.operationsPerWriteTran = benchmarkParameters.operationsPerWriteTran;
		this.writesPerTran = benchmarkParameters.writesPerTran;

		this.numberOfNodes = benchmarkParameters.numberOfNodes;
		this.localNumberOfThreads = benchmarkParameters.localNumberOfThreads;

		this.processId = benchmarkParameters.processId;
		this.threadId = threadId;

		this.reqExecutionTime = benchmarkParameters.reqExecutionTime;
		this.reqExecutionMillis = benchmarkParameters.reqExecutionTime / 1000;
		this.reqExecutionNanos = (int) benchmarkParameters.reqExecutionTime % 1000;

		setName("WorkerThread-" + threadId);
	}

	private final void elongateExecution(int v)
	{
		if (reqExecutionTime > 0)
		{
			try
			{
				semaphore.acquire();
				long timeToSleep = v % 2 + 1;
				long start = System.currentTimeMillis();
				while (System.currentTimeMillis() - start < timeToSleep)
					;
				// Thread.sleep(reqExecutionMillis, reqExecutionNanos);
				semaphore.release();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}

		}
	}

	public void run()
	{
//		int newRange = range / (numberOfNodes * localNumberOfThreads);
//		int offset = (processId * localNumberOfThreads + threadId) * newRange;

		startTime = System.currentTimeMillis();

		Random rand = new Random(startTime + hashCode());

		while (true)
		{
			while (Client.staticPrimary.get() == processId && !stop)
			{
				try
				{
					System.err.println("spie " + System.currentTimeMillis());
					Thread.sleep(2000);
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			if (stop)
				break;

			if (Math.abs(rand.nextInt()) % 100 >= percent)
			{
				final int[] keys = new int[readsPerTran];
				for (int j = 0; j < readsPerTran; j++)
				{
					keys[j] = rand.nextInt(range);
					// keys[j] = rand.nextInt(newRange) + offset;
				}
				new Transaction(true)
				{
					protected void atomic()
					{
						if (stop)
							return;

						for (int key : keys)
							hashtable.get(key);

						elongateExecution(keys[0]);
					}

					@Override
					protected void statistics(TransactionStatistics statistics)
					{
						switch (statistics.getState())
						{
						case Committed:
							roCommits++;
							totalCommitReadOnlyExecutionTimes += statistics
									.getExecutionTime();
							break;
						case RolledBack:
							rollbacksReadOnly++;
							totalRollbackReadOnlyExecutionTimes += statistics
									.getExecutionTime();
						}
					}
				};
			}
			else
			{
				final int getsPerWriteTran = operationsPerWriteTran
						- writesPerTran;
				final int[] keys = new int[getsPerWriteTran];
				for (int j = 0; j < getsPerWriteTran; j++)
				{
					keys[j] = rand.nextInt(range);
					// keys[j] = rand.nextInt(newRange) + offset;
				}
				new Transaction()
				{
					protected void atomic()
					{
						if (stop)
							return;

						Integer[] result = new Integer[writesPerTran];
						for (int j = 0; j < writesPerTran; j++)
							result[j] = hashtable.get(keys[j]);
						for (int j = 2 * writesPerTran; j < getsPerWriteTran; j++)
							hashtable.get(keys[j]);

						for (int j = 0; j < writesPerTran; j++)
						{
							if (result[j] == null)
								hashtable.put(keys[j], keys[j]);
							else
								hashtable.remove(keys[j]);
						}

						elongateExecution(keys[0]);
					}

					@Override
					protected void statistics(TransactionStatistics statistics)
					{
						switch (statistics.getState())
						{
						case Committed:

							rwCommits++;
							if (statistics.getPackageSize() != 0)
							{
								totalCommitReadWriteExecutionTimes += statistics
										.getExecutionTime();
								totalCommitCommittingPhaseLatency += statistics
										.getCommittingPhaseLatency();

								totalCommitPackageSizes += statistics
										.getPackageSize();
								totalCommitAbcastDuration += statistics
										.getAbcastDuration();
								totalCommitValidationTime += statistics
										.getValidationTime();
								totalCommitUpdateTime += statistics
										.getUpdateTime();
								totalCommitReadSetSizes += statistics
										.getReadSetSize();
								totalCommitWriteSetSizes += statistics
										.getWriteSetSize();
								totalCommitNewSetSizes += statistics
										.getNewSetSize();
								totalCommitTypeSetSizes += statistics
										.getTypeSetSize();
							}
							else
							{
								rwCommitsNoWrite++;
								totalCommitReadWriteNoWriteExecutionTimes += statistics
										.getExecutionTime();
							}
							break;

						case LocalAbort:
							localAborts++;
							totalLocalAbortExecutionTimes += statistics
									.getExecutionTime();
							break;
						case GlobalAbort:
							aborts++;
							totalAbortExecutionTimes += statistics
									.getExecutionTime();
							totalAbortAbcastDuration += statistics
									.getAbcastDuration();
							totalAbortValidationTime += statistics
									.getValidationTime();
							totalAbortCommittingPhaseLatency += statistics
									.getCommittingPhaseLatency();
							totalAbortPackageSizes += statistics
									.getPackageSize();
							totalAbortReadSetSizes += statistics
									.getReadSetSize();
							totalAbortWriteSetSizes += statistics
									.getWriteSetSize();
							totalAbortNewSetSizes += statistics.getNewSetSize();
							totalAbortTypeSetSizes += statistics
									.getTypeSetSize();
							break;
						case RolledBack:
							rollbacks++;
							totalRollbackExecutionTimes += statistics
									.getExecutionTime();
						}
					}
				};
			}
		}

		stopTime = System.currentTimeMillis();

		GlobalTransactionManager.getInstance().RRR();
	}
}
