package soa.paxosstm.benchmark.hashmap;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class BenchmarkStats
{
	public long totalTime;
	public int totalNumberOfThreads;
	
	public int totalROTransactions;
	public int totalRWTransactions;
	public int totalRWNoWriteTransactions;
	public int totalConflicts;
	public int totalLocalConflicts;
	
	public int totalRollbacks;
	public int totalRollbacksReadOnly;
		
	public long totalReadOnlyExecutionTime;
	public long totalReadWriteExecutionTime;
	public long totalReadWriteNoWriteExecutionTime;
	public long totalAbortExecutionTime;
	public long totalLocalAbortExecutionTime;
	public long totalRollbackExecutionTime;
	public long totalRollbackReadOnlyExecutionTime;
	
	public long totalCommitCommittingPhaseLatency;
	public long totalAbortCommittingPhaseLatency;
	public long totalCommitAbcastDuration;
	public long totalCommitValidationTime;
	public long totalCommitUpdateTime;
	public long totalAbortAbcastDuration;
	public long totalAbortValidationTime;
	public int totalCommitPackageSizes;
	public int totalCommitReadSetSizes;
	public int totalCommitWriteSetSizes;
	public int totalCommitNewSetSizes;
	public int totalCommitTypeSetSizes;
	public int totalAbortPackageSizes;
	public int totalAbortReadSetSizes;
	public int totalAbortWriteSetSizes;
	public int totalAbortNewSetSizes;
	public int totalAbortTypeSetSizes;

	public BenchmarkStats()
	{
	}
}
