package soa.paxosstm.benchmark.hashmap;

import java.util.Random;
import java.util.concurrent.Semaphore;

import soa.paxosstm.common.CheckpointListener;
import soa.paxosstm.common.Storage;
import soa.paxosstm.common.StorageException;
import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.global.IdPool;
import soa.paxosstm.utils.Holder;
import soa.paxosstm.utils.TransactionalHashTableIntInt;

public class Main
{
	private static final Object commitLock = new Object();

	static BenchmarkParameters benchmarkParameters = new BenchmarkParameters();

	// static int numberOfNodes;
	// static int localNumberOfThreads;
	// static int rounds;
	// static int percent;
	// static int milli = 10000;
	// static int id;
	// static int numberOfAccounts = 256;

	public static void main(String[] args) throws StorageException
	{
		PaxosSTM.getInstance().start();

		// System.out.println("Press enter to continue.");
		// Scanner sc = new Scanner(System.in);
		// sc.nextLine();

		try
		{
			benchmarkParameters.localNumberOfThreads = Integer.parseInt(args[0]);
			benchmarkParameters.capacity = Integer.parseInt(args[1]);
			benchmarkParameters.range = Integer.parseInt(args[2]);
			int reads = Integer.parseInt(args[3]);
			int writes = Integer.parseInt(args[4]);
			benchmarkParameters.writesPerTran = Integer.parseInt(args[5]);
			benchmarkParameters.rounds = Integer.parseInt(args[6]);
			benchmarkParameters.readsPerTran = Integer.parseInt(args[7]);
			benchmarkParameters.operationsPerWriteTran = Integer.parseInt(args[8]);
			benchmarkParameters.mills = Integer.parseInt(args[9]);

			benchmarkParameters.percent = 100 * writes / (reads + writes);

			if (args.length >= 11)
			{

				benchmarkParameters.reqExecutionTime = Long.parseLong(args[10]);
				int multiFactor = 1;
				if (benchmarkParameters.reqExecutionTime < 1000)
					multiFactor = 1000 / (int) benchmarkParameters.reqExecutionTime;
				int virtualNumberOfCores = multiFactor * Runtime.getRuntime().availableProcessors();
				WorkerThread.semaphore = new Semaphore(virtualNumberOfCores);
				System.err.println(virtualNumberOfCores);
				// measuring scaling with one CPU/node
				// WorkerThread.semaphore = new Semaphore(1);
			}
		}
		catch (Exception e)
		{
		}
		benchmarkParameters.numberOfNodes = PaxosSTM.getInstance().getNumberOfNodes();
		benchmarkParameters.processId = PaxosSTM.getInstance().getId();

		boolean isMaster = benchmarkParameters.processId == 0;

		initCommitListener();

		runBenchmark(isMaster);

		makeSnapshot();
		Runtime.getRuntime().gc();

		PaxosSTM.getInstance().enterBarrier("exit", benchmarkParameters.numberOfNodes);
	}

	private static void runBenchmark(boolean isMaster)
	{
		WorkerThread threads[] = new WorkerThread[benchmarkParameters.localNumberOfThreads];
		String formatEven = "%3d   %6d   %6d  %3d %7d  %7d  %7.2f   %9.2f  %7d  %7d  %9.2f  %7.3f  %5d  %5d  %5d\n";
		String formatOdd = "%3d   %6d   %6d  %3d %7d  %7d  %7.2f   %9.2f  %7d  %7d  %9.2f  %7.3f  %5d  %5d  %5d |  %7.2f  %7.2f |  %7.2f  %7.2f  %7.2f  %7.2f  %7.2f  %7.2f  %7.2f |  %7.2f  %7.2f  %7.2f  %7.2f  %7.2f |  %10.1f  %10.1f  %10.1f  %10.1f  %10.1f  %10.1f  %10.1f  %10.1f  %10.1f  %10.1f\n";

		if (benchmarkParameters.rounds % 2 == 0)
			System.out
					.println("round capacity  range  ths      RO       RW   avgTime        tps      GRO      GRW       Gtps      GAR    Glc     gc     lc");
		else
			System.out
					.println("round capacity  range  ths      RO       RW   avgTime        tps      GRO      GRW       Gtps      GAR    Glc     gc     lc |    CExRO    RExRO |      CEx   CNoWEx      AEx     lAEx      REx      CPL      APL |      Cab       CV       CU      Aab       AV |     CpkgSize        Crs         Cws         Cns         Cty     ApkgSize        Ars         Aws         Ans         Aty ");

		for (int round = 1; round <= benchmarkParameters.rounds; round++)
		{
			// TestCounter.getInstance().reset();

			if (isMaster)
			{
				// System.out.println("Setting up round...");
				masterSetup(benchmarkParameters);
			}

			PaxosSTM.getInstance().enterBarrier("init", benchmarkParameters.numberOfNodes);
			makeSnapshot();

			initWorkerThreads(threads);

			System.gc();
			System.gc();

			PaxosSTM.getInstance().enterBarrier("start", benchmarkParameters.numberOfNodes);
			// System.out.println("start");
			// long start = System.currentTimeMillis();

			long[] localCalls = processWork(threads);

			PaxosSTM.getInstance().enterBarrier("stop", benchmarkParameters.numberOfNodes);
			// long time = System.currentTimeMillis() - start;

			final BenchmarkStats stats = (BenchmarkStats) PaxosSTM.getInstance()
					.getFromSharedObjectRegistry("stats");

			final Holder<Integer> totalReadOnlyTransactions = new Holder<Integer>(0);
			final Holder<Integer> totalReadWriteTransactions = new Holder<Integer>(0);
			final Holder<Integer> totalReadWriteNoWriteTransactions = new Holder<Integer>(0);
			final Holder<Integer> totalRollbacks = new Holder<Integer>(0);
			final Holder<Integer> totalRollbacksReadOnly = new Holder<Integer>(0);
			final Holder<Integer> totalConflicts = new Holder<Integer>(0);
			final Holder<Integer> totalLocalConflicts = new Holder<Integer>(0);

			final Holder<Long> totalTime = new Holder<Long>(0l);
			final Holder<Integer> totalNumberOfThreads = new Holder<Integer>(0);

			final Holder<Long> totalReadOnlyExecutionTime = new Holder<Long>(0l);
			final Holder<Long> totalReadWriteExecutionTime = new Holder<Long>(0l);
			final Holder<Long> totalReadWriteNoWriteExecutionTime = new Holder<Long>(0l);
			final Holder<Long> totalAbortExecutionTime = new Holder<Long>(0l);
			final Holder<Long> totalLocalAbortExecutionTime = new Holder<Long>(0l);
			final Holder<Long> totalRollbackExecutionTime = new Holder<Long>(0l);
			final Holder<Long> totalRollbackReadOnlyExecutionTime = new Holder<Long>(0l);

			final Holder<Long> totalCommitCommittingPhaseLatency = new Holder<Long>(0l);
			final Holder<Long> totalAbortCommittingPhaseLatency = new Holder<Long>(0l);

			final Holder<Long> totalCommitAbcastDuration = new Holder<Long>(0l);
			final Holder<Long> totalCommitValidationTime = new Holder<Long>(0l);
			final Holder<Long> totalCommitUpdateTime = new Holder<Long>(0l);

			final Holder<Long> totalAbortAbcastDuration = new Holder<Long>(0l);
			final Holder<Long> totalAbortValidationTime = new Holder<Long>(0l);

			final Holder<Integer> totalCommitPackageSizes = new Holder<Integer>(0);
			final Holder<Integer> totalCommitReadSetSizes = new Holder<Integer>(0);
			final Holder<Integer> totalCommitWriteSetSizes = new Holder<Integer>(0);
			final Holder<Integer> totalCommitNewSetSizes = new Holder<Integer>(0);
			final Holder<Integer> totalCommitTypeSetSizes = new Holder<Integer>(0);

			final Holder<Integer> totalAbortPackageSizes = new Holder<Integer>(0);
			final Holder<Integer> totalAbortReadSetSizes = new Holder<Integer>(0);
			final Holder<Integer> totalAbortWriteSetSizes = new Holder<Integer>(0);
			final Holder<Integer> totalAbortNewSetSizes = new Holder<Integer>(0);
			final Holder<Integer> totalAbortTypeSetSizes = new Holder<Integer>(0);

			new Transaction()
			{
				@Override
				protected void atomic()
				{
					totalReadOnlyTransactions.value = stats.totalROTransactions;
					totalReadWriteTransactions.value = stats.totalRWTransactions;
					totalReadWriteNoWriteTransactions.value = stats.totalRWNoWriteTransactions;
					totalRollbacks.value = stats.totalRollbacks;
					totalRollbacksReadOnly.value = stats.totalRollbacksReadOnly;
					totalConflicts.value = stats.totalConflicts;
					totalLocalConflicts.value = stats.totalLocalConflicts;

					totalTime.value = stats.totalTime;
					totalNumberOfThreads.value = stats.totalNumberOfThreads;

					totalReadOnlyExecutionTime.value = stats.totalReadOnlyExecutionTime;
					totalReadWriteExecutionTime.value = stats.totalReadWriteExecutionTime;
					totalReadWriteNoWriteExecutionTime.value = stats.totalReadWriteNoWriteExecutionTime;
					totalAbortExecutionTime.value = stats.totalAbortExecutionTime;
					totalLocalAbortExecutionTime.value = stats.totalLocalAbortExecutionTime;
					totalRollbackExecutionTime.value = stats.totalRollbackExecutionTime;
					totalRollbackReadOnlyExecutionTime.value = stats.totalRollbackReadOnlyExecutionTime;

					totalCommitCommittingPhaseLatency.value = stats.totalCommitCommittingPhaseLatency;
					totalAbortCommittingPhaseLatency.value = stats.totalAbortCommittingPhaseLatency;

					totalCommitAbcastDuration.value = stats.totalCommitAbcastDuration;
					totalCommitValidationTime.value = stats.totalCommitValidationTime;
					totalCommitUpdateTime.value = stats.totalCommitUpdateTime;

					totalAbortAbcastDuration.value = stats.totalAbortAbcastDuration;
					totalAbortValidationTime.value = stats.totalAbortValidationTime;

					totalCommitPackageSizes.value = stats.totalCommitPackageSizes;
					totalCommitReadSetSizes.value = stats.totalCommitReadSetSizes;
					totalCommitWriteSetSizes.value = stats.totalCommitWriteSetSizes;
					totalCommitNewSetSizes.value = stats.totalCommitNewSetSizes;
					totalCommitTypeSetSizes.value = stats.totalCommitTypeSetSizes;

					totalAbortPackageSizes.value = stats.totalAbortPackageSizes;
					totalAbortReadSetSizes.value = stats.totalAbortReadSetSizes;
					totalAbortWriteSetSizes.value = stats.totalAbortWriteSetSizes;
					totalAbortNewSetSizes.value = stats.totalAbortNewSetSizes;
					totalAbortTypeSetSizes.value = stats.totalAbortTypeSetSizes;
				}
			};

			float gtps = 1f * (totalReadOnlyTransactions.value + totalReadWriteTransactions.value)
					/ (totalTime.value) * totalNumberOfThreads.value * 1000;
			float abrate = 1f
					* (totalConflicts.value + totalLocalConflicts.value)
					/ (totalConflicts.value + totalLocalConflicts.value
							+ totalReadOnlyTransactions.value + totalReadWriteTransactions.value);

			if (benchmarkParameters.rounds % 2 == 0)
				System.out
						.format(formatEven, round, benchmarkParameters.capacity,
								benchmarkParameters.range,
								benchmarkParameters.localNumberOfThreads, (int) localCalls[0],
								(int) localCalls[1], 1f * localCalls[2]
										/ benchmarkParameters.localNumberOfThreads / 1000,
								((int) localCalls[0] + (int) localCalls[1])
										/ (localCalls[2] / 1000f)
										* benchmarkParameters.localNumberOfThreads,
								totalReadOnlyTransactions.value, totalReadWriteTransactions.value,
								gtps, abrate, totalLocalConflicts.value, (int) localCalls[3],
								(int) localCalls[4]);
			else
				System.out
						.format(
								formatOdd,
								round,
								benchmarkParameters.capacity,
								benchmarkParameters.range,
								benchmarkParameters.localNumberOfThreads,
								(int) localCalls[0],
								(int) localCalls[1],
								1f * localCalls[2] / benchmarkParameters.localNumberOfThreads
										/ 1000,
								((int) localCalls[0] + (int) localCalls[1])
										/ (localCalls[2] / 1000f)
										* benchmarkParameters.localNumberOfThreads,
								totalReadOnlyTransactions.value,
								totalReadWriteTransactions.value,
								gtps,
								abrate,
								totalLocalConflicts.value,
								(int) localCalls[3],
								(int) localCalls[4],
								totalReadOnlyTransactions.value == 0 ? 0 : 1f
										* totalReadOnlyExecutionTime.value
										/ totalReadOnlyTransactions.value,
								totalRollbacksReadOnly.value == 0 ? 0 : 1f
										* totalRollbackReadOnlyExecutionTime.value
										/ totalRollbacksReadOnly.value,
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: 1f
												* totalReadWriteExecutionTime.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								totalReadWriteNoWriteTransactions.value == 0 ? 0 : 1f
										* totalReadWriteNoWriteExecutionTime.value
										/ totalReadWriteNoWriteTransactions.value,
								totalConflicts.value == 0 ? 0 : 1f * totalAbortExecutionTime.value
										/ totalConflicts.value,
								totalLocalConflicts.value == 0 ? 0 : 1f
										* totalLocalAbortExecutionTime.value
										/ totalLocalConflicts.value,
								totalRollbacks.value == 0 ? 0 : 1f
										* totalRollbackExecutionTime.value / totalRollbacks.value,
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: 1f
												* totalCommitCommittingPhaseLatency.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								totalConflicts.value == 0 ? 0 : 1f
										* totalAbortCommittingPhaseLatency.value
										/ totalConflicts.value,
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: 1f
												* totalCommitAbcastDuration.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: 1f
												* totalCommitValidationTime.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: 1f
												* totalCommitUpdateTime.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								totalConflicts.value == 0 ? 0 : 1f * totalAbortAbcastDuration.value
										/ totalConflicts.value,
								totalConflicts.value == 0 ? 0 : 1f * totalAbortValidationTime.value
										/ totalConflicts.value,
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: (float) totalCommitPackageSizes.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: (float) totalCommitReadSetSizes.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: (float) totalCommitWriteSetSizes.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: (float) totalCommitNewSetSizes.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								(totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value) == 0 ? 0
										: (float) totalCommitTypeSetSizes.value
												/ (totalReadWriteTransactions.value - totalReadWriteNoWriteTransactions.value),
								totalConflicts.value == 0 ? 0
										: (float) totalAbortPackageSizes.value
												/ totalConflicts.value,
								totalConflicts.value == 0 ? 0
										: (float) totalAbortReadSetSizes.value
												/ totalConflicts.value,
								totalConflicts.value == 0 ? 0
										: (float) totalAbortWriteSetSizes.value
												/ totalConflicts.value,
								totalConflicts.value == 0 ? 0 : (float) totalAbortNewSetSizes.value
										/ totalConflicts.value, totalConflicts.value == 0 ? 0
										: (float) totalAbortTypeSetSizes.value
												/ totalConflicts.value);

			// TestCounter.getInstance().printAvg();
			// System.out.println("latency:\t" +
			// TestCounter.getInstance().getAvgLatency());
			IdPool idPool = GlobalTransactionManager.getInstance().getSharedObjectLibrary().idPool;
			
			makeSnapshot();
			System.gc();
			System.gc();
			PaxosSTM.getInstance().enterBarrier("cleanup", benchmarkParameters.numberOfNodes);
			makeSnapshot();
			System.gc();
			System.gc();
		}

		PaxosSTM.getInstance().enterBarrier("end", benchmarkParameters.numberOfNodes);
	}

	private static void initWorkerThreads(WorkerThread[] threads)
	{
		final TransactionalHashTableIntInt hashtable = (TransactionalHashTableIntInt) PaxosSTM
				.getInstance().getFromSharedObjectRegistry("hashtable");

		for (int i = 0; i < threads.length; i++)
		{
			threads[i] = new WorkerThread(hashtable, benchmarkParameters, i);
		}
		WorkerThread.stop = false;
	}

	@SuppressWarnings("static-access")
	private static long[] processWork(final WorkerThread[] threads)
	{
		for (WorkerThread thread : threads)
		{
			thread.start();
		}

		try
		{
			Thread.currentThread().sleep(benchmarkParameters.mills);
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}

		WorkerThread.stop = true;

		final BenchmarkStats stats = (BenchmarkStats) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("stats");

		long[] localCalls = new long[5];
		for (int i = 0; i < threads.length; i++)
		{
			final WorkerThread thread = threads[i];
			try
			{
				thread.join();

				localCalls[0] += thread.roCommits;
				localCalls[1] += thread.rwCommits + thread.rwCommitsNoWrite;
				localCalls[2] += thread.stopTime - thread.startTime;
				localCalls[3] += thread.aborts;
				localCalls[4] += thread.localAborts;

				new Transaction()
				{
					@Override
					protected void atomic()
					{
						stats.totalTime += thread.stopTime - thread.startTime;
						stats.totalNumberOfThreads++;

						stats.totalROTransactions += thread.roCommits;
						stats.totalRWTransactions += thread.rwCommits;
						stats.totalRWNoWriteTransactions += thread.rwCommitsNoWrite;
						stats.totalConflicts += thread.aborts;
						stats.totalLocalConflicts += thread.localAborts;
						stats.totalRollbacks += thread.rollbacks;
						stats.totalRollbacksReadOnly += thread.rollbacksReadOnly;

						stats.totalReadOnlyExecutionTime += thread.totalCommitReadOnlyExecutionTimes;
						stats.totalReadWriteExecutionTime += thread.totalCommitReadWriteExecutionTimes;
						stats.totalReadWriteNoWriteExecutionTime += thread.totalCommitReadWriteNoWriteExecutionTimes;
						stats.totalAbortExecutionTime += thread.totalAbortExecutionTimes;
						stats.totalLocalAbortExecutionTime += thread.totalLocalAbortExecutionTimes;
						stats.totalRollbackExecutionTime += thread.totalRollbackExecutionTimes;
						stats.totalRollbackReadOnlyExecutionTime += thread.totalRollbackReadOnlyExecutionTimes;

						stats.totalCommitCommittingPhaseLatency += thread.totalCommitCommittingPhaseLatency;
						stats.totalAbortCommittingPhaseLatency += thread.totalAbortCommittingPhaseLatency;
						stats.totalCommitAbcastDuration += thread.totalCommitAbcastDuration;
						stats.totalCommitValidationTime += thread.totalCommitValidationTime;
						stats.totalCommitUpdateTime += thread.totalCommitUpdateTime;
						stats.totalAbortAbcastDuration += thread.totalAbortAbcastDuration;
						stats.totalAbortValidationTime += thread.totalAbortValidationTime;
						stats.totalCommitPackageSizes += thread.totalCommitPackageSizes;
						stats.totalCommitReadSetSizes += thread.totalCommitReadSetSizes;
						stats.totalCommitWriteSetSizes += thread.totalCommitWriteSetSizes;
						stats.totalCommitNewSetSizes += thread.totalCommitNewSetSizes;
						stats.totalCommitTypeSetSizes += thread.totalCommitTypeSetSizes;
						stats.totalAbortPackageSizes += thread.totalAbortPackageSizes;
						stats.totalAbortReadSetSizes += thread.totalAbortReadSetSizes;
						stats.totalAbortWriteSetSizes += thread.totalAbortWriteSetSizes;
						stats.totalAbortNewSetSizes += thread.totalAbortNewSetSizes;
						stats.totalAbortTypeSetSizes += thread.totalAbortTypeSetSizes;
					}
				};
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
			threads[i] = null;
		}

		return localCalls;
	}

	private static void masterSetup(final BenchmarkParameters benchmarkParameters)
	{
		new Transaction()
		{
			public void atomic()
			{
				TransactionalHashTableIntInt hashtable = new TransactionalHashTableIntInt(
						benchmarkParameters.capacity);
				BenchmarkStats stats = new BenchmarkStats();

				Random rand = new Random();
				for (int i = 0; i < benchmarkParameters.range / 2; i++)
				{
					int key;
					Integer res;
					do
					{
						key = rand.nextInt(benchmarkParameters.range);
						res = hashtable.get(key);
					} while (res != null);
					hashtable.put(key, key);
				}
				PaxosSTM.getInstance().addToSharedObjectRegistry("hashtable", hashtable);
				PaxosSTM.getInstance().addToSharedObjectRegistry("stats", stats);
			}
		};
		makeSnapshot();
	}

	private static void initCommitListener()
	{
		PaxosSTM.getInstance().addCheckpointListener(new CheckpointListener()
		{
			@Override
			public void onCheckpoint(int seqNumber, Storage storage)
			{
			}

			@Override
			public void onCheckpointFinished(int seqNumber)
			{
				synchronized (commitLock)
				{
					commitLock.notifyAll();
				}
			}
		});
	}

	private static void makeSnapshot()
	{
		synchronized (commitLock)
		{
			PaxosSTM.getInstance().scheduleCheckpoint();
			try
			{
				commitLock.wait();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
}
