package soa.paxosstm.benchmark.hashmap;

public class BenchmarkParameters
{
	int numberOfNodes;
	int processId;
	int localNumberOfThreads = 10;
	int rounds = 30;
	long mills = 10000;
	int capacity = 10000;
	int range = 10000;
	int percent = 50;
	int readsPerTran = 100;
	int operationsPerWriteTran = 10;
	int writesPerTran = 2;
	long reqExecutionTime = 0;
}
