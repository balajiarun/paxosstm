package soa.paxosstm.benchmark.gleetm;

import java.io.IOException;
import java.util.List;
import java.util.Vector;

import soa.paxosstm.benchmark.generic.Benchmark;
import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.utils.TransactionalMultiQueue;

public class LeeTM extends Benchmark
{
	public static Viewer view;
	private static List<WorkElement> controlList;

//	public static final boolean DEBUG = true;
	public static final boolean DEBUG = false;

	private static final String GRID = "grid";
	private static final String WORK_QUEUE = "workQueue";
	private static final String STATS = "stats";

	private String fileName;
	private int localNumberOfThreads;
	private int rounds;

	private LeeRouter leeRouter;
	private Grid grid;
	private TransactionalMultiQueue<WorkElement> workQueue;
	private Stats stats;

	public LeeTM(int processId, int numberOfProcesses, String[] args)
	{
		super(processId, numberOfProcesses, args);

		rounds = Integer.parseInt(args[0]);
		localNumberOfThreads = Integer.parseInt(args[1]);
		fileName = args[2];

		if (DEBUG && isMaster())
		{
			view = new Viewer(600, 600);
			view.display();
		}
	}

	@Override
	public void setupMaster()
	{
//		System.err.println("setup master");

		new Transaction()
		{
			public void atomic()
			{
				grid = new Grid(600, 600, 2);
			}
		};
		makeSnapshot();

		leeRouter = new LeeRouter(grid);

		new Transaction()
		{
			public void atomic()
			{
				workQueue = new TransactionalMultiQueue<WorkElement>(
						numberOfProcesses * 1);

				int numberOfJoints = 0;
				try
				{
					List<WorkElement> joints = FileParser.parseFile(fileName,
							leeRouter);

					for (WorkElement workElement : joints)
					{
						workQueue.offer(workElement);
					}
					numberOfJoints = joints.size();

					if (DEBUG)
					{
						controlList = new Vector<WorkElement>();
						for (WorkElement workElement : joints)
						{
							controlList.add(new WorkElement(workElement));
						}
					}
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}

				stats = new Stats();
				stats.numberOfJoints = numberOfJoints;

				PaxosSTM.getInstance().addToSharedObjectRegistry(GRID, grid);
				PaxosSTM.getInstance().addToSharedObjectRegistry(WORK_QUEUE,
						workQueue);
				PaxosSTM.getInstance().addToSharedObjectRegistry(STATS, stats);
			}
		};

		makeSnapshot();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setupSlave()
	{
//		System.err.println("setup slave");
		grid = (Grid) PaxosSTM.getInstance().getFromSharedObjectRegistry(GRID);
		workQueue = (TransactionalMultiQueue<WorkElement>) PaxosSTM
				.getInstance().getFromSharedObjectRegistry(WORK_QUEUE);
		stats = (Stats) PaxosSTM.getInstance().getFromSharedObjectRegistry(
				STATS);

		leeRouter = new LeeRouter(grid);
	}

	@Override
	public void cleanUpMaster()
	{
//		System.err.println("cleanUp master");

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				PaxosSTM.getInstance().removeFromSharedObjectRegistry(GRID);
				PaxosSTM.getInstance().removeFromSharedObjectRegistry(
						WORK_QUEUE);
				PaxosSTM.getInstance().removeFromSharedObjectRegistry(STATS);
			}
		};
	}

	@Override
	public void cleanUpSlave()
	{
//		System.err.println("cleanUp slave");
	}

	@Override
	public void run()
	{
		String format = "%3d  %3d  %8.3f  %8.3f  %5d  %5d  %5d  %5d  %4.3f  %5d  %5d  %5d  %4.3f  %4d  %4d\n";

		System.out
				.println("rNr   th      time       tps   nrJns   att   succ   fail     ar   gAtt  gSucc  gFail    gAr    ga   gla");

		for (int round = 1; round <= rounds; round++)
		{
			PaxosSTM.getInstance().enterBarrier("start", numberOfProcesses);

			if (isMaster())
				setupMaster();

			PaxosSTM.getInstance().enterBarrier("setup", numberOfProcesses);

			if (!isMaster())
				setupSlave();

			PaxosSTM.getInstance().enterBarrier("benchmark", numberOfProcesses);
			
			LeeWorker[] workers = new LeeWorker[localNumberOfThreads];
			LeeInBean[] inBeans = new LeeInBean[localNumberOfThreads];
			LeeOutBean[] outBeans = new LeeOutBean[localNumberOfThreads];
			for (int i = 0; i < localNumberOfThreads; i++)
			{
				inBeans[i] = new LeeInBean(leeRouter, grid, workQueue, stats);
				outBeans[i] = new LeeOutBean();
				workers[i] = new LeeWorker(inBeans[i], outBeans[i]);
			}

			PaxosSTM.getInstance().enterBarrier("startBenchmark", numberOfProcesses);
			
			long start = System.currentTimeMillis();

			for (LeeWorker worker : workers)
			{
				worker.start();
			}

			ViewerThread viewerThread = null;
			if (DEBUG && isMaster())
			{
				viewerThread = new ViewerThread(LeeTM.view, leeRouter, 100);
				viewerThread.start();
			}

			for (LeeWorker worker : workers)
			{
				try
				{
					worker.join();
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}

			if (LeeTM.DEBUG && isMaster())
			{
				// try
				// {
				// System.in.read();
				// }
				// catch (IOException e)
				// {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }

				viewerThread.stopFlag = true;
				try
				{
					viewerThread.join();
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
			}

			PaxosSTM.getInstance().enterBarrier("stop", numberOfProcesses);
			long time = System.currentTimeMillis() - start;

			final long[] localResults = new long[6];
			for (int i = 0; i < localNumberOfThreads; i++)
			{
				localResults[0] += outBeans[i].totalAttempts;
				localResults[1] += outBeans[i].totalSuccesses;
				localResults[2] += outBeans[i].totalFailures;
				localResults[3] += outBeans[i].totalNumThreads;
				localResults[4] += outBeans[i].totalLocalConflicts;
				localResults[5] += outBeans[i].totalGlobalConflicts;
			}

			final long[] results = new long[7];

			new Transaction()
			{
				@Override
				protected void atomic()
				{
					results[0] = stats.totalAttempts;
					results[1] = stats.totalSuccesses;
					results[2] = stats.totalFailures;
					results[3] = stats.totalNumThreads;
					results[4] = stats.totalLocalConflicts;
					results[5] = stats.totalGlobalConflicts;
					results[6] = stats.numberOfJoints;
				}
			};

			System.out.format(format, round, localNumberOfThreads, time / 1000.0,
					1000.0 * results[1] / time, results[6], localResults[0],
					localResults[1], localResults[2], 1f
							* (localResults[4] + localResults[5])
							/ localResults[0], results[0], results[1],
					results[2], 1f * (results[4] + results[5]) / results[0],
					results[5], results[4]);

			makeSnapshot();
		}
	}
}
