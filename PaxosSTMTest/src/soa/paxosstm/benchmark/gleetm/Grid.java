package soa.paxosstm.benchmark.gleetm;

import soa.paxosstm.dstm.ArrayWrapper;
import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class Grid
{
	public final int xSize, ySize, zSize;
	private ArrayWrapper<Cell> gridData;

	@TransactionObject
	public static class Cell
	{
		protected int layer1;
		protected int layer2;

		public Cell()
		{
		}

		public Cell(int layer1, int layer2)
		{
			this.layer1 = layer1;
			this.layer2 = layer2;
		}
	}

	public Grid(int xSize, int ySize, int zSize)
	{
		this.xSize = xSize;
		this.ySize = ySize;
		this.zSize = zSize;

		Cell[] initArray = new Cell[xSize * ySize];

		for (int y = 0; y < ySize; y++)
		{
			for (int x = 0; x < xSize; x++)
			{
				initArray[y * xSize + x] = new Cell();
			}
		}
		gridData = new ArrayWrapper<Cell>(initArray);
	}

	public final int get(int x, int y, int z)
	{
		if (z == 0)
			return gridData.get(y * xSize + x).layer1;
		else
			return gridData.get(y * xSize + x).layer2;
	}

	public final void set(int x, int y, int z, int value)
	{
		if (z == 0)
			gridData.get(y * xSize + x).layer1 = value;
		else
			gridData.get(y * xSize + x).layer2 = value;
	}

	public int getXSize()
	{
		return xSize;
	}

	public int getYSize()
	{
		return ySize;
	}

	public int getZSize()
	{
		return zSize;
	}
}
