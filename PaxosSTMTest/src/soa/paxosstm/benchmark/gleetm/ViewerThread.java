package soa.paxosstm.benchmark.gleetm;

import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;

public class ViewerThread extends Thread
{
	public volatile boolean stopFlag = false;
	private Viewer viewer;
	private LeeRouter leeRouter;
	private long sleep;
	
	public ViewerThread(Viewer viewer, LeeRouter leeRouter, long sleep)
	{
		this.viewer = viewer;
		this.sleep = sleep;
		this.leeRouter = leeRouter;
	}
	
	public void run()
	{
		while (!stopFlag)
		{
			new Transaction(true)
			{
				@Override
				protected void atomic()
				{
					Grid grid = (Grid) PaxosSTM.getInstance()
							.getFromSharedObjectRegistry("grid");
					
					leeRouter.draw(viewer);
					
					
					//viewer.pad(20, 50, Viewer.CYAN);
								
				}
			};
			viewer.flush();
			//System.out.println("!!!!!!!!!###############################################");
			try
			{
				Thread.sleep(sleep);
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
}
