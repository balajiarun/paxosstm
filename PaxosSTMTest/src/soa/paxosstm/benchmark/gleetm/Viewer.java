package soa.paxosstm.benchmark.gleetm;

import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

public class Viewer extends JFrame
{
	private static final long serialVersionUID = 1L;

	public static final int BLACK = 0x000000;
	public static final int BLUE = 0x0000FF;
	public static final int RED = 0xFF0000;
	public static final int GREEN = 0x00FF00;
	public static final int CYAN = 0x00FFFF;
	public static final int YELLOW = 0xFFFF00;
	public static final int MAGENTA = 0xFF00FF;
	public static final int WHITE = 0xFFFFFF;

	private BufferedImage image;

	public Viewer(int width, int heigth)
	{
		image = new BufferedImage(width, heigth, 1); // TYPE_INT_RGB
		image.flush();
		addWindowListener(new WindowAdapter()
		{
			public void windowClosing(WindowEvent e)
			{
				System.exit(0);
			}
		});
		setSize(image.getWidth() + 20, image.getHeight() + 40);
		setTitle("Picture");
	}

	public void drawSquare(int x1, int y1, int x2, int y2, int col)
	{
		for (int i = x1; i <= x2; i++)
			for (int j = y1; j <= y2; j++)
				image.setRGB(i, j, col);
	}

	public void paint(Graphics graphics)
	{
		// drawSquare(10,10,20,20,0xFFFFFF);
		// drawSquare(10,50,20,60,0xFFFF);
		// drawSquare(10,90,20,100,0xFF);
//		image.flush();
		graphics.drawImage(image, 10, 30, null);
	}

	public void display()
	{
		setVisible(true);
	}

	public void pad(int x, int y, int col)
	{
		// x = x*2;
		// y = y*2;
		drawSquare(x - 1, y - 1, x + 1, y + 1, col);
	}

	public void point(int x, int y, int col)
	{
		image.setRGB(x, y, col);
	}
	
	public void flush()
	{
		image.flush();
		repaint();
	}
}
