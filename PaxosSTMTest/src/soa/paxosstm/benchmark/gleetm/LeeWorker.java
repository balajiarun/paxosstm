package soa.paxosstm.benchmark.gleetm;

import java.util.List;

import soa.paxosstm.benchmark.generic.Worker;
import soa.paxosstm.benchmark.generic.WorkerInBean;
import soa.paxosstm.benchmark.generic.WorkerOutBean;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.TransactionStatistics;
import soa.paxosstm.utils.Holder;
import soa.paxosstm.utils.TransactionalMultiQueue;

public class LeeWorker extends Worker
{
	private final LeeRouter leeRouter;
	private final Grid grid;
	private final TransactionalMultiQueue<WorkElement> workQueue;
	private final Stats stats;

	private int tempg[];

	private int attempts;
	private int successes;
	private int failures;
	private int localConflicts;
	private int globalConflicts;

	public LeeWorker(WorkerInBean inBean, WorkerOutBean outBean)
	{
		super(inBean, outBean);

		LeeInBean in = (LeeInBean) inBean;
		this.leeRouter = in.leeRouter;
		this.grid = in.grid;
		this.workQueue = in.workQueue;
		this.stats = in.stats;
		new Transaction()
		{
			@Override
			protected void atomic()
			{
				tempg = new int[grid.xSize * grid.ySize * grid.zSize];
			}
		};

	}

	@Override
	public void run()
	{
		if (LeeTM.DEBUG)
			System.err.println("Thread " + Thread.currentThread().getName()
					+ " - start");

		while (true)
		{
			final Holder<WorkElement> workHolder = new Holder<WorkElement>();
			new Transaction()
			{
				public void atomic()
				{
					if (LeeTM.DEBUG)
						System.err.print(".");
					WorkElement myWork = workQueue.poll();
					workHolder.set(myWork);
				}
			};

			if (workHolder.get() == null)
				break;

			final Holder<List<LeeRouter.Position>> pathHolder = new Holder<List<LeeRouter.Position>>();
			final Holder<Boolean> finished = new Holder<Boolean>(false);

			while (!finished.get())
			{
				if (LeeTM.DEBUG)
					System.err.print("^");

				new Transaction(true)
				{
					public void atomic()
					{
						pathHolder.set(leeRouter.computeRoute(tempg,
								workHolder.get().x0, workHolder.get().y0,
								workHolder.get().x1, workHolder.get().y1,
								workHolder.get().jointNo));
						rollback();
					}
				};

				new Transaction()
				{
					public void atomic()
					{
						int dist = Math.abs(workHolder.get().x0
								- workHolder.get().x1)
								+ Math.abs(workHolder.get().y0
										- workHolder.get().y1);

						if (LeeTM.DEBUG)
							System.out.print("\t\tdistance=" + dist + " ");
						if (pathHolder.get() != null)
						{
							if (LeeTM.DEBUG)
							{
								System.out.print("pathLength="
										+ pathHolder.get().size() + " ");
								System.out.println("laying route "
										+ workHolder.get().jointNo + "\t"
										+ workHolder.get().x0 + " "
										+ workHolder.get().y0 + "\t"
										+ workHolder.get().x1 + " "
										+ workHolder.get().y1);
							}
							leeRouter.layRoute(pathHolder.get(),
									workHolder.get().jointNo);
						}
						else
						{
							if (LeeTM.DEBUG)
								System.err.println("cannot lay route "
										+ workHolder.get().jointNo + "\t"
										+ workHolder.get().x0 + " "
										+ workHolder.get().y0 + "\t"
										+ workHolder.get().x1 + " "
										+ workHolder.get().y1);
							rollback();
						}
					}

					@Override
					protected void statistics(TransactionStatistics statistics)
					{
						attempts++;
						switch (statistics.getState())
						{
						case Committed:
							successes++;
							finished.set(true);
							break;
						case RolledBack:
							failures++;
							finished.set(true);
							break;
						case LocalAbort:
							localConflicts++;
							break;
						case GlobalAbort:
							globalConflicts++;
							break;
						}
					}
				};
			}
		}

		((LeeOutBean) outBean).totalAttempts = attempts;
		((LeeOutBean) outBean).totalSuccesses = successes;
		((LeeOutBean) outBean).totalFailures = failures;
		((LeeOutBean) outBean).totalLocalConflicts = localConflicts;
		((LeeOutBean) outBean).totalGlobalConflicts = globalConflicts;

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				stats.totalAttempts += attempts;
				stats.totalSuccesses += successes;
				stats.totalFailures += failures;
				stats.totalNumThreads++;
				stats.totalLocalConflicts += localConflicts;
				stats.totalGlobalConflicts += globalConflicts;
			}
		};

		if (LeeTM.DEBUG)
			System.err.println("Thread " + Thread.currentThread().getName()
					+ " - stop");
	}

}
