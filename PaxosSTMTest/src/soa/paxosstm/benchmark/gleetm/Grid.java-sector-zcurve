package soa.paxosstm.benchmark.gleetm;

import soa.paxosstm.dstm.ArrayWrapper;
import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class Grid
{
	protected static final int SECTOR_EDGE_SIZE = 32;

	protected int innerXSize;
	protected int innerYSize;

	public final int xSize, ySize, zSize;
	private ArrayWrapper<ArrayWrapper<Cell>> gridData;

	@TransactionObject
	public static class Cell
	{
		protected int layer1;
		protected int layer2;

		public Cell()
		{
		}

		public Cell(int layer1, int layer2)
		{
			this.layer1 = layer1;
			this.layer2 = layer2;
		}
	}

	@SuppressWarnings("unchecked")
	public Grid(int xSize, int ySize, int zSize)
	{
		this.xSize = xSize;
		this.ySize = ySize;
		this.zSize = zSize;

		innerXSize = xSize / SECTOR_EDGE_SIZE + 1;
		innerYSize = ySize / SECTOR_EDGE_SIZE + 1;
		
		gridData = new ArrayWrapper<ArrayWrapper<Cell>>(
				new ArrayWrapper[innerXSize * innerYSize]);

		Cell[] initArray = new Cell[SECTOR_EDGE_SIZE * SECTOR_EDGE_SIZE];

		for (int y = 0; y < innerYSize; y++)
		{
			for (int x = 0; x < innerXSize; x++)
			{
				ArrayWrapper<Cell> minorGridData = new ArrayWrapper<Cell>(initArray);
				gridData.set(y * innerXSize + x, minorGridData);
			}
		}

		for (int x = 0; x < innerXSize; x++)
			for (int y = 0; y < innerYSize; y++)
			{
				final int xBlock = x;
				final int yBlock = y;
				initBlock(xBlock, yBlock);
			}

		// for (int x = 0; x < Grid.X_BLOCK_DIVISOR; x++)
		// for (int y = 0; y < Grid.Y_BLOCK_DIVISOR; y++)
		// {
		// final int xBlock = x;
		// final int yBlock = y;
		// new Transaction()
		// {
		// public void atomic()
		// {
		// grid.initBlock(xBlock, yBlock);
		// }
		// };
		// makeSnapshot();
		// }
	}

	public void init()
	{
		for (int y = 0; y < innerYSize; y++)
			for (int x = 0; x < innerXSize; x++)
				initBlock(x, y);
	}

	protected void initBlock(int x, int y)
	{
		ArrayWrapper<Cell> minorGridData = gridData
				.get(y * innerXSize + x);
		for (int mx = 0; mx < SECTOR_EDGE_SIZE; mx++)
			for (int my = 0; my < SECTOR_EDGE_SIZE; my++)
//				minorGridData.set(my * SECTOR_EDGE_SIZE + mx, new Cell(0, 0));
				minorGridData.set(interleave2(mx, my), new Cell(0, 0));
	}

	protected final Cell getCell(int x, int y)
	{
		int xBlock = x / SECTOR_EDGE_SIZE;
		int yBlock = y / SECTOR_EDGE_SIZE;
		
//		System.err.println(innerXSize + " " + innerYSize + "\t\t" + xBlock + " " + yBlock);

		ArrayWrapper<Cell> minorGridData = gridData.get(yBlock
				* innerXSize + xBlock);

		int mx = x - xBlock * SECTOR_EDGE_SIZE;
		int my = y - yBlock * SECTOR_EDGE_SIZE;

		//return minorGridData.get(my * SECTOR_EDGE_SIZE + mx);
		return minorGridData.get(interleave2(mx, my));
	}

	public final int get(int x, int y, int z)
	{
		return (z == 0) ? getCell(x, y).layer1 : getCell(x, y).layer2;
	}

	public final void set(int x, int y, int z, int value)
	{
		if (z == 0)
			getCell(x, y).layer1 = value;
		else
			getCell(x, y).layer2 = value;
	}

	private int part1by1(int n)
    {
            n &= 0x0000ffff;
            n = (n | (n << 8)) & 0x00FF00FF;
            n = (n | (n << 4)) & 0x0F0F0F0F;
            n = (n | (n << 2)) & 0x33333333;
            n = (n | (n << 1)) & 0x55555555;
            return n;
    }

    private int unpart1by1(int n)
    {
            n &= 0x55555555;
            n = (n ^ (n >> 1)) & 0x33333333;
            n = (n ^ (n >> 2)) & 0x0f0f0f0f;
            n = (n ^ (n >> 4)) & 0x00ff00ff;
            n = (n ^ (n >> 8)) & 0x0000ffff;
            return n;
    }

    private int interleave2(int x, int y)
    {
            return part1by1(x) | (part1by1(y) << 1);
    }

    private int[] deinterleave2(int n)
    {
            return new int[]
            { unpart1by1(n), unpart1by1(n >> 1) };
    }
	
	public int getXSize()
	{
		return xSize;
	}

	public int getYSize()
	{
		return ySize;
	}

	public int getZSize()
	{
		return zSize;
	}
}
