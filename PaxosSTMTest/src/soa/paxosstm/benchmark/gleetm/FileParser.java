package soa.paxosstm.benchmark.gleetm;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class FileParser
{
	public static List<WorkElement> parseFile(String fileName, LeeRouter leeRouter)
			throws IOException
	{
		Scanner scanner = new Scanner(new FileInputStream(fileName));
		boolean endOfFile = false;
		int jointNo = 1;
		ArrayList<WorkElement> joints = new ArrayList<WorkElement>();

		while (!endOfFile)
		{
			String token = scanner.next();
			char c = token.charAt(0);
			switch (c)
			{
			case 'E':
			{
				endOfFile = true;
				break;
			}
			case 'C':
			{
				int x0 = scanner.nextInt();
				int y0 = scanner.nextInt();
				int x1 = scanner.nextInt();
				int y1 = scanner.nextInt();
				leeRouter.occupy(x0, y0, x1, y1);
				break;
			}
			case 'P':
			{
				int x = scanner.nextInt();
				int y = scanner.nextInt();
				leeRouter.occupy(x, y);
				break;
			}
			case 'J':
			{
				int x0 = scanner.nextInt();
				int y0 = scanner.nextInt();
				int x1 = scanner.nextInt();
				int y1 = scanner.nextInt();
				//joints.add(new WorkElement(x0, y0, x1, y1, jointNo++));
				joints.add(0, new WorkElement(x0, y0, x1, y1, jointNo++));
				break;
			}
			default:
				throw new IOException("Error while parsing the input file.");
			}
		}
		Collections.sort(joints);
//		int ent = 0;
//		int a = 1;
//		while (a + 1 < joints.size())
//		{
//			int b = a + 1;
//			WorkElement aw = joints.get(a);
//			WorkElement bw = joints.get(b);
//			if (aw.less(bw))
//			{
//				joints.set(a, bw);
//				joints.set(b, aw);
//			}
//			ent++;
//			a++;
//		}
//		
//		for (WorkElement workElement : joints)
//		{
//			System.err.println(workElement.jointNo + "\t\t" + workElement.x0
//					+ "\t" + workElement.y0 + "\t" + workElement.x1 + "\t"
//					+ workElement.y1);
//		}
//		System.err.flush();
//		System.exit(0);
		
		// joints.setSize(1000);
		return joints;
	}
}
