package soa.paxosstm.benchmark.gleetm;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class WorkElement implements Comparable<WorkElement>
{
	protected int x0;
	protected int y0;
	protected int x1;
	protected int y1;
	protected int jointNo;
	
	public WorkElement(int x0, int y0, int x1, int y1, int jointNo)
	{
		this.x0 = x0;
		this.y0 = y0;
		this.x1 = x1;
		this.y1 = y1;
		this.jointNo = jointNo;
	}
	
	public WorkElement(WorkElement e)
	{
		this.x0 = e.x0;
		this.y0 = e.y0;
		this.x1 = e.x1;
		this.y1 = e.y1;
		this.jointNo = e.jointNo;
	}
	
	private int distSq()
	{
		return (x0 - x1) * (x0 - x1) + (y0 - y1) * (y0 - y1);
	}

	@Override
	public int compareTo(WorkElement arg0)
	{
		return distSq() - arg0.distSq();
	}
	
	public boolean less(WorkElement e) {
		return distSq() > e.distSq();
	}
}
