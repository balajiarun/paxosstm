package soa.paxosstm.benchmark.gleetm;

import soa.paxosstm.benchmark.generic.WorkerInBean;
import soa.paxosstm.utils.TransactionalMultiQueue;

public class LeeInBean extends WorkerInBean
{
	public LeeRouter leeRouter;
	public Grid grid;
	public TransactionalMultiQueue<WorkElement> workQueue;
	public Stats stats;
	
	public LeeInBean(LeeRouter leeRouter, Grid grid, TransactionalMultiQueue<WorkElement> workQueue, Stats stats)
	{
		this.leeRouter = leeRouter;
		this.grid = grid;
		this.workQueue = workQueue;
		this.stats = stats;
	}
}
