package soa.paxosstm.benchmark.gleetm;

import soa.paxosstm.benchmark.generic.WorkerOutBean;

public class LeeOutBean extends WorkerOutBean
{
	public int totalAttempts;
	public int totalSuccesses;
	public int totalFailures;
	public int totalNumThreads;
	public int totalLocalConflicts;
	public int totalGlobalConflicts;
}
