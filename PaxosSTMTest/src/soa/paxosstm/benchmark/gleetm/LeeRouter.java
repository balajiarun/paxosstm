package soa.paxosstm.benchmark.gleetm;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;

import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;

public class LeeRouter
{
	public static class Position
	{
		int x;
		int y;
		int z;

		Position(int x, int y, int z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}
	}

	Grid grid;
	int xSize;
	int ySize;
	int zSize;
	public int counter = 0;

	private static final int EMPTY = 10000;

	public static final int VIA = 0x20000000;
	public static final int BVIA = 0x40000000;
	public static final int OCCUPIED = 0x10000000;
	public static final int EXTRA_WEIGHT = 0x8000000;
	public static final int VISITED = 0x4000000;
	public static final int DISTANCE_MASK = 0xFFFFFF;

	static final int[][] dx =
	{
	{ -1, 1, 0, 0 },
	{ 0, 0, -1, 1 } };
	static final int[][] dy =
	{
	{ 0, 0, -1, 1 },
	{ -1, 1, 0, 0 } };

	static final int[] cdx =
	{ 0, 1, 0, -1 };
	static final int[] cdy =
	{ 1, 0, -1, 0 };

	private static boolean isOccupied(int val)
	{
		return (val & OCCUPIED) != 0;
	}

	private static int setOccupied(int val)
	{
		return (val | OCCUPIED);
	}

	private static boolean isEmpty(int val)
	{
		return (val == 0 || (val & ~EXTRA_WEIGHT) == 0);
	}

	private static boolean isExtraWeight(int val)
	{
		return (val & EXTRA_WEIGHT) != 0;
	}

	private static int setExtraWeight(int val)
	{
		return (val | EXTRA_WEIGHT);
	}

	private static boolean isPathElement(int val)
	{
		return (val & DISTANCE_MASK) != 0 && (val & VISITED) == 0;
	}

	private static final int getPathId(int val) // -1
	{
		return (val & (OCCUPIED | EXTRA_WEIGHT | VISITED | VIA | BVIA)) == 0 ? (val & DISTANCE_MASK)
				: -1;
	}

	private static int getDistance(int val)
	{
		return (val & VISITED) != 0 ? (val & DISTANCE_MASK) : -1;
	}

	private static final int setPathId(int id)
	{
		return (id & DISTANCE_MASK);
	}

	private static final int setDistance(int distance, boolean extraWeight)
	{
		if (extraWeight)
			return ((distance & DISTANCE_MASK) | VISITED | EXTRA_WEIGHT);
		return ((distance & DISTANCE_MASK) | VISITED);
	}

	private static boolean isVisited(int val)
	{
		return (val & VISITED) != 0;
	}

	private static final int setVia(int val)
	{
		return (val | VIA);
	}

	private static boolean isVia(int val)
	{
		return (val & VIA) != 0;
	}

	private static final int setBVia(int val)
	{
		return (val | BVIA);
	}

	private static final boolean isBVia(int val)
	{
		return (val & BVIA) != 0;
	}

	public LeeRouter(final Grid grid)
	{
		this.grid = grid;
		new Transaction()
		{
			@Override
			protected void atomic()
			{
				xSize = grid.xSize;
				ySize = grid.ySize;
				zSize = grid.zSize;
			}
		};

	}

	public void occupy(int x, int y)
	{
		grid.set(x, y, 0, setOccupied(grid.get(x, y, 0)));
		grid.set(x, y, 1, setOccupied(grid.get(x, y, 1)));

		for (int d = 0; d < 4; d++)
		{
			int wx = x + dx[0][d];
			int wy = y + dy[0][d];
			if (wx < 0 || wx >= grid.xSize)
				continue;
			if (wy < 0 || wy >= grid.ySize)
				continue;

			grid.set(wx, wy, 0, setExtraWeight(grid.get(wx, wy, 0)));
			grid.set(wx, wy, 1, setExtraWeight(grid.get(wx, wy, 1)));
		}
	}

	public void occupy(int x0, int y0, int x1, int y1)
	{
		for (int x = x0; x < x1; x++)
			for (int y = y0; y < y1; y++)
				occupy(x, y);
	}

	public List<Position> computeRoute(int[] tg, int x0, int y0, int x1,
			int y1, int id)
	{
		counter++;
		// if (counter == 620)
		// System.exit(1);

		Position s = new Position(x0, y0, 0);
		Position t = new Position(x1, y1, 0);

		// int[][][] tempg = new int[grid.xSize][grid.ySize][grid.zSize];
		Arrays.fill(tg, 0);
		// for (int x = 0; x < grid.xSize; x++)
		// for (int y = 0; y < grid.ySize; y++)
		// for (int z = 0; z < grid.zSize; z++)
		// tg[x][y][z] = 0;

		boolean bfs = breadthFirstSearch(s, t, tg);
		// System.err.println(bfs);
		if (!bfs)
			return null;

		// if (counter > 1500)
		// {
		// // for (int i = 0; i < 2; i++)
		// {
		// DebugWriter dw = new DebugWriter("results/ad_" + counter);
		// // DebugWriter dw = new DebugWriter("results/ad_" + counter +
		// // "_"
		// // + i);
		// try
		// {
		// dw.out.write(id + "\n");
		// dw.out.write(ascii(tempg));
		// // dw.out.write(asciiN(i, tempg));
		// }
		// catch (IOException e)
		// {
		// e.printStackTrace();
		// }
		// dw.close();
		// }
		// }

		List<Position> ret = backstepPath(s, t, id, tg);

		if (ret != null)
			return ret;

		return null;
	}

	private static final int SECTOR_SIZE = 4;
	private static final int SECTOR_BITS = 2; // bits

	private int getMagicNumber(int x, int y, int z)
	{
		int xb = x >> SECTOR_BITS;
		int yb = y >> SECTOR_BITS;

		int xr = x & (SECTOR_SIZE - 1);
		int yr = y & (SECTOR_SIZE - 1);

		int s = yb * (xSize >> SECTOR_BITS) + xb;

		int m = 2 * ((s << (2 * SECTOR_BITS)) + (yr << SECTOR_BITS) + xr) + z;

		return m;
	}

	private int gt(int[] tg, int x, int y, int z)
	{
		return tg[getMagicNumber(x, y, z)];
	}

	private void st(int[] tg, int x, int y, int z, int value)
	{
		tg[getMagicNumber(x, y, z)] = value;
	}

	private boolean breadthFirstSearch(Position s, Position t, int[] tg)
	{
		Queue<Position> currentFront = new ArrayDeque<Position>();
		Queue<Position> nextFront = new ArrayDeque<Position>();
		Queue<Position> afterNextFront = new ArrayDeque<Position>();

		int extraIterations = 50;

		for (int z = 0; z < zSize; z++)
		{
			currentFront.offer(new Position(s.x, s.y, z));
			st(tg, s.x, s.y, z, setDistance(1, false));
		}

		Position v = null;
		while (!currentFront.isEmpty() || !nextFront.isEmpty()
				|| !afterNextFront.isEmpty())
		{
			while (!currentFront.isEmpty())
			{
				v = currentFront.poll();

				int tThisVal = gt(tg, v.x, v.y, v.z);

				// String format =
				// "* %s \t %s \t %s \t %b \t %b \t %b \t %b \t %b \t %d \t %d \t %d\n";
				// System.err.format(format, v.x, v.y, v.z, isEmpty(thisVal),
				// isOccupied(thisVal), isExtraWeight(thisVal),
				// isVisited(tThisVal), isPathElement(thisVal),
				// getDistance(tThisVal), thisVal, tThisVal);

				for (int d = 0; d < 4; d++)
				{
					int x = v.x + cdx[d];
					int y = v.y + cdy[d];
					if (x < 1 || x >= xSize - 1)
						continue;
					if (y < 1 || y >= ySize - 1)
						continue;

					int val;
					int tval = gt(tg, x, y, v.z);
					if (tval == 0)
					{
						val = grid.get(x, y, v.z);
						st(tg, x, y, v.z, val);
					}
					else
						val = tval;

					if ((isOccupied(val) || isPathElement(val))
							&& !(x == t.x && y == t.y))
						continue;

					int prevVal = isVisited(tval) ? getDistance(tval) : EMPTY;

					int weight = (isExtraWeight(val) ? 2 : 1);

					boolean targetReached = (x == t.x && y == t.y);
					if ((prevVal > getDistance(tThisVal) + weight)
							|| targetReached)
					{
						st(tg,
								x,
								y,
								v.z,
								setDistance(getDistance(tThisVal) + weight,
										isExtraWeight(val)));

						if (!targetReached)
						{
							if (weight == 1)
								nextFront.offer(new Position(x, y, v.z));
							else
								afterNextFront.offer(new Position(x, y, v.z));
						}
					}
				}

				for (int z = v.z + 1; z < zSize; z++)
				{
					int val;
					int tval = gt(tg, v.x, v.y, z);
					if (tval == 0)
					{
						val = grid.get(v.x, v.y, z);
						st(tg, v.x, v.y, z, val);
					}
					else
						val = tval;

					if (isOccupied(val) || isPathElement(val))
						break;

					int prevVal = isVisited(tval) ? getDistance(tval) : EMPTY;

					if (prevVal > getDistance(tThisVal))
					{
						st(tg,
								v.x,
								v.y,
								z,
								setDistance(getDistance(tThisVal),
										isExtraWeight(val)));
						currentFront.offer(new Position(v.x, v.y, z));
					}
				}

				for (int z = v.z - 1; z >= 0; z--)
				{
					int val;
					int tval = gt(tg, v.x, v.y, z);
					if (tval == 0)
					{
						val = grid.get(v.x, v.y, z);
						st(tg, v.x, v.y, z, val);
					}
					else
						val = tval;

					if (isOccupied(val) || isPathElement(val))
						break;

					int prevVal = isVisited(tval) ? getDistance(tval) : EMPTY;

					if (prevVal > getDistance(tThisVal))
					{
						st(tg,
								v.x,
								v.y,
								z,
								setDistance(getDistance(tThisVal),
										isExtraWeight(val)));
						currentFront.offer(new Position(v.x, v.y, z));
					}
				}
				boolean allReached = true;
				boolean someReached = false;
				for (int z = 0; z < zSize; z++)
				{
					int tval = gt(tg, t.x, t.y, z);
					if (isVisited(tval))
						someReached = true;
					else
						allReached = false;
				}

				if (someReached && !allReached)
				{
					extraIterations = 100;
				}
				if ((extraIterations == 0 && someReached) || allReached)
				{
					return true;
				}
				else
					extraIterations--;
			}

			Queue<Position> tmp = currentFront;
			currentFront = nextFront;
			nextFront = afterNextFront;
			afterNextFront = tmp;
		}

		return false;
	}

	private boolean pathFromOtherSide(int x, int y, int z, int[] tg)
	{
		int zGoal = 1 - z;
		int goalVal = grid.get(x, y, zGoal);
		// int tGoalVal = tg[x][y][zGoal];
		int tGoalVal = gt(tg, x, y, zGoal);

		if (isVia(tGoalVal) || isBVia(tGoalVal))
			return false;

		int val = grid.get(x, y, z);
		// int tval = tg[x][y][z];
		int tval = gt(tg, x, y, z);

		int goalDistance = isEmpty(goalVal) && isEmpty(tGoalVal) ? EMPTY
				: getDistance(tGoalVal);
		int distance = isEmpty(val) ? EMPTY : getDistance(tval);

		boolean ok = (goalDistance <= distance);

		if (ok)
		{
			int west = grid.get(x - 1, y, zGoal);
			int south = grid.get(x, y + 1, zGoal);
			int east = grid.get(x + 1, y, zGoal);
			int north = grid.get(x, y - 1, zGoal);

			// int tWest = tg[x - 1][y][zGoal];
			// int tSouth = tg[x][y + 1][zGoal];
			// int tEast = tg[x + 1][y][zGoal];
			// int tNorth = tg[x][y - 1][zGoal];

			int tWest = gt(tg, x - 1, y, zGoal);
			int tSouth = gt(tg, x, y + 1, zGoal);
			int tEast = gt(tg, x + 1, y, zGoal);
			int tNorth = gt(tg, x, y - 1, zGoal);

			west = tWest == 0 || isPathElement(west) || isOccupied(west) ? EMPTY
					: getDistance(tWest);
			south = tSouth == 0 || isPathElement(south) || isOccupied(south) ? EMPTY
					: getDistance(tSouth);
			east = tEast == 0 || isPathElement(east) || isOccupied(west) ? EMPTY
					: getDistance(tEast);
			north = tNorth == 0 || isPathElement(north) || isOccupied(north) ? EMPTY
					: getDistance(tNorth);

			ok = (west < goalDistance || north < goalDistance
					|| east < goalDistance || south < goalDistance);
		}

		return ok;
	}

	private final int tlength(int x1, int y1, int x2, int y2)
	{
		int sq = (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
		return (int) Math.sqrt((double) sq);
	}

	private List<Position> backstepPath(Position s, Position t, int pathId,
			int[] tg)
	{
		final boolean DBG = false;

		List<Position> res = new ArrayList<Position>();
		int distSoFar = 0;

		int zGoal;
		if (Math.abs(t.x - s.x) > Math.abs(t.y - s.y))
			zGoal = 0;
		else
			zGoal = 1;

		int thisVal = grid.get(t.x, t.y, zGoal);
		// int tThisVal = tg[t.x][t.y][zGoal];
		int tThisVal = gt(tg, t.x, t.y, zGoal);

		if (isEmpty(thisVal) && isEmpty(tThisVal))
			zGoal = 1 - zGoal;

		int tempX = t.x;
		int tempY = t.y;
		int tempZ = zGoal;

		while ((tempX != s.x) || (tempY != s.y))
		{
			boolean advanced = false;
			int mind = 0;
			int minSquare = 100000;
			int d;
			for (d = 0; d < 4; d++)
			{
				// int tval = tg[tempX][tempY][tempZ];
				int tval = gt(tg, tempX, tempY, tempZ);
				int tmp = isVisited(tval) ? getDistance(tval) : EMPTY;

				// tval = tg[tempX + dx[tempZ][d]][tempY + dy[tempZ][d]][tempZ];
				tval = gt(tg, tempX + dx[tempZ][d], tempY + dy[tempZ][d], tempZ);
				int dir = isVisited(tval) ? getDistance(tval) : EMPTY;

				if ((dir < tmp) && !isEmpty(tval))
				{
					if (dir < minSquare)
					{
						minSquare = dir;
						mind = d;
						advanced = true;
					}
				}
			}
			if (advanced)
				distSoFar++;

			if (DBG)
				System.err.print("-- " + pathId + " " + tempX + " " + tempY
						+ " " + tempZ + "\t"
						+ pathFromOtherSide(tempX, tempY, tempZ, tg) + " "
						+ mind + " " + distSoFar + " "
						+ tlength(tempX, tempY, s.x, s.y) + "\t\t" + advanced
						+ " " + isVia(gt(tg, tempX, tempY, tempZ)) + " "
						+ isBVia(gt(tg, tempX, tempY, tempZ)));

			if (pathFromOtherSide(tempX, tempY, tempZ, tg)
					&& ((mind > 1) && (distSoFar > 15)
							&& (tlength(tempX, tempY, s.x, s.y) > 15) || (!advanced
							&& !isVia(gt(tg, tempX, tempY, tempZ)) && !isBVia(gt(
							tg, tempX, tempY, tempZ)))))
			{
				// int currentVal = tg[tempX][tempY][tempZ];
				int currentVal = gt(tg, tempX, tempY, tempZ);

				if (advanced)
				{
					st(tg, tempX, tempY, tempZ, setVia(currentVal));
					currentVal = gt(tg, tempX, tempY, 1 - tempZ);
					st(tg, tempX, tempY, 1 - tempZ, setVia(currentVal));
				}
				else
				{
					st(tg, tempX, tempY, tempZ, setBVia(currentVal));
					currentVal = gt(tg, tempX, tempY, 1 - tempZ);
					st(tg, tempX, tempY, 1 - tempZ, setBVia(currentVal));
				}

				res.add(new Position(tempX, tempY, tempZ));
				tempZ = 1 - tempZ;
				distSoFar = 0;

				if (DBG)
					System.err.println();
			}
			else
			{
				// int currentVal = tg[tempX][tempY][tempZ];
				int currentVal = gt(tg, tempX, tempY, tempZ);

				if (isVisited(currentVal))
				{
					// tg[tempX][tempY][tempZ] = setPathId(pathId);
					st(tg, tempX, tempY, tempZ, setPathId(pathId));
					res.add(new Position(tempX, tempY, tempZ));

					if (DBG)
						System.err.println(" -- placed");
				}
				else
				{
					if (DBG)
						System.err.println(" -- skipped");
				}

				tempX = tempX + dx[tempZ][mind];
				tempY = tempY + dy[tempZ][mind];
			}
		}

		res.remove(0);
		return res;
	}

	public void layRoute(List<Position> path, int pathId)
	{
		for (Position v : path)
		{
			grid.set(v.x, v.y, v.z, setOccupied(pathId));
		}
	}

	private boolean checkTrack(int x0, int y0, int x1, int y1, int pathId)
	{
		Position v = new Position(x0, y0, 0);
		while (v.x != x1 || v.y != y1)
		{
			Position nextV = null;
			for (int d = 0; d < 4; d++)
			{
				int x = v.x + dx[0][d];
				if (x < 0 || x >= grid.xSize)
					continue;
				int y = v.y + dy[0][d];
				if (y < 0 || y >= grid.ySize)
					continue;

				if (x == x1 && y == y1)
				{
					// int val = get(v.x, v.y, 1 - v.z);
					// int id = val & DISTANCE_MASK;
					// if ((val & OCCUPIED) != 0 && id == pathId)
					// {
					// set(v.x, v.y, 1 - v.z, 0);
					// }
					return true;
				}

				int val = grid.get(x, y, v.z);
				if (isOccupied(val))
					continue;

				if (getPathId(val) == pathId)
				{
					nextV = new Position(x, y, v.z);
					break;
				}
			}
			if (nextV == null)
			{
				int val = grid.get(v.x, v.y, 1 - v.z);
				if (getPathId(val) == pathId)
				{
					nextV = new Position(v.x, v.y, 1 - v.z);
				}
			}

			if (nextV == null)
			{
				if (v.x == x0 && v.y == y0 && v.z == 0)
					nextV = new Position(x0, y0, 1);
				else
					return false;
			}

			v = nextV;
			grid.set(v.x, v.y, v.z, 0);
		}

		return false;
	}

	// FIXME exclude routes that couldn't be layed down
	public boolean checkTracks(List<WorkElement> tracks)
	{
		boolean res = true;
		for (WorkElement track : tracks)
		{
			if (!checkTrack(track.x0, track.y0, track.x1, track.y1,
					track.jointNo))
			{
				System.out.println("Cannot validate track " + track.jointNo
						+ ": " + track.x0 + " " + track.y0 + " " + track.x1
						+ " " + track.y1);
				res = false;
			}
			// ((Observable) track).__tc_delete();

		}

		new Transaction(true)
		{
			@Override
			protected void atomic()
			{
				PaxosSTM.getInstance().getFromSharedObjectRegistry("grid");

				draw(LeeTM.view);
				LeeTM.view.flush();
			}
		};

		for (int z = 0; z < grid.zSize; z++)
			for (int y = 0; y < grid.ySize; y++)
				for (int x = 0; x < grid.xSize; x++)
				{
					int val = grid.get(x, y, z);
					if (getPathId(val) != 0)
					{
						System.out.println("Invalid cell: " + x + " " + y + " "
								+ z + "\t" + (val & DISTANCE_MASK) + " "
								+ (val & OCCUPIED));
						res = false;
					}
				}
		return res;
	}

	public String ascii(int[][][] tg)
	{
		StringBuilder builder = new StringBuilder();

		builder.append("================\n");
		for (int y = 0; y < grid.ySize; y++)
		{
			for (int x = 0; x < grid.xSize; x++)
			{
				int val0 = grid.get(x, y, 0);
				int val1 = grid.get(x, y, 1);

				int tval0 = tg[x][y][0];
				int tval1 = tg[x][y][1];

				if (isPathElement(val0) && !isPathElement(val1))
					builder.append("p");
				else if (!isPathElement(val0) && isPathElement(val1))
					builder.append("b");
				else if (isPathElement(val0) && isPathElement(val1))
					builder.append("B");
				else if (isVisited(tval0) && !isVisited(tval1))
					builder.append("/");
				else if (!isVisited(tval0) && isVisited(tval1))
					builder.append("\\");
				else if (isVisited(tval0) && isVisited(tval1))
					builder.append("X");
				else if (isOccupied(val0) || isOccupied(val1))
					builder.append(".");
				else
					builder.append(" ");
			}
			builder.append("\n");
		}
		builder.append("================\n");
		return builder.toString();
	}

	public String asciiN(int layer, int[][][] tg)
	{
		StringBuilder builder = new StringBuilder();

		builder.append("================\n");
		for (int y = 0; y < grid.ySize; y++)
		{
			for (int x = 0; x < grid.xSize; x++)
			{
				int val0 = grid.get(x, y, 0);
				int val1 = grid.get(x, y, 1);

				int tval0 = tg[x][y][0];
				int tval1 = tg[x][y][1];

				// if (isVisited(val0) && layer == 0)
				// builder.append(getDistance(val0) % 10);
				// else if (isVisited(val1) && layer == 1)
				// builder.append(getDistance(val1) % 10);

				if (isVisited(tval0) && layer == 0)
					builder.append(getDistance(tval0) % 10);
				else if (isVisited(tval1) && layer == 1)
					builder.append(getDistance(tval1) % 10);
				else if (isPathElement(val0) && !isPathElement(val1)
						&& layer == 0)
					builder.append("p");
				else if (!isPathElement(val0) && isPathElement(val1)
						&& layer == 1)
					builder.append("b");
				else if (isPathElement(val0) && isPathElement(val1))
					builder.append("B");
				else if (isOccupied(val0) && isOccupied(val1))
					builder.append(".");
				else
					builder.append(" ");
			}
			builder.append("\n");
		}
		builder.append("================\n");
		return builder.toString();
	}

	public void draw(Viewer view)
	{
		int[] layerCol = new int[]
		{ Viewer.MAGENTA, Viewer.GREEN };
		for (int y = 0; y < grid.ySize; y++)
			for (int x = 0; x < grid.xSize; x++)
			{

				int val0 = grid.get(x, y, 0);
				int val1 = grid.get(x, y, 1);
				if ((val1 & OCCUPIED) != 0)
				{
					if ((val1 & DISTANCE_MASK) == 0)
						view.point(x, y, Viewer.CYAN);
					else
						view.point(x, y, layerCol[1]);
				}
				else if ((val0 & OCCUPIED) != 0)
				{
					if ((val0 & DISTANCE_MASK) == 0)
						view.point(x, y, Viewer.CYAN);
					else
						view.point(x, y, layerCol[0]);
				}
				else
					view.point(x, y, Viewer.BLACK);
			}
	}
}
