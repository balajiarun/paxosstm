package soa.paxosstm.benchmark.gleetm;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class DebugWriter
{
	BufferedWriter out;
	
	public DebugWriter(String filename)
	{
		try
		{
			out = new BufferedWriter(new FileWriter(filename));
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public BufferedWriter out()
	{
		return out;
	}
	
	public void close()
	{
		try
		{
			out.close();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
