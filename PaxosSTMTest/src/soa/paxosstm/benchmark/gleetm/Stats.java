package soa.paxosstm.benchmark.gleetm;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class Stats
{
	public int totalAttempts;
	public int totalSuccesses;
	public int totalFailures;
	public int totalNumThreads;
	
	public int totalLocalConflicts;
	public int totalGlobalConflicts;
	
	public int numberOfJoints;
}
