package soa.paxosstm.benchmark.bank;

import java.util.Random;

import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.TransactionStatistics;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.global.UpdateManager;

public class WorkerThread extends Thread
{
	public static volatile boolean stop = false;
	Bank bank;

	private boolean testingJVSTMBadCase = false;
	private boolean testingReadWriteLeases = false;

	public long startTime;
	public long stopTime;

	// private int myTransferCalls = 0;
	// private int myCheckBalanceCalls = 0;

	final int numMachines;
	final int numThreads;
	final int numAccounts;
	final int processId;
	final int threadId;

	// percent transfers
	final int percent;

	public int roCommits, rwCommits, rwCommitsNoWrite, aborts, localAborts,
			rollbacks, rollbacksReadOnly;
	public int totalCommitPackageSizes, totalAbortPackageSizes;
	public int totalCommitReadSetSizes, totalAbortReadSetSizes;
	public int totalCommitWriteSetSizes, totalAbortWriteSetSizes;
	public int totalCommitNewSetSizes, totalAbortNewSetSizes;
	public int totalCommitTypeSetSizes, totalAbortTypeSetSizes;

	public long totalCommitReadOnlyExecutionTimes,
			totalCommitReadWriteExecutionTimes,
			totalCommitReadWriteNoWriteExecutionTimes,
			totalAbortExecutionTimes, totalLocalAbortExecutionTimes,
			totalRollbackExecutionTimes, totalRollbackReadOnlyExecutionTimes;
	public long totalCommitCommittingPhaseLatency,
			totalAbortCommittingPhaseLatency;
	public long totalCommitAbcastDuration, totalCommitValidationTime,
			totalCommitUpdateTime, totalAbortAbcastDuration,
			totalAbortValidationTime;

	WorkerThread(Bank bank, BenchmarkParameters benchmarkParameters,
			int threadId)
	{
		this.bank = bank;
		this.percent = benchmarkParameters.percent;
		this.numMachines = benchmarkParameters.numberOfNodes;
		this.numThreads = benchmarkParameters.localNumberOfThreads;
		this.numAccounts = benchmarkParameters.numberOfAccounts;
		this.processId = benchmarkParameters.processId;
		this.threadId = threadId;
		setName("WorkerThread-" + threadId);
	}

	public void run()
	{
		final Random random = new Random(this.hashCode());
		// random.setSeed(System.currentTimeMillis()); // comment out for
		// determinstic

		final int offset = (numAccounts / numMachines) * processId
				+ (numAccounts / (numMachines * numThreads)) * threadId;

		startTime = System.currentTimeMillis();

		while (true)
		{
			// while (Client.staticPrimary.get() == processId &&
			// !WorkerThread.stop)
			// {
			// try
			// {
			// System.err.println("spie " + System.currentTimeMillis());
			// Thread.sleep(2000);
			// }
			// catch (InterruptedException e)
			// {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }

			if (WorkerThread.stop)
				break;

			int element = random.nextInt();
//			if (testingJVSTMBadCase)
//			{
//				new Transaction()
//				{
//					@Override
//					protected void atomic()
//					{
//						if (stop)
//							return;
//
//						bank.doLotsOfReadsAndOneWrite(1, random.nextInt(10000));
//					}
//				};
//			}
//			else if (testingReadWriteLeases)
//			{
//				new Transaction()
//				{
//					@Override
//					protected void atomic()
//					{
//						if (stop)
//							return;
//
//						int write = processId + processId + 1;
//						bank.testRWLeases(write);
//					}
//				};
//			}
//			else 
			if (Math.abs(element) % 100 < percent)
			{
				final int numTransfers = 1;
				final int[] srcIndexes = new int[numTransfers];
				final int[] dstIndexes = new int[numTransfers];
				for (int i = 0; i < numTransfers; i++)
				{
					srcIndexes[i] = random.nextInt();
					dstIndexes[i] = random.nextInt();
				}
				final int additionalReads = 12;
				final int[] readIndexes = new int[additionalReads];
				for (int i = 0; i < additionalReads; i++)
					readIndexes[i] = random.nextInt();

				new Transaction()
				{
					@Override
					protected void atomic()
					{
						if (stop)
							return;

						for (int i = 0; i < numTransfers; i++)
						{
							bank.transfer(srcIndexes[i], dstIndexes[i], offset,
									numMachines, numThreads);
							// bank.transfer(0, 1, 0);
						}

						// bank.test(readIndexes);
						// int sum = 0;
						// for (int i = 0; i < readIndexes.length; i++)
						// sum += bank.accounts.get(Math.abs(readIndexes[i] %
						// numAccounts)).getBalance();
						// if (sum < 0)
						// System.err.println("to jest naprawde dziwne");
					}
					
					@Override
					protected void statistics(TransactionStatistics statistics)
					{
						switch (statistics.getState())
						{
						case Committed:

							rwCommits++;
							if (statistics.getPackageSize() != 0)
							{
								totalCommitReadWriteExecutionTimes += statistics
										.getExecutionTime();
								totalCommitCommittingPhaseLatency += statistics
										.getCommittingPhaseLatency();

								totalCommitPackageSizes += statistics
										.getPackageSize();
								totalCommitAbcastDuration += statistics
										.getAbcastDuration();
								totalCommitValidationTime += statistics
										.getValidationTime();
								totalCommitUpdateTime += statistics
										.getUpdateTime();
								totalCommitReadSetSizes += statistics
										.getReadSetSize();
								totalCommitWriteSetSizes += statistics
										.getWriteSetSize();
								totalCommitNewSetSizes += statistics
										.getNewSetSize();
								totalCommitTypeSetSizes += statistics
										.getTypeSetSize();
							}
							else
							{
								rwCommitsNoWrite++;
								totalCommitReadWriteNoWriteExecutionTimes += statistics
										.getExecutionTime();
							}
							break;

						case LocalAbort:
							localAborts++;
							totalLocalAbortExecutionTimes += statistics
									.getExecutionTime();
							break;
						case GlobalAbort:
							aborts++;
							totalAbortExecutionTimes += statistics
									.getExecutionTime();
							totalAbortAbcastDuration += statistics
									.getAbcastDuration();
							totalAbortValidationTime += statistics
									.getValidationTime();
							totalAbortCommittingPhaseLatency += statistics
									.getCommittingPhaseLatency();
							totalAbortPackageSizes += statistics
									.getPackageSize();
							totalAbortReadSetSizes += statistics
									.getReadSetSize();
							totalAbortWriteSetSizes += statistics
									.getWriteSetSize();
							totalAbortNewSetSizes += statistics.getNewSetSize();
							totalAbortTypeSetSizes += statistics
									.getTypeSetSize();
							break;
						case RolledBack:
							rollbacks++;
							totalRollbackExecutionTimes += statistics
									.getExecutionTime();
						}
					}
				};
//				if (!stop)
//					myTransferCalls++;
			}
			else
			{
				// Large RO transaction
				new Transaction(true)
				{
					@Override
					protected void atomic()
					{
						if (stop)
							return;

						bank.checkBalances();
					}
					
					@Override
					protected void statistics(TransactionStatistics statistics)
					{
						switch (statistics.getState())
						{
						case Committed:
							roCommits++;
							totalCommitReadOnlyExecutionTimes += statistics
									.getExecutionTime();
							break;
						case RolledBack:
							rollbacksReadOnly++;
							totalRollbackReadOnlyExecutionTimes += statistics
									.getExecutionTime();
						}
					}
				};
//				if (!stop)
//					myCheckBalanceCalls++;
			}
		}

		stopTime = System.currentTimeMillis();

		GlobalTransactionManager.getInstance().RRR();
	}

//	public int getMyTransferCalls()
//	{
//		return myTransferCalls;
//	}
//
//	int getMyCheckBalanceCalls()
//	{
//		return myCheckBalanceCalls;
//	}
}
