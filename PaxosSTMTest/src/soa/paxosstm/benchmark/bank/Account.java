package soa.paxosstm.benchmark.bank;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class Account
{
	private int balance;

	public Account()
	{
		balance = Bank.INITIAL_BALANCE;
	}

	int getBalance()
	{
		return balance;
	}

	void setBalance(int value)
	{
		balance = value;
	}
}
