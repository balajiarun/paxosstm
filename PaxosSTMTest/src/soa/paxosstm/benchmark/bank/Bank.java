package soa.paxosstm.benchmark.bank;

import soa.paxosstm.dstm.ArrayWrapper;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.TransactionObject;

/**
 * Base class that takes care of implementing Object ID generation and
 * Externalization
 */

@TransactionObject
public class Bank
{

	protected static boolean master = true;
	protected static final int DEFAULT_NUM_ACCOUNTS = 256;
	protected static final int INITIAL_BALANCE = 1000;

	protected int numAccounts = DEFAULT_NUM_ACCOUNTS;

	// array with all accounts
	protected ArrayWrapper<Account> accounts;

	int transferCalls = 0;
	int checkBalanceCalls = 0;

	private boolean bestCase = false;

	public Bank()
	{
	}

	public Bank(int numAccounts)
	{
		this.numAccounts = numAccounts;
		Account[] accountArray = new Account[numAccounts];
		accounts = new ArrayWrapper<Account>(accountArray);

		for (int i = 0; i < numAccounts; i++)
		{
			Account account = new Account();
			accounts.set(i, account);
		}
	}

	public void doLotsOfReadsAndOneWrite(final int accountNumber,
			final int numReads)
	{
		Account account = accounts.get(accountNumber % numAccounts);
		if (numAccounts < 0)
			throw new RuntimeException("num reads must be positive");
		int amount = 0;
		for (int i = 0; i < numReads; i++)
		{
			amount = account.getBalance();
		}
		account.setBalance(amount++);
	}

	public void transfer(final int srcAccount, final int dstAccount,
			final int offset, int numMachines, int numThreads)
	{
		final Account src, dst;

		if (bestCase)
		{
			int newMaxAccount = numAccounts / (numMachines * numThreads);
			src = accounts.get(offset + Math.abs(srcAccount % newMaxAccount));
			dst = accounts.get(offset + Math.abs(dstAccount % newMaxAccount));
			// int newMaxAccount =
			// numAccounts/BenchmarkTest.numMachines;
			// src = accounts[myOffset+Math.abs(srcAccount %
			// newMaxAccount)];
			// dst = accounts[myOffset+Math.abs(dstAccount %
			// newMaxAccount)];
			// System.out.println("BC - "+myOffset+"src:"+(myOffset+Math.abs(srcAccount
			// % newMaxAccount))+"-dst:"+(myOffset+Math.abs(dstAccount %
			// newMaxAccount)));
		}
		else
		{
			src = accounts.get(Math.abs(srcAccount % numAccounts));
			dst = accounts.get(Math.abs(dstAccount % numAccounts));
			// System.out.println("src:"+Math.abs(srcAccount %
			// numAccounts)+"-dst:"+Math.abs(dstAccount % numAccounts));
		}

		int srcAmount = src.getBalance();
		int amountToTransfer = srcAmount / 10;
		src.setBalance(srcAmount - amountToTransfer);
		dst.setBalance(dst.getBalance() + amountToTransfer);
	}
	
	public void test(final int readIndexes[])
	{
		new Transaction(true){

			@Override
			protected void atomic()
			{
				int sum = 0;
				for (int i = 0; i < readIndexes.length; i++)
					sum += accounts.get(Math.abs(readIndexes[i] % numAccounts)).getBalance();
				if (sum < 0)
					System.err.println("to jest naprawde dziwne");
			}};
		
		
	}

	public void testRWLeases(final int writeIndex)
	{
		int x1 = accounts.get(writeIndex - 1).getBalance();
		int x2 = accounts.get(writeIndex + 1).getBalance();
		int xw = accounts.get(writeIndex).getBalance();
		accounts.get(writeIndex).setBalance(xw + (x1 / 10) + (x2 / 10));
	}

	public int sumBalances()
	{
		int total = 0;
		int length = accounts.length();
		for (int i = 0; i < length; i++)
		{
			total += accounts.get(i).getBalance();
		}

		return total;
	}

	public boolean checkBalances()
	{
		int sum = sumBalances();
		if (sum != (INITIAL_BALANCE * numAccounts))
		{
			// System.out.printf("The sumBalances returned a value (%d) different than it should (%d)!\n",
			// sum, (INITIAL_BALANCE * numAccounts));
			// System.out.println("System.exit(1)");
			// System.exit(-1);
		}
		return true;
	}

	public void sanityCheck()
	{
		if (!checkBalances())
		{
			System.out.println(" --> System.exit(1); <--");
		}
	}
}
