package soa.paxosstm.benchmark.bank;

public class BenchmarkParameters
{
	int numberOfNodes;
	int localNumberOfThreads = 10;
	int rounds = 30;
	int percent = 50;
	int mills = 10000;
	int processId;
	int numberOfAccounts = 256;
}
