package soa.paxosstm.benchmark.dummy;

import soa.paxosstm.benchmark.generic.WorkerInBean;
import soa.paxosstm.utils.TransactionalInteger;

public class DummyInBean extends WorkerInBean
{
	public TransactionalInteger myInt;
	
	public DummyInBean(TransactionalInteger myInt)
	{
		this.myInt = myInt;
	}
}
