package soa.paxosstm.benchmark.dummy;

import soa.paxosstm.benchmark.generic.Benchmark;
import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.utils.TransactionalInteger;

public class DummyBenchmark extends Benchmark
{
	private static String MYINT = "myInt";
	TransactionalInteger myInt;

	public DummyBenchmark(int processId, int numberOfProcesses, String[] args)
	{
		super(processId, numberOfProcesses, args);
	}

	@Override
	public void setupMaster()
	{
		System.err.println("setup master");
		new Transaction()
		{
			@Override
			protected void atomic()
			{
				myInt = new TransactionalInteger();
				myInt.setValue(0);
				PaxosSTM.getInstance().addToSharedObjectRegistry(MYINT, myInt);
			}
		};

		makeSnapshot();
	}

	@Override
	public void setupSlave()
	{
		System.err.println("setup slave");
		myInt = (TransactionalInteger) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry(MYINT);
	}

	@Override
	public void cleanUpMaster()
	{
		System.err.println("cleanUp master");

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				System.out.println("Final value: " + myInt.getValue());
				PaxosSTM.getInstance().removeFromSharedObjectRegistry(MYINT);
			}
		};
	}

	@Override
	public void cleanUpSlave()
	{
		System.err.println("cleanUp slave");
	}

	@Override
	public void run()
	{
		System.err.println("run");

		int numberOfWorkers = 3;
		DummyWorker[] workers = new DummyWorker[numberOfWorkers];
		DummyInBean[] inBeans = new DummyInBean[numberOfWorkers];
		DummyOutBean[] outBeans = new DummyOutBean[numberOfWorkers];
		for (int i = 0; i < numberOfWorkers; i++)
		{
			inBeans[i] = new DummyInBean(myInt);
			outBeans[i] = new DummyOutBean();
			workers[i] = new DummyWorker(inBeans[i], outBeans[i]);
		}

		PaxosSTM.getInstance().enterBarrier("start", numberOfProcesses);

		for (int i = 0; i < numberOfWorkers; i++)
			workers[i].start();

		try
		{
			for (int i = 0; i < numberOfWorkers; i++)
				workers[i].join();
		}
		catch (InterruptedException e)
		{
		}

		int successes = 0;
		int failures = 0;
		for (int i = 0; i < numberOfWorkers; i++)
		{
			if (outBeans[i].success)
				successes++;
			else
				failures++;
		}

		System.err.println("run - successes: " + successes + "\t failures: "
				+ failures);

		makeSnapshot();
	}
}
