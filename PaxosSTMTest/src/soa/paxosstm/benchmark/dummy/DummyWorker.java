package soa.paxosstm.benchmark.dummy;

import soa.paxosstm.benchmark.generic.Worker;
import soa.paxosstm.benchmark.generic.WorkerInBean;
import soa.paxosstm.benchmark.generic.WorkerOutBean;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.TransactionStatistics;
import soa.paxosstm.dstm.internal.TransactionState;

public class DummyWorker extends Worker
{
	public DummyWorker(WorkerInBean inBean, WorkerOutBean outBean)
	{
		super(inBean, outBean);
	}

	@Override
	public void run()
	{
		System.err.println("Thread " + Thread.currentThread().getName()
				+ " - start");

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				((DummyInBean) inBean).myInt.increment();
				//int t = ((DummyInBean) inBean).myInt.getValue();
				//rollback();
			}

			@Override
			protected void statistics(TransactionStatistics statistics)
			{
				if (statistics.getState() == TransactionStatistics.State.LocalAbort)
				{
					System.err.println("Local abort");
					return;
				}
				if (statistics.getState() == TransactionStatistics.State.RolledBack)
				{
					System.err.println("Rollback");
					return;
				}
				if (statistics.getState() == TransactionStatistics.State.Committed
						&& statistics.getPackageSize() == 0)
				{
					System.err.println("Local commit (RO or RW with no W)");
					return;
				}
				StringBuilder builder = new StringBuilder();
				if (statistics.getState() == TransactionStatistics.State.GlobalAbort)
					builder.append("Global abort\n");
				if (statistics.getState() == TransactionStatistics.State.Committed)
					builder.append("Commit\n");
				builder.append("* Read Set:\t" + statistics.getReadSetSize() + "\n");
				builder.append("* Write Set:\t" + statistics.getWriteSetSize() + "\n");
				builder.append("* New Set:\t" + statistics.getNewSetSize() + "\n");
				builder.append("* Types Set:\t" + statistics.getTypeSetSize() + "\n");
				builder.append("* Package Set:\t" + statistics.getPackageSize() + "\n");
				System.err.print(builder.toString());
			}
		};

		((DummyOutBean) outBean).success = true;

		System.err.println("Thread " + Thread.currentThread().getName()
				+ " - stop");
	}

}
