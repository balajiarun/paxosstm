package soa.paxosstm.benchmark.dummy;

import soa.paxosstm.benchmark.generic.Benchmark;
import soa.paxosstm.benchmark.generic.BenchmarkFactory;
import soa.paxosstm.benchmark.generic.Worker;
import soa.paxosstm.benchmark.generic.WorkerInBean;
import soa.paxosstm.benchmark.generic.WorkerOutBean;

public class DummyBenchmarkFactory extends BenchmarkFactory
{
	public Benchmark createBenchmark(int processId, int numberOfProcesses, String[] args)
	{
		return new DummyBenchmark(processId, numberOfProcesses, args);
	}
	
	public Worker createWorker(WorkerInBean inBean, WorkerOutBean outBean)
	{
		return new DummyWorker(inBean, outBean);
	}
}
