package soa.paxosstm.benchmark.vacation;

import soa.paxosstm.benchmark.generic.WorkerOutBean;

public class VacationOutBean extends WorkerOutBean
{
	public int[] performedActions;
	public long duration;
}
