package soa.paxosstm.benchmark.vacation;

import java.util.Iterator;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class Manager
{
	TransactionalRBTree<Integer, Reservation> carTable;
	TransactionalRBTree<Integer, Reservation> roomTable;
	TransactionalRBTree<Integer, Reservation> flightTable;
	TransactionalRBTree<Integer, Customer> customerTable;

	public Manager()
	{
		carTable = new TransactionalRBTree<Integer, Reservation>();
		roomTable = new TransactionalRBTree<Integer, Reservation>();
		flightTable = new TransactionalRBTree<Integer, Reservation>();
		customerTable = new TransactionalRBTree<Integer, Customer>();
	}

	boolean addReservation(TransactionalRBTree<Integer, Reservation> table,
			int id, int num, int price)
	{
		Reservation reservation;

		reservation = (Reservation) table.find(id);
		if (reservation == null)
		{
			/* Create new reservation */
			if (num < 1 || price < 0)
			{
				return false;
			}
			reservation = new Reservation(id, num, price);
			// assert(reservationPtr != NULL);
			table.insert(id, reservation);
		}
		else
		{
			/* Update existing reservation */
			if (!reservation.reservation_addToTotal(num))
			{
				return false;
			}
			if (reservation.numTotal == 0)
			{
				table.remove(id);
			}
			else
			{
				reservation.reservation_updatePrice(price);
			}
		}

		return true;
	}

	/*
	 * ==========================================================================
	 * === manager_addCar -- Add cars to a city -- Adding to an existing car
	 * overwrite the price if 'price' >= 0 -- Returns TRUE on success, else
	 * FALSE
	 * ====================================================================
	 * =========
	 */
	final boolean addCar(int carId, int numCars, int price)
	{
		return addReservation(carTable, carId, numCars, price);
	}

	/*
	 * ==========================================================================
	 * === manager_deleteCar -- Delete cars from a city -- Decreases available
	 * car count (those not allocated to a customer) -- Fails if would make
	 * available car count negative -- If decresed to 0, deletes entire entry --
	 * Returns TRUE on success, else FALSE
	 * ======================================
	 * =======================================
	 */
	final boolean deleteCar(int carId, int numCar)
	{
		/* -1 keeps old price */
		return addReservation(carTable, carId, -numCar, -1);
	}

	/*
	 * ==========================================================================
	 * === manager_addRoom -- Add rooms to a city -- Adding to an existing room
	 * overwrite the price if 'price' >= 0 -- Returns TRUE on success, else
	 * FALSE
	 * ====================================================================
	 * =========
	 */
	final boolean addRoom(int roomId, int numRoom, int price)
	{
		return addReservation(roomTable, roomId, numRoom, price);
	}

	/*
	 * ==========================================================================
	 * === manager_deleteRoom -- Delete rooms from a city -- Decreases available
	 * room count (those not allocated to a customer) -- Fails if would make
	 * available room count negative -- If decresed to 0, deletes entire entry
	 * -- Returns TRUE on success, else FALSE
	 * ====================================
	 * =========================================
	 */
	final boolean deleteRoom(int roomId, int numRoom)
	{
		/* -1 keeps old price */
		return addReservation(roomTable, roomId, -numRoom, -1);
	}

	/*
	 * ==========================================================================
	 * === manager_addFlight -- Add seats to a flight -- Adding to an existing
	 * flight overwrite the price if 'price' >= 0 -- Returns TRUE on success,
	 * FALSE on failure
	 * ==========================================================
	 * ===================
	 */
	final boolean addFlight(int flightId, int numSeat, int price)
	{
		return addReservation(flightTable, flightId, numSeat, price);
	}

	/*
	 * ==========================================================================
	 * === manager_deleteFlight -- Delete an entire flight -- Fails if customer
	 * has reservation on this flight -- Returns TRUE on success, else FALSE
	 * ====
	 * =========================================================================
	 */
	boolean deleteFlight(int flightId)
	{
		Reservation reservation = (Reservation) flightTable
				.find(flightId);
		if (reservation == null)
		{
			return false;
		}

		if (reservation.numUsed > 0)
		{
			return false; /* somebody has a reservation */
		}

		return addReservation(flightTable, flightId,
				-reservation.numTotal, -1 /* -1 keeps old price */);
	}

	/*
	 * ==========================================================================
	 * === manager_addCustomer -- If customer already exists, returns failure --
	 * Returns TRUE on success, else FALSE
	 * ======================================
	 * =======================================
	 */
	boolean addCustomer(int customerId)
	{
		Customer customer;

		if (customerTable.contains(customerId))
		{
			return false;
		}

		customer = new Customer(customerId);
		// assert(customerPtr != null);
		customerTable.insert(customerId, customer);

		return true;
	}

	/*
	 * ==========================================================================
	 * === manager_deleteCustomer -- Delete this customer and associated
	 * reservations -- If customer does not exist, returns success -- Returns
	 * TRUE on success, else FALSE
	 * ==============================================
	 * ===============================
	 */
	@SuppressWarnings("unchecked")
	boolean deleteCustomer(int customerId)
	{
		Customer customer;
		TransactionalRBTree<Integer, Reservation> reservationTables[] = new TransactionalRBTree[Vacation.NUM_RESERVATION_TYPE];
		TransactionalSortedLinkedList<ReservationInfo> reservationInfoList;

		customer = (Customer) customerTable.find(customerId);
		if (customer == null)
		{
			return false;
		}

		reservationTables[Vacation.RESERVATION_CAR] = carTable;
		reservationTables[Vacation.RESERVATION_ROOM] = roomTable;
		reservationTables[Vacation.RESERVATION_FLIGHT] = flightTable;

		/* Cancel this customer's reservations */
		reservationInfoList = customer.reservationInfoList;
		Iterator<ReservationInfo> iter = reservationInfoList.iterator();
		while (iter.hasNext())
		{
			ReservationInfo reservationInfo = iter.next();
			Reservation reservationPtr = reservationTables[reservationInfo.type]
					.find(reservationInfo.id);
			reservationPtr.reservation_cancel();
		}

		customerTable.remove(customerId);
		return true;
	}

	int queryNumFree(TransactionalRBTree<Integer, Reservation> table, int id)
	{
		int numFree = -1;
		Reservation reservation = table.find(id);
		if (reservation != null)
		{
			numFree = reservation.numFree;
		}

		return numFree;
	}

	int queryPrice(TransactionalRBTree<Integer, Reservation> table, int id)
	{
		int price = -1;
		Reservation reservationPtr = table.find(id);
		if (reservationPtr != null)
		{
			price = reservationPtr.price;
		}

		return price;
	}

	final int queryCar(int carId)
	{
		return queryNumFree(carTable, carId);
	}

	final int queryCarPrice(int carId)
	{
		return queryPrice(carTable, carId);
	}

	final int queryRoom(int roomId)
	{
		return queryNumFree(roomTable, roomId);
	}

	final int queryRoomPrice(int roomId)
	{
		return queryPrice(roomTable, roomId);
	}

	final int queryFlight(int flightId)
	{
		return queryNumFree(flightTable, flightId);
	}

	final int queryFlightPrice(int flightId)
	{
		return queryPrice(flightTable, flightId);
	}

	int queryCustomerBill(int customerId)
	{
		int bill = -1;
		Customer customer;

		customer = customerTable.find(customerId);

		if (customer != null)
		{
			bill = customer.customer_getBill();
		}

		return bill;
	}

	static boolean reserve(TransactionalRBTree<Integer, Reservation> table,
			TransactionalRBTree<Integer, Customer> customerTable,
			int customerId, int id, int type)
	{
		Customer customer;
		Reservation reservation;

		customer = (Customer) customerTable.find(customerId);

		if (customer == null)
		{
			return false;
		}

		reservation = (Reservation) table.find(id);
		if (reservation == null)
		{
			return false;
		}

		if (!reservation.reservation_make())
		{
			return false;
		}

		if (!customer.customer_addReservationInfo(type, id,
				reservation.price))
		{
			/* Undo previous successful reservation */
			// boolean status =
			reservation.reservation_cancel();
			return false;
		}

		return true;
	}

	final boolean reserveCar(int customerId, int carId)
	{
		return reserve(carTable, customerTable, customerId, carId,
				Vacation.RESERVATION_CAR);
	}

	final boolean reserveRoom(int customerId, int roomId)
	{
		return reserve(roomTable, customerTable, customerId, roomId,
				Vacation.RESERVATION_ROOM);
	}

	final boolean reserveFlight(int customerId, int flightId)
	{
		return reserve(flightTable, customerTable, customerId, flightId,
				Vacation.RESERVATION_FLIGHT);
	}

	/*
	 * ==========================================================================
	 * === cancel -- Customer is not allowed to cancel multiple times -- Returns
	 * TRUE on success, else FALSE
	 * ==============================================
	 * ===============================
	 */
	static boolean cancel(TransactionalRBTree<Integer, Reservation> table,
			TransactionalRBTree<Integer, Customer> customerTable,
			int customerId, int id, int type)
	{
		Customer customerPtr;
		Reservation reservationPtr;

		customerPtr = customerTable.find(customerId);
		if (customerPtr == null)
		{
			return false;
		}

		reservationPtr = table.find(id);
		if (reservationPtr == null)
		{
			return false;
		}

		if (!reservationPtr.reservation_cancel())
		{
			return false;
		}

		if (!customerPtr.customer_removeReservationInfo(type, id))
		{
			/* Undo previous successful cancellation */
			// boolean status =
			reservationPtr.reservation_make();
			return false;
		}

		return true;
	}

	final boolean cancelCar(int customerId, int carId)
	{
		return cancel(carTable, customerTable, customerId, carId,
				Vacation.RESERVATION_CAR);
	}

	final boolean cancelRoom(int customerId, int roomId)
	{
		return cancel(roomTable, customerTable, customerId, roomId,
				Vacation.RESERVATION_ROOM);
	}

	final boolean cancelFlight(int customerId, int flightId)
	{
		return cancel(flightTable, customerTable, customerId, flightId,
				Vacation.RESERVATION_FLIGHT);
	}
}
