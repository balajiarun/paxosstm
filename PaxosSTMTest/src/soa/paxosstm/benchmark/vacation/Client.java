package soa.paxosstm.benchmark.vacation;

import soa.paxosstm.benchmark.generic.Worker;
import soa.paxosstm.benchmark.generic.WorkerInBean;
import soa.paxosstm.benchmark.generic.WorkerOutBean;
import soa.paxosstm.dstm.Transaction;

public class Client extends Worker
{
	public static volatile boolean stop = false;
	public long startTime;
	public long stopTime;
	int performedActions[] = new int[3];

	int processId;
	int id;
	Manager manager;
	Random randomPtr;
	int numOperation;
	int numQueryPerTransaction;
	int queryRange;
	int percentUser;

	public Client(WorkerInBean inBean, WorkerOutBean outBean)
	{
		super(inBean, outBean);

		VacationInBean in = (VacationInBean) inBean;

		this.processId = in.processId;
		this.randomPtr = new Random();
		this.randomPtr.random_alloc();
		this.id = in.id;
		this.manager = in.managerPtr;
		randomPtr.random_seed(id);
		this.numOperation = in.numOperation;
		this.numQueryPerTransaction = in.numQueryPerTransaction;
		this.queryRange = in.queryRange;
		this.percentUser = in.percentUser;
	}

	public int selectAction(int r, int percentUser)
	{
		if (r < percentUser)
		{
			performedActions[0]++;
			return Vacation.ACTION_MAKE_RESERVATION;
		}
		else if ((r & 1) == 1)
		{
			performedActions[1]++;
			return Vacation.ACTION_DELETE_CUSTOMER;
		}
		else
		{
			performedActions[2]++;
			return Vacation.ACTION_UPDATE_TABLES;
		}
	}

	@Override
	public void run()
	{
		final int types[] = new int[numQueryPerTransaction];
		final int ids[] = new int[numQueryPerTransaction];
		final int ops[] = new int[numQueryPerTransaction];
		final int prices[] = new int[numQueryPerTransaction];

		// int performedActions[] = new int[3];

		startTime = System.currentTimeMillis();

		while (true)
		{
			while (lsr.paxos.client.Client.staticPrimary.get() == processId && !Client.stop)
			{
				try
				{
					System.err.println("spie " + System.currentTimeMillis());
					Thread.sleep(2000);
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			//System.err.print(".");
			
			if (Client.stop)
				break;

			int r = randomPtr.posrandom_generate() % 100;
			int action = selectAction(r, percentUser);

			if (action == Vacation.ACTION_MAKE_RESERVATION)
			{
				final int maxPrices[] = new int[Vacation.NUM_RESERVATION_TYPE];
				final int maxIds[] = new int[Vacation.NUM_RESERVATION_TYPE];
				maxPrices[0] = -1;
				maxPrices[1] = -1;
				maxPrices[2] = -1;
				maxIds[0] = -1;
				maxIds[1] = -1;
				maxIds[2] = -1;

				final int numQuery = randomPtr.posrandom_generate()
						% numQueryPerTransaction + 1;
				final int customerId = randomPtr.posrandom_generate()
						% queryRange + 1;

				for (int n = 0; n < numQuery; n++)
				{
					types[n] = randomPtr.random_generate()
							% Vacation.NUM_RESERVATION_TYPE;
					ids[n] = (randomPtr.random_generate() % queryRange) + 1;
				}
				new Transaction()
				{
					@Override
					protected void atomic()
					{
						boolean isFound = false;

						for (int n = 0; n < numQuery; n++)
						{
							int t = types[n];
							int id = ids[n];
							int price = -1;
							if (t == Vacation.RESERVATION_CAR)
							{
								if (manager.queryCar(id) >= 0)
								{
									price = manager.queryCarPrice(id);
								}
							}
							else if (t == Vacation.RESERVATION_FLIGHT)
							{
								if (manager.queryFlight(id) >= 0)
								{
									price = manager.queryFlightPrice(id);
								}
							}
							else if (t == Vacation.RESERVATION_ROOM)
							{
								if (manager.queryRoom(id) >= 0)
								{
									price = manager.queryRoomPrice(id);
								}
							}
							if (price > maxPrices[t])
							{
								maxPrices[t] = price;
								maxIds[t] = id;
								isFound = true;
							}
						} /* for n */
						if (isFound)
						{
							manager.addCustomer(customerId);
						}
						if (maxIds[Vacation.RESERVATION_CAR] > 0)
						{
							manager.reserveCar(customerId,
									maxIds[Vacation.RESERVATION_CAR]);
						}
						if (maxIds[Vacation.RESERVATION_FLIGHT] > 0)
						{
							manager.reserveFlight(customerId,
									maxIds[Vacation.RESERVATION_FLIGHT]);
						}
						if (maxIds[Vacation.RESERVATION_ROOM] > 0)
						{
							manager.reserveRoom(customerId,
									maxIds[Vacation.RESERVATION_ROOM]);
						}
					}
				};

			}
			else if (action == Vacation.ACTION_DELETE_CUSTOMER)
			{
				final int customerId = randomPtr.posrandom_generate()
						% queryRange + 1;

				new Transaction()
				{
					@Override
					protected void atomic()
					{
						int bill = manager.queryCustomerBill(customerId);
						if (bill >= 0)
						{
							manager.deleteCustomer(customerId);
						}
					}
				};

			}
			else if (action == Vacation.ACTION_UPDATE_TABLES)
			{
				final int numUpdate = randomPtr.posrandom_generate()
						% numQueryPerTransaction + 1;
				for (int n = 0; n < numUpdate; n++)
				{
					types[n] = randomPtr.posrandom_generate()
							% Vacation.NUM_RESERVATION_TYPE;
					ids[n] = (randomPtr.posrandom_generate() % queryRange) + 1;
					ops[n] = randomPtr.posrandom_generate() % 2;
					if (ops[n] == 1)
					{
						prices[n] = ((randomPtr.posrandom_generate() % 5) * 10) + 50;
					}
				}

				new Transaction()
				{

					@Override
					protected void atomic()
					{
						for (int n = 0; n < numUpdate; n++)
						{
							int t = types[n];
							int id = ids[n];
							int doAdd = ops[n];
							if (doAdd == 1)
							{
								int newPrice = prices[n];
								if (t == Vacation.RESERVATION_CAR)
								{
									manager.addCar(id, 100, newPrice);
								}
								else if (t == Vacation.RESERVATION_FLIGHT)
								{
									manager.addFlight(id, 100, newPrice);
								}
								else if (t == Vacation.RESERVATION_ROOM)
								{
									manager.addRoom(id, 100, newPrice);
								}
							}
							else
							{ /* do delete */
								if (t == Vacation.RESERVATION_CAR)
								{
									manager.deleteCar(id, 100);
								}
								else if (t == Vacation.RESERVATION_FLIGHT)
								{
									manager.deleteFlight(id);
								}
								else if (t == Vacation.RESERVATION_ROOM)
								{
									manager.deleteRoom(id, 100);
								}
							}
						}
					}
				};
			}
		}

		stopTime = System.currentTimeMillis();

		((VacationOutBean) outBean).performedActions = performedActions;
		((VacationOutBean) outBean).duration = stopTime - startTime;
	}
}
