package soa.paxosstm.benchmark.vacation;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class ReservationInfo implements Comparable<ReservationInfo>
{
	int id;
	int type;
	int price;

	public ReservationInfo(int type, int id, int price)
	{
		this.type = type;
		this.id = id;
		this.price = price;
	}

	public static int reservation_info_compare(ReservationInfo aPtr,
			ReservationInfo bPtr)
	{
		int typeDiff;

		typeDiff = aPtr.type - bPtr.type;

		return ((typeDiff != 0) ? (typeDiff) : (aPtr.id - bPtr.id));
	}

	@Override
	public final int compareTo(ReservationInfo o)
	{
		int typeDiff = type - o.type;

		return ((typeDiff != 0) ? (typeDiff) : (id - o.id));
	}

}
