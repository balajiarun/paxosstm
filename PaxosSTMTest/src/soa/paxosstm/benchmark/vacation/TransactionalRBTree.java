package soa.paxosstm.benchmark.vacation;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalRBTree<K extends Comparable<K>, V>
{
	private static int RED = 0;
	private static int BLACK = 1;

	@TransactionObject
	public static class Node<K extends Comparable<K>, V> implements
			Comparable<Node<K, V>>
	{
		K k; // key
		V v; // value
		Node<K, V> p; // parent
		Node<K, V> l; // left
		Node<K, V> r; // right
		int c; // color

		public Node()
		{
		}

		@Override
		public int compareTo(Node<K, V> o)
		{
			return k.compareTo(o.k);
		}
	}

	Node<K, V> root = null;
	int compID;

	public TransactionalRBTree()
	{
		this(0);
	}

	public TransactionalRBTree(int compID)
	{
		this.compID = compID;
	}

	public int verify(int verbose)
	{
		if (root == null)
		{
			return 1;
		}
		if (verbose != 0)
		{
			System.out.println("Integrity check: ");
		}

		if (root.p != null)
		{
			System.out.println("  (WARNING) root = " + root + " parent = "
					+ root.p);
			return -1;
		}
		if (root.c != BLACK)
		{
			System.out.println("  (WARNING) root = " + root + " color = "
					+ root.c);
		}

		/* Weak check of binary-tree property */
		int ctr = 0;
		Node<K, V> its = firstEntry();
		while (its != null)
		{
			ctr++;
			Node<K, V> child = its.l;
			if (child != null && child.p != its)
			{
				System.out.println("bad parent");
			}
			child = its.r;
			if (child != null && child.p != its)
			{
				System.out.println("Bad parent");
			}
			Node<K, V> nxt = successor(its);
			if (nxt == null)
			{
				break;
			}
			if (its.compareTo(nxt) >= 0)
			{
				System.out.println("Key order " + its + " (" + its.k + " "
						+ its.v + ") " + nxt + " (" + nxt.k + " " + nxt.v
						+ ") ");
				return -3;
			}
			its = nxt;
		}

		int vfy = verifyRedBlack(root, 0);
		if (verbose != 0)
		{
			System.out.println(" Nodes = " + ctr + " Depth = " + vfy);
		}

		return vfy;

	}

	@Deprecated
	public static <K extends Comparable<K>, V> TransactionalRBTree<K, V> alloc(int compID)
	{
		TransactionalRBTree<K, V> n = new TransactionalRBTree<K, V>();
		if (n != null)
		{
			n.compID = compID;
			n.root = null;
		}

		return n;
	}

	public boolean insert(K key, V val)
	{
		Node<K, V> node = new Node<K, V>();
		Node<K, V> ex = insert(key, val, node);
		if (ex != null)
		{
			node = null;
		}
		return ex == null;
	}

	public boolean remove(K key)
	{
		Node<K, V> node = null;
		node = lookup(key);

		if (node != null)
		{
			node = deleteNode(node);
		}
		// if(node != null) {
		// this should do a release
		// }
		return node != null;
	}

	public boolean update(K key, V val)
	{
		Node<K, V> nn = new Node<K, V>();
		Node<K, V> ex = insert(key, val, nn);
		if (ex != null)
		{
			ex.v = val;
			nn = null;
			return true;
		}
		return false;
	}

	public V find(K key)
	{
		Node<K, V> n = lookup(key);
		if (n != null)
		{
			return n.v;
		}
		return null;
	}

	public boolean contains(K key)
	{
		Node<K, V> n = lookup(key);

		return (n != null);
	}

	/* private methods */

	private Node<K, V> lookup(K k)
	{
		Node<K, V> p = root;

		while (p != null)
		{
			int cmp = k.compareTo(p.k);
			if (cmp == 0)
			{
				return p;
			}
			p = (cmp < 0) ? p.l : p.r;
		}

		return null;
	}

	private void rotateLeft(Node<K, V> x)
	{
		Node<K, V> r = x.r;
		Node<K, V> rl = r.l;
		x.r = rl;
		if (rl != null)
		{
			rl.p = x;
		}

		Node<K, V> xp = x.p;
		r.p = xp;
		if (xp == null)
		{
			root = r;
		}
		else if (xp.l == x)
		{
			xp.l = r;
		}
		else
		{
			xp.r = r;
		}
		r.l = x;
		x.p = r;
	}

	private void rotateRight(Node<K, V> x)
	{
		Node<K, V> l = x.l;
		Node<K, V> lr = l.r;
		x.l = lr;
		if (lr != null)
		{
			lr.p = x;
		}
		Node<K, V> xp = x.p;
		l.p = xp;
		if (xp == null)
		{
			root = l;
		}
		else if (xp.r == x)
		{
			xp.r = l;
		}
		else
		{
			xp.l = l;
		}

		l.r = x;
		x.p = l;
	}

	private final Node<K, V> parentOf(Node<K, V> n)
	{
		return ((n != null) ? n.p : null);
	}

	private final Node<K, V> leftOf(Node<K, V> n)
	{
		return ((n != null) ? n.l : null);
	}

	private final Node<K, V> rightOf(Node<K, V> n)
	{
		return ((n != null) ? n.r : null);
	}

	private final int colorOf(Node<K, V> n)
	{
		return ((n != null) ? n.c : BLACK);
	}

	private final void setColor(Node<K, V> n, int c)
	{
		if (n != null)
		{
			n.c = c;
		}
	}

	private void fixAfterInsertion(Node<K, V> x)
	{
		x.c = RED;

		while (x != null && x != root)
		{
			Node<K, V> xp = x.p;
			if (xp.c != RED)
			{
				break;
			}

			if (parentOf(x) == leftOf(parentOf(parentOf(x))))
			{
				Node<K, V> y = rightOf(parentOf(parentOf(x)));
				if (colorOf(y) == RED)
				{
					setColor(parentOf(x), BLACK);
					setColor(y, BLACK);
					setColor(parentOf(parentOf(x)), RED);
					x = parentOf(parentOf(x));
				}
				else
				{
					if (x == rightOf(parentOf(x)))
					{
						x = parentOf(x);
						rotateLeft(x);
					}
					setColor(parentOf(x), BLACK);
					setColor(parentOf(parentOf(x)), RED);
					if (parentOf(parentOf(x)) != null)
					{
						rotateRight(parentOf(parentOf(x)));
					}
				}
			}
			else
			{
				Node<K, V> y = leftOf(parentOf(parentOf(x)));
				if (colorOf(y) == RED)
				{
					setColor(parentOf(x), BLACK);
					setColor(y, BLACK);
					setColor(parentOf(parentOf(x)), RED);
					x = parentOf(parentOf(x));
				}
				else
				{
					if (x == leftOf(parentOf(x)))
					{
						x = parentOf(x);
						rotateRight(x);
					}
					setColor(parentOf(x), BLACK);
					setColor(parentOf(parentOf(x)), RED);
					if (parentOf(parentOf(x)) != null)
					{
						rotateLeft(parentOf(parentOf(x)));
					}
				}
			}
		}

		Node<K, V> ro = root;
		if (ro.c != BLACK)
		{
			ro.c = BLACK;
		}
	}

	private Node<K, V> insert(K k, V v, Node<K, V> n)
	{
		Node<K, V> t = root;
		if (t == null)
		{
			if (n == null)
			{
				return null;
			}
			/* Note: the following STs don't really need to be transactional */
			n.l = null;
			n.r = null;
			n.p = null;
			n.k = k;
			n.v = v;
			n.c = BLACK;
			root = n;
			return null;
		}

		while (true)
		{
			int cmp = k.compareTo(t.k);
			if (cmp == 0)
			{
				return t;
			}
			else if (cmp < 0)
			{
				Node<K, V> tl = t.l;
				if (tl != null)
				{
					t = tl;
				}
				else
				{
					n.l = null;
					n.r = null;
					n.k = k;
					n.v = v;
					n.p = t;
					t.l = n;
					fixAfterInsertion(n);
					return null;
				}
			}
			else
			{ /* cmp > 0 */
				Node<K, V> tr = t.r;
				if (tr != null)
				{
					t = tr;
				}
				else
				{
					n.l = null;
					n.r = null;
					n.k = k;
					n.v = v;
					n.p = t;
					t.r = n;
					fixAfterInsertion(n);
					return null;
				}
			}
		}
	}

	private Node<K, V> successor(Node<K, V> t)
	{
		if (t == null)
		{
			return null;
		}
		else if (t.r != null)
		{
			Node<K, V> p = t.r;
			while (p.l != null)
			{
				p = p.l;
			}
			return p;
		}
		else
		{
			Node<K, V> p = t.p;
			Node<K, V> ch = t;
			while (p != null && ch == p.r)
			{
				ch = p;
				p = p.p;
			}
			return p;
		}

	}

	private void fixAfterDeletion(Node<K, V> x)
	{
		while (x != root && colorOf(x) == BLACK)
		{
			if (x == leftOf(parentOf(x)))
			{
				Node<K, V> sib = rightOf(parentOf(x));
				if (colorOf(sib) == RED)
				{
					setColor(sib, BLACK);
					setColor(parentOf(x), RED);
					rotateLeft(parentOf(x));
					sib = rightOf(parentOf(x));
				}
				if (colorOf(leftOf(sib)) == BLACK
						&& colorOf(rightOf(sib)) == BLACK)
				{
					setColor(sib, RED);
					x = parentOf(x);
				}
				else
				{
					if (colorOf(rightOf(sib)) == BLACK)
					{
						setColor(leftOf(sib), BLACK);
						setColor(sib, RED);
						rotateRight(sib);
						sib = rightOf(parentOf(x));
					}
					setColor(sib, colorOf(parentOf(x)));
					setColor(parentOf(x), BLACK);
					setColor(rightOf(sib), BLACK);
					rotateLeft(parentOf(x));
					x = root;
				}
			}
			else
			{ /* symmetric */
				Node<K, V> sib = leftOf(parentOf(x));
				if (colorOf(sib) == RED)
				{
					setColor(sib, BLACK);
					setColor(parentOf(x), RED);
					rotateRight(parentOf(x));
					sib = leftOf(parentOf(x));
				}
				if (colorOf(rightOf(sib)) == BLACK
						&& colorOf(leftOf(sib)) == BLACK)
				{
					setColor(sib, RED);
					x = parentOf(x);
				}
				else
				{
					if (colorOf(leftOf(sib)) == BLACK)
					{
						setColor(rightOf(sib), BLACK);
						setColor(sib, RED);
						rotateLeft(sib);
						sib = leftOf(parentOf(x));
					}
					setColor(sib, colorOf(parentOf(x)));
					setColor(parentOf(x), BLACK);
					setColor(leftOf(sib), BLACK);
					rotateRight(parentOf(x));
					x = root;
				}
			}
		}

		if (x != null && x.c != BLACK)
		{
			x.c = BLACK;
		}
	}

	private Node<K, V> deleteNode(Node<K, V> p)
	{
		/*
		 * If strictly internal, copy successor's element to p and then make p
		 * point to successor
		 */
		if (p.l != null && p.r != null)
		{
			Node<K, V> s = successor(p);
			p.k = s.k;
			p.v = s.v;
			p = s;
		} /* p has 2 children */

		/* Start fixup at replacement node, if it exists */
		Node<K, V> replacement = (p.l != null) ? p.l : p.r;

		if (replacement != null)
		{
			/* Link replacement to parent */
			replacement.p = p.p;
			Node<K, V> pp = p.p;
			if (pp == null)
			{
				root = replacement;
			}
			else if (p == pp.l)
			{
				pp.l = replacement;
			}
			else
			{
				pp.r = replacement;
			}

			/* Null out links so they are OK to use by fixAfterDeletion */
			p.l = null;
			p.r = null;
			p.p = null;

			/* Fix replacement */
			if (p.c == BLACK)
			{
				fixAfterDeletion(replacement);
			}
		}
		else if (p.p == null)
		{ /* return if we are the only node */
			root = null;
		}
		else
		{ /* No children. Use self as phantom replacement and unlink */
			if (p.c == BLACK)
			{
				fixAfterDeletion(p);
			}
			Node<K, V> pp = p.p;
			if (pp != null)
			{
				if (p == pp.l)
				{
					pp.l = null;
				}
				else if (p == pp.r)
				{
					pp.r = null;
				}
				p.p = null;
			}
		}
		return p;
	}

	/*
	 * Diagnostic section
	 */

	private Node<K, V> firstEntry()
	{
		Node<K, V> p = root;
		if (p != null)
		{
			while (p.l != null)
			{
				p = p.l;
			}
		}
		return p;
	}

	private int verifyRedBlack(Node<K, V> root, int depth)
	{
		int height_left;
		int height_right;

		if (root == null)
		{
			return 1;
		}

		height_left = verifyRedBlack(root.l, depth + 1);
		height_right = verifyRedBlack(root.r, depth + 1);
		if (height_left == 0 || height_right == 0)
		{
			return 0;
		}
		if (height_left != height_right)
		{
			System.out.println(" Imbalace @depth = " + depth + " : "
					+ height_left + " " + height_right);
		}

		if (root.l != null && root.l.p != root)
		{
			System.out.println(" lineage");
		}
		if (root.r != null && root.r.p != root)
		{
			System.out.println(" lineage");
		}

		/* Red-Black alternation */
		if (root.c == RED)
		{
			if (root.l != null && root.l.c != BLACK)
			{
				System.out.println("VERIFY in verifyRedBlack");
				return 0;
			}

			if (root.r != null && root.r.c != BLACK)
			{
				System.out.println("VERIFY in verifyRedBlack");
				return 0;
			}
			return height_left;
		}
		if (root.c != BLACK)
		{
			System.out.println("VERIFY in verifyRedBlack");
			return 0;
		}

		return (height_left + 1);
	}
}
