package soa.paxosstm.benchmark.vacation;

import java.util.Iterator;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class Customer
{
	int id;
	TransactionalSortedLinkedList<ReservationInfo> reservationInfoList;

	public Customer(int id)
	{
		this.id = id;
		reservationInfoList = new TransactionalSortedLinkedList<ReservationInfo>();
	}

	int customer_compare(Customer aPtr, Customer bPtr)
	{
		return (aPtr.id - bPtr.id);
	}

	boolean customer_addReservationInfo(int type, int id, int price)
	{
		ReservationInfo reservationInfoPtr = new ReservationInfo(type, id,
				price);
		// assert(reservationInfoPtr != NULL);

		return reservationInfoList.insert(reservationInfoPtr);
	}

	boolean customer_removeReservationInfo(int type, int id)
	{
		ReservationInfo findReservationInfo = new ReservationInfo(type, id, 0);

		ReservationInfo reservationInfoPtr = (ReservationInfo) reservationInfoList
				.find(findReservationInfo);

		if (reservationInfoPtr == null)
		{
			return false;
		}

		reservationInfoList.remove(findReservationInfo);
		return true;
	}

	int customer_getBill()
	{
		int bill = 0;
		Iterator<ReservationInfo> iter = reservationInfoList.iterator();
		while (iter.hasNext())
		{
			//ReservationInfo val = iter.next();
			//bill += val.price;
			bill += iter.next().price;
		}

		return bill;
	}
}
