package soa.paxosstm.benchmark.vacation;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class Stats
{
	public int totalActionMakeResevation;
	public int totalActionDeleteCustomer;
	public int totalActionUpdateTables;
	public int totalNumThreads;
	public long totalDuration;
	public int totalLocalConflicts;
}
