package soa.paxosstm.benchmark.vacation;

import java.util.Iterator;

import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class TransactionalSortedLinkedList<E extends Comparable<E>> implements
		Iterable<E>
{
	@TransactionObject
	static class Node<E>
	{
		E data = null;
		Node<E> next = null;

		public Node()
		{
		}

		public Node(E data)
		{
			this.data = data;
		}
	}

	public Node<E> head = null;

	public TransactionalSortedLinkedList()
	{
	}

	public boolean isEmpty()
	{
		return (head.next == null);
	}

	public int getSize()
	{
		int counter = 0;
		Node<E> current = head;
		while (current != null)
		{
			current = current.next;
			counter++;
		}
		return counter;
	}

	private Node<E> findPrevious(E data)
	{
		if (head == null)
			return null;
	
		Node<E> prev = null;
		Node<E> node = head;

		while (node != null && node.data.compareTo(data) < 0)
		{
			prev = node;
			node = node.next;
		}

		return prev;
	}

	public E find(E data)
	{
		Node<E> node;
		Node<E> prev = findPrevious(data);

		if (prev == null)
			node = head;
		else
			node = prev.next;

		if ((node == null) || (compare(node.data, data) != 0))
		{
			return null;
		}

		return node.data;
	}

	public final int compare(E obj1, E obj2)
	{
		return obj1.compareTo(obj2);
		//
		// Reservation_Info aPtr = (Reservation_Info) obj1;
		// Reservation_Info bPtr = (Reservation_Info) obj2;
		// int typeDiff;
		//
		// typeDiff = aPtr.type - bPtr.type;
		//
		// return ((typeDiff != 0) ? (typeDiff) : (aPtr.id - bPtr.id));
	}

	public boolean insert(E data)
	{
		Node<E> prev;
		Node<E> node;
		Node<E> curr;

		prev = findPrevious(data);
		node = new Node<E>(data);
		if (prev == null)
		{
			if (head != null)
				node.next = head;
			head = node;
		}
		else
		{
			curr = prev.next;
			node.next = curr;
			prev.next = node;
		}
		return true;
	}

	public boolean remove(E data)
	{
		Node<E> prev;
		Node<E> node;

		prev = findPrevious(data);
		if (prev == null)
		{
			if (compare(head.data, data) == 0)
			{
				head = head.next;
				return true;
			}
			else
				return false;
		}

		node = prev.next;

		if ((node != null) && (compare(node.data, data) == 0))
		{
			prev.next = node.next;
			node.next = null;
			node = null;

			return true;
		}

		return false;
	}

	int compareObject(Object obj1, Object obj2)
	{
		return 1;
	}

	public void clear()
	{
		head = null;
	}

	public class Iter<T> implements Iterator<T>
	{
		Node<T> next = null;
		Node<T> lastReturned = null;
		Node<T> lastBeforeThat = null;

		@SuppressWarnings("unchecked")
		Iter()
		{
			next = (Node<T>) head;
		}

		@Override
		public boolean hasNext()
		{
			return next != null;
		}

		@Override
		public T next()
		{
			lastBeforeThat = lastReturned;
			lastReturned = next;
			next = next.next;
			return lastReturned.data;
		}

		@SuppressWarnings("unchecked")
		@Override
		public void remove()
		{
			if (lastReturned == null)
				return;
			if (lastBeforeThat == null) // returned head
			{
				head = (Node<E>) next;
			}
			else
			{
				lastBeforeThat.next = next;
			}
			lastReturned = null;
		}

	}

	@Override
	public Iterator<E> iterator()
	{
		return new Iter<E>();
	}

	public void print()
	{
		Node<E> node = head;
		System.out.print("[");
		while (node != null)
		{
			System.out.print(node.data + " ");
			node = node.next;
		}
		System.out.println("]");
	}
}
