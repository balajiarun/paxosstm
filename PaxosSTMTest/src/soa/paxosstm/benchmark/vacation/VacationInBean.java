package soa.paxosstm.benchmark.vacation;

import soa.paxosstm.benchmark.generic.WorkerInBean;

public class VacationInBean extends WorkerInBean
{
	public int processId;
	public int id;
	public Manager managerPtr;
	public int numOperation;
	public int numQueryPerTransaction;
	public int queryRange;
	public int percentUser;
	
	public VacationInBean(int processId, int id, Manager managerPtr, int numQueriesPerTransactions, int queryRange, int percentOfUser)
	{
		this.processId = processId;
		this.id = id;
		this.managerPtr = managerPtr;
		this.numQueryPerTransaction = numQueriesPerTransactions;
		this.queryRange = queryRange;
		this.percentUser = percentOfUser;
	}
}
