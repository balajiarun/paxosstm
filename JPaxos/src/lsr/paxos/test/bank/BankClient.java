package lsr.paxos.test.bank;

import java.io.IOException;
import java.util.Random;

import lsr.paxos.ReplicationException;
import lsr.paxos.client.Client;
import lsr.paxos.test.utils.BarrierClient;
import lsr.paxos.test.utils.SimpleClient;

public class BankClient
{

	private Client client;
	private RandomRequestGenerator requestGenerator;
	private final Random random = new Random();

	private int accountsNum;
	private int percent;
	private long millis;
	private boolean isSimpleClient = false;
	private String host;
	private int port;
	private int requests;
	private long start, end;

	public BankClient(int accountsNum, int percent, long millis)
	{
		this.accountsNum = accountsNum;
		this.percent = percent;
		this.millis = millis;
	}

	public BankClient(int accountsNum, int percent, long millis, String host,
			int port)
	{
		this.accountsNum = accountsNum;
		this.percent = percent;
		this.millis = millis;
		this.isSimpleClient = true;
		this.host = host;
		this.port = port;
	}

	public void run() throws IOException, ReplicationException
	{
		if (!isSimpleClient)
		{
			client = new Client();
		}
		else
		{
			client = new SimpleClient(host, port);
		}
		client.connect();

		requestGenerator = new RandomRequestGenerator(this.hashCode()
				+ (int) System.currentTimeMillis());
		execute(accountsNum, percent, millis);
		if (isSimpleClient)
			((SimpleClient) client).disconnect();
	}

	private void execute(int accountsNum, int percent, long millis)
			throws ReplicationException
	{

		requests = 0;
		start = System.currentTimeMillis();
		while ((end = System.currentTimeMillis()) - start <= millis)
		{
			byte[] request = requestGenerator.generate(accountsNum, percent);
			byte[] response = client.execute(request);
			requests++;
		}
		// System.out.println((end-start)/1000f);
	}

	public static class TestThread extends Thread
	{
		public BankClient client;
		private String host;
		private int port;

		public TestThread(BankClient client, String host, int port)
		{
			this.client = client;
			this.host = host;
			this.port = port;
		}

		public void run()
		{
			// BarrierClient barrierClient = new BarrierClient();
			// barrierClient.enterBarrier(host, port, 0, 0);
			try
			{
				client.run();
			}
			catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (ReplicationException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			// barrierClient.enterBarrier(host, port, client.requests,
			// client.end - client.start);
		}
	}

	public static void main(String[] args) throws IOException,
			ReplicationException
	{

		String host = "localhost";
		int port = 1234;

		int numberOfThreads = 20;
		int accountsNum = 10000;
		int percent = 100;
		int rounds = 30;
		long millis = 10000;

		String serviceHost = "localhost";
		int servicePort = 4444;

		try
		{
			numberOfThreads = Integer.parseInt(args[0]);
			accountsNum = Integer.parseInt(args[1]);
			percent = Integer.parseInt(args[2]);
			rounds = Integer.parseInt(args[3]);
			millis = Long.parseLong(args[4]);
			host = args[5];
			port = Integer.parseInt(args[6]);
			if (args.length >= 8)
			{
				serviceHost = args[7];
				servicePort = Integer.parseInt(args[8]);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		for (int round = 1; round <= rounds; round++)
		{
			BarrierClient barrierClient = new BarrierClient();
			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				BankClient client = null;
				if (args.length >= 8)
					client = new BankClient(accountsNum, percent, millis,
							serviceHost, servicePort);
				else
					client = new BankClient(accountsNum, percent, millis);
				threads[i] = new TestThread(client, host, port);
			}
			barrierClient.enterBarrier(host, port, 0, 0, numberOfThreads);
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			int totalRequests = 0;
			long totalTime = 0;
			for (int i = 0; i < numberOfThreads; i++)
			{
				try
				{
					threads[i].join();
					totalRequests += threads[i].client.requests;
					totalTime += threads[i].client.end
							- threads[i].client.start;
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}

				threads[i] = null;
			}
			barrierClient.enterBarrier(host, port, totalRequests, totalTime,
					numberOfThreads);
			threads = null;
		}
	}
}
