package lsr.paxos.test.bank;

import java.io.Serializable;

public class Account implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int balance;

    public Account()
    {
        balance = Bank.INITIAL_BALANCE;
    }

    int getBalance()
    {
        return balance;
    }

    void setBalance(int value)
    {
        balance = value;
    }
}
