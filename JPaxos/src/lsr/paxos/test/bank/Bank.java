package lsr.paxos.test.bank;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

import lsr.paxos.test.hashmap.TransactionService;
import lsr.paxos.test.hashmap.TransactionService.RequestHandlerThread;
import lsr.service.SimplifiedService;

public class Bank extends SimplifiedService 
{
    protected static final int DEFAULT_NUM_ACCOUNTS = 10000;
    protected static final int INITIAL_BALANCE = 1000;

    protected int numAccounts = DEFAULT_NUM_ACCOUNTS;

    // array with all accounts
    protected Account[] accounts;

    int transferCalls = 0;
    int checkBalanceCalls = 0;

    private boolean bestCase = false;

    public Bank()
    {
        this(DEFAULT_NUM_ACCOUNTS);
    }

    public Bank(int numAccounts)
    {
        this.numAccounts = numAccounts;
        accounts = new Account[numAccounts];

        for (int i = 0; i < numAccounts; i++)
        {
            accounts[i] = new Account();
        }
    }
    
    public class RequestHandlerThread extends Thread
	{
		private Socket socket;
		private DataOutputStream outStream;
		private DataInputStream inStream;

		public RequestHandlerThread(Socket socket) throws IOException
		{
			this.socket = socket;
			this.outStream = new DataOutputStream(new BufferedOutputStream(
					socket.getOutputStream()));
			this.inStream = new DataInputStream(new BufferedInputStream(
					socket.getInputStream()));
		}

		public void run()
		{
			try
			{
				while (true)
				{
					int bytesToRead;
					bytesToRead = inStream.readInt();
					byte[] msg = new byte[bytesToRead];

					int bytesRead = 0;
					while (bytesRead != bytesToRead)
					{
						int x = inStream.read(msg, bytesRead, bytesToRead
								- bytesRead);
						if (x == -1)
							throw new IOException("invalid message");
						bytesRead += x;
					}

					byte[] response = execute(msg);
					outStream.writeInt(response.length);
					outStream.write(response);
					outStream.flush();
				}
			}
			catch (EOFException e)
			{
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					socket.close();
				}
				catch (IOException e)
				{
				}
			}
		}
	}

 // this is to run non-replicated service
	public static void main(String[] args) throws IOException
	{
		long reqExecutionTime = 0;
		if (args.length >= 2)
			reqExecutionTime = Long.parseLong(args[1]);
		
		Bank bank = new Bank();

		int port = Integer.parseInt(args[0]);
		ServerSocket srvr = new ServerSocket(port);

		try
		{
			while (true)
			{
				Socket socket = srvr.accept();
				bank.new RequestHandlerThread(socket).start();
			}
		}
		finally
		{
			srvr.close();
		}
	}
    
    public void doLotsOfReadsAndOneWrite(final int accountNumber,
            final int numReads)
    {
        Account account = accounts[accountNumber % numAccounts];
        if (numReads < 0)
            throw new RuntimeException("num reads must be positive");
        int amount = 0;
        for (int i = 0; i < numReads; i++)
        {
            amount = account.getBalance();
        }
        account.setBalance(amount++);
    }

    public void transfer(final int srcAccount, final int dstAccount,
            final int offset, int numMachines, int numThreads)
    {
        final Account src, dst;

        if (bestCase)
        {
            int newMaxAccount = numAccounts / (numMachines * numThreads);
            src = accounts[offset + Math.abs(srcAccount % newMaxAccount)];
            dst = accounts[offset + Math.abs(dstAccount % newMaxAccount)];
            // int newMaxAccount =
            // numAccounts/BenchmarkTest.numMachines;
            // src = accounts[myOffset+Math.abs(srcAccount %
            // newMaxAccount)];
            // dst = accounts[myOffset+Math.abs(dstAccount %
            // newMaxAccount)];
            // System.out.println("BC - "+myOffset+"src:"+(myOffset+Math.abs(srcAccount
            // % newMaxAccount))+"-dst:"+(myOffset+Math.abs(dstAccount %
            // newMaxAccount)));
        }
        else
        {
            src = accounts[Math.abs(srcAccount % numAccounts)];
            dst = accounts[Math.abs(dstAccount % numAccounts)];
            // System.out.println("src:"+Math.abs(srcAccount %
            // numAccounts)+"-dst:"+Math.abs(dstAccount % numAccounts));
        }

        int srcAmount = src.getBalance();
        int amountToTransfer = srcAmount / 10;
        src.setBalance(srcAmount - amountToTransfer);
        dst.setBalance(dst.getBalance() + amountToTransfer);
    }

    public void testRWLeases(final int writeIndex)
    {
        int x1 = accounts[writeIndex - 1].getBalance();
        int x2 = accounts[writeIndex + 1].getBalance();
        int xw = accounts[writeIndex].getBalance();
        accounts[writeIndex].setBalance(xw + (x1 / 10) + (x2 / 10));
    }

    public int sumBalances()
    {
        int total = 0;
        for (int i = 0; i < accounts.length; i++)
        {
            total += accounts[i].getBalance();
        }

        return total;
    }

    public boolean checkBalances()
    {
        int sum = sumBalances();
        if (sum != (INITIAL_BALANCE * numAccounts))
        {
            // System.out.printf("The sumBalances returned a value (%d) different than it should (%d)!\n",
            // sum, (INITIAL_BALANCE * numAccounts));
            // System.out.println("System.exit(1)");
            // System.exit(-1);
        }
        return true;
    }

    public void sanityCheck()
    {
        if (!checkBalances())
        {
            System.out.println(" --> System.exit(1); <--");
        }
    }

    @Override
    protected byte[] execute(byte[] value) {
        Command command;
        try {
            command = new Command(value);
        } catch (IOException e) {
            logger.log(Level.WARNING, "Incorrect request", e);
            return null;
        }

        if (command.getCheckBalances()) {
            boolean res = checkBalances();
            ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
            DataOutputStream dataOutput = new DataOutputStream(byteArrayOutput);
            try {
                dataOutput.writeBoolean(res);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
            return byteArrayOutput.toByteArray();
        }
        else {
            transfer(command.getAccountA(), command.getAccountB(), 0, 0, 0);
            return new byte[0];
        }
    }

    @Override
    protected byte[] makeSnapshot() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(stream);
            objectOutputStream.writeObject(accounts);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return stream.toByteArray();
    }

    @Override
    protected void updateToSnapshot(byte[] snapshot) {
        ByteArrayInputStream stream = new ByteArrayInputStream(snapshot);
        ObjectInputStream objectInputStream;
        try {
            objectInputStream = new ObjectInputStream(stream);
            accounts = (Account[]) objectInputStream.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    // static class BankStats
    // {
    // private int transferCalls;
    // private int checkCalls;
    //
    // BankStats(int transferCalls, int checkCalls)
    // {
    // this.transferCalls = transferCalls;
    // this.checkCalls = checkCalls;
    // }
    //
    // public void reportRawInfo(char separator)
    // {
    // System.out.printf("%ctransfer:%d%ccheck:%d", separator,
    // transferCalls, separator, checkCalls);
    // }
    // }

    private static final Logger logger = Logger.getLogger(Bank.class.getCanonicalName());
}
