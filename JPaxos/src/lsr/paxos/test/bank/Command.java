package lsr.paxos.test.bank;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.Serializable;

public class Command implements Serializable {
    private static final long serialVersionUID = 1L;
    private final boolean checkBalances;
    private final int accountA;
    private final int accountB;

    public Command() {
        this.checkBalances = true;
        this.accountA = 0;
        this.accountB = 0;
    }

    public Command(int accountA, int accountB) {
        this.checkBalances = false;
        this.accountA = accountA;
        this.accountB = accountB;
    }

    public Command(boolean checkBalances, int accountA, int accountB) {
        this.checkBalances = checkBalances;
        this.accountA = accountA;
        this.accountB = accountB;
    }

    public Command(byte[] value) throws IOException {
        ByteArrayInputStream byteArrayInput = new ByteArrayInputStream(value);
        DataInputStream dataInput = new DataInputStream(byteArrayInput);
        checkBalances = dataInput.readBoolean();
        if (!checkBalances) {
            accountA = dataInput.readInt();
            accountB = dataInput.readInt();
        } else {
            accountA = 0;
            accountB = 0;
        }
    }

    public boolean getCheckBalances() {
        return checkBalances;
    }

    public int getAccountA() {
        return accountA;
    }

    public int getAccountB() {
        return accountB;
    }

    public String toString() {
        return String.format("[checkBalances=%d accountA=%d accountB=%d]", checkBalances ? 1 : 0,
                accountA, accountB);
    }
}
