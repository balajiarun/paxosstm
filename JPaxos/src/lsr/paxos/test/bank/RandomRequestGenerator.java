package lsr.paxos.test.bank;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;

public class RandomRequestGenerator {
    private final Random random;

    public RandomRequestGenerator() {
        random = new Random();
    }

    public RandomRequestGenerator(int seed) {
        random = new Random(seed);
    }

    public byte[] generate(int size, byte b) {
        byte[] value = new byte[size];
        Arrays.fill(value, b);
        return value;
    }

    public byte[] generate(int numAccounts, int percent) {
        boolean checkBalances = random.nextInt(100) >= percent;
        int a = random.nextInt(numAccounts);
        int b = random.nextInt(numAccounts);
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        DataOutputStream dataStream = new DataOutputStream(byteStream);
        try {
            dataStream.writeBoolean(checkBalances);
            if (!checkBalances) {
                dataStream.writeInt(a);
                dataStream.writeInt(b);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteStream.toByteArray();
    }
}
