package lsr.paxos.test.hashmap;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Random;

public class TransactionRequestGenerator {
    private final Random random;

    public TransactionRequestGenerator() {
        random = new Random();
    }

    public TransactionRequestGenerator(int seed) {
        random = new Random(seed);
    }

    public byte[] generate(int reads, int writes, int range) {
        ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
        DataOutputStream dataStream = new DataOutputStream(byteStream);
        try {
            dataStream.writeInt(reads);
            dataStream.writeInt(writes);
            for (int i = 0; i < reads; i++)
                dataStream.writeInt(random.nextInt(range));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return byteStream.toByteArray();
    }
}
