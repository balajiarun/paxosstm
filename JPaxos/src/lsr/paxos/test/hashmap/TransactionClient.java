package lsr.paxos.test.hashmap;

import java.io.IOException;
import java.util.Random;

import lsr.paxos.ReplicationException;
import lsr.paxos.client.Client;
import lsr.paxos.test.utils.BarrierClient;
import lsr.paxos.test.utils.SimpleClient;

public class TransactionClient
{

	private Client client;
	private TransactionRequestGenerator requestGenerator;

	private int range;
	private int writesPerTran;
	private int readsPerTran;
	private int operationsPerWriteTran;
	private boolean isSimpleClient = false;
	private String host;
	private int port;
	private int requests;
	private int percent;
	private long start, end;
	private long mills = 10000; 
	
	public TransactionClient(int range, int percent, 
			int writesPerTran, int readsPerTran, int operationsPerWriteTran, long mills)
	{
		this.range = range;
		this.percent = percent;
		this.writesPerTran = writesPerTran;
		this.readsPerTran = readsPerTran;
		this.operationsPerWriteTran = operationsPerWriteTran;
		this.mills = mills;
	}

	public TransactionClient(int range, int percent,
			int writesPerTran, int readsPerTran, int operationsPerWriteTran, long mills,
			String host, int port)
	{
		this.range = range;
		this.percent = percent;
		this.writesPerTran = writesPerTran;
		this.readsPerTran = readsPerTran;
		this.operationsPerWriteTran = operationsPerWriteTran;
		this.isSimpleClient = true;
		this.mills = mills;
		this.host = host;
		this.port = port;
	}

	public void run() throws IOException, ReplicationException
	{
		if (!isSimpleClient)
		{
			client = new Client();
		}
		else
		{
			client = new SimpleClient(host, port);
		}
		client.connect();

		requestGenerator = new TransactionRequestGenerator();
		execute(percent, mills);
		if (isSimpleClient)
			((SimpleClient) client).disconnect();
	}

//	private void execute(int readTrans, int writeTrans)
//	throws ReplicationException
//{
//
//int readsDone = 0;
//int writesDone = 0;
//final int total = readTrans + writeTrans;
//
//for (int i = 0; i < total; i++)
//{
//	int reads, writes;
//	if (random.nextInt(total - i) < readTrans - readsDone)
//	{
//		// readOnly = true;
//		reads = readsPerTran;
//		writes = 0;
//		readsDone++;
//	}
//	else
//	{
//		// readOnly = false;
//		reads = operationsPerWriteTran - writesPerTran;
//		writes = writesPerTran;
//		writesDone++;
//	}
//
//	byte[] request = requestGenerator.generate(reads, writes, range);
//
//	client.execute(request);
//
//}
//}
	
	private void execute(int percent, long mills)
			throws ReplicationException
	{
		int reads, writes;
		requests = 0;
        start = System.currentTimeMillis();
        Random rand = new Random(start + hashCode());
        while ((end = System.currentTimeMillis()) - start <= mills) {
        	if (Math.abs(rand.nextInt()) % 100 >= percent)
			{
        		reads = readsPerTran;
        		writes = 0;
			}
        	else
        	{
        		reads = operationsPerWriteTran - writesPerTran;
        		writes = writesPerTran;
        	}
        	byte[] request = requestGenerator.generate(reads, writes, range);
            client.execute(request);
            requests++;
        }
	}

	public static class TestThread extends Thread
	{
		public TransactionClient client;
		private String host;
		private int port;

		public TestThread(TransactionClient client, String host, int port)
		{
			this.client = client;
			this.host = host;
			this.port = port;
		}

		public void run()
		{
			//BarrierClient barrierClient = new BarrierClient();
			//barrierClient.enterBarrier(host, port, 0, 0);
			try
			{
				client.run();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			catch (ReplicationException e)
			{
				e.printStackTrace();
			}
			//barrierClient.enterBarrier(host, port, client.requests, client.end - client.start);
		}
	}

	public static void main(String[] args) throws IOException,
			ReplicationException
	{

		String host = "localhost";
		int port = 1234;

		int numberOfThreads = 20;
		int range = 10000;
		int percent = 50;
		int writesPerTran = 2;
		int rounds = 30;
		int readsPerTran = 100;
		int operationsPerWriteTran = 10;
		long mills = 10000;

		String serviceHost = "localhost";
		int servicePort = 4444;

		try
		{
			numberOfThreads = Integer.parseInt(args[0]);
			range = Integer.parseInt(args[1]);
			percent = Integer.parseInt(args[2]);
			writesPerTran = Integer.parseInt(args[3]);
			rounds = Integer.parseInt(args[4]);
			readsPerTran = Integer.parseInt(args[5]);
			operationsPerWriteTran = Integer.parseInt(args[6]);
			mills = Long.parseLong(args[7]);
			host = args[8];
			port = Integer.parseInt(args[9]);
			if (args.length >= 11)
			{
				serviceHost = args[10];
				servicePort = Integer.parseInt(args[11]);
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}

		
		for (int round = 1; round <= rounds; round++)
		{
			BarrierClient barrierClient = new BarrierClient();
			TestThread[] threads = new TestThread[numberOfThreads];
			for (int i = 0; i < numberOfThreads; i++)
			{
				TransactionClient client = null;
				if (args.length >= 11)
					client = new TransactionClient(range, percent,
							writesPerTran, readsPerTran,
							operationsPerWriteTran, mills, serviceHost, servicePort);
				else
					client = new TransactionClient(range, percent,
							writesPerTran, readsPerTran, operationsPerWriteTran, mills);
				threads[i] = new TestThread(client, host, port);
			}
			barrierClient.enterBarrier(host, port, 0, 0, numberOfThreads);
			for (int i = 0; i < numberOfThreads; i++)
			{
				threads[i].start();
			}
			int totalRequests = 0;
			long totalTime = 0;
			for (int i = 0; i < numberOfThreads; i++)
			{
				try
				{
					threads[i].join();
					totalRequests += threads[i].client.requests;
					totalTime += threads[i].client.end - threads[i].client.start; 
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}

				threads[i] = null;
			}
			barrierClient.enterBarrier(host, port, totalRequests, totalTime, numberOfThreads);
			threads = null;
		}
	}
}
