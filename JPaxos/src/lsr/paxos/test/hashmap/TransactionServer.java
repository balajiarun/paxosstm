package lsr.paxos.test.hashmap;

import java.io.IOException;

import lsr.common.Configuration;
import lsr.paxos.replica.Replica;

public class TransactionServer {
    public static void main(String[] args) throws IOException {

        /** First, we acquire the ReplicaID **/
        if (args.length < 1) {
            System.exit(1);
        }
        int localId = Integer.parseInt(args[0]);

    	long reqExecutionTime = 0;
		if (args.length >= 2)
			reqExecutionTime = Long.parseLong(args[1]);

        /** Then we create the replica, passing to it the service **/
        Replica replica = new Replica(new Configuration(), localId, new TransactionService(reqExecutionTime));

        /** Then we start the replica **/
        replica.start();

        /** And the service runs until the enter key is triggered **/
        System.in.read();
        System.exit(0);
    }
}
