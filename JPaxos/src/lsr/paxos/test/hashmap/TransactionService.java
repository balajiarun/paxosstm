package lsr.paxos.test.hashmap;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.Semaphore;
import java.util.logging.Level;
import java.util.logging.Logger;

import lsr.service.SimplifiedService;

public class TransactionService extends SimplifiedService
{

	private static final int HASHMAP_CAPACITY = 10000;
	private static final int range = 10000;
	private static final int seed = 2011;

	long reqExecutionTime = 0;
	long reqExecutionMillis = 0;
	int reqExecutionNanos = 0;

	Semaphore semaphore = new Semaphore(1);
	
	private HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(
			HASHMAP_CAPACITY, 10);

	public class RequestHandlerThread extends Thread
	{
		private Socket socket;
		private DataOutputStream outStream;
		private DataInputStream inStream;

		public RequestHandlerThread(Socket socket) throws IOException
		{
			this.socket = socket;
			this.outStream = new DataOutputStream(new BufferedOutputStream(
					socket.getOutputStream()));
			this.inStream = new DataInputStream(new BufferedInputStream(
					socket.getInputStream()));
		}

		public void run()
		{
			try
			{
				while (true)
				{
					int bytesToRead;
					bytesToRead = inStream.readInt();
					byte[] msg = new byte[bytesToRead];

					int bytesRead = 0;
					while (bytesRead != bytesToRead)
					{
						int x = inStream.read(msg, bytesRead, bytesToRead
								- bytesRead);
						if (x == -1)
							throw new IOException("invalid message");
						bytesRead += x;
					}

					byte[] response = execute(msg);
					outStream.writeInt(response.length);
					outStream.write(response);
					outStream.flush();
				}
			}
			catch (EOFException e)
			{
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			finally
			{
				try
				{
					socket.close();
				}
				catch (IOException e)
				{
				}
			}
		}
	}

	// this is to run non-replicated service
	public static void main(String[] args) throws IOException
	{
		long reqExecutionTime = 0;
		if (args.length >= 2)
			reqExecutionTime = Long.parseLong(args[1]);
		
		TransactionService service = new TransactionService(reqExecutionTime);

		int port = Integer.parseInt(args[0]);
		ServerSocket srvr = new ServerSocket(port);

		try
		{
			while (true)
			{
				Socket socket = srvr.accept();
				service.new RequestHandlerThread(socket).start();
			}
		}
		finally
		{
			srvr.close();
		}
	}

	public TransactionService()
	{
		init();
	}

	public TransactionService(long reqExecutionTime)
	{
		this();

		this.reqExecutionTime = reqExecutionTime;
		this.reqExecutionMillis = reqExecutionTime / 1000;
		this.reqExecutionNanos = (int) reqExecutionTime % 1000;
	}

	protected void init()
	{
		map.clear();
		Random random = new Random(seed);
		for (int i = 0; i < range / 2; i++)
		{
			int x = random.nextInt(range);
			if (map.put(x, x) != null)
			{
				i--;
			}
		}
	}

	public static int overslept = 0;
	
	protected synchronized byte[] execute(byte[] value)
	{
		TransactionCommand command;
		try
		{
			command = new TransactionCommand(value);
		}
		catch (IOException e)
		{
			logger.log(Level.WARNING, "Incorrect request", e);
			return null;
		}


		int keys[] = command.getKeys();
		Integer values[] = new Integer[keys.length];
		int nullCounter = 0;
		for (int i = 0; i < keys.length; i++)
		{
			values[i] = map.get(keys[i]);
			if (values[i] == null)
				nullCounter++;
		}

		if (!command.isReadOnly())
		{
			for (int i = 0; i < command.getWrites(); i++)
			{
				int j = i % values.length;
				if (values[j] == null)
				{
					map.put(keys[j], keys[j]);
					values[j] = keys[j];
				}
				else
				{
					map.remove(keys[j]);
					values[j] = null;
				}
			}
		}


		if (reqExecutionTime > 0)
		{
			long timeToSleep = keys[0] % 2 + 1;
			long start = System.currentTimeMillis();
			long stop = 0;
			while ((stop = System.currentTimeMillis()) - start < timeToSleep);
			
			if (stop-start > 2) System.err.println(stop-start);
//			try
//			{
//				semaphore.acquire();
//				//System.err.print(".");
//				Thread.sleep(reqExecutionMillis, reqExecutionNanos);
//				semaphore.release();
//			}
//			catch (InterruptedException e)
//			{
//				e.printStackTrace();
//			}
		}
		
		ByteArrayOutputStream byteArrayOutput = new ByteArrayOutputStream();
		DataOutputStream dataOutput = new DataOutputStream(byteArrayOutput);
		try
		{
			dataOutput.writeInt(nullCounter);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}

		return byteArrayOutput.toByteArray();
	}

	protected byte[] makeSnapshot()
	{
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		try
		{
			ObjectOutputStream objectOutputStream = new ObjectOutputStream(
					stream);
			objectOutputStream.writeObject(map);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
		return stream.toByteArray();
	}

	@SuppressWarnings("unchecked")
	protected void updateToSnapshot(byte[] snapshot)
	{
		ByteArrayInputStream stream = new ByteArrayInputStream(snapshot);
		ObjectInputStream objectInputStream;
		try
		{
			objectInputStream = new ObjectInputStream(stream);
			map = (HashMap<Integer, Integer>) objectInputStream.readObject();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	public void instanceExecuted(int instanceId)
	{
		// ignoring
	}

	private static final Logger logger = Logger
			.getLogger(TransactionService.class.getCanonicalName());
}
