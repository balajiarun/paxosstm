package lsr.paxos.test.hashmap;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.Serializable;

public class TransactionCommand implements Serializable {
    private static final long serialVersionUID = 1L;
    private final int reads;
    private final int writes;
    private final int[] keys;

    public TransactionCommand(int reads, int writes, int keys[]) {
        this.reads = reads;
        this.writes = writes;
        this.keys = keys;
    }

    public TransactionCommand(byte[] value) throws IOException {
        ByteArrayInputStream byteArrayInput = new ByteArrayInputStream(value);
        DataInputStream dataInput = new DataInputStream(byteArrayInput);
        reads = dataInput.readInt();
        writes = dataInput.readInt();
        keys = new int[reads];
        for (int i = 0; i < reads; i++)
            keys[i] = dataInput.readInt();
    }

    public boolean isReadOnly() {
        return writes == 0;
    }

    public int getReads() {
        return reads;
    }

    public int getWrites() {
        return writes;
    }

    public int[] getKeys() {
        return keys;
    }

    public String toString() {
        return String.format("[reads=%d writes=%d]", reads, writes);
    }
}
