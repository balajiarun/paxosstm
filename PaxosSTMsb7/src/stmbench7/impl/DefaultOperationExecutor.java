package stmbench7.impl;

import stmbench7.OperationExecutor;
import stmbench7.ResultBean;
import stmbench7.core.Operation;
import stmbench7.core.OperationFailedException;

/**
 * Default implementation of an OperationExecutor.
 * Does not provide any thread-safety.
 */
public class DefaultOperationExecutor implements OperationExecutor {

	private static int clock = 0;
	
	private final Operation op;
	private int lastTimestamp = 0;
	
	public DefaultOperationExecutor(Operation op) {
		this.op = op;
	}
	
	public ResultBean execute() throws OperationFailedException {
		lastTimestamp = clock++;
		ResultBean resultBean = new ResultBean();
		resultBean.result = op.performOperation();
		return resultBean;
	}

	public int getLastOperationTimestamp() {
		return lastTimestamp;
	}	
}
