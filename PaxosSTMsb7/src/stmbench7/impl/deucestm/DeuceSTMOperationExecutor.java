package stmbench7.impl.deucestm;

import stmbench7.OperationExecutor;
import stmbench7.ResultBean;
import stmbench7.core.Operation;
import stmbench7.core.OperationFailedException;

public class DeuceSTMOperationExecutor implements OperationExecutor {

	private final Operation op;
	
	public DeuceSTMOperationExecutor(Operation op) {
		this.op = op;
	}
    
        @org.deuce.Atomic
	public ResultBean execute() throws OperationFailedException {
        	ResultBean resultBean = new ResultBean();
        	resultBean.result = op.performOperation();
		return resultBean;
	}

	public int getLastOperationTimestamp() {
		return 0;
	}

}
