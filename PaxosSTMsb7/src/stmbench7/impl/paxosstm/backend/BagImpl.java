package stmbench7.impl.paxosstm.backend;

import soa.paxosstm.dstm.TransactionObject;
import soa.paxosstm.utils.TransactionalArrayList;
import stmbench7.annotations.ContainedInAtomic;
import stmbench7.backend.ImmutableCollection;

/**
 * Simple implementation of a bag of objects.
 */
@TransactionObject
@ContainedInAtomic
public class BagImpl<E> extends TransactionalArrayList<E> {

	private static final long serialVersionUID = 5329072640119174542L;
	
	public BagImpl() {
		super(2);
	}
	
	public BagImpl(BagImpl<E> source) {
		super(source);
	}
	
	public ImmutableCollection<E> immutableView() {
		return new ImmutableViewImpl<E>(this);
	}
}
