package stmbench7.impl.paxosstm.backend;

import java.util.NoSuchElementException;

import soa.paxosstm.dstm.TransactionObject;
import soa.paxosstm.utils.TransactionalLinkedListInt;
import stmbench7.annotations.Update;
import stmbench7.backend.IdPool;
import stmbench7.core.OperationFailedException;

/**
 * Used to generate ids of various elements. This default implementation is NOT
 * thread-safe.
 */
@TransactionObject
public class IdPoolImpl extends TransactionalLinkedListInt implements IdPool, Cloneable
{
	public IdPoolImpl(int maxNumberOfIds)
	{
		for (int id = 1; id <= maxNumberOfIds; id++)
		{
			offer(id);
		}
	}

	@Update
	public int getId() throws OperationFailedException
	{
		try
		{
			return removeFirstInt();
		}
		catch (NoSuchElementException e)
		{
			throw new OperationFailedException();
		}
	}

	@Update
	public void putUnusedId(int id)
	{
		offer(id);
	}

	public String toString()
	{
		String txt = "IdPool:";
		for (int id : this)
			txt += " " + id;
		return txt;
	}
}
