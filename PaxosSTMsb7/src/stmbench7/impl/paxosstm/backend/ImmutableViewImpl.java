package stmbench7.impl.paxosstm.backend;

import java.util.Iterator;

import soa.paxosstm.utils.TransactionalArrayList;
import stmbench7.backend.ImmutableCollection;

public class ImmutableViewImpl<E> implements ImmutableCollection<E>, Cloneable {

	private final TransactionalArrayList<E> elements;
	
	public ImmutableViewImpl(TransactionalArrayList<E> elements) {
		this.elements = elements;
		//this.elements = new TransactionalArrayList<E>(elements);
	}
	
	public boolean contains(E element) {
		return elements.contains(element);
	}

	public int size() {
		return elements.size();
	}

	public Iterator<E> iterator() {
		return elements.iterator();
	}
	
	public ImmutableCollection<E> clone() {
		return new ImmutableViewImpl<E>((TransactionalArrayList<E>) elements.clone());
	}
}