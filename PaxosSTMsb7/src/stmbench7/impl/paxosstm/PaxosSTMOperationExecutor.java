package stmbench7.impl.paxosstm;

import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.dstm.TransactionStatistics;
import soa.paxosstm.dstm.internal.global.GlobalTransactionManager;
import soa.paxosstm.dstm.internal.global.IdPool;
import soa.paxosstm.utils.Holder;
import stmbench7.OperationExecutor;
import stmbench7.ResultBean;
import stmbench7.annotations.ReadOnly;
import stmbench7.core.Operation;
import stmbench7.core.OperationFailedException;

public class PaxosSTMOperationExecutor implements OperationExecutor
{

	private final Operation op;

	public PaxosSTMOperationExecutor(Operation op)
	{
		this.op = op;
	}

	static int cnt = 0;

	@Override
	public ResultBean execute() throws OperationFailedException
	{
		// co z lastTimestamp
		// System.err.print(".");

		// final Holder<Integer> result = new Holder<Integer>();
		final Holder<OperationFailedException> exception = new Holder<OperationFailedException>();

		final ResultBean resultBean = new ResultBean();

		try
		{
			//System.err.println(System.currentTimeMillis()/1000 + " " + Thread.currentThread().getName() + " " + op.getOperationId());

			if (op.getClass().getMethod("performOperation", (Class<?>[]) null).getAnnotation(
					ReadOnly.class) != null)
			{
				// System.err.println("BAAAAAAM! " + op.getClass().getName());
				new Transaction(true)
				{
					@Override
					protected void atomic()
					{
						try
						{
							resultBean.result = op.performOperation();
							// result.set(op.performOperation());
						}
						catch (OperationFailedException e)
						{
							exception.set(e);
							rollback();
						}
					}

					@Override
					protected void statistics(TransactionStatistics statistics)
					{
						resultBean.readOnly = true;
						resultBean.executionTime = statistics.getExecutionTime();
						switch (statistics.getState())
						{
						case Committed:
							resultBean.committed = true;
							resultBean.commitPackageSize = 0;
							break;
						case RolledBack:
							resultBean.rolledback = true;
							break;
						}

						// if (exception.get() != null)
						// {
						// System.err.println("BAM!");
						// resultBean.committed = true;
						// resultBean.rolledback = true;
						// }
					}
				};
			}
			else
			{
				new Transaction()
				{
					@Override
					protected void atomic()
					{
						try
						{
							resultBean.result = op.performOperation();
							// result.set(op.performOperation());
							// rollback();
						}
						catch (OperationFailedException e)
						{
							exception.set(e);
							rollback();
						}
					}

					@Override
					protected void statistics(TransactionStatistics statistics)
					{
						resultBean.readOnly = false;
						resultBean.executionTime = statistics.getExecutionTime();
						switch (statistics.getState())
						{
						case Committed:
							resultBean.committed = true;
							resultBean.committingPhaseLatency = statistics
									.getCommittingPhaseLatency();
							resultBean.commitPackageSize = statistics.getPackageSize();
							if (resultBean.commitPackageSize != 0)
							{
								resultBean.commitAbcastDuration = statistics.getAbcastDuration();
								resultBean.commitValidationTime = statistics.getValidationTime();
								resultBean.commitUpdateTime = statistics.getUpdateTime();
								resultBean.commitReadSetSize = statistics.getReadSetSize();
								resultBean.commitWriteSetSize = statistics.getWriteSetSize();
								resultBean.commitNewSetSize = statistics.getNewSetSize();
								resultBean.commitTypeSetSize = statistics.getTypeSetSize();
							}
							break;

						case LocalAbort:
							resultBean.localAborts++;
							break;
						case GlobalAbort:
							resultBean.aborts++;
							resultBean.abortAbcastDuration = statistics.getAbcastDuration();
							resultBean.abortValidationTime = statistics.getValidationTime();
							resultBean.committingPhaseLatency = statistics
									.getCommittingPhaseLatency();
							resultBean.totalAbortPackageSize += statistics.getPackageSize();
							resultBean.totalAbortReadSetSize += statistics.getReadSetSize();
							resultBean.totalAbortWriteSetSize += statistics.getWriteSetSize();
							resultBean.totalAbortNewSetSize += statistics.getNewSetSize();
							resultBean.totalAbortTypeSetSize += statistics.getTypeSetSize();
							break;
						case RolledBack:
							resultBean.rolledback = true;
						}

						// if (exception.get() != null)
						// {
						// System.err.println("BAM!");
						// resultBean.committed = true;
						// resultBean.rolledback = true;
						// }
					}
				};
			}
		}
		catch (SecurityException e)
		{
			e.printStackTrace();
		}
		catch (NoSuchMethodException e)
		{
			e.printStackTrace();
		}

		// if (exception.get() != null)
		// {
		// throw exception.get();
		// }

		// return result.get();
		return resultBean;
	}

	@Override
	public int getLastOperationTimestamp()
	{
		return 0;
	}
}
