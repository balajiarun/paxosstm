package stmbench7.impl.paxosstm;

import soa.paxosstm.dstm.ArrayWrapper;
import soa.paxosstm.dstm.TransactionObject;

@TransactionObject
public class BenchThreadResultBean
{
	ArrayWrapper<Integer> successfulOperations, failedOperations;
	ArrayWrapper<ArrayWrapper<Integer>> operationsTTC, operationsHighTTCLog;
	int lc = 0;

	private ArrayWrapper<Long> totalTime;
	private ArrayWrapper<Integer> commits;
	private ArrayWrapper<Integer> commitsNoWrite;
	private ArrayWrapper<Integer> aborts;
	private ArrayWrapper<Integer> rollbacks;
	private ArrayWrapper<Integer> localAborts;
	private ArrayWrapper<Integer> totalCommitPackageSizes;
	private ArrayWrapper<Integer> totalAbortPackageSizes;
	private ArrayWrapper<Integer> totalCommitReadSetSizes;
	private ArrayWrapper<Integer> totalAbortReadSetSizes;
	private ArrayWrapper<Integer> totalCommitWriteSetSizes;
	private ArrayWrapper<Integer> totalAbortWriteSetSizes;
	private ArrayWrapper<Integer> totalCommitNewSetSizes;
	private ArrayWrapper<Integer> totalAbortNewSetSizes;
	private ArrayWrapper<Integer> totalCommitTypeSetSizes;
	private ArrayWrapper<Integer> totalAbortTypeSetSizes;

	private ArrayWrapper<Long> totalCommitExecutionTimes;
	private ArrayWrapper<Long> totalCommitNoWriteExecutionTimes;
	private ArrayWrapper<Long> totalAbortExecutionTimes;
	private ArrayWrapper<Long> totalLocalAbortExecutionTimes;
	private ArrayWrapper<Long> totalRollbackExecutionTimes;
	private ArrayWrapper<Long> totalCommitCommittingPhaseLatency;
	private ArrayWrapper<Long> totalAbortCommittingPhaseLatency;

	private ArrayWrapper<Long> totalCommitAbcastDuration;
	private ArrayWrapper<Long> totalCommitValidationTime;
	private ArrayWrapper<Long> totalCommitUpdateTime;
	private ArrayWrapper<Long> totalAbortAbcastDuration;
	private ArrayWrapper<Long> totalAbortValidationTime;

	@SuppressWarnings("unchecked")
	public BenchThreadResultBean(int[] successfulOperations,
			int[] failedOperations, int[][] operationsTTC,
			int[][] operationsHighTTCLog, int lc, long[] totalTime,
			int[] commits, int[] commitsNoWrite, int[] aborts, int[] rollbacks,
			int[] localAborts, int[] totalCommitPackageSizes,
			int[] totalAbortPackageSizes, int[] totalCommitReadSetSizes,
			int[] totalAbortReadSetSizes, int[] totalCommitWriteSetSizes,
			int[] totalAbortWriteSetSizes, int[] totalCommitNewSetSizes,
			int[] totalAbortNewSetSizes, int[] totalCommitTypeSetSizes,
			int[] totalAbortTypeSetSizes, long[] totalCommitExecutionTimes,
			long[] totalCommitNoWriteExecutionTimes,
			long[] totalAbortExecutionTimes,
			long[] totalLocalAbortExecutionTimes,
			long[] totalRollbackExecutionTimes,
			long[] totalCommitCommittingPhaseLatency,
			long[] totalAbortCommittingPhaseLatency,
			long[] totalCommitAbcastDuration, long[] totalCommitValidationTime,
			long[] totalCommitUpdateTime, long[] totalAbortAbcastDuration,
			long[] totalAbortValidationTime)
	{
		this.lc = lc;

		if (successfulOperations != null)
		{
			Integer[] successfulOperationsCopy = new Integer[successfulOperations.length];
			for (int i = 0; i < successfulOperations.length; i++)
				successfulOperationsCopy[i] = successfulOperations[i];
			this.successfulOperations = new ArrayWrapper<Integer>(
					successfulOperationsCopy);
		}

		if (failedOperations != null)
		{
			Integer[] failedOperationsCopy = new Integer[failedOperations.length];
			for (int i = 0; i < failedOperations.length; i++)
				failedOperationsCopy[i] = failedOperations[i];
			this.failedOperations = new ArrayWrapper<Integer>(
					failedOperationsCopy);
		}

		if (operationsTTC != null)
		{
			ArrayWrapper<Integer>[] operationsTTCCopy = new ArrayWrapper[operationsTTC.length];
			this.operationsTTC = new ArrayWrapper<ArrayWrapper<Integer>>(
					operationsTTCCopy);
			for (int i = 0; i < operationsTTC.length; i++)
			{
				Integer[] innerArray = new Integer[operationsTTC[i].length];
				for (int j = 0; j < operationsTTC[i].length; j++)
				{
					innerArray[j] = operationsTTC[i][j];
				}
				this.operationsTTC
						.set(i, new ArrayWrapper<Integer>(innerArray));
			}
		}

		if (operationsHighTTCLog != null)
		{
			ArrayWrapper<Integer>[] operationsHighTTCLogCopy = new ArrayWrapper[operationsHighTTCLog.length];
			this.operationsHighTTCLog = new ArrayWrapper<ArrayWrapper<Integer>>(
					operationsHighTTCLogCopy);
			for (int i = 0; i < operationsHighTTCLog.length; i++)
			{
				Integer[] innerArray = new Integer[operationsHighTTCLog[i].length];
				for (int j = 0; j < operationsHighTTCLog[i].length; j++)
				{
					innerArray[j] = operationsHighTTCLog[i][j];
				}
				this.operationsHighTTCLog.set(i, new ArrayWrapper<Integer>(
						innerArray));
			}
		}

		/*******************************/

		if (totalTime != null)
			this.totalTime = new ArrayWrapper<Long>(getArray(totalTime));
		if (commits != null)
			this.commits = new ArrayWrapper<Integer>(
					(Integer[]) getArray(commits));
		if (commitsNoWrite != null)
			this.commitsNoWrite = new ArrayWrapper<Integer>(
					(Integer[]) getArray(commitsNoWrite));
		if (aborts != null)
			this.aborts = new ArrayWrapper<Integer>(
					(Integer[]) getArray(aborts));
		if (rollbacks != null)
			this.rollbacks = new ArrayWrapper<Integer>(
					(Integer[]) getArray(rollbacks));
		if (localAborts != null)
			this.localAborts = new ArrayWrapper<Integer>(
					(Integer[]) getArray(localAborts));
		if (commits != null)
			this.totalCommitPackageSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalCommitPackageSizes));
		if (totalAbortPackageSizes != null)
			this.totalAbortPackageSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalAbortPackageSizes));
		if (totalCommitReadSetSizes != null)
			this.totalCommitReadSetSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalCommitReadSetSizes));
		if (totalAbortReadSetSizes != null)
			this.totalAbortReadSetSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalAbortReadSetSizes));
		if (totalCommitWriteSetSizes != null)
			this.totalCommitWriteSetSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalCommitWriteSetSizes));
		if (totalAbortWriteSetSizes != null)
			this.totalAbortWriteSetSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalAbortWriteSetSizes));
		if (totalCommitNewSetSizes != null)
			this.totalCommitNewSetSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalCommitNewSetSizes));
		if (totalAbortNewSetSizes != null)
			this.totalAbortNewSetSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalAbortNewSetSizes));
		if (totalCommitTypeSetSizes != null)
			this.totalCommitTypeSetSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalCommitTypeSetSizes));
		if (totalAbortTypeSetSizes != null)
			this.totalAbortTypeSetSizes = new ArrayWrapper<Integer>(
					(Integer[]) getArray(totalAbortTypeSetSizes));

		if (totalCommitExecutionTimes != null)
			this.totalCommitExecutionTimes = new ArrayWrapper<Long>(
					(Long[]) getArray(totalCommitExecutionTimes));
		if (totalCommitNoWriteExecutionTimes != null)
			this.totalCommitNoWriteExecutionTimes = new ArrayWrapper<Long>(
					(Long[]) getArray(totalCommitNoWriteExecutionTimes));
		if (totalAbortExecutionTimes != null)
			this.totalAbortExecutionTimes = new ArrayWrapper<Long>(
					(Long[]) getArray(totalAbortExecutionTimes));
		if (totalLocalAbortExecutionTimes != null)
			this.totalLocalAbortExecutionTimes = new ArrayWrapper<Long>(
					(Long[]) getArray(totalLocalAbortExecutionTimes));
		if (totalRollbackExecutionTimes != null)
			this.totalRollbackExecutionTimes = new ArrayWrapper<Long>(
					(Long[]) getArray(totalRollbackExecutionTimes));
		if (totalCommitCommittingPhaseLatency != null)
			this.totalCommitCommittingPhaseLatency = new ArrayWrapper<Long>(
					(Long[]) getArray(totalCommitCommittingPhaseLatency));
		if (totalAbortCommittingPhaseLatency != null)
			this.totalAbortCommittingPhaseLatency = new ArrayWrapper<Long>(
					(Long[]) getArray(totalAbortCommittingPhaseLatency));
		
		if (totalCommitAbcastDuration != null)
			this.totalCommitAbcastDuration = new ArrayWrapper<Long>(
					(Long[]) getArray(totalCommitAbcastDuration));
		if (totalCommitValidationTime != null)
			this.totalCommitValidationTime = new ArrayWrapper<Long>(
					(Long[]) getArray(totalCommitValidationTime));
		if (totalCommitUpdateTime != null)
			this.totalCommitUpdateTime = new ArrayWrapper<Long>(
					(Long[]) getArray(totalCommitUpdateTime));
		if (totalAbortAbcastDuration != null)
			this.totalAbortAbcastDuration = new ArrayWrapper<Long>(
					(Long[]) getArray(totalAbortAbcastDuration));
		if (totalAbortValidationTime != null)
			this.totalAbortValidationTime = new ArrayWrapper<Long>(
					(Long[]) getArray(totalAbortValidationTime));
		
	}

	private Integer[] getArray(int[] array)
	{
		Integer[] res = new Integer[array.length];
		for (int i = 0; i < array.length; i++)
			res[i] = array[i];
		return res;
	}

	private Long[] getArray(long[] array)
	{
		Long[] res = new Long[array.length];
		for (int i = 0; i < array.length; i++)
			res[i] = array[i];
		return res;
	}

	public ArrayWrapper<Integer> getSuccessfulOperations()
	{
		return successfulOperations;
	}

	public ArrayWrapper<Integer> getFailedOperations()
	{
		return failedOperations;
	}

	public ArrayWrapper<ArrayWrapper<Integer>> getOperationsTTC()
	{
		return operationsTTC;
	}

	public ArrayWrapper<ArrayWrapper<Integer>> getOperationsHighTTCLog()
	{
		return operationsHighTTCLog;
	}

	public int getLocalConflicts()
	{
		return lc;
	}

	public ArrayWrapper<Integer> getCommits()
	{
		return commits;
	}

	public ArrayWrapper<Integer> getCommitsNoWrite()
	{
		return commitsNoWrite;
	}

	public ArrayWrapper<Integer> getAborts()
	{
		return aborts;
	}

	public ArrayWrapper<Integer> getRollbacks()
	{
		return rollbacks;
	}

	public ArrayWrapper<Integer> getLocalAborts()
	{
		return localAborts;
	}

	public ArrayWrapper<Long> getTotalTime()
	{
		return totalTime;
	}

	public ArrayWrapper<Integer> getTotalCommitPackageSizes()
	{
		return totalCommitPackageSizes;
	}

	public ArrayWrapper<Integer> getTotalAbortPackageSizes()
	{
		return totalAbortPackageSizes;
	}

	public ArrayWrapper<Integer> getTotalCommitReadSetSizes()
	{
		return totalCommitReadSetSizes;
	}

	public ArrayWrapper<Integer> getTotalAbortReadSetSizes()
	{
		return totalAbortReadSetSizes;
	}

	public ArrayWrapper<Integer> getTotalCommitWriteSetSizes()
	{
		return totalCommitWriteSetSizes;
	}

	public ArrayWrapper<Integer> getTotalAbortWriteSetSizes()
	{
		return totalAbortWriteSetSizes;
	}

	public ArrayWrapper<Integer> getTotalCommitNewSetSizes()
	{
		return totalCommitNewSetSizes;
	}

	public ArrayWrapper<Integer> getTotalAbortNewSetSizes()
	{
		return totalAbortNewSetSizes;
	}

	public ArrayWrapper<Integer> getTotalCommitTypeSetSizes()
	{
		return totalCommitTypeSetSizes;
	}

	public ArrayWrapper<Integer> getTotalAbortTypeSetSizes()
	{
		return totalAbortTypeSetSizes;
	}

	public ArrayWrapper<Long> getTotalCommitExecutionTimes()
	{
		return totalCommitExecutionTimes;
	}

	public ArrayWrapper<Long> getTotalCommitNoWriteExecutionTimes()
	{
		return totalCommitNoWriteExecutionTimes;
	}

	public ArrayWrapper<Long> getTotalAbortExecutionTimes()
	{
		return totalAbortExecutionTimes;
	}

	public ArrayWrapper<Long> getTotalLocalAbortExecutionTimes()
	{
		return totalLocalAbortExecutionTimes;
	}

	public ArrayWrapper<Long> getTotalRollbackExecutionTimes()
	{
		return totalRollbackExecutionTimes;
	}

	public ArrayWrapper<Long> getTotalCommitCommittingPhaseLatency()
	{
		return totalCommitCommittingPhaseLatency;
	}

	public ArrayWrapper<Long> getTotalAbortCommittingPhaseLatency()
	{
		return totalAbortCommittingPhaseLatency;
	}

	public ArrayWrapper<Long> getTotalCommitAbcastDuration()
	{
		return totalCommitAbcastDuration;
	}

	public ArrayWrapper<Long> getTotalCommitValidationTime()
	{
		return totalCommitValidationTime;
	}

	public ArrayWrapper<Long> getTotalCommitUpdateTime()
	{
		return totalCommitUpdateTime;
	}

	public ArrayWrapper<Long> getTotalAbortAbcastDuration()
	{
		return totalAbortAbcastDuration;
	}

	public ArrayWrapper<Long> getTotalAbortValidationTime()
	{
		return totalAbortValidationTime;
	}
}
