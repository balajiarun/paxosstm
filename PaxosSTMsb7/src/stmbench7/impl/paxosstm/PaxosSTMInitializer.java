package stmbench7.impl.paxosstm;

import stmbench7.OperationExecutorFactory;

import stmbench7.backend.BackendFactory;
import stmbench7.core.DesignObjFactory;
import stmbench7.impl.paxosstm.PaxosSTMOperationExecutorFactory;
import stmbench7.impl.NoSynchronizationInitializer;
import stmbench7.impl.paxosstm.backend.BackendFactoryImpl;
import stmbench7.impl.paxosstm.core.DesignObjFactoryImpl;

public class PaxosSTMInitializer extends NoSynchronizationInitializer
{
	@Override
	public BackendFactory createBackendFactory()
	{
		return new BackendFactoryImpl();
	}

	@Override
	public DesignObjFactory createDesignObjFactory()
	{
		return new DesignObjFactoryImpl();
	}

	@Override
	public OperationExecutorFactory createOperationExecutorFactory()
	{
		return new PaxosSTMOperationExecutorFactory();
	}
}
