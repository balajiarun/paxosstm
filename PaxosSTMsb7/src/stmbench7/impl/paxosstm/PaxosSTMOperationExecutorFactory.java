package stmbench7.impl.paxosstm;

import stmbench7.OperationExecutor;
import stmbench7.OperationExecutorFactory;
import stmbench7.core.Operation;
import stmbench7.impl.DefaultOperationExecutor;

public class PaxosSTMOperationExecutorFactory extends OperationExecutorFactory {

	@Override
	public OperationExecutor createOperationExecutor(Operation op) {
		if(op.getOperationId() != null)
			return new PaxosSTMOperationExecutor(op);
		return new DefaultOperationExecutor(op);
	}

}
