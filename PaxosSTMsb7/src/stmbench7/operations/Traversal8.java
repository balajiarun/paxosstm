package stmbench7.operations;

import stmbench7.OperationId;
import stmbench7.Setup;
import stmbench7.annotations.ReadOnly;
import stmbench7.annotations.Transactional;
import stmbench7.core.Manual;
import stmbench7.core.Module;

/**
 * Traversal T8 / Operation OP4 (see the specification).
 * Read-only.
 */
public class Traversal8 extends BaseOperation {

	protected Module module;

	public Traversal8(Setup oo7setup) {
		this.module = oo7setup.getModule();
	}

	@Override
	@Transactional @ReadOnly
	public int performOperation() {
//		try {
		Manual manual = module.getManual();
		return traverse(manual);
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//			System.err.println("CHCHCHCHCHCHCHCH");
//			Observable o = (Observable) module;
//			System.err.println(o.__tc_getId());
//			System.err.println(o.__tc_isHandle());
//			System.err.println(o.__tc_isVersion());
//			System.err.println(o.__tc_isShadowCopy());
//			System.err.println(o.getClass().getName());
//			System.err.println(o);
//			System.exit(1);
//			throw new RuntimeException(e);
//		}
	}

	protected int traverse(Manual manual) {
		return manual.countOccurences('I');
	}
	
    @Override
    public OperationId getOperationId() {
    	return OperationId.OP4;
    }
}
