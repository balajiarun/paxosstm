package stmbench7;

public class ResultBean
{
	public int result;
	
	public boolean readOnly;
	
	public boolean committed;
	public boolean rolledback;
	
	public int commitPackageSize;
	public int commitReadSetSize;
	public int commitWriteSetSize;
	public int commitNewSetSize;
	public int commitTypeSetSize;
	
	public int localAborts;
	public int aborts;
	public int totalAbortPackageSize;
	public int totalAbortReadSetSize;
	public int totalAbortWriteSetSize;
	public int totalAbortNewSetSize;
	public int totalAbortTypeSetSize;

	public long executionTime;
	public long committingPhaseLatency;

	public long commitAbcastDuration;
	public long commitValidationTime;
	public long commitUpdateTime;
	public long abortAbcastDuration;
	public long abortValidationTime;
}
