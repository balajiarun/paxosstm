package stmbench7;

import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.test.TestCounter;
import soa.paxosstm.utils.TransactionalArrayList;
import stmbench7.annotations.Immutable;
import stmbench7.backend.BackendFactory;
import stmbench7.backend.IdPool;
import stmbench7.backend.Index;
import stmbench7.backend.LargeSet;
import stmbench7.core.AssemblyBuilder;
import stmbench7.core.AtomicPart;
import stmbench7.core.BaseAssembly;
import stmbench7.core.ComplexAssembly;
import stmbench7.core.CompositePart;
import stmbench7.core.CompositePartBuilder;
import stmbench7.core.Document;
import stmbench7.core.Module;
import stmbench7.core.ModuleBuilder;
import stmbench7.impl.paxosstm.BenchThreadResultBean;
import stmbench7.operations.SetupDataStructure;

/**
 * Sets up the benchmark structures according to given parameters, including
 * indexes.
 */
@Immutable
public class Setup
{

	protected Module module;

	protected Index<Integer, AtomicPart> atomicPartIdIndex;
	protected Index<Integer, LargeSet<AtomicPart>> atomicPartBuildDateIndex;
	protected Index<String, Document> documentTitleIndex;
	protected Index<Integer, CompositePart> compositePartIdIndex;
	protected Index<Integer, BaseAssembly> baseAssemblyIdIndex;
	protected Index<Integer, ComplexAssembly> complexAssemblyIdIndex;

	protected CompositePartBuilder compositePartBuilder;
	protected ModuleBuilder moduleBuilder;

	protected IdPool baseAssemblyIdPool;
	protected IdPool complexAssemblyIdPool;
	protected IdPool atomicIdPool;
	protected IdPool compositeIdPool;
	protected IdPool documentIdPool;
	protected IdPool manualIdPool;
	protected IdPool moduleIdPool;

	public Setup() throws InterruptedException
	{
		if (PaxosSTM.getInstance().getId() == 0)
		{
			setupMaster();
			PaxosSTM.getInstance().enterBarrier("setup",
					PaxosSTM.getInstance().getNumberOfNodes());
		}
		else
		{
			PaxosSTM.getInstance().enterBarrier("setup",
					PaxosSTM.getInstance().getNumberOfNodes());
			setupSlave();
		}
	}

	private void setupMaster() throws InterruptedException
	{
		final BackendFactory backendFactory = BackendFactory.instance;

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				atomicPartIdIndex = backendFactory
						.<Integer, AtomicPart> createIndex();
				atomicPartBuildDateIndex = backendFactory
						.<Integer, LargeSet<AtomicPart>> createIndex();
				documentTitleIndex = backendFactory
						.<String, Document> createIndex();
				compositePartIdIndex = backendFactory
						.<Integer, CompositePart> createIndex();
				baseAssemblyIdIndex = backendFactory
						.<Integer, BaseAssembly> createIndex();
				complexAssemblyIdIndex = backendFactory
						.<Integer, ComplexAssembly> createIndex();
			}
		};

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				baseAssemblyIdPool = BackendFactory.instance
						.createIdPool(Parameters.MaxBaseAssemblies);
			}
		};

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				complexAssemblyIdPool = BackendFactory.instance
						.createIdPool(Parameters.MaxComplexAssemblies);			
				atomicIdPool = BackendFactory.instance
						.createIdPool(Parameters.MaxAtomicParts);
			}
		};

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				compositeIdPool = BackendFactory.instance
						.createIdPool(Parameters.MaxCompParts);
			}
		};

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				documentIdPool = BackendFactory.instance
						.createIdPool(Parameters.MaxCompParts);
			}
		};

		new Transaction()
		{
			@Override
			protected void atomic()
			{

				manualIdPool = BackendFactory.instance
						.createIdPool(Parameters.NumModules);
			}
		};

		new Transaction()
		{
			@Override
			protected void atomic()
			{

				moduleIdPool = BackendFactory.instance
						.createIdPool(Parameters.NumModules);
			}
		};

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				compositePartBuilder = new CompositePartBuilder(
						compositePartIdIndex, documentTitleIndex,
						atomicPartIdIndex, atomicPartBuildDateIndex,
						compositeIdPool, documentIdPool, atomicIdPool);
				moduleBuilder = new ModuleBuilder(baseAssemblyIdIndex,
						complexAssemblyIdIndex, moduleIdPool, manualIdPool,
						baseAssemblyIdPool, complexAssemblyIdPool);
			}
		};

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"baseAssemblyIdPool", baseAssemblyIdPool);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"complexAssemblyIdPool", complexAssemblyIdPool);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"atomicIdPool", atomicIdPool);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"compositeIdPool", compositeIdPool);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"documentIdPool", documentIdPool);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"manualIdPool", manualIdPool);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"moduleIdPool", moduleIdPool);

				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"atomicPartIdIndex", atomicPartIdIndex);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"atomicPartBuildDateIndex", atomicPartBuildDateIndex);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"documentTitleIndex", documentTitleIndex);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"compositePartIdIndex", compositePartIdIndex);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"baseAssemblyIdIndex", baseAssemblyIdIndex);
				PaxosSTM.getInstance().addToSharedObjectRegistry(
						"complexAssemblyIdIndex", complexAssemblyIdIndex);
			}
		};

		// new Transaction()
		// {
		// @Override
		// protected void atomic()
		// {
		// atomicPartIdIndex = backendFactory
		// .<Integer, AtomicPart> createIndex();
		// atomicPartBuildDateIndex = backendFactory
		// .<Integer, LargeSet<AtomicPart>> createIndex();
		// documentTitleIndex = backendFactory
		// .<String, Document> createIndex();
		// compositePartIdIndex = backendFactory
		// .<Integer, CompositePart> createIndex();
		// baseAssemblyIdIndex = backendFactory
		// .<Integer, BaseAssembly> createIndex();
		// complexAssemblyIdIndex = backendFactory
		// .<Integer, ComplexAssembly> createIndex();
		//
		// IdPool baseAssemblyIdPool = BackendFactory.instance
		// .createIdPool(Parameters.MaxBaseAssemblies);
		// IdPool complexAssemblyIdPool = BackendFactory.instance
		// .createIdPool(Parameters.MaxComplexAssemblies);
		// IdPool atomicIdPool = BackendFactory.instance
		// .createIdPool(Parameters.MaxAtomicParts);
		// IdPool compositeIdPool = BackendFactory.instance
		// .createIdPool(Parameters.MaxCompParts);
		// IdPool documentIdPool = BackendFactory.instance
		// .createIdPool(Parameters.MaxCompParts);
		// IdPool manualIdPool = BackendFactory.instance
		// .createIdPool(Parameters.NumModules);
		// IdPool moduleIdPool = BackendFactory.instance
		// .createIdPool(Parameters.NumModules);
		//
		// compositePartBuilder = new CompositePartBuilder(
		// compositePartIdIndex, documentTitleIndex,
		// atomicPartIdIndex, atomicPartBuildDateIndex,
		// compositeIdPool, documentIdPool, atomicIdPool);
		// moduleBuilder = new ModuleBuilder(baseAssemblyIdIndex,
		// complexAssemblyIdIndex, moduleIdPool, manualIdPool,
		// baseAssemblyIdPool, complexAssemblyIdPool);
		//
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "baseAssemblyIdPool", baseAssemblyIdPool);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "complexAssemblyIdPool", complexAssemblyIdPool);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "atomicIdPool", atomicIdPool);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "compositeIdPool", compositeIdPool);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "documentIdPool", documentIdPool);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "manualIdPool", manualIdPool);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "moduleIdPool", moduleIdPool);
		//
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "atomicPartIdIndex", atomicPartIdIndex);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "atomicPartBuildDateIndex", atomicPartBuildDateIndex);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "documentTitleIndex", documentTitleIndex);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "compositePartIdIndex", compositePartIdIndex);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "baseAssemblyIdIndex", baseAssemblyIdIndex);
		// PaxosSTM.getInstance().addToSharedObjectRegistry(
		// "complexAssemblyIdIndex", complexAssemblyIdIndex);
		// }
		// };

		SetupDataStructure setupOperation = new SetupDataStructure(this);
		OperationExecutorFactory.executeSequentialOperation(setupOperation);

		// System.err.println("=== after whole setupDataStructure");
		// TestCounter.getInstance().printAvg();
		// TestCounter.getInstance().reset();

		module = setupOperation.getModule();

		new Transaction()
		{

			@Override
			protected void atomic()
			{
				TransactionalArrayList<BenchThreadResultBean> resultList = new TransactionalArrayList<BenchThreadResultBean>();
				PaxosSTM.getInstance().addToSharedObjectRegistry("resultList",
						resultList);

				PaxosSTM.getInstance().addToSharedObjectRegistry("module",
						module);
			}
		};
//		System.err.println("=== setup structures transactions statistics");
//		TestCounter.getInstance().printAvg();
//		TestCounter.getInstance().reset();
	}

	@SuppressWarnings("unchecked")
	private void setupSlave()
	{
		module = (Module) PaxosSTM.getInstance().getFromSharedObjectRegistry(
				"module");
		atomicPartIdIndex = (Index<Integer, AtomicPart>) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("atomicPartIdIndex");
		atomicPartBuildDateIndex = (Index<Integer, LargeSet<AtomicPart>>) PaxosSTM
				.getInstance().getFromSharedObjectRegistry(
						"atomicPartBuildDateIndex");
		documentTitleIndex = (Index<String, Document>) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("documentTitleIndex");
		compositePartIdIndex = (Index<Integer, CompositePart>) PaxosSTM
				.getInstance().getFromSharedObjectRegistry(
						"compositePartIdIndex");
		baseAssemblyIdIndex = (Index<Integer, BaseAssembly>) PaxosSTM
				.getInstance().getFromSharedObjectRegistry(
						"baseAssemblyIdIndex");
		complexAssemblyIdIndex = (Index<Integer, ComplexAssembly>) PaxosSTM
				.getInstance().getFromSharedObjectRegistry(
						"complexAssemblyIdIndex");

		baseAssemblyIdPool = (IdPool) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("baseAssemblyIdPool");
		complexAssemblyIdPool = (IdPool) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("complexAssemblyIdPool");
		atomicIdPool = (IdPool) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("atomicIdPool");
		compositeIdPool = (IdPool) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("compositeIdPool");
		documentIdPool = (IdPool) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("documentIdPool");
		manualIdPool = (IdPool) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("manualIdPool");
		moduleIdPool = (IdPool) PaxosSTM.getInstance()
				.getFromSharedObjectRegistry("moduleIdPool");

		compositePartBuilder = new CompositePartBuilder(compositePartIdIndex,
				documentTitleIndex, atomicPartIdIndex,
				atomicPartBuildDateIndex, compositeIdPool, documentIdPool,
				atomicIdPool);
		moduleBuilder = new ModuleBuilder(baseAssemblyIdIndex,
				complexAssemblyIdIndex, moduleIdPool, manualIdPool,
				baseAssemblyIdPool, complexAssemblyIdPool);
	}

	public Index<Integer, LargeSet<AtomicPart>> getAtomicPartBuildDateIndex()
	{
		return atomicPartBuildDateIndex;
	}

	public Index<Integer, AtomicPart> getAtomicPartIdIndex()
	{
		return atomicPartIdIndex;
	}

	public Index<Integer, BaseAssembly> getBaseAssemblyIdIndex()
	{
		return baseAssemblyIdIndex;
	}

	public Index<Integer, ComplexAssembly> getComplexAssemblyIdIndex()
	{
		return complexAssemblyIdIndex;
	}

	public Index<Integer, CompositePart> getCompositePartIdIndex()
	{
		return compositePartIdIndex;
	}

	public Index<String, Document> getDocumentTitleIndex()
	{
		return documentTitleIndex;
	}

	public Module getModule()
	{
		return module;
	}

	public CompositePartBuilder getCompositePartBuilder()
	{
		return compositePartBuilder;
	}

	public ModuleBuilder getModuleBuilder()
	{
		return moduleBuilder;
	}

	public AssemblyBuilder getAssemblyBuilder()
	{
		return moduleBuilder.getAssemblyBuilder();
	}
}
