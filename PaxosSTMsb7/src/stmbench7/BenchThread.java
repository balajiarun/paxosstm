package stmbench7;

import java.lang.reflect.Constructor;
import java.util.ArrayList;

import soa.paxosstm.dstm.PaxosSTM;
import soa.paxosstm.dstm.Transaction;
import soa.paxosstm.utils.TransactionalArrayList;
import stmbench7.annotations.NonAtomic;
import stmbench7.core.Operation;
import stmbench7.core.OperationFailedException;
import stmbench7.core.RuntimeError;
import stmbench7.impl.paxosstm.BenchThreadResultBean;
import lsr.paxos.client.Client;

/**
 * A single thread of the STMBench7 benchmark. Executes operations assigned to
 * it one by one, randomly choosing the next operation and respecting the
 * expected ratios of operations' counts.
 */
@NonAtomic
public class BenchThread implements Runnable
{

	protected static volatile boolean stop = false;
	protected static volatile boolean warmUp = true;
	protected double[] operationCDF;
	protected OperationExecutor[] operations;
	protected final short myThreadNum;
	protected int processId;

	public int[] successfulOperations, failedOperations;
	public int[][] operationsTTC, operationsHighTTCLog;

	public long[] totalTime;

	public int[] commits, commitsNoWrite, aborts, localAborts, rollbacks;
	public int[] totalCommitPackageSizes, totalAbortPackageSizes;
	public int[] totalCommitReadSetSizes, totalAbortReadSetSizes;
	public int[] totalCommitWriteSetSizes, totalAbortWriteSetSizes;
	public int[] totalCommitNewSetSizes, totalAbortNewSetSizes;
	public int[] totalCommitTypeSetSizes, totalAbortTypeSetSizes;

	public long[] totalCommitExecutionTimes, totalCommitNoWriteExecutionTimes,
			totalAbortExecutionTimes, totalLocalAbortExecutionTimes,
			totalRollbackExecutionTimes;
	public long[] totalCommitCommittingPhaseLatency,
			totalAbortCommittingPhaseLatency;
	public long[] totalCommitAbcastDuration, totalCommitValidationTime,
			totalCommitUpdateTime, totalAbortAbcastDuration,
			totalAbortValidationTime;

	public class ReplayLogEntry implements Comparable<ReplayLogEntry>
	{
		public final short threadNum;
		public final int timestamp, result;
		public final boolean failed;
		public final int opNum;

		public ReplayLogEntry(int timestamp, int result, boolean failed,
				int opNum)
		{
			this.threadNum = myThreadNum;
			this.timestamp = timestamp;
			this.result = result;
			this.failed = failed;
			this.opNum = opNum;
		}

		public int compareTo(ReplayLogEntry entry)
		{
			return timestamp - entry.timestamp;
		}
	}

	public ArrayList<ReplayLogEntry> replayLog;

	public BenchThread(Setup setup, double[] operationCDF, short myThreadNum,
			int processId)
	{
		this.operationCDF = operationCDF;

		int numOfOperations = OperationId.values().length;
		operationsTTC = new int[numOfOperations][Parameters.MAX_LOW_TTC + 1];
		operationsHighTTCLog = new int[numOfOperations][Parameters.HIGH_TTC_ENTRIES];
		successfulOperations = new int[numOfOperations];
		failedOperations = new int[numOfOperations];
		operations = new OperationExecutor[numOfOperations];
		this.myThreadNum = myThreadNum;
		this.processId = processId;

		totalTime = new long[numOfOperations];

		commits = new int[numOfOperations];
		commitsNoWrite = new int[numOfOperations];
		aborts = new int[numOfOperations];
		rollbacks = new int[numOfOperations];
		localAborts = new int[numOfOperations];
		totalCommitPackageSizes = new int[numOfOperations];
		totalAbortPackageSizes = new int[numOfOperations];
		totalCommitReadSetSizes = new int[numOfOperations];
		totalAbortReadSetSizes = new int[numOfOperations];
		totalCommitWriteSetSizes = new int[numOfOperations];
		totalAbortWriteSetSizes = new int[numOfOperations];
		totalCommitNewSetSizes = new int[numOfOperations];
		totalAbortNewSetSizes = new int[numOfOperations];
		totalCommitTypeSetSizes = new int[numOfOperations];
		totalAbortTypeSetSizes = new int[numOfOperations];

		totalCommitExecutionTimes = new long[numOfOperations];
		totalCommitNoWriteExecutionTimes = new long[numOfOperations];
		totalAbortExecutionTimes = new long[numOfOperations];
		totalLocalAbortExecutionTimes = new long[numOfOperations];
		totalRollbackExecutionTimes = new long[numOfOperations];
		totalCommitCommittingPhaseLatency = new long[numOfOperations];
		totalAbortCommittingPhaseLatency = new long[numOfOperations];

		totalCommitAbcastDuration = new long[numOfOperations];
		totalCommitValidationTime = new long[numOfOperations];
		totalCommitUpdateTime = new long[numOfOperations];
		totalAbortAbcastDuration = new long[numOfOperations];
		totalAbortValidationTime = new long[numOfOperations];

		createOperations(setup);

		if (Parameters.sequentialReplayEnabled)
			replayLog = new ArrayList<ReplayLogEntry>();
	}

	protected BenchThread(Setup setup, double[] operationCDF)
	{
		this.operationCDF = operationCDF;
		operations = new OperationExecutor[OperationId.values().length];
		createOperations(setup);
		myThreadNum = 0;
	}

	public void run()
	{
		while (!stop)
		{
			while (Client.staticPrimary.get() == processId && !stop)
			{
				try
				{
					System.err.println("spie " + System.currentTimeMillis());
					Thread.sleep(2000);
				}
				catch (InterruptedException e)
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			// if (i++ > 55) continue;
			int operationNumber = getNextOperationNumber();

			// OperationType type = OperationId.values()[operationNumber]
			// .getType();
			// if( (type != OperationType.SHORT_TRAVERSAL) ) continue;
			// (type != OperationType.SHORT_TRAVERSAL_RO) &&
			// (type != OperationType.OPERATION) )
			// continue;

			// System.out.println(i + " > "
			// + OperationId.values()[operationNumber]);

			OperationExecutor currentExecutor = operations[operationNumber];
			int result = 0;
			boolean failed = false;

			try
			{
				long startTime = System.currentTimeMillis();
				// System.out.print("Thread #" + myThreadNum + ": ");

				ResultBean resultBean = currentExecutor.execute();
				result = resultBean.result;

				long endTime = System.currentTimeMillis();
				// System.out.println("success");

				if (warmUp)
					continue;

				if (!resultBean.rolledback)
				{
					successfulOperations[operationNumber]++;
					int ttc = (int) (endTime - startTime);
					if (ttc <= Parameters.MAX_LOW_TTC)
						operationsTTC[operationNumber][ttc]++;
					else
					{
						double logHighTtc = (Math.log(ttc) - Math
								.log(Parameters.MAX_LOW_TTC + 1))
								/ Math.log(Parameters.HIGH_TTC_LOG_BASE);
						int intLogHighTtc = Math.min((int) logHighTtc,
								Parameters.HIGH_TTC_ENTRIES - 1);
						operationsHighTTCLog[operationNumber][intLogHighTtc]++;
					}
				}
				
				if (resultBean.committed)
				{
					totalTime[operationNumber] += endTime - startTime;
					commits[operationNumber]++;

					totalCommitCommittingPhaseLatency[operationNumber] += resultBean.committingPhaseLatency;

					if (resultBean.commitPackageSize == 0)
					{
						commitsNoWrite[operationNumber]++;
						totalCommitNoWriteExecutionTimes[operationNumber] += resultBean.executionTime;
					}
					else
					{
						totalCommitAbcastDuration[operationNumber] += resultBean.commitAbcastDuration;
						totalCommitValidationTime[operationNumber] += resultBean.commitValidationTime;
						totalCommitUpdateTime[operationNumber] += resultBean.commitUpdateTime;

						totalCommitExecutionTimes[operationNumber] += resultBean.executionTime;
						totalCommitPackageSizes[operationNumber] += resultBean.commitPackageSize;
						totalCommitReadSetSizes[operationNumber] += resultBean.commitReadSetSize;
						totalCommitWriteSetSizes[operationNumber] += resultBean.commitWriteSetSize;
						totalCommitNewSetSizes[operationNumber] += resultBean.commitNewSetSize;
						totalCommitTypeSetSizes[operationNumber] += resultBean.commitTypeSetSize;
					}
				}
				if (resultBean.rolledback)
				{
					rollbacks[operationNumber]++;
					totalRollbackExecutionTimes[operationNumber] += resultBean.executionTime;
					failedOperations[operationNumber]++;
					failed = true;
				}
				if (resultBean.localAborts != 0)
				{
					localAborts[operationNumber] += resultBean.localAborts;
					totalLocalAbortExecutionTimes[operationNumber] += resultBean.executionTime;
				}
				if (resultBean.aborts != 0)
				{
					aborts[operationNumber] += resultBean.aborts;

					totalAbortAbcastDuration[operationNumber] += resultBean.abortAbcastDuration;
					totalAbortValidationTime[operationNumber] += resultBean.abortValidationTime;

					totalAbortExecutionTimes[operationNumber] += resultBean.executionTime;
					totalAbortCommittingPhaseLatency[operationNumber] += resultBean.committingPhaseLatency;

					totalAbortPackageSizes[operationNumber] += resultBean.totalAbortPackageSize;
					totalAbortReadSetSizes[operationNumber] += resultBean.totalAbortReadSetSize;
					totalAbortWriteSetSizes[operationNumber] += resultBean.totalAbortWriteSetSize;
					totalAbortNewSetSizes[operationNumber] += resultBean.totalAbortNewSetSize;
					totalAbortTypeSetSizes[operationNumber] += resultBean.totalAbortTypeSetSize;
				}

			}
			catch (OperationFailedException e)
			{
				// FIXME - uwaga, zakomentowane rzucanie execptiona w executorze
				// wszystko powinno isc przez bean

				// System.out.println("failed");
				rollbacks[operationNumber]++;
				// totalRollbackExecutionTimes[operationNumber] +=
				// resultBean.executionTime;
				failedOperations[operationNumber]++;
				failed = true;
			}

			if (Parameters.sequentialReplayEnabled)
			{
				ReplayLogEntry newEntry = new ReplayLogEntry(
						currentExecutor.getLastOperationTimestamp(), result,
						failed, operationNumber);
				replayLog.add(newEntry);
				// System.out.println("ts: " + newEntry.timestamp);
			}
		}

		System.err.println("Thread #" + myThreadNum + " finished. ("
				+ Thread.currentThread().getId() + ") " + System.currentTimeMillis()/1000);
		// i = 0;
		// for (ReplayLogEntry entry : replayLog)
		// System.out.println(i++ + " % " + OperationId.values()[entry.opNum]
		// + " -- " + entry.timestamp);
	}

	public static void stopThread()
	{
		stop = true;
	}

	public static void finishWarmUp()
	{
		warmUp = false;
	}

	protected void createOperations(Setup setup)
	{
		for (OperationId operationDescr : OperationId.values())
		{
			Class<? extends Operation> operationClass = operationDescr
					.getOperationClass();
			int operationIndex = operationDescr.ordinal();

			try
			{
				Constructor<? extends Operation> operationConstructor = operationClass
						.getConstructor(Setup.class);
				Operation operation = operationConstructor.newInstance(setup);

				operations[operationIndex] = OperationExecutorFactory.instance
						.createOperationExecutor(operation);
				assert (operation.getOperationId().getOperationClass()
						.equals(operationClass));
			}
			catch (Exception e)
			{
				throw new RuntimeError("Error while creating operation "
						+ operationDescr, e);
			}
		}
	}

	protected int getNextOperationNumber()
	{
		// return OperationId.T3c.ordinal();
		// return OperationId.OP11.ordinal();
		double whichOperation = ThreadRandom.nextDouble();
		int operationNumber = 0;
		while (whichOperation >= operationCDF[operationNumber])
		{
			operationNumber++;
		}
		return operationNumber;
	}

	public void submitResults()
	{
		System.err.println("Thread #" + myThreadNum + " submits results");

		new Transaction()
		{
			@Override
			protected void atomic()
			{
				@SuppressWarnings("unchecked")
				TransactionalArrayList<BenchThreadResultBean> resultList = (TransactionalArrayList<BenchThreadResultBean>) PaxosSTM
						.getInstance()
						.getFromSharedObjectRegistry("resultList");

				resultList.add(new BenchThreadResultBean(successfulOperations,
						failedOperations, operationsTTC, operationsHighTTCLog,
						-1, totalTime, commits, commitsNoWrite, aborts,
						rollbacks, localAborts, totalCommitPackageSizes,
						totalAbortPackageSizes, totalCommitReadSetSizes,
						totalAbortReadSetSizes, totalCommitWriteSetSizes,
						totalAbortWriteSetSizes, totalCommitNewSetSizes,
						totalAbortNewSetSizes, totalCommitTypeSetSizes,
						totalAbortTypeSetSizes, totalCommitExecutionTimes,
						totalCommitNoWriteExecutionTimes,
						totalAbortExecutionTimes,
						totalLocalAbortExecutionTimes,
						totalRollbackExecutionTimes,
						totalCommitCommittingPhaseLatency,
						totalAbortCommittingPhaseLatency,
						totalCommitAbcastDuration, totalCommitValidationTime,
						totalCommitUpdateTime, totalAbortAbcastDuration,
						totalAbortValidationTime));

			}
		};
		System.err.println("Thread #" + myThreadNum + " submits results - end");
	}
}
